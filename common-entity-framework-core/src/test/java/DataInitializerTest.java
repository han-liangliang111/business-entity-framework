import com.inspur.edp.cef.core.crypt.BefBuiltInEncrypt;
import com.inspur.edp.cef.core.data.DataInitializer;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.entity.exception.ExceptionCode;
import io.iec.edp.caf.commons.utils.InvokeService;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DataInitializerTest {
    @Test
    public void setStaticProperty(){
        String propertyValue = "e05a7a9d-cd1b-4fbd-b97c-51b9db8a7b0e";
        DataInitializer.initialize();

        assertEquals(BefBuiltInEncrypt.key, propertyValue,"值应该相同");
    }
}
