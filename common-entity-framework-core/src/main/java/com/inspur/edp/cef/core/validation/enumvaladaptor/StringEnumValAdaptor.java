package com.inspur.edp.cef.core.validation.enumvaladaptor;

import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.spi.entity.info.EnumValueInfo;

import java.util.List;

public class StringEnumValAdaptor extends EnumValAdaptor<String> {
    public StringEnumValAdaptor(String propName, List<EnumValueInfo> enumValueInfos) {
        super(propName, enumValueInfos);
    }

    @Override
    protected String getDisplayMessage(ICefValidationContext context) {
        //国际化异常调用资源文件
        String exceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties",
                "Gsp_EnumFieldDisabled_0001");
        Object value = context.getData().getValue(propName);
        EnumValueInfo enumValueInfo = new EnumValueInfo();
        for (EnumValueInfo valueInfo : enumValueInfos) {
            if (valueInfo.getEnumCode().equals(value)) {
                enumValueInfo = valueInfo;
                break;
            }
        }
        return String.format(exceptionCode, enumValueInfo.getEnumCode());
    }

    @Override
    protected boolean isValid(String value) {
        for (EnumValueInfo valueInfo : enumValueInfos) {
            if (valueInfo.getEnumCode().equals(value) && valueInfo.getEnumItemDisabled()) {
                return false;
            }
        }
        return true;
    }
}
