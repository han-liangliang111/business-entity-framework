package com.inspur.edp.cef.core.crypt;

import com.inspur.edp.cef.core.data.DataInitializer;
import com.inspur.edp.cef.spi.encrypt.IEncrypt;
import io.iec.edp.caf.common.enums.SymmetricAlgorithmEnum;
import io.iec.edp.caf.commons.cryptography.SymmetricCryptographer;

public class BefBuiltInEncrypt implements IEncrypt {
    public static String key = "";

    static {
        DataInitializer.initialize();
    }

    //获取实例
    SymmetricCryptographer cryptographerUtil = SymmetricCryptographer.getInstance(SymmetricAlgorithmEnum.SM4);

    public BefBuiltInEncrypt() {
    }

    @Override
    public String encrypt(String encryptContent) {
        return cryptographerUtil.encrypt(encryptContent, key);
    }

    @Override
    public String decrypt(String decryptContent) {
        return cryptographerUtil.decrypt(decryptContent, key);
    }
}
