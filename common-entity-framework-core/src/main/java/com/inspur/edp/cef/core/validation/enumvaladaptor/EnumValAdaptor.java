package com.inspur.edp.cef.core.validation.enumvaladaptor;

import com.inspur.edp.cef.api.exceptions.ErrorCodes;
import com.inspur.edp.cef.api.message.CefException;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.ISupportBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.exception.ExceptionCode;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.entity.info.EnumValueInfo;
import com.inspur.edp.cef.spi.validation.IValidation;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

import java.util.Arrays;
import java.util.List;

public abstract class EnumValAdaptor<T> implements IValidation {
    protected String propName;
    protected List<EnumValueInfo> enumValueInfos;

    public EnumValAdaptor(String propName, List<EnumValueInfo> enumValueInfos) {
        this.propName = propName;
        this.enumValueInfos = enumValueInfos;
    }

    @Override
    public final boolean canExecute(IChangeDetail change) {
        if (change == null) {
            return true;
        }
        AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail) ((change instanceof AbstractModifyChangeDetail) ? change : null);
        if (modifyChange != null) {
            return modifyChange.getPropertyChanges() != null && modifyChange.getPropertyChanges().containsKey(propName);
        }
        return change.getChangeType() == ChangeType.Added;
    }

    @Override
    public final void execute(ICefValidationContext context, IChangeDetail change) {
        try {
            if (context.getData() == null) {
                return;
            }
            Object value = context.getData().getValue(propName);
            // 目前只对解析型工程进行判断
            if (value == null || value.getClass().isEnum()) {
                return;
            }
            if (isValid((T) value)) {
                return;
            }
        } catch (Exception e) {
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_4023, e, propName);
        }
        onInvalid(context);
    }

    private void onInvalid(ICefValidationContext context) {
        if (context instanceof ISupportBizMessage) {
            IBizMessage msg = ((ISupportBizMessage) context)
                    .createMessageWithLocation(MessageLevel.Error, Arrays.asList(propName),
                            getDisplayMessage(context));
            ((ISupportBizMessage) context).addMessage(msg);
        } else {
//            throw new CAFRuntimeException("", ErrorCodes.Length, getDisplayMessage(context), null,
//                    ExceptionLevel.Warning, true);
            throw new CefException(ErrorCodes.Length, getDisplayMessage(context), null, ExceptionLevel.Warning);
        }
    }

    protected abstract String getDisplayMessage(ICefValidationContext context);

    protected abstract boolean isValid(T value);
}
