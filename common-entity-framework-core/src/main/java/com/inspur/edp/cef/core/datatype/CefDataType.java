/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.datatype;

import com.inspur.edp.cef.api.dataType.base.ICefDataType;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.core.entityaction.EntityActionExecutor;
import com.inspur.edp.cef.core.exception.ExceptionCode;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.entity.resourceInfo.DataTypeResInfo;
import com.inspur.edp.cef.spi.entityaction.CefDataTypeAction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class CefDataType implements ICefDataType {
    private ICefDataTypeContext _context;
    private ICefDataType privateParent;

    public ICefData getData() {
        return getContext().getData();
    }

    public final ICefDataType getParent() {
        return privateParent;
    }

    public final void setParent(ICefDataType value) {
        privateParent = value;
    }

    public ICefDataTypeContext getContext() {
        return _context;
    }

    public final void setContext(ICefDataTypeContext value) {
        if (_context != null) {
            _context.setDataType(null);
        }
        _context = value;
        if (_context != null) {
            _context.setDataType(this);
        }
    }

    public void assignDefaultValue(IEntityData data) {
    }

    protected Date getDateValue(String value) {
        if (value == null || value.equals("")) {
            return null;
        }
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return sDateFormat.parse(value);
        } catch (ParseException e) {
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_4013, value);
        }
    }

    protected Date getDateTimeValue(String value) {
        if (value == null || value.equals("")) {
            return null;
        }
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return sDateFormat.parse(value);
        } catch (ParseException e) {
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_4013, value);
        }
    }

    public abstract void modify(IChangeDetail change);


    ///#region ExecuteAction
    protected <TResult> TResult execute(CefDataTypeAction<TResult> action) {
        DataValidator.checkForNullReference(action, "action");

        EntityActionExecutor<TResult> tempVar = new EntityActionExecutor<TResult>();
        tempVar.setAction(action);
        tempVar.setContext(getContext());
        return tempVar.execute();
    }

    public DataTypeResInfo getDataTypeResInfo() {
        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_4014);
    }

    protected int getVersion() {
        return 0;
    }

    protected CefDataTypeCacheInfo getDataTypeCacheInfo() {
        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_4015);
    }
}
