package com.inspur.edp.cef.core.crypt;

import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.encrypt.IEncrypt;
import com.inspur.edp.cef.spi.exception.ExceptionCode;

import java.util.concurrent.ConcurrentHashMap;


public class EncryptManager {
    private static EncryptManager instance = new EncryptManager();
    private ConcurrentHashMap<String, IEncrypt> encryptMap = new ConcurrentHashMap<>();

    private EncryptManager() {
    }

    public static EncryptManager getInstance() {
        return instance;
    }

    public IEncrypt getEncrypt(String className) {
        if (encryptMap.containsKey(className)) {
            return encryptMap.get(className);
        } else {
            if (className.equals("")) {
                BefBuiltInEncrypt encrypt = new BefBuiltInEncrypt();
                encryptMap.put(className, encrypt);
                return encrypt;
            } else {
                try {
                    IEncrypt encrypt = (IEncrypt) Class.forName(className).newInstance();
                    encryptMap.put(className, encrypt);
                    return encrypt;
                } catch (Exception e) {
                    throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3021, e, className);
                }
            }
        }
    }

}
