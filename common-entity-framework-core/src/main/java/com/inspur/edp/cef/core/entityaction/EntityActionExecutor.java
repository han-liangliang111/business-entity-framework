/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.entityaction;

import com.inspur.edp.cef.api.dataType.action.ICefDataTypeAction;
import com.inspur.edp.cef.api.dataType.action.IDataTypeActionExecutor;
import com.inspur.edp.cef.api.dataType.base.ICefDataType;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.base.IDataTypeActionAssembler;
import com.inspur.edp.cef.api.dataType.entity.ICefEntity;
import com.inspur.edp.cef.spi.entityaction.CefDataTypeAction;

public class EntityActionExecutor<T> implements IDataTypeActionExecutor<T> {
    private ICefDataTypeContext privateContext;
    private ICefDataTypeAction<T> privateAction;
    private IDataTypeActionAssembler privateAssembler;

    public final ICefDataTypeContext getContext() {
        return privateContext;
    }

    public final void setContext(ICefDataTypeContext value) {
        privateContext = value;
    }

    public final ICefDataTypeAction<T> getAction() {
        return privateAction;
    }

    public final void setAction(ICefDataTypeAction<T> value) {
        privateAction = value;
    }

    protected final IDataTypeActionAssembler getAssembler() {
        return privateAssembler;
    }

    protected final void setAssembler(IDataTypeActionAssembler value) {
        privateAssembler = value;
    }

    public final T execute() {
        try {
            getAction().setContext(getContext());
            setAssembler(((CefDataTypeAction<T>) getAction()).internalGetActionAssembler());

            onBeforeExecute();

            ((CefDataTypeAction<T>) getAction()).internalExecute();

            onAfterExecute();

            determinateAndValidate();
            return getAction().getResult();
        } catch (Throwable e) {
            if (!handleException(
                    e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e))) {
                throw e;
            }
            return null;
        } finally {
            onFinally();
        }
    }

    protected void onFinally() {
    }

    protected void determinateAndValidate() {
        ICefEntity root = getRootEntity(getContext().getDataType());
        root.afterModifyDeterminate();
        root.afterModifyValidate();
    }

    protected void onAfterExecute() {
        if (getAssembler() == null) {
            return;
        }

        getAssembler().afterExecute();
    }

    protected void onBeforeExecute() {
        if (getAssembler() == null) {
            return;
        }

        getAssembler().beforeExecute();
    }

    protected boolean handleException(RuntimeException e) {
        if (getAssembler() == null) {
            return false;
        }

        return getAssembler().handleException(e);
    }

    private ICefEntity getRootEntity(ICefDataType current) {
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return (ICefEntity) ((current instanceof ICefEntity) ? current : null);
    }
}
