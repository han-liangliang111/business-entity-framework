/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.datatype;

import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;

public class CefEntityCacheInfo extends CefDataTypeCacheInfo {

    private IEntityRTValidationAssembler afterModifyValAssembler;
    private IEntityRTValidationAssembler beforeSaveValAssembler;
    /**
     * 删除检查组装器
     */
    private IEntityRTValidationAssembler deleteCheckAssembler;
    private IEntityRTDtmAssembler beforeSaveDtmAssembler;
    private IEntityRTDtmAssembler afterSaveDtmAssembler;
    private IEntityRTDtmAssembler afterModifyDtmAssembler;
    private IEntityRTDtmAssembler retrieveDefaultDtmAssembler;

    public IEntityRTValidationAssembler getDeleteCheckAssembler() {
        return deleteCheckAssembler;
    }

    public void setDeleteCheckAssembler(IEntityRTValidationAssembler deleteCheckAssembler) {
        this.deleteCheckAssembler = deleteCheckAssembler;
    }

    public IEntityRTValidationAssembler getAfterModifyValAssembler() {
        return afterModifyValAssembler;
    }

    public void setAfterModifyValAssembler(
            IEntityRTValidationAssembler afterModifyValAssembler) {
        this.afterModifyValAssembler = afterModifyValAssembler;
    }

    public IEntityRTValidationAssembler getBeforeSaveValAssembler() {
        return beforeSaveValAssembler;
    }

    public void setBeforeSaveValAssembler(
            IEntityRTValidationAssembler beforeSaveValAssembler) {
        this.beforeSaveValAssembler = beforeSaveValAssembler;
    }

    public IEntityRTDtmAssembler getBeforeSaveDtmAssembler() {
        return beforeSaveDtmAssembler;
    }

    public void setBeforeSaveDtmAssembler(
            IEntityRTDtmAssembler beforeSaveDtmAssembler) {
        this.beforeSaveDtmAssembler = beforeSaveDtmAssembler;
    }

    public IEntityRTDtmAssembler getAfterSaveDtmAssembler() {
        return afterSaveDtmAssembler;
    }

    public void setAfterSaveDtmAssembler(
            IEntityRTDtmAssembler afterSaveDtmAssembler) {
        this.afterSaveDtmAssembler = afterSaveDtmAssembler;
    }

    public IEntityRTDtmAssembler getAfterModifyDtmAssembler() {
        return afterModifyDtmAssembler;
    }

    public void setAfterModifyDtmAssembler(
            IEntityRTDtmAssembler afterModifyDtmAssembler) {
        this.afterModifyDtmAssembler = afterModifyDtmAssembler;
    }

    public IEntityRTDtmAssembler getRetrieveDefaultDtmAssembler() {
        return retrieveDefaultDtmAssembler;
    }

    public void setRetrieveDefaultDtmAssembler(
            IEntityRTDtmAssembler retrieveDefaultDtmAssembler) {
        this.retrieveDefaultDtmAssembler = retrieveDefaultDtmAssembler;
    }
}
