/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.core.validation.requiredvaladaptor.RequiredStringValAdaptor;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.extend.entity.ICefEntityExtend;
import com.inspur.edp.cef.spi.validation.IRuntimeValidationAssembler;

import java.util.List;
import java.util.Map;

public class LengthValUtils {
    public static void addLengthVals(
            IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo) {
        addLengthVals(validationAssembler, dataTypeInfo, 0);
    }

    /**
     * 长度校验传入扩展信息
     *
     * @param extendList
     * @param validationAssembler
     * @param dataTypeInfo
     * @param cefVersion
     */
    public static void addLengthVals(List<ICefEntityExtend> extendList, IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo, int cefVersion) {
        int index = 0;
        for (DataTypePropertyInfo propertyInfo : dataTypeInfo.getPropertyInfos().values()) {
            if (propertyInfo.getObjectType() == ObjectType.Association) {
                validationAssembler.getValidations().add(index++, new newAssociationLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(), propertyInfo.getPrecision(), cefVersion));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Normal) {
                switch (propertyInfo.getFieldType()) {
                    case String:
                        Integer length = getLength(extendList, propertyInfo);
                        validationAssembler.getValidations().add(index++, new StringLengthValAdaptor(propertyInfo.getPropertyName(), length, propertyInfo.getPrecision(), cefVersion));
                        break;
                    case Text:
                        validationAssembler.getValidations().add(index++, new StringLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(), propertyInfo.getPrecision(), cefVersion));
                        break;
                    case Decimal:
                        validationAssembler.getValidations().add(index++, new DecimalLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(), propertyInfo.getPrecision(), cefVersion));
                        break;
                    case Integer:
                    {
                        if(propertyInfo.getDbDataType() == GspDbDataType.SmallInt){
                            validationAssembler.getValidations().add(index++, new SmallIntLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getPrecision(), cefVersion));
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * 根据扩展信息和当前基础字段的PropertyInfo获取字段的长度
     *
     * @param extendList
     * @param propertyInfo
     * @return
     */
    private static Integer getLength(List<ICefEntityExtend> extendList, DataTypePropertyInfo propertyInfo) {
        if (extendList == null || extendList.size() == 0)
            return propertyInfo.getLength();
        ICefEntityExtend entityExtend = extendList.get(0);
        Map<String, Integer> lengthMaps = entityExtend.getExtendLengthInfos();
        if (lengthMaps != null && lengthMaps.size() > 0 && lengthMaps.containsKey(propertyInfo.getPropertyName()))
            return lengthMaps.get(propertyInfo.getPropertyName());
        return propertyInfo.getLength();
    }

    public static void addLengthVals(IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo, int cefVersion) {
        int index = 0;
        for (DataTypePropertyInfo propertyInfo : dataTypeInfo.getPropertyInfos().values()) {
            if (propertyInfo.getObjectType() == ObjectType.Association) {
                validationAssembler.getValidations().add(index++, new newAssociationLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(), propertyInfo.getPrecision(), cefVersion));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Normal) {
                switch (propertyInfo.getFieldType()) {
                    case String:
                    case Text:
                        validationAssembler.getValidations().add(index++,
                                new StringLengthValAdaptor(propertyInfo.getPropertyName(), propertyInfo.getLength(),
                                        propertyInfo.getPrecision(), cefVersion));
                        break;
                    case Decimal:
                        validationAssembler.getValidations().add(index++,
                                new DecimalLengthValAdaptor(propertyInfo.getPropertyName(),
                                        propertyInfo.getLength(), propertyInfo.getPrecision(), cefVersion));
                        break;
                }
            }
        }
    }
}
