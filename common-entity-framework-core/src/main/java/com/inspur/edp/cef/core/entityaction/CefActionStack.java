/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.entityaction;

import com.inspur.edp.cef.api.action.ActionExecuteContext;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;

import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CefActionStack {
    private static ThreadLocal<CefActionStack> actionStackThreadLocal = new ThreadLocal<CefActionStack>();
    LinkedList<ActionExecuteContext> contexts = new LinkedList<>();
    private java.util.Stack stack;

    public CefActionStack() {
        stack = new java.util.Stack();
    }

    private static ThreadLocal<CefActionStack> getActionStackThreadLocal() {
        if (actionStackThreadLocal.get() == null)
            actionStackThreadLocal.set(new CefActionStack());
        return actionStackThreadLocal;
    }

    public static void push(ActionExecuteContext context) {
        DataValidator.checkForNullReference(context, "push进来的context为空。");
        if (getActionStackThreadLocal().get().stack.isEmpty() == false)
            context.setPreContext((ActionExecuteContext) getActionStackThreadLocal().get().stack.lastElement());
        getActionStackThreadLocal().get().stack.push(context);

    }

    public static ActionExecuteContext pop() {
        return (ActionExecuteContext) getActionStackThreadLocal().get().stack.pop();
    }

    public static int count() {
        return getActionStackThreadLocal().get().stack.size();
    }

    public static ActionExecuteContext peek() {
        return (ActionExecuteContext) getActionStackThreadLocal().get().stack.peek();
    }

    public static List<ActionExecuteContext> getAllContexts() {
        List<ActionExecuteContext> contexts = new ArrayList<>(getActionStackThreadLocal().get().stack.size());
        for (Object obj : getActionStackThreadLocal().get().stack) {
            contexts.add((ActionExecuteContext) obj);
        }
        return contexts;
    }


}
