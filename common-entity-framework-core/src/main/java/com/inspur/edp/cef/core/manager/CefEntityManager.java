/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.manager;


import com.inspur.edp.cef.api.dataType.entity.ICefEntity;
import com.inspur.edp.cef.api.manager.ICefEntityManager;
import com.inspur.edp.cef.api.manager.ICefManagerContext;
import com.inspur.edp.cef.api.manager.entity.ICefEntityManagerContext;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;

public abstract class CefEntityManager extends CefDataTypeManager implements ICefEntityManager {
    public abstract IEntityData createEntity();

    protected abstract String getEntityType();

    @Override
    public final ICefData createDataType() {
        return createEntity();
    }

    public abstract ICefEntity createEntity(String id);

    @Override
    protected final ICefManagerContext _CreateMgrContext() {
        return createMgrContext();
    }

    protected abstract ICefEntityManagerContext createMgrContext();
}
