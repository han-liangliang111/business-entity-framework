/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.lengthvaladaptor;


import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.api.validation.IValueObjValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;


/**
 * SmallInt长度校验
 */
public class SmallIntLengthValAdaptor extends LengthValAdaptor {

    private int cefVersion;

    public SmallIntLengthValAdaptor(String propName, int length, int prec) {
        super(propName, length, prec);
    }


    @Override
    protected String getDisplayMessage(ICefValidationContext context) {
        String exceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Cef_LenVal_0004");
        return String.format(exceptionCode, this.propName);
    }

    @Override
    protected boolean isValid(Object value) {
        if (value == null || "".equals(value)) {
            return true;
        }
        int intValue = 0;
        if (value instanceof String) {
            intValue = Integer.valueOf(value.toString());
        }
        if(value instanceof Integer){
            intValue = (Integer)value;
        }
        if(value instanceof Short){
            intValue = (Short)value;
        }
        if(intValue > 32767 || intValue < -32768){
            return false;
        }
        return true;
    }
}
