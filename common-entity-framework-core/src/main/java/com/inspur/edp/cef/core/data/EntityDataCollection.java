/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.entity.condition.SortCondition;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class EntityDataCollection extends com.inspur.edp.cef.entity.entity.EntityDataCollection {

    private final CefEntityResInfoImpl resInfo;

    public EntityDataCollection() {
        this.resInfo = null;
    }

    public EntityDataCollection(CefEntityResInfoImpl resInfo) {
        Objects.requireNonNull(resInfo, "resInfo");

        this.resInfo = resInfo;
    }

    @Override
    public Iterator<IEntityData> iterator() {
        List<SortCondition> sorts = getSorts();
        if (sorts == null || sorts.isEmpty()) {
            return super.iterator();
        }
        return innerGetAll().stream().sorted(new EntityDataCollectionComparator(sorts)).iterator();
    }

    private List<SortCondition> getSorts() {
        if (resInfo == null) {
            return null;
        }

        return resInfo.getSorts();
    }
}
