package com.inspur.edp.cef.core.validation.enumvaladaptor;

import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.validation.IRuntimeValidationAssembler;

public class EnumValUtil {
    public static void addEnumValUtil(IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo) {
        int index = 0;
        for (DataTypePropertyInfo propertyInfo : dataTypeInfo.getPropertyInfos().values()) {
            if (propertyInfo.getObjectType() == ObjectType.Enum) {
                if (propertyInfo.getFieldType() == FieldType.String) {
                    validationAssembler.getValidations().add(index++, new StringEnumValAdaptor(propertyInfo.getPropertyName(), ((EnumPropertyInfo) propertyInfo.getObjectInfo()).getEnumValueInfos()));
                    continue;
                }
                if (propertyInfo.getFieldType() == FieldType.Integer) {
                    validationAssembler.getValidations().add(index++, new IntegerEnumValAdaptor(propertyInfo.getPropertyName(), ((EnumPropertyInfo) propertyInfo.getObjectInfo()).getEnumValueInfos()));
                }
            }
        }
    }

}
