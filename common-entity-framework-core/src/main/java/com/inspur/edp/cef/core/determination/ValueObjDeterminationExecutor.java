/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.determination;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;

import java.util.Objects;

public class ValueObjDeterminationExecutor {

    private ICefValueObjContext privateNodeContext;
    private IValueObjRTDtmAssembler privateAssembler;
    private IChangeDetail privateChange;

    public ValueObjDeterminationExecutor(ICefValueObjContext context,
                                         IValueObjRTDtmAssembler assembler, ValueObjModifyChangeDetail changedetail) {
        DataValidator.checkForNullReference(assembler, "assembler");
        privateNodeContext = context;
        privateAssembler = assembler;
        setChange(changedetail);
    }

    protected ICefValueObjContext getNodeContext() {
        return privateNodeContext;
    }

    protected final IValueObjRTDtmAssembler getAssembler() {
        return privateAssembler;
    }

    protected IChangeDetail getChange() {
        return privateChange;
    }

    protected void setChange(IChangeDetail value) {
        privateChange = value;
    }

    public final void execute() {
        ICefDeterminationContext dtmCtx = createDeterminationContext();
        Objects.requireNonNull(dtmCtx, "createDeterminationContext() returns null");

        executeNested(dtmCtx);
        executeSelf(dtmCtx);
    }

    private void executeNested(ICefDeterminationContext dtmCtx) {
        java.util.List<IDetermination> dtms = getAssembler().getBelongingDeterminations();
        if (dtms == null || dtms.isEmpty()) {
            return;
        }

        executeDtms(dtmCtx, getChange(), dtms);
    }

    private void executeSelf(ICefDeterminationContext dtmCtx) {
        java.util.List<IDetermination> dtms = getAssembler().getDeterminations();
        if (dtms == null || dtms.isEmpty()) {
            return;
        }

        executeDtms(dtmCtx, getChange(), dtms);
    }

    private void executeDtms(ICefDeterminationContext dtmCtx, IChangeDetail change,
                             Iterable<IDetermination> dtms) {
        for (IDetermination dtm : dtms) {
            if (dtm.canExecute(change)) {
                dtm.execute(dtmCtx, change);
            }
        }
    }

    protected ICefDeterminationContext createDeterminationContext() {
        return getAssembler().getDeterminationContext(getNodeContext());
    }
}
