/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.determination.builtinimpls;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors.AbstractAfterCreateDtmAdaptor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.cef.spi.determination.IRuntimeDtmAssembler;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
//import com.sun.xml.bind.v2.model.core.ID;
import java.util.ArrayList;
import java.util.List;

public abstract class CefAbstractDtmAssembler implements IEntityRTDtmAssembler {
    private List<IDetermination> belongingDeterminations;
    private List<IDetermination> determinations;
    private List<IDetermination> childAssemblers;

    @Override
    public List<IDetermination> getBelongingDeterminations() {
        if (belongingDeterminations == null)
            belongingDeterminations = new ArrayList<>();
        return belongingDeterminations;
    }

    public final void addBelongingDetermination(IDetermination determination) {
        getBelongingDeterminations().add(determination);
    }

    public final void insertBelongingDetermination(IDetermination determination, int index) {
        getBelongingDeterminations().add(index, determination);
    }

    @Override
    public List<IDetermination> getDeterminations() {
        if (determinations == null)
            determinations = new ArrayList<>();
        return determinations;
    }

    public final void addDetermination(IDetermination determination) {
        getDeterminations().add(determination);
    }

    @Override
    public List<IDetermination> getChildAssemblers() {
        if (childAssemblers == null)
            childAssemblers = new ArrayList<>();
        return childAssemblers;
    }

    public final void addChildAssembler(IDetermination determination) {
        getChildAssemblers().add(determination);
    }
}
