package com.inspur.edp.cef.core.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/18
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 spi:3001开头 core:4001开头 repository:5001
    /**
     * innerCreateChild没有实现！
     * 由子类覆写
     */
    public static final String CEF_RUNTIME_4001 = "CEF_RUNTIME_4001";
    /**
     * 对象[{0}]上的字段[{1}]检查是否引用删除数据时发生错误，相关信息：referer：{2},propertyName:{3}
     */
    public static final String CEF_RUNTIME_4002 = "CEF_RUNTIME_4002";
    /**
     * innerCreateAndSetChildCollection没有实现
     */
    public static final String CEF_RUNTIME_4003 = "CEF_RUNTIME_4003";
    /**
     * innerCreateChild 没有实现
     */
    public static final String CEF_RUNTIME_4004 = "CEF_RUNTIME_4004";
    /**
     * 没有找到构件[{0}]上的构造函数，请检查构件代码是否部署
     */
    public static final String CEF_RUNTIME_4005 = "CEF_RUNTIME_4005";
    /**
     * 实例化构件[{0}]的对象失败
     */
    public static final String CEF_RUNTIME_4006 = "CEF_RUNTIME_4006";
    /**
     * innerMergeChildData没有实现，请检查！
     */
    public static final String CEF_RUNTIME_4007 = "CEF_RUNTIME_4007";
    /**
     * 通过session获取到session items为空，请联系管理员
     */
    public static final String CEF_RUNTIME_4008 = "CEF_RUNTIME_4008";
    /**
     * maxBuffer数据为空，请联系管理员
     */
    public static final String CEF_RUNTIME_4009 = "CEF_RUNTIME_4009";
    public static final String CEF_RUNTIME_4010 = "CEF_RUNTIME_4010";
    public static final String CEF_RUNTIME_4011 = "CEF_RUNTIME_4011";
    public static final String CEF_RUNTIME_4012 = "CEF_RUNTIME_4012";
    public static final String CEF_RUNTIME_4013 = "CEF_RUNTIME_4013";
    public static final String CEF_RUNTIME_4014 = "CEF_RUNTIME_4014";
    public static final String CEF_RUNTIME_4015 = "CEF_RUNTIME_4015";
    public static final String CEF_RUNTIME_4016 = "CEF_RUNTIME_4016";
    /**
     * 反序列化变更集失败
     */
    public static final String CEF_RUNTIME_4017 = "CEF_RUNTIME_4017";
    public static final String CEF_RUNTIME_4018 = "CEF_RUNTIME_4018";
    public static final String CEF_RUNTIME_4019 = "CEF_RUNTIME_4019";
    public static final String CEF_RUNTIME_4020 = "CEF_RUNTIME_4020";
    public static final String CEF_RUNTIME_4021 = "CEF_RUNTIME_4021";
    /**
     * sessionItem为空，请检查
     */
    public static final String CEF_RUNTIME_4022 = "CEF_RUNTIME_4022";
    /**
     * 枚举字段[{0}]检查枚举值是否有效发生异常
     */
    public static final String CEF_RUNTIME_4023 = "CEF_RUNTIME_4023";

    /**
     * CEF_RUNTIME_4024
     */
    public static final String CEF_RUNTIME_4024 = "CEF_RUNTIME_4024";
    public static final String CEF_RUNTIME_4025 = "CEF_RUNTIME_4025";
    /**
     * 字段[{0}]执行必填检查时发生异常
     */
    public static final String CEF_RUNTIME_4026 = "CEF_RUNTIME_4026";
    /**
     * 当前的输入参数不规范,getValue的输入参数不能为null,请检查。
     */
    public static final String CEF_RUNTIME_4027 = "CEF_RUNTIME_4027";

    /**
     * 当前关联字段反射失败，字段：{0}
     */
    public static final String CEF_RUNTIME_4028 = "CEF_RUNTIME_4028";
    /**
     * level{0}只能往最外层buffer合并变更
     */
    public static final String CEF_RUNTIME_4029 = "CEF_RUNTIME_4029";
    public static final String CEF_RUNTIME_4030 = "CEF_RUNTIME_4030";
    public static final String CEF_RUNTIME_4031 = "CEF_RUNTIME_4031";
    public static final String CEF_RUNTIME_4032 = "CEF_RUNTIME_4032";
    public static final String CEF_RUNTIME_4033 = "CEF_RUNTIME_4033";
    public static final String CEF_RUNTIME_4034 = "CEF_RUNTIME_4034";
}
