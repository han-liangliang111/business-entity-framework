/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data.extendhandler;

import com.inspur.edp.cef.core.data.RootEntityAccessor;
import com.inspur.edp.cef.entity.accessor.entity.IChildAccessor;
import com.inspur.edp.cef.entity.accessor.entity.IRootAccessor;
import com.inspur.edp.cef.entity.accessor.entity.OnChildAddedListener;
import com.inspur.edp.cef.entity.accessor.entity.OnChildRemovedListener;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.spi.extend.datatype.ICefDataExtend;

public class RootEntityAccExtHandler extends EntityAccExtHandler implements OnChildAddedListener,
        OnChildRemovedListener {

    public RootEntityAccExtHandler(RootEntityAccessor belong, boolean r) {
        super(belong, r);
    }

    @Override
    protected RootEntityAccessor getBelong() {
        return (RootEntityAccessor) super.getBelong();
    }

    @Override
    protected void onAddingExtend(ICefDataExtend ext, ICefData extendData) {
        super.onAddingExtend(ext, extendData);
        if (extendData instanceof IRootAccessor) {
            IRootAccessor rootAcc = (IRootAccessor) extendData;
            rootAcc.addItemAddedListener(this);
            rootAcc.addItemRemovedListener(this);
        }
    }

    //region onChildAdded/Removed
    @Override
    public void OnChildAdded(IChildAccessor iChildAccessor,
                             IEntityDataCollection iEntityDataCollection, IRootAccessor iRootAccessor) {
        getBelong().fireChildAdded(iChildAccessor, iEntityDataCollection);
    }

    @Override
    public void OnChildRemoved(IChildAccessor iChildAccessor,
                               IEntityDataCollection iEntityDataCollection, IRootAccessor iRootAccessor) {
        getBelong().fireChildRemoved(iChildAccessor, iEntityDataCollection);
    }
    //endregion
}
