/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.requiredvaladaptor;

import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.AssocationPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ObjectType;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;
import com.inspur.edp.cef.spi.validation.IRuntimeValidationAssembler;

public class RequiredValsUtils {
    public static void addRequiredVals(IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo) {
        int index = 0;
        for (DataTypePropertyInfo propertyInfo : dataTypeInfo.getPropertyInfos().values()) {
            if (!propertyInfo.getRequired()) {
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.UDT) {
                validationAssembler.getValidations().add(index++, new RefTypeRequiredValAdaptor(propertyInfo.getPropertyName()));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Association) {
                validationAssembler.getValidations().add(index++, new NewRequriedAssValAdaptor(propertyInfo.getPropertyName()));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Normal) {
                switch (propertyInfo.getFieldType()) {
                    case String:
                    case Text:
                        validationAssembler.getValidations()
                                .add(index++, new RequiredStringValAdaptor(propertyInfo.getPropertyName()));
                        break;
                    case Date:
                    case DateTime:
                        validationAssembler.getValidations().add(index++, new RequiredDateTimeValAdaptor(
                                propertyInfo.getPropertyName()));
                        break;
                    case Integer:
                        validationAssembler.getValidations()
                                .add(index++, new RequiredIntValAdaptor(propertyInfo.getPropertyName()));
                        break;
                    case Decimal:
                        validationAssembler.getValidations()
                                .add(index++, new RequiredDecimalValAdaptor(propertyInfo.getPropertyName()));
                        break;
                    case Binary:
                        validationAssembler.getValidations()
                                .add(index++, new RequiredByteArrayValAdaptor(propertyInfo.getPropertyName()));
                        break;
                    case Boolean:
                        validationAssembler.getValidations()
                                .add(index++, new RequiredValAdaptor(propertyInfo.getPropertyName()));
                }
            }
        }
    }

    public static void addUdtRequiredVals(IRuntimeValidationAssembler validationAssembler, CefDataTypeInfo dataTypeInfo) {
        int index = 0;
        for (DataTypePropertyInfo propertyInfo : dataTypeInfo.getPropertyInfos().values()) {
//      if(!propertyInfo.getRequired()){
//        continue;
//      }
            if (propertyInfo.getObjectType() == ObjectType.UDT) {
                validationAssembler.getValidations().add(index++, new TplRequiredValDecorator(new RefTypeRequiredValAdaptor(propertyInfo.getPropertyName())));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Association) {
                validationAssembler.getValidations().add(index++, new TplRequiredValDecorator(new NewRequriedAssValAdaptor(propertyInfo.getPropertyName())));
                continue;
            }
            if (propertyInfo.getObjectType() == ObjectType.Normal) {
                switch (propertyInfo.getFieldType()) {
                    case String:
                    case Text:
                        validationAssembler.getValidations()
                                .add(index++, new TplRequiredValDecorator(new RequiredStringValAdaptor(propertyInfo.getPropertyName())));
                        break;
                    case Date:
                    case DateTime:
                        validationAssembler.getValidations().add(index++, new TplRequiredValDecorator(new RequiredDateTimeValAdaptor(
                                propertyInfo.getPropertyName())));
                        break;
                    case Integer:
                        validationAssembler.getValidations()
                                .add(index++, new TplRequiredValDecorator(new RequiredIntValAdaptor(propertyInfo.getPropertyName())));
                        break;
                    case Decimal:
                        validationAssembler.getValidations()
                                .add(index++, new TplRequiredValDecorator(new RequiredDecimalValAdaptor(propertyInfo.getPropertyName())));
                        break;
                    case Binary:
                        validationAssembler.getValidations()
                                .add(index++, new TplRequiredValDecorator(new RequiredByteArrayValAdaptor(propertyInfo.getPropertyName())));
                        break;
                }
            }
        }
    }
}
