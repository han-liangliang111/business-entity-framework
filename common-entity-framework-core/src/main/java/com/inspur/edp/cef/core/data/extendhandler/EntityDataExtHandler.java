/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data.extendhandler;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.spi.extend.datatype.ICefDataExtend;
import com.inspur.edp.cef.spi.extend.entity.ICefAddedChildDataExtend;
import com.inspur.edp.cef.spi.extend.entity.ICefEntityDataExtend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityDataExtHandler extends CefDataExtHandler {

    private Map<String, IEntityDataCollection> extendChildDataCollection;

    @Override
    protected void onAddingExtend(ICefDataExtend ext, ICefData data) {
        super.onAddingExtend(ext, data);
        if (!(ext instanceof ICefEntityDataExtend)) {
            return;
        }
        ICefEntityDataExtend entityDataExt = (ICefEntityDataExtend) ext;
        List<ICefAddedChildDataExtend> addedChilds = entityDataExt.getAddedChilds();
        if (addedChilds != null && !addedChilds.isEmpty()) {
            extendChildDataCollection = new HashMap<>();
            addedChilds.forEach(child -> {
                IEntityDataCollection childCollection = child.createDataCollection();
                extendChildDataCollection.put(child.getCode(), childCollection);
            });
        }

    }

    public boolean createChild(String childCode, RefObject<ICefData> result) {
        for (ICefDataExtend extend : getExtendList()) {
            ICefEntityDataExtend dataExt =
                    extend instanceof ICefEntityDataExtend ? (ICefEntityDataExtend) extend : null;
            if (dataExt == null) {
                continue;
            }
            List<ICefAddedChildDataExtend> addedChilds = dataExt.getAddedChilds();
            if (addedChilds == null || addedChilds.isEmpty()) {
                continue;
            }
            for (ICefAddedChildDataExtend childExt : addedChilds) {
                if (!childExt.getCode().equals(childCode)) {
                    continue;
                }
                result.argvalue = childExt.createData();
                return true;
            }
        }
        result.argvalue = null;
        return false;
    }

    public boolean mergeChildData(String nodeCode, ArrayList<IEntityData> childData) {
        if (extendChildDataCollection == null) {
            return false;
        }
        IEntityDataCollection childCollection = extendChildDataCollection.get(nodeCode);
        if (childCollection == null) {
            return false;
        }
        childCollection.addAll(childData);
        return true;
    }

    public boolean getChild(String nodeCode, RefObject<IEntityDataCollection> result) {
        result.argvalue = extendChildDataCollection.get(nodeCode);
        return result.argvalue != null;
    }

    public Map<String, IEntityDataCollection> getChilds() {
        return extendChildDataCollection;
    }

    @Override
    public void clear() {
        super.clear();
        extendChildDataCollection.clear();
    }

    @Override
    public CefDataExtHandler copy() throws CloneNotSupportedException {
        EntityDataExtHandler rez = (EntityDataExtHandler) super.copy();
        if (extendChildDataCollection != null) {
            extendChildDataCollection.entrySet().forEach(entry -> {
                IEntityDataCollection rezCollection = rez.extendChildDataCollection.get(entry.getKey());
                entry.getValue().forEach(item -> rezCollection.add((IEntityData) item.copy()));
            });
        }
        return rez;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        EntityDataExtHandler rez = (EntityDataExtHandler) super.clone();
        rez.extendChildDataCollection = null;
        return rez;
    }
}
