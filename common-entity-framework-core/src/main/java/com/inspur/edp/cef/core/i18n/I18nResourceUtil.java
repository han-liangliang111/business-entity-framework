/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.i18n;


import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.i18n.framework.api.language.ILanguageService;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import org.springframework.context.annotation.Bean;
import io.iec.edp.caf.i18n.framework.api.resource.IResourceLocalizer;

import java.util.List;


/**
 * 运行时获取国际化资源项的值
 */
public final class I18nResourceUtil {
    /**
     * 运行时，根据资源项key，获取value
     *
     * @param currentSu
     * @param metadataId
     * @param key
     * @return
     */
    @Bean
    public static String getResourceItemValue(String currentSu, String metadataId, String key) {
        IResourceLocalizer localizer = CefBeanUtil.getAppCtx().getBean(IResourceLocalizer.class);
        return localizer.getString(key, metadataId, currentSu, "");
    }
}
