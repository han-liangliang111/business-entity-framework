package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.core.crypt.BefBuiltInEncrypt;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.entity.exception.ExceptionCode;
import io.iec.edp.caf.commons.utils.InvokeService;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public class DataInitializer {
    public static void initialize() {
        List<String> names = Arrays.asList("k","e","y");
        String propertyName = String.join("", names);
        String className = "com.inspur.edp.cef.core.crypt.BefBuiltInEncrypt";
        String propertyValue = "e05a7a9d-cd1b-4fbd-b97c-51b9db8a7b0e";
        setStaticPropertyValue(className,propertyName,propertyValue);
    }

    static void setStaticPropertyValue(String className, String propertyName, Object propertyValue) {
        Class<?> clazz = InvokeService.getClass(className);
        try {
            if (propertyName != null) {
                //获取MyClass类的静态字段staticFieId的Field对象
                Field field = clazz.getDeclaredField(propertyName);
                //设置为可访问私有字段
                //field.setAccessible (true) ;
                //为静态字段staticField赋值
                field.set(null,propertyValue); //注意这里的第一个参数传递nu1l,
            }
        } catch (Throwable throwable) {
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_1002, new Exception(throwable), propertyName);
        }
    }
}
