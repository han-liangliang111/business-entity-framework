package com.inspur.edp.cef.variable.api.variable;


import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;

public interface IVariable extends ICefValueObject {
    //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing via the 'new' keyword:
//ORIGINAL LINE: new IVariableContext getContext();
    IVariableContext getVariableContext();
}