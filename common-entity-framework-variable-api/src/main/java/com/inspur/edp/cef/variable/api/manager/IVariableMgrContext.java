package com.inspur.edp.cef.variable.api.manager;

import com.inspur.edp.cef.api.manager.valueObj.ICefValueObjManagerContext;

public interface IVariableMgrContext extends ICefValueObjManagerContext {
    IVariableManager getVarManager();
}