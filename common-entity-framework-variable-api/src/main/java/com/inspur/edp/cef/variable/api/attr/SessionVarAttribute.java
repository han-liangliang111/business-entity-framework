package com.inspur.edp.cef.variable.api.attr;

public @interface SessionVarAttribute {
    String sessionKey();
}