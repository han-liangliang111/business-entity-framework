package com.inspur.edp.cef.variable.api.determination;


import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.variable.api.data.IVariableData;

public interface IVarDeterminationContext extends ICefDeterminationContext {
    //C# TO JAVA CONVERTER WARNING: There is no Java equivalent to C#'s shadowing via the 'new' keyword:
//ORIGINAL LINE: new IVariableData getData();
    IVariableData getVarData();
}