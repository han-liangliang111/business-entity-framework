package com.inspur.edp.cef.variable.api.variable;


import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public interface IVariableContext extends ICefValueObjContext {
    /**
     * 最后一次Modify触发的联动计算产生的内部变更, 不包含Modify传入的变更
     */
    IChangeDetail getInnerChange();
}