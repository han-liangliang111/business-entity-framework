package com.inspur.edp.bef.core.debug;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @Author wangmaojian
 * @create 2023/5/30
 */
public class NetworkAddressUtil {
    private static final String UNKNOWN_LOCALHOST = "UNKNOWN";
    //结果缓存30天
    //TODO 后续考虑使用后台任务定时更新，兼顾准确性与性能
    private static final long CACHE_TIMEOUT = TimeUnit.DAYS.toMillis(30);
    private static final Logger logger = LoggerFactory.getLogger(NetworkAddressUtil.class);
    private static HashMap<String, String> cachedLocalHost = new HashMap<>();
    private static long lastUpdateTime;
    private static Object lockObj = new Object();

    public static HashMap<String, String> getLocalAddressInfo() {
        try {
            if (cachedLocalHost != null && cachedLocalHost.size() > 0 && isCacheValid()) {
                return cachedLocalHost;
            }
            synchronized (lockObj) {
                if (cachedLocalHost != null && cachedLocalHost.size() > 0 && isCacheValid()) {
                    return cachedLocalHost;
                }
                Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
                while (interfaces != null && interfaces.hasMoreElements()) {
                    Enumeration<InetAddress> addresses = interfaces.nextElement().getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        InetAddress address = addresses.nextElement();
                        if (acceptableAddress(address)) {
                            cachedLocalHost.put("address", address.getHostAddress());
                            cachedLocalHost.put("hostname", address.getHostName());
                            cachedLocalHost.put("hostdomain", address.getCanonicalHostName());
                            updateCacheTime();
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("获取本机IP地址失败", e);
            cachedLocalHost.put("address", UNKNOWN_LOCALHOST);
            cachedLocalHost.put("hostname", UNKNOWN_LOCALHOST);
            cachedLocalHost.put("hostdomain", UNKNOWN_LOCALHOST);
        } finally {
            return cachedLocalHost;
        }
    }

    private static boolean acceptableAddress(InetAddress address) {
        return address != null && !address.isLoopbackAddress() && !address.isAnyLocalAddress() && !address.isLinkLocalAddress();
    }

    private static boolean isCacheValid() {
        return System.currentTimeMillis() - lastUpdateTime < CACHE_TIMEOUT;
    }

    private static void updateCacheTime() {
        lastUpdateTime = System.currentTimeMillis();
    }
}
