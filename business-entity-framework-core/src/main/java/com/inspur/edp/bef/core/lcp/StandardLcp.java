/*
 * Copyright (c) 2020 - present, Inspur Genersoft Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.inspur.edp.bef.core.lcp;


import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.exceptions.BizMessageException;
import com.inspur.edp.bef.api.lcp.BefContext;
import com.inspur.edp.bef.api.lcp.BefTempTableContext;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.api.lcp.ResponseContext;
import com.inspur.edp.bef.api.lcp.StandardLcpParam;
import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.api.parameter.retrieve.RequestedBufferType;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveChildResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
import com.inspur.edp.bef.api.parameter.save.SaveParameter;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.action.base.ActionStack;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.action.save.ILcpSaveAction;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.session.BEFuncSessionBase;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.core.session.transaction.BufferTransactionManager;
import com.inspur.edp.bef.core.tcc.BefTccParamItem;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefEntityResInfoImpl;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefModelResInfoImpl;
import com.inspur.edp.bef.spi.extend.IBEManagerExtend;
import com.inspur.edp.caf.transaction.api.GlobalTransactionService;
import com.inspur.edp.caf.transaction.api.annoation.tcc.BusinessActionContext;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.action.EditResult;
import com.inspur.edp.cef.api.message.BizMessage;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.spi.entity.info.CefEntityTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefModelResInfoImpl;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.commonmodel.api.ICMManager;
import com.inspur.edp.commonmodel.core.session.distributed.SessionEditToken;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public abstract class StandardLcp implements IStandardLcp {
    private final StandardLcpParam param = new StandardLcpParam();
    @Getter
    private FuncSession currentSession;
    private BEFuncSessionBase sessionItem;
    private com.inspur.edp.bef.api.lcp.BefContext befContext;
    //运行时定制扩展be的type
    private String beType;
    //基础BE类型
    private String basicBeType;
    //运行时定制扩展be的id
    private String beId;
    private String basicBeId;
    //region [运行时定制]扩展
    private List<IBEManagerExtend> extList;

    //region Constructor & Session
    @Deprecated
    protected StandardLcp(String sessionID) {
    }
    //endregion Constructor


    //region 字段属性

    protected StandardLcp() {

    }
    //private set { FuncSessionManager.getCurrentSession().ResponseContext = value; }

    private static void throwNoSession() {
        throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5023, null, ExceptionLevel.Error, false);
    }

    private static com.inspur.edp.bef.entity.exception.ExceptionLevel getTransLevel(io.iec.edp.caf.commons.exception.ExceptionLevel level) {
        com.inspur.edp.bef.entity.exception.ExceptionLevel level1 = null;
        if (level == io.iec.edp.caf.commons.exception.ExceptionLevel.Error) {
            level1 = com.inspur.edp.bef.entity.exception.ExceptionLevel.Error;
        }

        if (level == io.iec.edp.caf.commons.exception.ExceptionLevel.Warning) {
            level1 = com.inspur.edp.bef.entity.exception.ExceptionLevel.Warning;
        }

        if (level == io.iec.edp.caf.commons.exception.ExceptionLevel.Info) {
            level1 = com.inspur.edp.bef.entity.exception.ExceptionLevel.Info;
        }

        return level1;
    }

    //endregion 字段属性

    public final void initSession() {
        currentSession = FuncSessionManager.getCurrentSession();
        if (currentSession == null) {
            throwNoSession();
        }
        sessionItem = (BEFuncSessionBase) currentSession.getSessionItems().get(getBEType());
        if (sessionItem == null) {
            currentSession.getSessionItems()
                    .put(getBEType(), sessionItem = createSessionItem(currentSession));
        }
    }

    public void setCurrentSession(FuncSession value) {
        Objects.requireNonNull(value);
        if (currentSession != null) {
            throw new BefExceptionBase();
        }
        currentSession = value;
    }

    public void initExtends() {
        List<IBEManagerExtend> exts = getExtends();
        if (exts == null) {
            return;
        }
        exts.stream().forEach(item -> addExtend(item));
    }

    protected List<IBEManagerExtend> getExtends() {
        return null;
    }

    @Override
    public final ResponseContext getResponseContext() {
        return sessionItem == null ? null : sessionItem.getResponseContext();
    }

    @Override
    public BefContext getBefContext() {
        return currentSession == null ? (befContext != null) ? befContext
                : (befContext = new BefContext()) : currentSession.getBefContext();
    }

    /**
     * 增
     */
    @Override
    public final IEntityData retrieveDefault() {
        return getBEManager().retrieveDefault();
    }

    @Override
    public final IEntityData retrieveDefault(String dataID) {
        return getBEManager().retrieveDefault(dataID);
    }

    @Override
    public final IEntityData retrieveDefault(String dataID, Map<String, Object> defaultValues) {
        return getBEManager().retrieveDefault(dataID, defaultValues);
    }

    /**
     * 复制指定数据
     *
     * @param cloneDataID 被复制数据id
     * @return
     */
    public IEntityData clone(String cloneDataID, CloneParameter cloneParameter) {
        return clone(cloneDataID, "", cloneParameter);
    }

    public IEntityData clone(String cloneDataID, String dataID, CloneParameter cloneParameter) {
        return getBEManager().clone(cloneDataID, dataID, cloneParameter);
    }

    @Override
    public Map<String, IEntityData> retrieveDefault(List<String> dataIDs) {
        return getBEManager().retrieveDefault(dataIDs);
    }

    /**
     * 增, 可自定义除主键字段外的默认值
     */
    @Override
    public final IEntityData retrieveDefault(java.util.Map<String, Object> defaultValues) {
        return getBEManager().retrieveDefault(defaultValues);
    }

    /**
     * 新增业务实体的从(从)表数据
     * 1. 新增从表数据时，nodeCodes包含从表实体编号，hierachyIdList包含主表数据的唯一标识；
     * 2. 该方法不只支持新增从表数据，同样的也可以新增从从表数据。新增从从表数据时，
     * nodeCodes中要包含从从表所属的从表实体编号，以及从从表的实体编号；
     * hierachyIdList列表包括对应主表数据的唯一标识、子表数据的唯一标识。
     *
     * @param nodeCodes      要新增数据的从(从)表的实体编号
     * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
     * @return 新增的从(从)表实体数据
     */
    @Override
    public final IEntityData retrieveDefaultChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList) {
        return getBEManager().retrieveDefaultChild(nodeCodes, hierachyIdList);
    }

    /**
     * 新增业务实体的从(从)表数据
     * 1. 新增从表数据时，nodeCodes包含从表实体编号，hierachyIdList包含主表数据的唯一标识；
     * 2. 该方法不只支持新增从表数据，同样的也可以新增从从表数据。新增从从表数据时，
     * nodeCodes中要包含从从表所属的从表实体编号，以及从从表的实体编号；
     * hierachyIdList列表包括对应主表数据的唯一标识、子表数据的唯一标识。
     *
     * @param nodeCodes      要新增数据的从(从)表的实体编号
     * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
     * @return 新增的从(从)表实体数据
     */
    @Override
    public final IEntityData retrieveDefaultChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, String dataID) {
        return getBEManager().retrieveDefaultChild(nodeCodes, hierachyIdList, dataID);
    }

    @Override
    public final IEntityData retrieveDefaultChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, Map<String, Object> values) {
        return getBEManager().retrieveDefaultChild(nodeCodes, hierachyIdList, values);
    }

    @Override
    public final Map<String, IEntityData> retrieveDefaultChild(java.util.List<String> nodeCodes,
                                                               java.util.List<String> hierachyIdList, List<String> dataIDs) {
        return getBEManager().retrieveDefaultChild(nodeCodes, hierachyIdList, dataIDs);
    }

    @Override
    public final IEntityData createObjectData() {
        return innerCreateBeManager().createObjectData();
    }

    /**
     * 删
     */
    @Override
    public final void delete(java.util.List<String> dataIds) {
        DataValidator.checkForNullReference(dataIds, "dataIds");
        if(log.isInfoEnabled()){
            log.info("执行数据批量删除操作，要删除的数据ID:[{}]", String.valueOf(dataIds));
        }
        getBEManager().delete(dataIds);
    }

    @Override
    public final void delete(String dataId) {
        DataValidator.checkForNullReference(dataId, "dataId");
        if(log.isInfoEnabled()){
            log.info("执行数据删除操作，要删除的数据ID:[{}]", dataId);
        }
        getBEManager().delete(dataId);
    }

    /**
     * 删除实体数据上指定ID的从(从)表数据，将删除变更集提交到内存。若需要将该删除变更集提交到数据库，需要在调用DeleteChild方法后，再调用Save方法。
     * 1. 删除从表数据时，nodeCodes包含从表实体编号，hierachyIdList包含主表数据的唯一标识，ids包含要删除的从表数据的唯一标识
     * 2. 删除从(从)表数据时，nodeCodes包含从从表所属的从表实体编号、以及从从表的实体编号；hierachyIdList包含从从表数据所属的主表数据的唯一标识，
     * 以及从表数据的唯一标识，ids包含要删除的从从表数据的唯一标识
     *
     * @param nodeCodes      要删除数据的从(从)表的实体编号
     * @param hierachyIdList 要删除数据的从(从)表的所属实体数据的唯一标识
     * @param ids            要删除的从(从)表数据的唯一标识集合
     */
    @Override
    public final void deleteChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.List<String> ids) {
        DataValidator.checkForNullReference(nodeCodes, "nodeCodes");
        DataValidator.checkForNullReference(hierachyIdList, "hierachyIdList");
        DataValidator.checkForNullReference(ids, "ids");

        getBEManager().deleteChild(nodeCodes, hierachyIdList, ids);
    }

    @Override
    public final RetrieveResult retrieve(List<String> dataIds) {
        return retrieve(dataIds, new RetrieveParam());
    }

    /**
     * 批量检索
     *
     * @param dataIds
     * @param retrieveParam
     * @return
     */
    @Override
    public final RetrieveResult retrieve(List<String> dataIds, RetrieveParam retrieveParam) {
        if(dataIds==null){
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5044);
        }
        if(retrieveParam == null){
            retrieveParam = new RetrieveParam();
        }
        if(log.isInfoEnabled()){
            log.info("执行数据批量检索操作，要检索的数据ID:[{}], 检索参数:{}", dataIds, SerializerUtil.writeObject(retrieveParam));
        }
        return getBEManager().retrieve(dataIds, retrieveParam);
    }

    @Override
    public final EditResult edit(String dataId) {
        return getBEManager().edit(dataId);
    }

    @Override
    public final RespectiveRetrieveResult retrieve(String dataId) {
        RetrieveParam tempVar = new RetrieveParam();
        return retrieve(dataId, tempVar);
    }

    @Override
    public final RespectiveRetrieveResult retrieve(String dataId, RetrieveParam retrieveParam) {
        if(retrieveParam == null){
            retrieveParam = new RetrieveParam();
        }
        if(log.isInfoEnabled()){
            log.info("执行数据检索操作，要检索的数据ID:[{}], 检索参数:{}", dataId, SerializerUtil.writeObject(retrieveParam));
        }
        return getBEManager().retrieve(dataId, retrieveParam);
    }

    @Override
    public final RespectiveRetrieveResult retrieveChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, RetrieveParam retrieveParam) {
        if (nodeCodes == null || nodeCodes.isEmpty()) {
            throw new NullPointerException("nodeCodes");
        }
        if (hierachyIdList == null || hierachyIdList.isEmpty()) {
            throw new NullPointerException("hierachyIdList");
        }

        return getBEManager().retrieveChild(nodeCodes, hierachyIdList, retrieveParam);
    }

    /**
     * 检索子表数据
     *
     * @param nodeCodes      除根节点外从上级到下级的节点编号
     * @param hierachyIdList 除根节点外从上级到下级的数据ID
     * @param retrieveParam
     * @return
     */
    public final RetrieveChildResult retrieveChild(java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, ArrayList<String> ids, RetrieveParam retrieveParam) {
//		try {
        if (nodeCodes == null || nodeCodes.isEmpty()) {
            throw new NullPointerException("nodeCodes");
        }
        if (hierachyIdList == null || hierachyIdList.isEmpty()) {
            throw new NullPointerException("hierachyIdList");
        }
        if (ids == null || ids.isEmpty()) {
            throw new NullPointerException("ids");
        }
        return getBEManager().retrieveChild(nodeCodes, hierachyIdList, ids, retrieveParam);
    }

    /**
     * 根据变更集更新数据
     *
     * @param changeDetail
     */
    @Override
    public final void modify(IChangeDetail changeDetail) {
//		try {
        Objects.requireNonNull(changeDetail, "changeDetail can not be null.");
        Objects.requireNonNull(changeDetail.getDataID(), "changeDetail's DataId can not be null.");
        if(log.isInfoEnabled()){
            log.info("执行数据修改操作，要修改的数据ID:[{}], 变更集:{}", changeDetail.getDataID(), SerializerUtil.writeObject(changeDetail));
        }
        getBEManager().modify(changeDetail);
    }

    @Override
    public final void modify(java.util.ArrayList<IChangeDetail> changeDetails) {
        DataValidator.checkForNullReference(changeDetails, "changeDetails");
        getBEManager().modify(changeDetails);
    }

    @Override
    public final void modify(EntityFilter filter, ModifyChangeDetail changeDetail) {
//		try {
        DataValidator.checkForNullReference(filter, "filter");
        DataValidator.checkForNullReference(changeDetail, "changeDetail");
        if (changeDetail.getChildChanges().size() > 0) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5060);
        }

        getBEManager().modify(filter, changeDetail);
//		} catch (RuntimeException e) {
//			throw buildBefException("Modify", e);
//		}
    }

    @Override
    public final List<IEntityData> queryWithoutAuthInfo(EntityFilter filter) {

        return getBEManager().queryWithoutAuthInfo(filter);
    }

    @Override
    public final List<IEntityData> queryWithoutAuthInfo(String nodeCode, EntityFilter filter) {

        return getBEManager().queryWithoutAuthInfo(nodeCode, filter);
    }

    @Override
    public final List<IEntityData> query(EntityFilter filter) {
        DataValidator.checkForNullReference(filter, "filter");
        if(log.isInfoEnabled()){
            log.info("执行查询操作，查询条件：{}", SerializerUtil.writeObject(filter));
        }
        return getBEManager().query(filter);
    }

    @Override
    public final List<IEntityData> queryWithBuffer(EntityFilter filter) {
        DataValidator.checkForNullReference(filter, "filter");
        return getBEManager().queryWithBuffer(filter);
    }

    @Override
    public final List<IEntityData> query() {
        return query(new EntityFilter());
    }

    @Override
    public final List<IEntityData> query(String nodeCode, EntityFilter filter) {
        if(log.isInfoEnabled()){
            log.info("执行查询操作，查询实体[{}],条件：{}", nodeCode, SerializerUtil.writeObject(filter));
        }
        return getBEManager().query(nodeCode, filter);
    }

    //ORIGINAL LINE: public List<IEntityData> queryWithBuffer(string nodeCode, EntityFilter filter = null)
    @Override
    public final List<IEntityData> queryWithBuffer(String nodeCode, EntityFilter filter) {
//		try {
        return getBEManager().queryWithBuffer(nodeCode, filter);
//		} catch (RuntimeException e) {
//			throw buildBefException("QueryWithBuffer", e);
//
//		}
    }

    //region Transaction

    @Override
    public final int getResultCount(EntityFilter filter) {
        if (filter.getIsUsePagination()) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5028);
        }
        return this.query(filter).size();
    }

    protected java.util.HashMap<String, String> getNodeRecord() {
        BefModelResInfoImpl modelResInfo = (BefModelResInfoImpl) getBEManager().getModelInfo();
        HashMap<String, String> map = new HashMap<>(modelResInfo.getEntityResInfos().size(), 1);
        for (EntityResInfo resInfo : modelResInfo.getEntityResInfos().values()) {
            BefEntityResInfoImpl befEntityResInfo = (BefEntityResInfoImpl) resInfo;
            map.put(befEntityResInfo.getEntityCode(), befEntityResInfo.getEntityId());
        }
        return map;
    }

    public BEFuncSessionBase createSessionItem(FuncSession session) {
        BEFuncSessionBase result = new BEFuncSessionBase(session, getBEType(), getBasicBeType());
        BEManager beMgr = innerCreateBeManager();
        if (extList != null) {
            beMgr.setExtends(extList);
        }
        beMgr.setBEType(getBEType());
        beMgr.setBasicBeType(getBasicBeType());
        beMgr.setResponseContext(result.getResponseContext());
        result.setBEManager(beMgr);
        beMgr.getBEManagerContext().setSession(session);
        beMgr.getBEManagerContext().setChangesetManager(result.getBufferChangeManager());
        beMgr.getBEManagerContext().setBufferManager(result.getBufferChangeManager());
        return result;
    }

    private IBEManager tryGetBeManager() {
        if (currentSession == null) {
            return innerCreateBeManager();
        }
        currentSession.checkValid();

        BEFuncSessionBase beFuncSession = currentSession.getFuncSessionItem(getBEType());
        if (beFuncSession == null)
            return innerCreateBeManager();
        BEManager rez = (BEManager) beFuncSession.getBEManager();
        rez.setLcpParam(param);
        return rez;
    }

    protected final IBEManager getBEManager() {
        if (currentSession == null) {
            throwNoSession();
        }
        currentSession.checkValid();

        BEFuncSessionBase beFuncSession = currentSession.getFuncSessionItem(getBEType());
        if (beFuncSession == null)
            return innerCreateBeManager();
        BEManager beManager = (BEManager) beFuncSession.getBEManager();
        beManager.setLcpParam(param);
        return beManager;
    }

    private BEManager innerCreateBeManager() {
        BEManager beManager = (BEManager) createBEManager();
        beManager.setBEInfo(getBEInfo());
        if (extList != null) {
            beManager.setExtends(extList);
        }
        return beManager;
    }

    @Override
    public void save() {
        if(log.isInfoEnabled()){
            log.info("执行save保存操作");
        }
        save(SaveParameter.getDefault());
    }

    //TODO:事务在外层service中启动, 此处改成单be类型的处理, 不启事务
    @Override
    public final void save(SaveParameter par) {
        ILcpSaveAction action = SpringBeanUtils.getBean(ILcpSaveAction.class);
        GlobalTransactionService tccService = SpringBeanUtils.getBean(GlobalTransactionService.class);
        boolean inTcc = (tccService.inTransaction() && tccService.isEnable());
        boolean isExecuteTcc = false;
        if (inTcc) {
            //存在老版本的生成代码，getModelInfo类型不是BefModelResInfoImpl，需要增加兼容判断
            if(getModelInfo() != null && getModelInfo() instanceof BefModelResInfoImpl){
                if((getModelInfo() != null && ((BefModelResInfoImpl)getModelInfo()).getTccSupported())){
                    isExecuteTcc = true;
                }
            }
        }
        if(isExecuteTcc){
            action.executeTcc(null, this.currentSession, (BEManager)getBEManager(), par);
        }
        else {
            action.execute(null, this.currentSession, (BEManager)getBEManager(), par);
        }
    }

    @Override
    public boolean hasChanges() {
        return getBEManager().hasChanges();
    }

    @Override
    public final void cancel() {
        ClearChangeset();
        ClearLock();
    }

    @Override
    public final void cleanUp() {
        ClearChangeset();
        ClearLock();
        ClearBuffer();
    }

    protected final void ClearChangeset() {
        getBEManager().clearChangeset();
    }

    protected final void ClearLock() {
        getBEManager().clearLock();
    }

    protected final void ClearBuffer() {
        getBEManager().clearBuffer();
    }

    @Override
    public final String serializeData(IEntityData data) {
        String jsonData = innerCreateBeManager().serializeData(data);
        if(log.isInfoEnabled()){
            log.info("数据序列化结构：{}", jsonData);
        }
        return jsonData;
    }

    @Override
    public final IEntityData deSerializeData(String data) {
        DataValidator.checkForEmptyString(data, "data");
        if(log.isInfoEnabled()){
            log.info("反序列化data结构：{}", data);
        }
        return innerCreateBeManager().deSerializeData(data);
    }

    /**
     * 提供给AIF使用,将数据反序列化成IAccessor类型：新增主表和子表序列化后，反序列化
     * @param data
     * @return
     */
    @Override
    public final IEntityData deSerializeAccessorData(String data) {
        DataValidator.checkForEmptyString(data, "data");
        return innerCreateBeManager().deSerializeAccessorData(data);
    }

    protected abstract IBEManager createBEManager();

    @Override
    public final IEntityData createData(String dataID) {
        return innerCreateBeManager().createData(dataID);
    }

    //ORIGINAL LINE: public IEntityData createChildData(string childCode, string dataID = "")
    @Override
    public final IEntityData createChildData(String childCode, String dataID) {
        DataValidator.checkForEmptyString(childCode, "childCode");
        return innerCreateBeManager().createChildData(childCode, dataID);
    }

    @Override
    public IEntityData createUnlistenableData(String dataID) {
        return innerCreateBeManager().createUnlistenableData(dataID);
    }

    @Override
    public IEntityData createUnlistenableChildData(String childCode, String dataID) {
        return innerCreateBeManager().createUnlistenableChildData(childCode, dataID);
    }

    @Override
    public final IAccessor createUnlistenableChildAccessor(String childCode, ICefData data) {
        IAccessor acs = innerCreateBeManager().createUnlistenableChildAccessor(childCode, data);
        return acs;
    }
    @Override
    public String serializeData(List<String> nodeCodes, IEntityData data) {
        throw new UnsupportedOperationException();
    }

    @Override
    public IEntityData deserializeData(List<String> nodeCodes, String serializedData) {
        throw new UnsupportedOperationException();
    }

    @Override
    public final IEntityData deserializeData(String serializedData) {
        DataValidator.checkForEmptyString(serializedData, "serializedData");
        return innerCreateBeManager().deserializeData(serializedData);
    }

    @Override
    public final String serializeChanges(List<IChangeDetail> changes) {
        return innerCreateBeManager().serializeChanges(changes);
    }

    @Override
    public final List<IChangeDetail> deserializeChanges(String strChanges) {
        if(log.isInfoEnabled()){
            log.info("反序列化变更集：{}", strChanges);
        }
        DataValidator.checkForEmptyString(strChanges, "strChanges");
        return ((ICMManager) innerCreateBeManager()).deserializeChanges(strChanges);
    }

    @Override
    public String serializeChange(IChangeDetail change) {
        return innerCreateBeManager().serializeChange(change);
    }

    @Override
    public IChangeDetail deserializeChange(String changeStr) {
        if(log.isInfoEnabled()){
            log.info("反序列化变更集：{}", changeStr);
        }
        return innerCreateBeManager().deserializeChange(changeStr);
    }

    /**
     * 获取节点的ID
     *
     * @param nodeName 节点的编号名称
     * @return
     */
    @Override
    public final String getNodeId(String nodeName) {
        if (!getNodeRecord().containsKey(nodeName)) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5061, nodeName);
        }
        String rez = getNodeRecord().get(nodeName);
        if (rez == null) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5062);
        }
        return rez;
    }

    /**
     * 获取主节点的ID
     *
     * @return
     */
    @Override
    public final String getMainNodeId() {
//		try {
        return getNodeId(((BusinessEntity) innerCreateBeManager().createSessionlessEntity("")).getNodeCode());
//		} catch (RuntimeException e) {
//			throw buildBefException("GetMainNodeId", e);
//		}
    }

    @Override
    public final RespectiveRetrieveResult retrieve(String nodeId, String dataId, RetrieveParam retrieveParam) {
//		try {
//			DataValidator.checkForNullReference(dataId, "dataId");
        Objects.requireNonNull(dataId, "dataId");
        Objects.requireNonNull(nodeId, "nodeId");

        String mainNodeId = getMainNodeId();
        if (!nodeId.equals(mainNodeId)) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5063);
        }

        return getBEManager().retrieve(dataId, retrieveParam);
//		} catch (RuntimeException e) {
//			throw buildBefException("Retrieve", e);
//		}
    }

    @Override
    public final boolean exists(String dataId) {
//		try {
        if (DotNetToJavaStringHelper.isNullOrEmpty(dataId)) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5064);
        }
        RetrieveParam tempVar = new RetrieveParam();
        tempVar.setNeedLock(false);
        tempVar.setCacheType(RequestedBufferType.CurrentData);
        boolean result = getResult(dataId, tempVar);
        //缓存中没有，再查询数据库，且仅查询主表数据
        if (!result) {
            EntityFilter entityFilter = new EntityFilter();
            FilterCondition tempVar2 = new FilterCondition();
            tempVar2.setFilterField("ID");
            tempVar2.setValue(dataId);
            entityFilter.setFilterConditions(
                    new java.util.ArrayList<>(java.util.Arrays.asList(new FilterCondition[]{tempVar2})));
            return !query(entityFilter).isEmpty();
        }
        return result;
//		} catch (RuntimeException e) {
//			throw buildBefException("Exists", e);
//		}
    }

    private boolean getResult(String dataId, RetrieveParam retrieveParam) {
        return this.retrieve(dataId, retrieveParam).getData() == null ? false : true;
    }

    /**
     * 新增getBEInfo()兼容处理
     *
     * @return
     */
    protected String getBEType() {
        if (!StringUtils.isEmpty(beType)) {
            return beType;
        }
        return innerGetBEInfo().getBEType();
    }

    @Deprecated
    public void setBeType(String beType) {
        if (StringUtils.isEmpty(beType)) {
            throw new IllegalArgumentException("beType cannot be null.");
        }
        this.beType = beType;
    }

    public String getBasicBeType() {
        if (!StringUtils.isEmpty(basicBeType)) {
            return basicBeType;
        }
        return innerGetBEInfo().getBasicBeType();
    }

    public void setBasicBeType(String basicBeType) {
        if (StringUtils.isEmpty(basicBeType)) {
            throw new IllegalArgumentException("basicBeType cannot be null.");
        }
        this.basicBeType = basicBeType;
    }

    public void setBEInfo(String beType, String beId) {
        setBEInfo(beType, beId, beType, beId);
    }

    public void setBEInfo(String beType, String beId, String basicBeType, String basicBeId) {
        Objects.requireNonNull(beType);
        Objects.requireNonNull(beId);
        this.beType = beType;
        this.beId = beId;
        this.basicBeType = basicBeType;
        this.basicBeId = basicBeId;
    }

    //endregion

    /**
     * 子类继承实现
     *
     * @return
     */
    protected BEInfo getBEInfo() {
        BEInfo info = innerGetBEInfo();
        if (info != null) {
            if (!StringUtils.isEmpty(beType)) {
                info.setBEType(getBEType());
            }
            if (!StringUtils.isEmpty(beId)) {
                info.setBEID(beId);
            }
            if (!StringUtils.isEmpty(basicBeType)) {
                info.setBasicBeType(getBasicBeType());
            }
            if (!StringUtils.isEmpty(basicBeId)) {
                info.setBasicBeId(basicBeId);
            }
        } else {
            info = new BEInfo();
            info.setBEType(getBEType());
            info.setBEID(beId);
            info.setBasicBeType(getBasicBeType());
            info.setBasicBeId(basicBeId);
        }
        return info;
    }

//	public IVariableData Variables => ((BEManager)getBEManager()).ReadonlyVariables;

    //endregion

    protected BEInfo innerGetBEInfo() {
        return null;
    }

    //region Variable
    @Override
    public void modifyVariable(IChangeDetail change) {
        getBEManager().modifyVariable(change);
    }

    @Override
    public final void generateTable(String nodeCode) {
//		try {
        getBEManager().generateTable(nodeCode);
//		} catch (RuntimeException e) {
//			throw buildBefException("GenerateTable", e);
//		}
    }

    @Override
    public final void dropTable(String nodeCode) {
//		try {
        getBEManager().dropTable(nodeCode);
//		} catch (RuntimeException e) {
//			throw buildBefException("DropTable", e);
//		}
    }

    //ORIGINAL LINE: public bool isReferred(string befConfig, string dataId, string nodeCode = "")
    @Override
    public final boolean isReferred(String befConfig, String dataId, String nodeCode) {
//		try {
        return getBEManager().isReferred(befConfig, dataId, nodeCode);
//		} catch (RuntimeException e) {
//			throw buildBefException("IsReferred", e);
//		}
    }

    private BefRunTimeBaseException buildBefException(String methodName, RuntimeException e) {
        ExceptionLevel level = ExceptionLevel.Error;

        if (e instanceof BizMessageException) {
            getResponseContext().mergeMessage(new java.util.ArrayList<>(
                    java.util.Arrays.asList(new IBizMessage[]{((BizMessageException) e).getBizMessage()})));
        }
//		else if (e instanceof GSPException)
//		{
//			BizMessage tempVar = new BizMessage();
//			tempVar.MessageFormat = gspException.I18nMessage;
//			tempVar.Level = MessageLevel.Error;
//			tempVar.RuntimeException = gspException;
//			BizMessage message = tempVar;
//			getResponseContext().mergeMessage(new java.util.ArrayList<IBizMessage>(java.util.Arrays.asList(new IBizMessage[] { message})));
//			level = gspException.Level;
//		}
        else if (e instanceof CAFRuntimeException) {
            BizMessage tempVar = new BizMessage();
            tempVar.setMessageFormat(((CAFRuntimeException) e).getI18nMessage());
            tempVar.setLevel(MessageLevel.Error);
            tempVar.setException(e);
            BizMessage message = tempVar;
            getResponseContext().mergeMessage(
                    new java.util.ArrayList<>(java.util.Arrays.asList(new IBizMessage[]{message})));
            level = getTransLevel(((CAFRuntimeException) e).getLevel());
        }
        return new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5065, e, level, methodName);
    }

    //TODO: 兼容be变更, 1906 response挪到sessionItem后去掉
    @Override
    public final java.util.Collection<String> getDataIds() {
        if (currentSession != null) {
            BEFuncSessionBase sessionItem = currentSession.getFuncSessionItem(getBEType());
            if (sessionItem != null && sessionItem.bizEntityCacheManager != null) {
                return sessionItem.bizEntityCacheManager.getDataIds();
            }
        }
        return new java.util.HashSet<>();
    }

    @Override
    public final void setRepositoryVariables(String key, String value) {
        getBEManager().setRepositoryVariable(key, value);
    }

    @Override
    public final boolean isEffective(String nodeCode, String dataId) {
        return getBEManager().isEffective(nodeCode, dataId);
    }

    public final boolean isDatasEffective(String nodeCode, List<String> dataIDs, RefObject<List<String>> noneEffectiveDatas) {
        return getBEManager().isDatasEffective(nodeCode, dataIDs, noneEffectiveDatas);
    }

    public final boolean isDatasEffective(String nodeCode, String propName, List<String> dataIDs, RefObject<List<String>> noneEffectiveDatas) {
        return getBEManager().isDatasEffective(nodeCode, propName, dataIDs, noneEffectiveDatas);
    }

    public IVariableData getVariables() {
        return getBEManager().getVariables();
    }

    public List<String> getDistinctFJM(String fjnPropertyName, EntityFilter filter, Integer parentLayer) {
        return getBEManager().getDistinctFJM(fjnPropertyName, filter, parentLayer);
    }

    /**
     * 执行action保证其中的所有be操作的事务性(同时成功同时失败)
     * @param action
     */
    @Override
    public void atomicallyInvoke(Runnable action) {
        Objects.requireNonNull(action);

        if (!ActionStack.isEmpty()) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5077);
        }
//		boolean succss = true;
        Objects.requireNonNull(currentSession);
        SessionEditToken editToken = FuncSessionManager.getCurrent().beginEdit(currentSession, false);
        try {
            BufferTransactionManager instance = new BufferTransactionManager(currentSession);
            instance.Begin();
            try {
                action.run();
                if (getResponseContext().hasErrorMessage()) {
                    instance.SetAbort();
                } else {
                    instance.SetComplete();
                }
            } catch (Throwable e) {
                try {
                    instance.SetAbort();
                } catch (Throwable ee) {
                    log.error("bef内部错误:", ee);
                }
                throw e;
            }
        } finally {
            FuncSessionManager.getCurrent().endEdit(editToken);
        }
    }

    /**
     * 获取模型相关信息
     *
     * @return 模型信息类
     */
    @Override
    public ModelResInfo getModelInfo() {
        return tryGetBeManager().getModelInfo();
    }

    protected void addExtend(IBEManagerExtend ext) {
        Objects.requireNonNull(ext, "ext");

        if (extList == null) {
            extList = new ArrayList<>();
        }
        extList.add(ext);
    }
    //endregion

    @Override
    public Object executeCustomAction(String actionCode, Object... pars) {
        ActionUtil.requireNonNull(actionCode, "actionCode动作编号");
        Method[] methods = getClass().getMethods();
        Method method = null;
        for (Method item : methods) {
            if (item.getName().equals(actionCode))
                method = item;
        }
        if (method == null) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5083, actionCode);
        }
        try {
            return method.invoke(this, pars);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new BefRunTimeException(e);
        }
    }

    //region 全量接口
    @Override
    public IEntityData retrieveDefault(IEntityData data) {
        Objects.requireNonNull(data, "data");

        return getBEManager().retrieveDefault(data);
    }

    @Override
    public void modify(IEntityData data) {
        Objects.requireNonNull(data, "data");

        getBEManager().modify(data);
    }

    @Override
    public BefTempTableContext createTempTable(List<String> nodeCodes) {
        return getBEManager().createTempTable(nodeCodes);
    }

    @Override
    public void dropTempTable(BefTempTableContext tempTableContext) {
        getBEManager().dropTempTable(tempTableContext);
    }

    @Override
    public Object createPropertyDefaultValue(String nodeCode, String propertyName) {
        return getBEManager().createPropertyDefaultValue(nodeCode, propertyName);
    }

    @Override
    public final String serializeDataForExpression(IEntityData iEntityData) {
        return getBEManager().serializeDataForExpression(iEntityData);
    }

    //region tcc
    public void tccCancel(BusinessActionContext tccContext, BefTccParamItem item)
            throws SQLException {
        ((BEManager) getBEManager()).tccCancel(tccContext, item);
    }

    public void tccConfirm(BusinessActionContext tccContext, BefTccParamItem item)
            throws SQLException {
        ((BEManager) getBEManager()).tccConfirm(tccContext, item);
    }
    //endregion


    @Override
    public Object generateDataId(String nodeCode) {
        return getBEManager().generateDataId(nodeCode);
    }

    @Override
    public DataTypePropertyInfo getDataTypePropertyInfo(String fieldCode, String nodeCode){
        // 获取基础资源
        CefModelResInfoImpl modelResInfo = (CefModelResInfoImpl)this.getModelInfo();

        // 若未指定节点，则默认为根节点
        if (StringUtils.isEmpty(nodeCode)) {
            nodeCode = modelResInfo.getRootNodeCode();
        }
        // 预处理节点和字段为小写
        nodeCode = StringUtils.lowerCase(nodeCode);
        fieldCode = StringUtils.lowerCase(fieldCode);

        CefEntityResInfoImpl entityResInfo = (CefEntityResInfoImpl)modelResInfo.getEntityResInfos().get(nodeCode);
        if (entityResInfo != null) {
            // 获取字段类型
            CefEntityTypeInfo entityTypeInfo = entityResInfo.getEntityTypeInfo();
            // 变更集JSON中字段编号与实际VO上大小写不一致，需要用小写的寻找
            DataTypePropertyInfo basePropertyInfo = entityTypeInfo.getPropertyInfoByLower(fieldCode);
            if (basePropertyInfo != null) {
                return basePropertyInfo;
            }
        }

        if (this.getExtends() == null) {
            return null;
        }
        for (IBEManagerExtend iBffManagerExtend : this.getExtends()) {
            CefModelResInfoImpl extModelResInfo = (CefModelResInfoImpl)iBffManagerExtend.getModelInfo();
            // 没有该表实体，或没有该字段，在扩展中寻找
            if (extModelResInfo != null) {
                CefEntityResInfoImpl extEntityResInfo = (CefEntityResInfoImpl) extModelResInfo.getEntityResInfos().get(nodeCode);
                // 获取字段类型
                CefEntityTypeInfo extEntityTypeInfo = extEntityResInfo.getEntityTypeInfo();
                // 变更集JSON中字段编号与实际VO上大小写不一致，需要用小写的寻找
                DataTypePropertyInfo extPropertyInfo = extEntityTypeInfo.getPropertyInfoByLower(fieldCode);
                if (extPropertyInfo != null) {
                    return extPropertyInfo;
                }
            }
        }
        return null;
    }
}
