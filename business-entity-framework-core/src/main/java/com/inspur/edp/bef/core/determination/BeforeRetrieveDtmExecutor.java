/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.FieldsFilter;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

import javax.swing.text.html.parser.Entity;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class BeforeRetrieveDtmExecutor {
    private IEntityRTDtmAssembler assembler;
    private IBEManagerContext mgrContext;

    public BeforeRetrieveDtmExecutor() {

    }

    public final void execute(IEntityRTDtmAssembler assembler, IBEManagerContext mgrContext, List<String> dataIds, RetrieveParam retrieveParam) {
        List<IDetermination> dtms = assembler.getDeterminations();
        if (dtms == null) {
            return;
        }

        for (IDetermination dtm : dtms) {
            if (!dtm.canExecute(null)) {
                continue;
            }
            BeforeRetrieveDtmContext context = new BeforeRetrieveDtmContext(mgrContext, Collections.unmodifiableList(dataIds), retrieveParam);
            dtm.execute(context, null);
            if (context.getNodeFilters() != null && context.getNodeFilters().size() > 0) {
                mergeNodeFiltersToRetrieveParam(context.getNodeFilters(), retrieveParam);
            }
        }
    }

    private void mergeNodeFiltersToRetrieveParam(Map<String, EntityFilter> nodeFilters, RetrieveParam retrieveParam) {
        if (nodeFilters == null || nodeFilters.isEmpty())
            return;
        for (Map.Entry<String, EntityFilter> entry : nodeFilters.entrySet()) {
            if (!retrieveParam.getRetrieveFilter().getNodeFilters().containsKey(entry.getKey()))
                retrieveParam.getRetrieveFilter().getNodeFilters().put(entry.getKey(), entry.getValue());
            else {
                mergeEntityFilter(entry.getValue(), retrieveParam.getRetrieveFilter().getNodeFilters().get(entry.getKey()));
            }
        }
    }

    private void mergeEntityFilter(EntityFilter source, EntityFilter target) {
        if (source == null)
            return;
        if (target == null) {
            return;
        }
        target.setFieldsFilter(FieldsFilter.mergeFieldsFilter(source.getFieldsFilter(), target.getFieldsFilter()));
    }
}
