/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;


import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;

import java.util.ArrayList;
import java.util.List;

public class RetrieveManagerAction extends AbstractManagerAction<RespectiveRetrieveResult> {

    //region Consturctor

    private String dataID;


    //endregion Consturctor


    //region 字段属性
    private RetrieveParam para;
    public RetrieveManagerAction(IBEManagerContext managerContext, String id, RetrieveParam para) {
        super(managerContext);
        ActionUtil.requireNonNull(id, "id");
        this.dataID = id;
        this.para = para == null ? new RetrieveParam() : para;
    }
    //internal RetrieveResult RetrieveResult { get; private set; }

    //endregion 字段属性


    //region Override

    @Override
    public final void execute() {
        getBEManagerContext().checkAuthority("Retrieve");
//		AuthorityUtil.checkAuthority("Retrieve");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Retrieve");
        List<String> dataIds = new ArrayList<>();
        dataIds.add(dataID);
        RetrieveUtils.executeBeforeRetrieve(getBEManagerContext(), dataIds, para);
        IBusinessEntity be = getBEManagerContext().getEntity(dataID, para.getAddDataToCache());
        setResult(be.retrieve(para));
        if (!para.getAddDataToCache() && !((BEContext) be.getBEContext()).hasChange()) {
            ((CoreBEContext) be.getBEContext()).clearData();
        }
        //SetRetrieveResult(com.inspur.edp.bef.core.be);
    }

    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory().getRetrieveMgrActionAssembler(getBEManagerContext(), para);
    }


    //endregion Override


    //region private

    //private void setRetrieveResult(IBusinessEntity com.inspur.edp.bef.core.be)
    //{
    //	RetrieveResult = new RetrieveResult();
    //	RetrieveResult.DicAccessor.add(com.inspur.edp.bef.core.be.BEContext.ID, com.inspur.edp.bef.core.be.BEContext.CurrentData);
    //          if (!com.inspur.edp.bef.core.be.BEContext.isLocked())
    //              RetrieveResult.LockFailedIds.add(com.inspur.edp.bef.core.be.ID);
    //}

    //endregion
}
