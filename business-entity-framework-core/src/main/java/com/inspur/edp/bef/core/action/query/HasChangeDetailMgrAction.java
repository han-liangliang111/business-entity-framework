package com.inspur.edp.bef.core.action.query;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.condition.EntityFilter;

import java.util.List;

public class HasChangeDetailMgrAction extends AbstractManagerAction<Boolean> {

    BEManagerContext beManagerContext;

    public HasChangeDetailMgrAction(BEManagerContext beManagerContext) {
        this.beManagerContext = beManagerContext;
        super.isReadOnly = true;
    }

    @Override
    public void execute() {
        if (beManagerContext.getAllEntities() != null && beManagerContext.getAllEntities().size() > 0) {
            for (IBusinessEntity entity : beManagerContext.getAllEntities()) {
                if (((BEContext) entity.getBEContext()).hasChange()) {
                    setResult(true);
                    return;
                }
            }
        }
        setResult(false);
    }
}
