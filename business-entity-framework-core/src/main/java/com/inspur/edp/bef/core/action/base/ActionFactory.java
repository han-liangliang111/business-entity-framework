/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.clear.ClearBufferAction;
import com.inspur.edp.bef.core.action.clear.ClearChangesetAction;
import com.inspur.edp.bef.core.action.clear.ClearLockAction;
import com.inspur.edp.bef.core.action.delete.DeleteAction;
import com.inspur.edp.bef.core.action.delete.DeleteChildAction;
import com.inspur.edp.bef.core.action.determination.OnCancelDtmAction;
import com.inspur.edp.bef.core.action.determination.AfterLoadingDtmAction;
import com.inspur.edp.bef.core.action.modify.ModifyAction;
import com.inspur.edp.bef.core.action.retrieve.AddLockAction;
import com.inspur.edp.bef.core.action.retrieve.EditAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveChildAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveChildAndChildAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveWithScopeAction;
import com.inspur.edp.bef.core.action.retrievedefault.*;
import com.inspur.edp.bef.core.action.save.SaveAction;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.extend.entity.ICefEntityExtend;

import java.util.List;
import java.util.ArrayList;

public class ActionFactory {
    public final RetrieveWithScopeAction getRetrieveWithScopeAction(
            IBEContext beContext, RetrieveParam para) {
        return new RetrieveWithScopeAction(beContext, para);
    }

    public final RetrieveAction getRetrieveAction(IBEContext beContext, RetrieveParam para) {
        return new RetrieveAction(beContext, para);
    }

    public final EditAction getEditAction(IBEContext beContext) {
        return new EditAction(beContext);
    }

    public final RetrieveChildAction getRetrieveChildAction(
            java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, RetrieveParam par) {
        return new RetrieveChildAction(nodeCodes, hierachyIdList, par);
    }

    public final RetrieveChildAndChildAction getRetrieveChildAndChildAction(
            java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList,
            ArrayList<String> ids, RetrieveParam par) {
        return new RetrieveChildAndChildAction(nodeCodes, hierachyIdList, ids, par);
    }

    public final AddLockAction getAddLockAction() {
        return new AddLockAction();
    }

    public final ModifyAction getModifyAction(IBEContext beContext, IChangeDetail changeDetail) {
        return new ModifyAction(beContext, changeDetail);
    }

    // internal BeforeCheckDtmAction GetBeforeCheckDtmAction(CoreDeterminContext context)
    // {
    //    return new BeforeCheckDtmAction(context);
    // }

    // internal RetrieveDefaultDtmAction GetRetrieveDefaultDtmAction(ICoreDeterminContext context)
    // {
    //    return new RetrieveDefaultDtmAction(context);
    // }

    // internal RetrieveDefaultChildDtmAction GetRetrieveDefaultChildDtmAction(IDtmExecutorContext
    // context,
    //    IList<string> nodeCodes, IEntityData entityData, AddChangeDetail changeDetail)
    // {
    //    return new RetrieveDefaultChildDtmAction(context, nodeCodes, entityData, changeDetail);
    // }

    // internal AfterModifyDtmAction GetAfterModifyDtmAction(ICoreDeterminContext context)
    // {
    //    return new AfterModifyDtmAction(context);
    // }

    public final AfterLoadingDtmAction getAfterLoadingDtmAction(IBENodeEntityContext context, List<ICefEntityExtend> extList) {
        return new AfterLoadingDtmAction(context, extList);
    }

    public final OnCancelDtmAction getOnCancelDtmAction(IBENodeEntityContext context) {
        return new OnCancelDtmAction(context);
    }

    // internal BeforeSaveValidationAction GetValidationAction(IBEContext beContext)
    // {
    //    return new BeforeSaveValidationAction(beContext);
    // }

    public final SaveAction getSaveAction() {
        return new SaveAction();
    }


    public final RetrieveDefaultAction getRetrieveDefaultAction(
            java.util.Map<String, Object> defaultValues) {
        return new RetrieveDefaultAction(defaultValues);
    }

    public final FullRetrieveDefaultAction getFullRetrieveDefaultAction(IBEContext bEContext,
                                                                        IEntityData data) {
        return new FullRetrieveDefaultAction(bEContext, data);
    }

    public final CloneAction getCloneAction(String cloneDataID, CloneParameter cloneParameter) {
        return getCloneAction(cloneDataID, "", cloneParameter);
    }

    public final CloneAction getCloneAction(String cloneDataID, String dataId, CloneParameter cloneParameter) {
        return new CloneAction(cloneDataID, dataId, cloneParameter);
    }

    // TODO:[cef] 所有的beContext参数都可以去掉, 有时间确认一下是否影响已生成的dll
    public final ClearChangesetAction getClearChangesetAction() {
        return new ClearChangesetAction();
    }

    public final ClearBufferAction getClearBufferAction() {
        return new ClearBufferAction();
    }

    public final ClearLockAction getClearLockAction(IBEContext beContext) {
        return new ClearLockAction(beContext);
    }

    public final CreateObjectDataAction getCreateObjectDataAction(IBEContext beContext) {
        return new CreateObjectDataAction(beContext);
    }

    public final CreateChildDataAction getCreateChildDataAction(
            IBEContext bEContext, String childCode, String dataID) {
        return new CreateChildDataAction(bEContext, childCode, dataID);
    }

    public final RetrieveDefaultChildAction getRetrieveDefaultChildAction(
            java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, String dataID,
            java.util.Map<String, Object> values) {
        return new RetrieveDefaultChildAction(nodeCodes, hierachyIdList, dataID, values);
    }

    public final DeleteAction getDeleteAction() {
        return new DeleteAction();
    }

    //	public final AdjustNumberAction getAdjustNumberAction(AdjustNumberActionContext context)
    //	{
    //		return new AdjustNumberAction(context);
    //	}

    public final DeleteChildAction getDeleteChildAction(
            java.util.List<String> nodeCodes,
            java.util.List<String> hierachyIdList,
            java.util.List<String> ids) {
        return new DeleteChildAction(nodeCodes, hierachyIdList, ids);
    }
}
