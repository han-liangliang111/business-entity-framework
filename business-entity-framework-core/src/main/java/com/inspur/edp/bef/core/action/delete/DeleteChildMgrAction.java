/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.delete;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;

import java.util.ArrayList;
import java.util.List;

public class DeleteChildMgrAction extends AbstractManagerAction<VoidActionResult> {
    private java.util.List<String> nodeCodes;
    private java.util.List<String> hierachyIdList;
    private java.util.List<String> ids;

    public DeleteChildMgrAction(BEManagerContext managerContext, List<String> nodeCodes,
                                List<String> hierachyIdList, List<String> ids) {
        ActionUtil.requireNonNull(nodeCodes, "nodeCodes");
        ActionUtil.requireNonNull(hierachyIdList, "hierachyIdList");
        ActionUtil.requireNonNull(ids, "ids");

        this.nodeCodes = nodeCodes;
        this.hierachyIdList = hierachyIdList;
        this.ids = ids;
    }

    @Override
    public final void execute() {
        getBEManagerContext().checkAuthority("Modify");
//		AuthorityUtil.checkAuthority("Modify");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Modify");
        IBusinessEntity be = getBEManagerContext().getEntity(hierachyIdList.get(0));
        RetrieveParam tempVar = new RetrieveParam();
        tempVar.setNeedLock(true);
        //拉取数据到缓存中
        RespectiveRetrieveResult retrieveRez = be.retrieve(tempVar);
        if (be.getBEContext().getCurrentData() == null) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5005, null, ExceptionLevel.Error); //TODO: 使用统一的消息机制
        }
        LockUtils.checkLocked(be.getBEContext());
        be.deleteChild(nodeCodes, hierachyIdList, ids);

        DeleteChecker deleteChecker = new DeleteChecker();
        List<IBusinessEntity> entityList = new ArrayList<>();
        entityList.add(be);
        deleteChecker.check(entityList, ((BEManager)getBEManagerContext().getBEManager()).getResponseContext());
    }

    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory().getDeleteChildMgrActionAssembler(getBEManagerContext(), nodeCodes, hierachyIdList, ids);
    }

    //internal override void DemandPermissionCore()
    //{
    //    ModifyMgrAction.demandFuncPermission(BEManagerContext);
    //}
}
