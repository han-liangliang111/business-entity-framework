/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save.saveEvent;

public enum SaveEventType {
    BeforeSave(0),
    AfterSave(1),
    SaveFailed(2);

    private static java.util.HashMap<Integer, SaveEventType> mappings;
    private int intValue;

    private SaveEventType(int value) {
        intValue = value;
        SaveEventType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, SaveEventType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, SaveEventType>();
        }
        return mappings;
    }

    public static SaveEventType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
