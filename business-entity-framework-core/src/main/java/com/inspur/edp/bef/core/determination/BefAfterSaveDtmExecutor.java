/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class BefAfterSaveDtmExecutor extends EntityDeterminationExecutor {

    private IChangeDetail change;
    private CoreBEContext rootContext;
    // endregion

    // region Ctor
    public BefAfterSaveDtmExecutor(
            IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx, IChangeDetail change) {
        super(assembler, entityCtx);
    }

    // region Execute
    protected IBENodeEntityContext getBEContext() {
        return (IBENodeEntityContext) super.getEntityContext();
    }

    @Override
    public void execute() {
        setContext(GetContext());
        setChange(getChangeset());
        if ((getChange() == null)) {
            return;
        }
        executeBelongingDeterminations();
        executeDeterminations();
        executeChildDeterminations();
        getRootContext().mergeIntoInnerChange();
        getRootContext().acceptListenerChange();

    }

    protected IChangeDetail getChangeset() {
        IChangeDetail change = getAssembler().getChangeset(getEntityContext());
        if (change == null)
            return null;
        return change;
    }
    @Override
    protected ICefDeterminationContext GetContext() {
        return new BeforeSaveDtmContext((IBENodeEntityContext) getEntityContext());
    }

    public final CoreBEContext getRootContext() {
        if (rootContext == null) {
            IBENodeEntityContext current = getBEContext();
            while (((CoreBEContext) ((current instanceof CoreBEContext) ? current : null)) == null
                    || current == null) {
                if (current != null) {
                    current = current.getParentContext();
                }
            }
            rootContext = (CoreBEContext) current;
        }
        return rootContext;
    }

    private boolean isRoot() {
        return getBEContext() instanceof CoreBEContext;
    }
    // endregion
}
