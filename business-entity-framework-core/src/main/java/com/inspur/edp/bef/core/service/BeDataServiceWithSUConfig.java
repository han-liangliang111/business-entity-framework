//package com.inspur.edp.bef.core.service;
//
//import com.inspur.edp.bef.api.be.BeDataServiceWithSU;
//import com.inspur.edp.bef.core.be.BESUDataServiceWithSUImpl;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration(proxyBeanMethods = false)
//public class BeDataServiceWithSUConfig {
//
//    @Bean
//    public BeDataServiceWithSU beDataServiceWithSU() {
//        return new BESUDataServiceWithSUImpl();
//    }
//}
