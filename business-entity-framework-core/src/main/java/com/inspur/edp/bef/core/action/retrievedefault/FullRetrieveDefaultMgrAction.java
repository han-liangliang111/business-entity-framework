/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.changeset.InnerUtil;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.entity.entity.IValueObjData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

public class FullRetrieveDefaultMgrAction extends AbstractManagerAction<IEntityData> {

    private final IEntityData data;
    private final BEManager manager;
    private BusinessEntity businessEntity;

    public FullRetrieveDefaultMgrAction(BEManager manager, IEntityData data) {
        super(manager.getBEManagerContext());
        this.data = data;
        this.manager = manager;
    }

    @Override
    public void execute() {
        String dataID = StringUtils.isEmpty(data.getID()) ? UUID.randomUUID().toString() : data.getID();
        businessEntity = (BusinessEntity) manager.getEntity(dataID);
        businessEntity.retrieveDefault(data);
        setResult(businessEntity.getBEContext().getData());
    }
}
