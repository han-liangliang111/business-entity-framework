//package com.inspur.edp.bef.core.action.save;
//
//import com.inspur.edp.bef.core.session.FuncSessionManager;
//import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
//
//public class TransactionManager
//{
//
//		//region 单例模式构造函数
//	private TransactionManager()
//	{
//	}
//
//	public static TransactionManager getInstance()
//	{
//		TransactionManager transactionManger = FuncSessionManager.getCurrentSession().getTransactionManager();
//		if (transactionManger == null)
//		{
//			FuncSessionManager.getCurrentSession().setTransactionManager((transactionManger = new TransactionManager()));
//		}
//		return transactionManger;
//	}
//
//		//endregion
//
//
//		//region 属性
//
//	/**
//	 是否是根事务
//	 根事务就是由当前TM创建事务边界.
//	 根事务才有权提交整个事务
//
//	 @return
//	*/
//	public final boolean isRootTrans()
//	{
//		return true;
//	}
//
//	/**
//	 当前是否已经存在环境事务
//
//	*/
//	public final boolean getHasAmbientTransaction()
//	{
//			//TODo【迭代9】 应该从环境事务中获取,为了编译通过加入的这个属性
//		return getTransaction() != null;
//	}
//
//	private IGSPTransaction privateTransaction;
//	private IGSPTransaction getTransaction()
//	{
//		return privateTransaction;
//	}
//	private void setTransaction(IGSPTransaction value)
//	{
//		privateTransaction = value;
//	}
//
//		//endregion
//
//
//		//region 方法
//	//TODO:caf 事务
//	/**
//	 创建事务
//
//	*/
//	public final void createTransaction()
//	{
//		//Transaction = GSPTransaction.getTransaction(TransactionType.VirtualTransaction);
//		//Transaction.beginTransaction();
//	}
//	/**
//	 事务结束,当前事务是依赖事务,
//	 需要通知事务中心此操作已经结束,可以等待最终的提交动作
//
//	*/
//	public final void complete()
//	{
//		//CheckTransaction();
//		//Transaction.setComplete();
//	}
//	/**
//	 事务创建者才有权提交事务
//
//	*/
//	public final void commit()
//	{
//		//CheckTransaction();
//		//Transaction.setComplete();
//		//Transaction = null;
//	}
//	/**
//	 回滚事务
//
//	*/
//	public final void rollBack()
//	{
//		//CheckTransaction();
//		//Transaction.setAbort();
//	}
//
//	//验证当前事务是有效的
//	private void checkTransaction()
//	{
//		DataValidator.checkForNullReference(getTransaction(), "Transaction");
//	}
//
//		//endregion
//}