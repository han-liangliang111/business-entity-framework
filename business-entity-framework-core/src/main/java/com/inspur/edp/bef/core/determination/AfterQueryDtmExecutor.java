/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.spi.event.queryactionevent.BefQueryEventBroker;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class AfterQueryDtmExecutor extends EntityDeterminationExecutor {

    // region Ctor

    private IBEManager beManager;

    // private readonly IBEContext entityCtx;
    public AfterQueryDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx) {
        super(assembler, entityCtx);
    }

    public AfterQueryDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx, IBEManager beManager) {
        super(assembler, entityCtx);
        this.beManager = beManager;
        //this.entityCtx = entityCtx;
    }

    private IBENodeEntityContext innerGetEntityContext() {
        return (IBENodeEntityContext) super.getEntityContext();
    }
    // endregion

    // region Execute
    @Override
    public void execute() {
        List<IDetermination> dtms = getAssembler().getDeterminations();
        int count = 0;
        if (dtms == null || dtms.size() == 0) {
            return;
        }

        ICefDeterminationContext context = this.GetContext();
        try {

            BefQueryEventBroker.fireBeforeDataCXHDeterminate(beManager, context);
            for (IDetermination dtm : dtms) {
                if (!dtm.canExecute(null)) {
                    continue;
                }
                if(log.isInfoEnabled()){
                    //默认都会增加密级的查询后。
                    log.info("开始执行数据:[{}]的BE查询后联动计算,联动计算名称:{}",  ((IDeterminationContext)context).getCurrentData().getID(), dtm.getName());
                }
                count++;
                try {
                    BefQueryEventBroker.firebeforeCXHDeterminate(beManager, context);
                    dtm.execute(context, null);
                    BefQueryEventBroker.fireafterCXHDeterminate(beManager, context);
                } catch (Exception e) {
                    BefQueryEventBroker.fireQueryUnnormalStop(e);
                    throw new BefRunTimeException(e);
                }
            }
            BefQueryEventBroker.fireAfterDataCXHDeterminate(beManager, context, count);
        } catch (Exception e) {
            BefQueryEventBroker.fireQueryUnnormalStop(e);
            throw new BefRunTimeException(e);
        }
    }

    // endregion
}
