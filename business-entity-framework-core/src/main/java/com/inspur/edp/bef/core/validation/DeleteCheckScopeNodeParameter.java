package com.inspur.edp.bef.core.validation;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.spi.scope.AbstractCefScopeNodeParameter;
import lombok.Getter;

public class DeleteCheckScopeNodeParameter extends AbstractCefScopeNodeParameter {

    @Getter
    private final String source;
    @Getter
    private final IValidationContext context;

    public DeleteCheckScopeNodeParameter(IValidationContext context, String source) {
        this.context = context;
        this.source = source;
    }

    @Override
    public String getParameterType() {
        return "PfCommon_Bef_DeleteCheck";
    }

    @Override
    public boolean isMergableCrossType() {
        return true;
    }
}
