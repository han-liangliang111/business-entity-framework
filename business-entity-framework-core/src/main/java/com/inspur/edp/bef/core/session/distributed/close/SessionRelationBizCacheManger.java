/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed.close;

import com.inspur.edp.bef.core.session.FuncSessionUtil;
import com.inspur.edp.caf.cef.schema.base.utils.JsonUtil;
import io.iec.edp.caf.boot.context.CAFContext;

import io.iec.edp.caf.caching.api.CacheManager;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.caching.api.Cache;
import io.iec.edp.caf.caching.setting.LayeringCacheSetting;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class SessionRelationBizCacheManger {

    private static final String cacheNameInBiz = "BefSessionRelationInBiz";
    private static volatile SessionRelationBizCacheManger Instance = new SessionRelationBizCacheManger();
    private static Object createCacheLock = new Object();
    private volatile Cache cacheInBiz;

    public static SessionRelationBizCacheManger getInstance() {
        return Instance;
    }

    public Cache getCache() {
        if (cacheInBiz == null) {
            synchronized (createCacheLock) {

                if (cacheInBiz == null) {
                    cacheInBiz = createCache();
                }
            }
        }
        return cacheInBiz;
    }

    private Cache createCache() {
        CacheManager mgr = SpringBeanUtils.getBean(CacheManager.class);
        LayeringCacheSetting setting = new LayeringCacheSetting.Builder().disableFirstCache()
                .enableSecondCache().secondCacheTimeUnit(TimeUnit.HOURS)
                .depict(cacheNameInBiz).build();
        return mgr.getCache(cacheNameInBiz, setting);
    }

    public List<String> getSessionRelation(String tokenId) {
        Objects.requireNonNull(tokenId);
        return getCache().getHashValue("BefSessionRelationInBiz", tokenId, List.class);
    }

    public void setSessionRelation(String tokenId, String sessionId, String suCode) {
        Objects.requireNonNull(tokenId);
        List<String> tem = getSessionRelation(tokenId);
        if (tem == null) {
            tem = new ArrayList<>();
        }
        tem.add(FuncSessionUtil.createCombinedId(sessionId, suCode));
        getCache().putAsHash("BefSessionRelationInBiz", tokenId, tem);
    }

    public void removeSessionRelation(String tokenId, String sessionId, String suCode) {
        Objects.requireNonNull(tokenId);
        List<String> tem = getSessionRelation(tokenId);
        if (tem == null || tem.isEmpty()) {
            return;
        }
        tem.remove(FuncSessionUtil.createCombinedId(sessionId, suCode));
        getCache().putAsHash("BefSessionRelationInBiz", tokenId, tem);
    }

    public void evict(String key) {
        Objects.requireNonNull(key);
        getCache().deleteHashKey("BefSessionRelationInBiz", key);
    }

    public Set<String> getHashKeySet() {
        return (Set) getCache().getHashKeys("BefSessionRelationInBiz");
    }

}
