/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.core.scope.ScopeNodeParameter;

import java.util.function.Consumer;

public class AddLockScopeNodeParameter extends ScopeNodeParameter {

    //region 属性字段
    private Consumer<IBEContext> action;

    //region Constructor
    public AddLockScopeNodeParameter(IBEContext context, Consumer<IBEContext> action) {
        this(context);
        this.action = action;
    }


    //endregion Constructor


    private AddLockScopeNodeParameter(IBEContext context) {
        super(context);
    }

    @Override
    public String getParameterType() {
        return "PlatformCommon_Bef_AddLockScopeNodeParameter";
    }

    //endregion


    public final void executeAction(IBEContext beContext) {
        action.accept(beContext);
    }

}
