/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.core.determination.builtinimpls.predicate.ExecutePredicate;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class BefAftModifyPredicateDtmAdaptor extends BefBaseDtmAdaptor {

    private final ExecutePredicate predicate;
    private final boolean runOnce;
    private String runOnceKey;

    //region 第一版构造函数
    public BefAftModifyPredicateDtmAdaptor(String name, Class type, ExecutePredicate predicate,
                                           boolean runOnce) {
        super(name, type);
        Objects.requireNonNull(predicate, "predicate");
        this.predicate = predicate;
        this.runOnce = runOnce;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return predicate.test(change);
    }

    @Override
    public void execute(ICefDeterminationContext ctx, IChangeDetail change) {
        if (runOnce) {
            IBefCallContext callContext = ((IDeterminationContext) ctx).getBEContext().getCallContext();
            String dataId = ((IDeterminationContext) ctx).getBENodeEntity().getID();
            if (callContext.containsKey(getRunOnceKey(dataId)))
                return;
            callContext.setData(getRunOnceKey(dataId), null);
        }
        super.execute(ctx, change);
    }

    private String getRunOnceKey(String dataId) {
        if (runOnceKey == null) {
            runOnceKey = UUID.randomUUID().toString();
        }
        return runOnceKey.concat(dataId);
    }
}
