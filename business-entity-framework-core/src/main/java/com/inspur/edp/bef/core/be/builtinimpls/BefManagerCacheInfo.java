/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.be.builtinimpls;

import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;

public class BefManagerCacheInfo {

    private IEntityRTDtmAssembler b4RetrieveDtmAssembler;
    private IEntityRTDtmAssembler b4QueryDtmAssembler;
    private ModelResInfo modelResInfo;

    public IEntityRTDtmAssembler getB4RetrieveDtmAssembler() {
        return b4RetrieveDtmAssembler;
    }

    public void setB4RetrieveDtmAssembler(
            IEntityRTDtmAssembler b4RetrieveDtmAssembler) {
        this.b4RetrieveDtmAssembler = b4RetrieveDtmAssembler;
    }

    public IEntityRTDtmAssembler getB4QueryDtmAssembler() {
        return b4QueryDtmAssembler;
    }

    public void setB4QueryDtmAssembler(
            IEntityRTDtmAssembler b4QueryDtmAssembler) {
        this.b4QueryDtmAssembler = b4QueryDtmAssembler;
    }

    public ModelResInfo getModelResInfo() {
        return modelResInfo;
    }

    public void setModelResInfo(ModelResInfo modelResInfo) {
        this.modelResInfo = modelResInfo;
    }
}
