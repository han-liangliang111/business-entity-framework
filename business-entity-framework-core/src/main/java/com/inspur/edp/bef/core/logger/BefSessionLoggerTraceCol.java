package com.inspur.edp.bef.core.logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Path("")
@Produces(javax.ws.rs.core.MediaType.APPLICATION_JSON)
public class BefSessionLoggerTraceCol {

    @Autowired
    private BefSessionLog befSessionLog;

    @Path("openloggertrace")
    @GET
    public String openLoggerTrace() {
        return befSessionLog.openLoggerTrace();
    }

    @Path("closeloggertrace")
    @GET
    public String closeLoggerTrace() {
        return befSessionLog.closeLoggerTrace();
    }

    @Path("openstacktrace")
    @GET
    public String openStackTrace() {
        return befSessionLog.openStackTrace();
    }

    @Path("closestacktrace")
    @GET
    public String closeStackTrace() {
        return befSessionLog.closeStackTrace();
    }

    @Path("enablesetRedisinfo")
    @GET
    public String enableSetRedisInfo() {
        return befSessionLog.enableSetRedisInfo();
    }

    @Path("disenablesetredisinfo")
    @GET
    public String disenableSetRedisInfo() {
        return befSessionLog.disEnableSetRedisInfo();
    }

    @Path("getsessiontraceinfo")
    @GET
    @Produces("application/json")
    //@GetMapping("/getsessiontraceinfo/filterParam")
    public List<Map<String, Object>> getSessionTraceInfo(
            @QueryParam(value = "minutes") int minutes,
            @QueryParam(value = "maxCount") int maxCount,
            @QueryParam(value = "filePath") String filePath) {
        if (filePath == null) {
            filePath = "";
        }
        FilterParam filterParam = new FilterParam(minutes, maxCount, filePath);
        return befSessionLog.getSessionTraceInfo(filterParam);
    }
//    @Path("getSessionTraceInfoByTime")
//    @GET
//    public ArrayList<Map<String,Object>> getSessionTraceInfoByTime(@QueryParam("minutes") int minutes){
//        return befSessionLog.getSessionTraceInfoByTime(minutes);
//    }


}
