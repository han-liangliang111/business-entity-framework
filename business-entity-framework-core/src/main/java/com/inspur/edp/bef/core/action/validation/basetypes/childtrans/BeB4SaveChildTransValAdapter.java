/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.validation.basetypes.childtrans;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public final class BeB4SaveChildTransValAdapter extends BeChildTransValAdapter {

    public BeB4SaveChildTransValAdapter(String name, String childCode) {
        super(name, childCode);
    }

    @Override
    protected void doExecute(IValidationContext compContext, IBENodeEntity itemEntity) {
        itemEntity.beforeSaveValidate();

    }
}
