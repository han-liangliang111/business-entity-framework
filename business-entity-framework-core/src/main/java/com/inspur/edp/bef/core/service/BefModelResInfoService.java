package com.inspur.edp.bef.core.service;

import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.spi.manager.ModelResInfoService;
import com.inspur.edp.commonmodel.api.ICMManager;


/**
 * 模型国际化资源信息服务接口实现类
 * 仅为解耦cef-spi和bef-api，增加此接口，其内部转调IStandardLcp。
 */
public class BefModelResInfoService implements ModelResInfoService {
    private ILcpFactory lcpFactory = null;

    /**
     * 构造函数，注入ILcpFactory
     * @param lcpFactory
     */
    public BefModelResInfoService(ILcpFactory lcpFactory){
        this.lcpFactory = lcpFactory;
    }

    /**
     * 获取模型相关信息
     * @return 模型信息类
     */
    @Override
    public ModelResInfo getModelInfo(String configId) {
        IStandardLcp standardLcp = null;
        //通过ICMManager的方式，不需要BefSession
        ICMManager icmManager = lcpFactory.createCMManager(configId);
        if (icmManager!=null && icmManager instanceof IStandardLcp) {
            standardLcp = (IStandardLcp) icmManager;
        } else {
            //createLcp内部会check BefSession
            standardLcp = this.lcpFactory.createLcp(configId);
        }
        return standardLcp.getModelInfo();
    }
}
