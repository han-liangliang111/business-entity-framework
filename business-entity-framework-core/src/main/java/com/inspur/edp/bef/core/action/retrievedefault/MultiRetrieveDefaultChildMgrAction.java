/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.entity.IEntityData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.inspur.edp.commonmodel.core.session.sessionconfig.SessionConfigServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class MultiRetrieveDefaultChildMgrAction extends
        AbstractManagerAction<Map<String, IEntityData>> {

    //region 字段属性
    private static final Logger logger = LoggerFactory.getLogger(MultiRetrieveDefaultChildMgrAction.class);
    private List<String> nodeCodes;
    private List<String> hierachyIdList;
    private List<String> dataIds;

    //endregion

    //region Consturctor

    public MultiRetrieveDefaultChildMgrAction(IBEManagerContext managerContext,
                                              List<String> nodeCodes, List<String> hierachyIdList, List<String> dataIds) {
        ActionUtil.requireNonNull(nodeCodes, "nodeCodes");
        ActionUtil.requireNonNull(hierachyIdList, "hierachyIdList");
        ActionUtil.requireNonNull(dataIds, "dataIds");

        this.nodeCodes = nodeCodes;
        this.hierachyIdList = hierachyIdList;
        this.dataIds = dataIds;
    }

    //endregion

    //region execute

    @Override
    public final void execute() {
        getBEManagerContext().checkAuthority("Modify");
        IBusinessEntity be = getBEManagerContext().getEntity(hierachyIdList.get(0));

        RetrieveParam tempVar = new RetrieveParam();
        tempVar.setNeedLock(true);
        if (be.retrieve(tempVar).getData() == null) {
            logger.error("获取主表数据报错：be类型: {}, befsessionId{}, dataId:{}",be.getBEType(), FuncSessionManager.getCurrentFuncSessionId(), be.getID());
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5048, hierachyIdList.get(0));
        }
        LockUtils.checkLocked(be.getBEContext());
        Map<String, IEntityData> result = new HashMap<>(dataIds.size(), 1);
        for (String id : dataIds) {
            if (StringUtils.isEmpty(id)) {
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5049);
            }
            IEntityData curr = be.retrieveDefaultChild(nodeCodes, hierachyIdList, id);
            if (result.put(id, curr) != null) {
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5050, id);
            }
        }
        setResult(result);
    }

    //TODO:
    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory()
                .getRetrieveDefaultChildMgrActionAssembler(getBEManagerContext(), nodeCodes,
                        hierachyIdList);
    }
    //endregion
}
