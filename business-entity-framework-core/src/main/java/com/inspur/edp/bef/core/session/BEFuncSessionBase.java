/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.api.be.BufferClearingType;
import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.lcp.ResponseContext;
import com.inspur.edp.bef.core.action.authorityinfo.BefAuthorityInfo;
import com.inspur.edp.bef.core.action.base.BefCallContext;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BeBufferChangeManager;
import com.inspur.edp.bef.core.be.BizEntityCacheManager;
import com.inspur.edp.bef.core.be.IBeBufferChangeManager;
import com.inspur.edp.bef.core.lock.LockServiceItem;
import com.inspur.edp.bef.core.scope.BefScopeManager;
import com.inspur.edp.bef.core.session.transaction.IBefTransactionItem;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.spi.auth.DataPermissionCache;
import com.inspur.edp.bef.spi.auth.DataPermissionController;
import com.inspur.edp.cef.api.buffer.ICefBuffer;
import com.inspur.edp.cef.api.changeListener.EntityDataChangeListener;
import com.inspur.edp.cef.api.dataType.entity.ICefRootEntity;
import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.variable.api.manager.IVariableManager;
import com.inspur.edp.commonmodel.core.session.distributed.DistributedSessionPart;
import com.inspur.edp.commonmodel.core.session.distributed.RootEditToken;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;
import com.inspur.edp.commonmodel.core.variable.VarBufferManager;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public final class BEFuncSessionBase implements ICefSessionItem, IBefTransactionItem, Cloneable {

    public static final String Category = "BEFuncSessionBase";

    @Getter
    private final String configId;

    @Getter
    private final FuncSession funcSession;
    /**
     * 获取当前BEManager对应的CacheManager
     *
     * @return
     */
    public com.inspur.edp.bef.core.be.BizEntityCacheManager bizEntityCacheManager;
    @Getter
    private String basicConfigId;
    /**
     * 存储BE字典，[Key:DataId, Value:BizEntity]
     */
    private java.util.HashMap<String, ICefRootEntity> dicBE = new java.util.LinkedHashMap<>();
    private IBEManager beManager;
    @Getter
    @Setter
    private Object actionMark;
    private BufferClearingType privateInnerBufferClearingType = BufferClearingType.None;
    private ResponseContext privateResponseContext = new ResponseContext();
    private BefCallContext callContext;
    private LockServiceItem lockServiceItem;
    private com.inspur.edp.bef.core.be.IBeBufferChangeManager bufferChangeManager;
    private Optional<VarBufferManager> varBufferManager;
    private Map buffers = new ConcurrentHashMap<>();
    private Map changes = new ConcurrentHashMap<>();
    private Map<String, ICefRootEntity> rootEntities = new LinkedHashMap<>();
    private ConcurrentHashMap<Integer, ICefBuffer> variableBuffers = new ConcurrentHashMap<>();
    private java.util.HashMap<String, String> repositoryVariables = new java.util.HashMap<>();
    @Getter
    @Setter
    private DataPermissionController dataPermissionCtrl;
    @Getter
    @Setter
    private DataPermissionCache dataPermissionCache;
    private Map<String, BefAuthorityInfo> authorityInfos = new HashMap<>();
    private EntityDataChangeListener privateVarListener = EntityDataChangeListener.createInstance();
    // endregion
    private RootEditToken token;

    // region ICefSession

    public BEFuncSessionBase(FuncSession session, String type) {
        this(session, type, type);
    }

    public BEFuncSessionBase(FuncSession session, String type, String basicType) {
        this.funcSession = session;
        this.configId = type;
        this.basicConfigId = basicType;
    }

    // Scope相关，待定
    public static BefScopeManager getBefScopeManager() {
        throw new NotImplementedException();
    }

    protected java.util.HashMap<String, ICefRootEntity> getDicBE() {
        return dicBE;
    }

    public IBEManager getBEManager() {
        return beManager;
    }

    public final void setBEManager(IBEManager value) {
        // com.inspur.edp.bef.core.lcp.createData等方法内部走的是InnerCreateBEManager,
        // BizEntityCacheManager.BEManager为null,
        // 后来再调RetrieveDefault等方法内部走的CreateBEManager会给this.BeManager赋值,
        // 但BizEntityCacheManager.BEManager没有再给赋值了;
        // if (beManager != null)
        //    throw new InvalidOperationException();
        beManager = value;
        if (beManager != null && bizEntityCacheManager != null) {
            bizEntityCacheManager.setBEManager(beManager);
        }
    }

    public BizEntityCacheManager getBizEntityCacheManager() {
        if (bizEntityCacheManager == null) {
            bizEntityCacheManager = new BizEntityCacheManager(getBEManager(), getDicBE());
        }
        return bizEntityCacheManager;
    }

    @Deprecated
    public BufferClearingType getBufferClearingType() {
        return privateInnerBufferClearingType;
    }

    @Deprecated
    public final void setBufferClearingType(BufferClearingType value) {
        if (getInnerBufferClearingType().getValue() < value.getValue()) {
            setInnerBufferClearingType(value);
        }
    }

    @Deprecated
    public final BufferClearingType getInnerBufferClearingType() {
        return privateInnerBufferClearingType;
    }

    @Deprecated
    public final void setInnerBufferClearingType(BufferClearingType value) {
        privateInnerBufferClearingType = value;
    }

    public final ResponseContext getResponseContext() {
        return privateResponseContext;
    }

    public BefCallContext getCallContext() {
        if (callContext == null) {
            callContext = new BefCallContext();
        }
        return callContext;
    }

    public LockServiceItem getLockServiceItem() {
        if (!TempLockServiceItemStatus.isUseBaseBeLockServiceItem) {
            //如果扩展be和基础be不共用锁，则使用旧逻辑（此处用于解决项目更新补丁后下方逻辑引起问题）
            if (lockServiceItem == null) {
                lockServiceItem = lazilyEdit(new LockServiceItem(getConfigId()));
            }
            return lockServiceItem;
        }
        //扩展Be和基础Be使用同一数据库表，所以应使用同一锁（基础be）
        if (lockServiceItem == null) {
            String lockServiceBeType = StringUtils.isBlank(getBasicConfigId()) ? getConfigId() : getBasicConfigId();
            if (StringUtils.isBlank(lockServiceBeType)) {
                throw new IllegalArgumentException("lockServiceBeType：" + lockServiceBeType);
            }
            //lockServiceItem需要从当前SessionItemMap中获取(同一个sessionMap中，同一Be的锁需要共享使用)
            LockServiceItem item = findLocServiceItem(lockServiceBeType);
            //不应存储其它BEFuncSessionBase对象的LockServiceItem，否则在本层级Commit时会出现错乱
            if(item !=null)
                return item;
            //如果未找到，则初始化一个
            if(item ==null){
                lockServiceItem = lazilyEdit(new LockServiceItem(lockServiceBeType));
            }
        }
        return lockServiceItem;
    }

    public IBeBufferChangeManager getBufferChangeManager() {
        if (bufferChangeManager == null) {
            bufferChangeManager = lazilyEdit(new BeBufferChangeManager(funcSession, getConfigId(),
                    getBEManager().getAccessorCreator()));
        }
        return bufferChangeManager;
    }

    private <T extends DistributedSessionPart> T lazilyEdit(T item) {
        if (token != null) {
            item.beginEdit(token);
        }
        return item;
    }

    public VarBufferManager getVarBufferManager() {
        if (varBufferManager == null) {
            IVariableManager varMgr = ((BEManager) getBEManager()).internalCreateVariableManager();
            if (varMgr != null) {
                VarBufferManager buffer = new VarBufferManager(this, varMgr, getConfigId());
                buffer.initBuffer();
                lazilyEdit(buffer);
                varBufferManager = Optional.of(buffer);
            } else {
                varBufferManager = Optional.empty();
            }
        }
        return varBufferManager.orElse(null);
    }

    @Override
    public String getCategory() {
        return Category;
    }

    @Override
    public Map<String, Map<Integer, ICefBuffer>> getBuffers() {
        return buffers;
    }

    @Override
    public java.util.Map<String, Map<Integer, IChangeDetail>> getChanges() {
        return changes;
    }

    @Override
    public java.util.Map<String, ICefRootEntity> getRootEntities() {
        return rootEntities;
    }

//  private List<String> opAuths=new ArrayList<>();

    @Override
    public java.util.Map<Integer, ICefBuffer> getVariableBuffers() {
        return variableBuffers;
    }

    public java.util.HashMap<String, String> getRepositoryVariables() {
        return repositoryVariables;
    }

//  public boolean hasOpAuth(String actionCode)
//  {
//    return  opAuths.contains(actionCode);
//  }
//
//  public void addOpAuth(String actionCode)
//  {opAuths.add(actionCode);
//  }

    public Map<String, BefAuthorityInfo> getAuthorityInfos() {
        return authorityInfos;
    }

    public void addAuthorityInfos(String actionCode, String[] values) {
        if (authorityInfos.containsKey(actionCode) == false) {
            BefAuthorityInfo info = new BefAuthorityInfo(actionCode);
            info.addList(values);
            authorityInfos.put(actionCode, info);
        } else {
            BefAuthorityInfo befAuthorityInfo = authorityInfos.get(actionCode);
            befAuthorityInfo.addList(values);
        }
    }
    // endregion

    public boolean hasValues(String actionCode, String[] values) {
        if (getAuthorityInfos().containsKey(actionCode))
            return getAuthorityInfos().get(actionCode).hasValues(values);
        return false;
    }

    @Override
    public final EntityDataChangeListener getVarListener() {
        return privateVarListener;
    }

    /**
     * this 待提交的对象
     * @param upper this的信息commit给upper对象
     */
    @Override
    public void commit(IBefTransactionItem upper) {
        if(upper!=null){
            BEFuncSessionBase upperItem = (BEFuncSessionBase) upper;
            getBufferChangeManager().commit(upperItem.getBufferChangeManager());
            //BufferChangeManager.Commit(upperItem.BufferChangeManager);
            //getCallContext().commit(upperItem.getCallContext());
            //仅当自身创建了LockServiceItem的情况下，才需要commit
        }
        //不管上级是否为空，锁需要向upper LockServiceItem合并，合并完后将自身置空
        if(this.lockServiceItem!=null && this.lockServiceItem.getUpper()!=null) {
            this.lockServiceItem.commit(this.lockServiceItem.getUpper());
            this.lockServiceItem = null;
        }
    }

    @Override
    public void rollBack() {
//    BEFuncSessionBase upperItem = (BEFuncSessionBase)upper;
        getBufferChangeManager().rollBack();
        getLockServiceItem().rollBack();
        //TODO:变量回滚
    }

    @Override
    public IBefTransactionItem begin() {
        BEFuncSessionBase result;
        try {
            result = (BEFuncSessionBase) this.clone();
        } catch (CloneNotSupportedException e) {
            throw new BefRunTimeException(e);
        }
        result.bufferChangeManager = (IBeBufferChangeManager) getBufferChangeManager().begin();
        result.bizEntityCacheManager = getBizEntityCacheManager();
        //result.callContext = (BefCallContext)getCallContext().begin();
        // 2024/04/18，为解决自锁问题，锁在同层级SessionItems之间共享，BEFuncSessionBase调整为拥有锁和共享它人锁
        // 此处仍然调用getLockServiceItem()是为了确保了拷贝前所在层级有锁
        this.getLockServiceItem();
        if(this.lockServiceItem!=null) {
            result.lockServiceItem = this.lockServiceItem.begin();
        }
        //TODO:变量回滚
        return result;
    }

    @Override
    public void beginEdit(RootEditToken token) {
        this.token = token;
        if (bufferChangeManager != null) {
            bufferChangeManager.beginEdit(token);
        }
        if (lockServiceItem != null) {
            lockServiceItem.beginEdit(token);
        }
        if (varBufferManager != null && varBufferManager.isPresent()) {
            varBufferManager.get().beginEdit(token);
        }
        if (!repositoryVariables.isEmpty()) {
            token.putCallContextOnlyAbsent(getConfigId().concat("RepoVar"), new HashMap<>(repositoryVariables));
        }
    }

    @Override
    public void notifySave() {
        if (bufferChangeManager != null) {
            bufferChangeManager.notifySave();
        }
        if (lockServiceItem != null) {
            lockServiceItem.notifySave();
        }
    }

//  @Override
//  public FuncSessionItemIncrement buildIncrement(SessionEditToken token) {
//    FuncSessionItemIncrement value = new FuncSessionItemIncrement();
//    if (bufferChangeManager != null) {
//      this.bufferChangeManager.fillIncrement(value);
//    }
//    if (this.varBufferManager != null && this.varBufferManager.isPresent()) {
//      this.varBufferManager.get().fillIncrement(value);
//    }
//    return value;
//  }

    @Override
    public void endEdit(RootEditToken token) {
        this.token = null;
        if (bufferChangeManager != null) {
            bufferChangeManager.endEdit(token);
        }
        if (lockServiceItem != null) {
            lockServiceItem.endEdit(token);
        }
        if (varBufferManager != null && varBufferManager.isPresent()) {
            varBufferManager.get().endEdit(token);
        }
        if (!FuncSessionUtil.compareMap(getRepositoryVariables(),
                (Map) token.getCallContext().get(getConfigId().concat("RepoVar")))) {
            FuncSessionItemIncrement inc = token.getItemIncrement(getConfigId());
            inc.setCustoms(new HashMap<>());
            inc.getCustoms().put("RepoVar", FuncSessionUtil.convertMap2Binary(getRepositoryVariables()));
        }
    }
    //endregion

    /**
     * 从当前SessionItemMap中查找LockServiceItem (同一个sessionMap对应一个大的Transaction，同一Be的锁需要共享使用)
     * 查找时，要忽略当前BEFuncSessionBase对象，避免死循环
     * @param lockServiceBeType
     * @return
     */
    private LockServiceItem findLocServiceItem(String lockServiceBeType){
        Map<String, ICefSessionItem> map = funcSession.getSessionItems();
        if(map == null || map.isEmpty())
            return null;

        for (ICefSessionItem item : map.values()) {
            if (!(item instanceof BEFuncSessionBase))
                continue;

            BEFuncSessionBase funcSessionBase = (BEFuncSessionBase) item;
            //查找时，要忽略当前BEFuncSessionBase对象，避免死循环
            if(funcSessionBase==this)
                continue;
            //此处不能用funcSessionBase.getLockServiceItem，否则还是会存在死循环
            if(funcSessionBase.lockServiceItem!=null && lockServiceBeType.equals(funcSessionBase.lockServiceItem.getLockType())){
                return funcSessionBase.lockServiceItem;
            }
        }
        return null;
    }

}
