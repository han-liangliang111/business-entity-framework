/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.exception.ExceptionCode;
import io.iec.edp.caf.core.context.BizContextBuilder;
import io.iec.edp.caf.core.session.Session;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;

public class BefBizContextBuilder implements BizContextBuilder<BefBizContext, Session> {
    private static final String PREFIX = "bef";

    private static String getString() {
        //Random random = new Random();
        //return PREFIX + digits(random.nextLong()) + digits(random.nextLong());
        try {
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            return PREFIX + digits(random.nextLong()) + digits(random.nextLong());
        }catch (NoSuchAlgorithmException nsae) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_4003,nsae);
        }
    }

    private static String digits(long val) {
        String str = Long.toHexString(val);
        return StringUtils.leftPad(str, 16, '0');
    }

    @Override
    public BefBizContext build(Session session) {
        return new BefBizContext(getString(), ZonedDateTime.now(), session, null);
    }
}
