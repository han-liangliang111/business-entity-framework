package com.inspur.edp.bef.core.action.authorityinfo;

import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.auth.DataPermissionController;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.ServiceLoader;
import javax.annotation.Priority;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DataPermissionCtrlProvider {

    private static final String NAME_DEFAULT = "default";
    private static final int ORDER_DEFAULT = 0;
    private static DataPermissionCtrlProvider instance = new DataPermissionCtrlProvider();
    private Map<String, DataPermissionController> controllers;

    private DataPermissionCtrlProvider() {
    }

    public static DataPermissionCtrlProvider getInstance() {
        return instance;
    }

    public DataPermissionController getDefaultController() {
        DataPermissionController rez = getController(NAME_DEFAULT, false);
        return rez == null ? new EmptyDataPermissionController() : rez;
    }

    public DataPermissionController getController(String name, boolean throwe) {
        if (name == null || name.isEmpty()) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5080);
        }

        if (controllers == null) {
            buildControllers();
        }
        DataPermissionController rez = controllers.get(name);
        if (rez == null && throwe) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5001, name);
        }
        return rez;
    }

    private synchronized void buildControllers() {
        if (controllers != null) {
            return;
        }
        controllers = new HashMap<>();

        ServiceLoader<DataPermissionController> loader = ServiceLoader
                .load(DataPermissionController.class);
        for (DataPermissionController controller : loader) {
            DataPermissionController previous = controllers.putIfAbsent(controller.getName(), controller);
            if (previous != null && compareOrder(previous, controller, ORDER_DEFAULT) > 0) {
                controllers.put(controller.getName(), controller);
            }
        }
    }

    private int compareOrder(Object v1, Object v2, int defaultOder) {
        Priority orderAnno1 = v1.getClass().getAnnotation(Priority.class);
        Priority orderAnno2 = v2.getClass().getAnnotation(Priority.class);
        int order1 = orderAnno1 != null ? orderAnno1.value() : defaultOder;
        int order2 = orderAnno2 != null ? orderAnno2.value() : defaultOder;
        return Integer.compare(order1, order2);
    }
}
