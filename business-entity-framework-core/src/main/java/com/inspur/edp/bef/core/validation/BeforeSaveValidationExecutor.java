/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.core.validation.requiredvaladaptor.EntityValidationExecutor;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.cef.spi.validation.basetypes.nestedtrans.CefNestedTransValAdapter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class BeforeSaveValidationExecutor extends EntityValidationExecutor {
    ObjectMapper mapper = new ObjectMapper();

    /**
     * 格式:"[{\"beConfigId\":\"com.inspur.gs.perftest.perfsu.testdtmgen.TestDtmGen\",\"udtConfigId\":[\"com.inspur.gs.perftest.perfsu.testdtmgen.udt.zdyudt1.zdyUdt1\"]}]"
     */
    private static final String disableUdtValConfigInfoKey = "DisableUdtValConfigInfo";

    public BeforeSaveValidationExecutor(IEntityRTValidationAssembler assembler, ICefEntityContext entityCtx) {
        super(assembler, entityCtx);
    }

    @Override
    protected boolean canUdtValExecute(IValidation item){
        if(!(item instanceof CefNestedTransValAdapter)){
            return true;
        }
        ValidationContext validationContext = (ValidationContext) getContext();
        if(!validationContext.getBefContext().getParams().containsKey(disableUdtValConfigInfoKey)){
            return true;
        }
        String udtConfigInfo = (String) validationContext.getBefContext().getParams().get(disableUdtValConfigInfoKey);
        if(StringUtils.isEmpty(udtConfigInfo)){
            return true;
        }
        JavaType javaType = getCollectionType(ArrayList.class, DisableUdtValidationConfigInfo.class);
        ArrayList<DisableUdtValidationConfigInfo> configs = null;
        try {
            configs = mapper.readValue(udtConfigInfo, javaType);
        } catch (JsonProcessingException e) {
            log.error("获取上下文中禁用Udt校验信息出错", e);
        }

        if(configs == null || configs.size() == 0){
            return true;
        }
        
        DisableUdtValidationConfigInfo configInfo = configs.stream().filter(it -> it.getBeConfigId().equals(validationContext.getConfigId())).findFirst().orElse(null);

        CefNestedTransValAdapter cefNestedTransValAdapter = (CefNestedTransValAdapter) item;
        if(configInfo.getUdtConfigId().contains(cefNestedTransValAdapter.getUdtConfig())){
            return false;
        }
        return true;
    }

    public JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }


    @Data
    static class DisableUdtValidationConfigInfo {

        /**
         * BE的ConfigID
         */
        private String beConfigId;

        /**
         * 禁用校验的Udt元数据ConfigId
         */
        private List<String> udtConfigId;
    }
}
