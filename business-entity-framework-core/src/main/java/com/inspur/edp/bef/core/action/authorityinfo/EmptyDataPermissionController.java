package com.inspur.edp.bef.core.action.authorityinfo;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.lcp.AuthInfo;
import com.inspur.edp.bef.spi.auth.DataPermissionCache;
import com.inspur.edp.bef.spi.auth.DataPermissionController;
import com.inspur.edp.cef.api.authority.AuthorityInfo;

import java.util.Collections;
import java.util.List;
import javax.annotation.Priority;

@Priority(Integer.MAX_VALUE)
public class EmptyDataPermissionController implements DataPermissionController {

    @Override
    public String getName() {
        return "default";
    }

    @Override
    public DataPermissionCache buildCache() {
        return null;
    }

    @Override
    public boolean checkDataAuthority(String actionCode, IBEContext beContext,
                                      AuthInfo authInfo, DataPermissionCache cache) {
        return true;
    }

    @Override
    public List<AuthorityInfo> getQueryAuthInfo(AuthInfo authInfo,
                                                DataPermissionCache cache) {
        return Collections.emptyList();
    }
}
