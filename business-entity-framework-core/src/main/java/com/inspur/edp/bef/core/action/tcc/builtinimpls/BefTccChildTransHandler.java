/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.tcc.builtinimpls;

import com.inspur.edp.bef.api.action.tcc.TccHandlerContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.core.action.tcc.TccHandlerContextImpl;
import com.inspur.edp.bef.core.be.BENodeEntity;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.action.tcc.AbstractBefTccHandler;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.changeset.AddedChildChangeCollection;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.DeletedChildChangeCollection;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class BefTccChildTransHandler extends AbstractBefTccHandler {

    private final String code;
    private final String name;

    public BefTccChildTransHandler(String childCode) {
        Objects.requireNonNull(childCode);

        this.code = childCode;
        this.name = String.format("[%s]TccChildTrans", childCode);
    }

    private static Iterable<IChangeDetail> getChildChanges(String childCode, TccHandlerContext ctx) {
        IChangeDetail chg = ((TccHandlerContextImpl) ctx).getReverseChg();
        switch (chg.getChangeType()) {
            case Deleted:
                if (ctx.getData() == null) {
                    return Collections.emptyList();
                }
                return new DeletedChildChangeCollection((DeleteChangeDetail) chg, ctx.getData(), childCode);
            case Modify:
                Map<String, IChangeDetail> childChange = ((ModifyChangeDetail) chg).getChildChanges()
                        .get(childCode);
                return childChange == null ? Collections.emptyList() : childChange.values();
            case Added:
                return new AddedChildChangeCollection((AddChangeDetail) chg, childCode);
            default:
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5029, chg.getChangeType().toString());
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean canExecute(TccHandlerContext ctx) {
        return true;
    }

    @Override
    public void confirm(TccHandlerContext ctx) {
        for (IChangeDetail change : getChildChanges(code, ctx)) {
            getChildEntity(code, change.getDataID(), ctx)
                    .tccConfirmByCustom(ctx.getBusinessActionContext(), change);
        }
    }

    @Override
    public void cancel(TccHandlerContext ctx) {
        for (IChangeDetail change : getChildChanges(code, ctx)) {
            getChildEntity(code, change.getDataID(), ctx)
                    .tccCancelByCustom(ctx.getBusinessActionContext(), change);
        }
    }

    private BENodeEntity getChildEntity(String childCode, String id, TccHandlerContext ctx) {
        return (BENodeEntity) ((IBENodeEntity) ((TccHandlerContextImpl) ctx).getBEContext()
                .getDataType()).getChildBEEntity(childCode, id);
    }
}
