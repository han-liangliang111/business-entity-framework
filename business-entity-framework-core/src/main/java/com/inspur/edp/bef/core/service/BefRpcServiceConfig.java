//package com.inspur.edp.bef.core.service;
//
//import com.inspur.edp.bef.api.be.IBESUDataRpcService;
//import com.inspur.edp.bef.core.be.BESUDataRpcServiceImpl;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration(proxyBeanMethods = false)
//public class BefRpcServiceConfig {
//
//    @Bean
//    public IBESUDataRpcService createIBESUDataRpcService(){
//        return  new BESUDataRpcServiceImpl();
//    }
//}
