/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.lock;

import com.inspur.edp.bef.core.DebugAdapter;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.debugtool.api.Tracer;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.commons.utils.StringUtils;
import io.iec.edp.caf.core.session.SessionType;
import io.iec.edp.caf.lock.service.api.api.LockService;
import io.iec.edp.caf.lockservice.api.BatchLockResult;
import io.iec.edp.caf.sysmanager.api.data.user.User;
import io.iec.edp.caf.sysmanager.api.manager.user.UserManager;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class LockAdapter {
    // 锁永不过期
    private static final Duration LOCK_PERSISTENCE_TIME = Duration.ZERO;
    private static final LockAdapter instance = new LockAdapter();
    private static final SessionType targetType = SessionType.mobile;
    private static final String DEBUG_METHOD_NAME = "DataLock";
    private final String lockMkID = "DEV";
    private final String lockFuncID = "";
    private final String lockComment = "lockByBefRuntime";
    private final String isLockedComment = "lockByBefRuntime-islocked";
    private final LockService innerLockSvr;

    public LockAdapter() {
        //单元测试需要注释
        innerLockSvr = SpringBeanUtils.getBean(LockService.class);
    }

    public static LockAdapter getInstance() {
        return instance;
    }

    private static void traceWithDataIds(String mark, List<String> dataIds) {
        if (!Tracer.isEnabled()) {
            return;
        }
        DebugAdapter.trace(DEBUG_METHOD_NAME, mark, null, "dataIds-" + DebugAdapter.buildString(dataIds, false));
    }

    private static void traceWithGroupIdAndDataIds(String mark, String groupId, List<String> dataIds) {
        if (!Tracer.isEnabled()) {
            return;
        }
        DebugAdapter.trace(DEBUG_METHOD_NAME, mark, null, "groupId-" + groupId,
                "dataIds-" + DebugAdapter.buildString(dataIds, false));
    }

    private static void traceWithBizContextId(String mark, String bizContextId) {
        if (!Tracer.isEnabled()) {
            return;
        }
        DebugAdapter.trace(DEBUG_METHOD_NAME, mark, null, "bizContextId-" + bizContextId);
    }

    public static boolean isMobileClient() {
        return CAFContext.current.getCurrentSession().getSessionType() == targetType;
    }

    public final boolean addLock(String beType, List<String> dataIds, RefObject<String> groupId, RefObject<List<LockEntity>> lockEntities) {
        traceWithDataIds("addingLock", dataIds);
        BatchLockResult lockResult = innerLockSvr.addBatchLock(lockMkID, beType, dataIds, groupId.argvalue, LOCK_PERSISTENCE_TIME, lockFuncID,
                lockComment);
        groupId.argvalue = lockResult.getGroupId();
        boolean success = lockResult.isSuccess();
        LockEntity failedLockEntity = new LockEntity();
        traceWithGroupIdAndDataIds("addLock" + (success ? "Success" : "Failed"), groupId.argvalue, dataIds);
        if (!success) {
            Objects.requireNonNull(lockResult.getLockedEntities(), "lockResult.getLockedEntities()");
            if (!StringUtils.isEmpty(lockResult.getLockedEntities().get(0).getUserId())) {
                User user = SpringBeanUtils.getBean(UserManager.class).getUser(
                        lockResult.getLockedEntities().get(0).getUserId());
                if (user != null) {
                    failedLockEntity.setUserName(user.getName());
                }
            }
            failedLockEntity.setDataId(dataIds.get(0));
            lockEntities.argvalue = Arrays.asList(failedLockEntity);
        }
        return success;
    }

    public final boolean addIgnoreLock(String beType, List<String> dataIds, String groupId) {

        traceWithDataIds("addingIgnoreLock", dataIds);
        boolean success = isIgnoreLock(beType, dataIds, groupId);
        traceWithGroupIdAndDataIds("addLockToGroup" + (success ? "Success" : "Failed"), groupId, dataIds);
        return success;
    }

    private boolean isIgnoreLock(String beType, List<String> dataIds, String groupId) {
        List<String> data = new ArrayList<>();
        for (int i = 0; i <= dataIds.size() - 1; i++) {
            data.clear();
            data.add(dataIds.get(i));
            BatchLockResult lockResult = innerLockSvr.addBatchLock(lockMkID, beType, data, groupId, LOCK_PERSISTENCE_TIME, lockFuncID, lockComment);
            Objects.requireNonNull(lockResult.getLockedEntities(), "lockResult.getLockedEntities()");
            if (!CAFContext.current.getSessionId().equals(lockResult.getLockedEntities().get(0).getSessionId()) || !CAFContext.current.getUserId().equals(lockResult.getLockedEntities().get(0).getUserId())) {
                return false;
            }
        }
        return true;
    }

    public final boolean addLock2Group(String beType, List<String> dataIds, String groupId, RefObject<List<LockEntity>> lockEntities) {
        traceWithDataIds("addingLockToGroup", dataIds);
        DataValidator.checkForEmptyString(groupId, "groupId");
        BatchLockResult lockResult = innerLockSvr.addBatchLock(lockMkID, beType, dataIds, groupId, LOCK_PERSISTENCE_TIME, lockFuncID,
                lockComment);
        boolean success = lockResult.isSuccess();
        traceWithGroupIdAndDataIds("addLockToGroup" + (success ? "Success" : "Failed"), groupId, dataIds);
        return success;
    }

    public final void releaseLock(String groupId) {
        traceWithGroupIdAndDataIds("ReleaseDataLockByGroup", groupId, null);
        innerLockSvr.removeBatchLock(groupId);
    }

    //判断数据是否已被加锁, 用于临时解决广水移动端自己锁自己
    public boolean isLocked(String beType, List<String> dataIds) {
        Objects.requireNonNull(beType);
        Objects.requireNonNull(dataIds);
        BatchLockResult lockResult = innerLockSvr.addBatchLock(lockMkID, beType, dataIds, null, Duration.ofMillis(5), lockFuncID, isLockedComment);
        boolean rez = lockResult.isSuccess();
        if (rez) {
            innerLockSvr.removeBatchLock(lockResult.getGroupId());
        }
        return !rez;
    }

    public Collection<String> getDataIdsByGroupId(String id) {
        return innerLockSvr.getDataIds(id);
    }

    public void removeBatchLockByContext(String bizCtxId) {
        traceWithBizContextId("RemoveBatchLockByContext", bizCtxId);
        innerLockSvr.removeBatchLockByContext(bizCtxId);
    }

    public static class LockEntity {
        private String privateDataId;
        private String privateUserName;

        public final String getDataId() {
            return privateDataId;
        }

        public final void setDataId(String value) {
            privateDataId = value;
        }

        public final String getUserName() {
            return privateUserName;
        }

        public final void setUserName(String value) {
            privateUserName = value;
        }
    }
}
