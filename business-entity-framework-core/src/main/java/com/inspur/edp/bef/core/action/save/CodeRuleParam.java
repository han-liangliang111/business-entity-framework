/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save;


import com.inspur.edp.cef.entity.entity.IEntityData;
import lombok.Getter;
import lombok.Setter;

public class CodeRuleParam {

    private String privateCode;
    private String privateRuleId;
    private java.util.HashMap<String, Object> privateDic;
    private IEntityData data;
    @Getter
    @Setter
    private String nodeCode;
    @Getter
    @Setter
    private String labelId;

    public final String getCode() {
        return privateCode;
    }

    public final void setCode(String value) {
        privateCode = value;
    }

    public final String getRuleId() {
        return privateRuleId;
    }

    public final void setRuleId(String value) {
        privateRuleId = value;
    }

    @Deprecated
    public final java.util.HashMap<String, Object> getDic() {
        return privateDic;
    }

    @Deprecated
    public final void setDic(java.util.HashMap<String, Object> value) {
        privateDic = value;
    }

    public IEntityData getData() {
        return data;
    }

    public void setData(IEntityData value) {
        this.data = value;
    }
}
