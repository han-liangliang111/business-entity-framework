/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.modify;


import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.accessor.base.AccessorComparer;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.InnerUtil;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.entity.entity.IValueObjData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class FullModifyMgrAction extends AbstractManagerAction<IEntityData> {

    private final IEntityData data;
    private final BEManager manager;

    public FullModifyMgrAction(BEManager manager, IEntityData data) {
        super(manager.getBEManagerContext());
        this.manager = manager;
        this.data = data;
    }

    @Override
    public void execute() {
        RetrieveParam para = new RetrieveParam();
        para.setNeedLock(true);
        BusinessEntity entity = (BusinessEntity) getBEManagerContext().getEntity(data.getID());
        entity.retrieve(para);
        checkRetrieveResult(entity);
        entity.modify(data);
    }

    private void checkRetrieveResult(IBusinessEntity be) {
        if (be.getBEContext().getCurrentData() == null) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5036, be.getID());
        }
        LockUtils.checkLocked(be.getBEContext());
    }
}
