/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;


import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.spi.event.queryactionevent.BefQueryEventBroker;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

import java.util.List;

public class BeforeQueryDtmExecutor {

    //region Ctor
    private IEntityRTDtmAssembler assembler;
    private IBEManagerContext mgrContext;
    //region Execute
    private EntityFilter privateFilter;

    //private IQueryDeterminationContext Context { get; set; }

    //endregion


    //private readonly IBEContext entityCtx;
    public BeforeQueryDtmExecutor(IEntityRTDtmAssembler assembler, IBEManagerContext mgrContext) {
        this.assembler = assembler;
        this.mgrContext = mgrContext;
        //this.entityCtx = entityCtx;
    }

    public final EntityFilter getFilter() {
        return privateFilter;
    }

    public final void setFilter(EntityFilter value) {
        privateFilter = value;
    }

    public final int execute() {
        int count = 0;
        List<IDetermination> dtms = assembler.getDeterminations();
        if (dtms == null) {
            return 0;
        }

        setFilter(new EntityFilter());

        for (IDetermination dtm : dtms) {
            if (!dtm.canExecute(null)) {
                continue;
            }
            count++;
            QueryDeterminationContext context = getContext();
            try {
                BefQueryEventBroker.fireBeforeCXQDeterminate(getFilter());
                dtm.execute(context, null);
                if (context.getFilter() != null) {
                    if (context.getFilter().getFilterConditions() != null) {
                        getFilter().addFilterConditions(context.getFilter().getFilterConditions());
                    }
                    if (context.getFilter().getSortConditions() != null) {
                        getFilter().addSortConditions(context.getFilter().getSortConditions());
                    }
                }
                BefQueryEventBroker.fireAfterCXQDeterminate(getFilter());
            } catch (Exception e) {
                BefQueryEventBroker.fireQueryUnnormalStop(e);
                throw new BefRunTimeException(e);
            }
        }
        return count;
    }

    private QueryDeterminationContext getContext() {
        QueryDeterminationContext tempVar = new QueryDeterminationContext(mgrContext);
        tempVar.setFilter(new EntityFilter());
//		tempVar.setFinalFilter(getFinalFilter());
        return tempVar;
    }
    //endregion
}
