/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.validation;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.cef.entity.repository.DataSaveResult;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.validation.requiredvaladaptor.EntityValidationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;

public class AfterSaveValidationExecutor extends EntityValidationExecutor {

    private final DataSaveResult saveResult;

    public AfterSaveValidationExecutor(IEntityRTValidationAssembler assembler,
                                       IBENodeEntityContext entityCtx, DataSaveResult saveResult) {
        super(assembler, entityCtx);
        this.saveResult = saveResult;
    }

    protected IBENodeEntityContext getBEContext() {
        return (IBENodeEntityContext) super.getEntityContext();
    }

    @Override
    protected ICefValidationContext GetContext() {
        return new AfterSaveValidationContext(getBEContext(), saveResult);
    }

    @Override
    protected IChangeDetail getChangeset() {
        return getBEContext().getTransactionalChange();
    }
}
