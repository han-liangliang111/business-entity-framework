/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.action.assembler.IDefaultValueProcessor;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.bef.spi.action.assembler.entityAssemblerFactory.IDefaultEntityActionAssFactory;
import com.inspur.edp.bef.spi.entity.CodeRuleInfo;
import com.inspur.edp.cdp.coderule.api.CodeRuleBEAssignService;
import com.inspur.edp.cef.entity.accessor.entity.IEntityAccessor;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class CloneAction extends AbstractAction<VoidActionResult> {
    private String cloneDataID;
    private String dataId;
    private CloneParameter cloneParameter;

    public CloneAction(String cloneDataID, CloneParameter cloneParameter) {
        this.cloneDataID = cloneDataID;
        this.cloneParameter = cloneParameter;
    }

    public CloneAction(String cloneDataID, String dataId, CloneParameter cloneParameter) {
        this.cloneDataID = cloneDataID;
        this.dataId = dataId;
        this.cloneParameter = cloneParameter;
    }

    public static IDefaultValueProcessor getDefaultValueProcessor(IBEContext context) {
        IDefaultValueProcessor processor = FuncSessionManager.getCurrentSession().getDefaultValueProcessor();
        if (processor == null) {
            Object tempVar = context.getBizEntity().getAssemblerFactory();
            IDefaultEntityActionAssFactory factory = (IDefaultEntityActionAssFactory) ((tempVar instanceof IDefaultEntityActionAssFactory) ? tempVar : null);
            if (factory != null) {
                FuncSessionManager.getCurrentSession().setDefaultValueProcessor(processor = (((factory.getDefaultValueProcessor(null))) != null) ? (factory.getDefaultValueProcessor(null)) : new InnerDefaultValueProcessor());
            }
        }
        return processor;
    }

    @Override
    public void execute() {
        AuthorityUtil.checkDataAuthority(FuncSessionManager.getCurrentSession().getBefContext().getCurrentOperationType(), ActionUtil.getBEContext(this));

        BusinessEntity be = ActionUtil.getRootEntity(this);
//        if (be.getBEContext().hasData()) {
//            throw new BefException(ErrorCodes.InvalidRecreation, "不能使用相同主键重复新增数据", null, ExceptionLevel.Error);
//        }
//
//        IEntityData ed = be.createEntityData(be.getID());
//
//        IEntityData currData = (IEntityData) be.getBEContext().getBufferChangeManager().createCurrentBuffer(ed);
//        be.getBEContext().setCurrentData(currData);
//
//        assignCode(be);
//          be.getBEContext().startChangeListener();
//        be.getBEContext().appendTempCurrentChange(new AddChangeDetail(currData));
//        executeDetermination();

        setCloneValue(be);
//        be.getBEContext().acceptListenerChange();
    }

    //复制主表数据
    //id和编号不能复制
    private void setCloneValue(BusinessEntity businessEntity) {
        try {
            Class c = Class.forName("com.inspur.edp.common.component.copydata.CloneUtil");
            for (Method method : c.getDeclaredMethods()) {
                if (method.getName().equals("execute")) {
                    method.invoke(c.newInstance(), cloneDataID, dataId, this.cloneParameter, getBEContext().getModelResInfo().getCustomResource(getBEContext().getModelResInfo().getRootNodeCode()), businessEntity);
                    break;
                }
            }
        } catch (Exception e) {
            throw new BefRunTimeException(e);
        }
    }

    private void executeDetermination() {
        ActionUtil.getRootEntity(this).retrieveDefaultDeterminate();
        //var dtmContext = CoreBEContext.createDeterminationContext();
        //var com.inspur.edp.bef.core.action = new ActionFactory().getRetrieveDefaultDtmAction(dtmContext);
        //com.inspur.edp.bef.core.action.do();
    }

    @Override
    protected IBEActionAssembler getAssembler() {
        return GetAssemblerFactory().getRetrieveDefaultActionAssembler((IBEContext) this.getBEContext());
    }
}
