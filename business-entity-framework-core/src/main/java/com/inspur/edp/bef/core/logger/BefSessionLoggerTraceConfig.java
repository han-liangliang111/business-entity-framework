package com.inspur.edp.bef.core.logger;

import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;

public class BefSessionLoggerTraceConfig {
    @Bean
    public BefSessionLog BefSessionLog() {
        return new BefSessionLoggerTrace();
    }

    @Bean
    public BefSessionLoggerTraceCol befSessionLoggerTraceCol() {
        return new BefSessionLoggerTraceCol();
    }

    @Bean
    public RESTEndpoint getBefSessionLoggerTraceCol(BefSessionLoggerTraceCol befSessionLoggerTraceCol) {
        return new RESTEndpoint(
                "/befruntime/logger/v1.0/befcontextlog",
                befSessionLoggerTraceCol
        );

    }

}
