/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;

import java.security.SecureRandom;
import java.util.List;

public class BefAftModifyDtmAdaptor extends BefBaseDtmAdaptor {

    private static SecureRandom idGenerator = new SecureRandom();
    private final List<ChangeType> changeTypes;
    private final List<String> elements;
    private final List<ChildElementsTuple> childElements;
    private boolean runOnce;
    private String runOnceKey;

    //region 第一版构造函数
    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes) {
        this(name, type, changeTypes, null, null);
    }
    //endregion

    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                                  List<String> elements) {
        this(name, type, changeTypes, elements, null);
    }

    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                                  List<String> elements,
                                  List<ChildElementsTuple> childElements) {
        this(name, type, changeTypes, false, elements, childElements);
    }

    //region 增加是否执行一次构造函数
    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                                  boolean runOnce) {
        this(name, type, changeTypes, runOnce, null, null);
    }
    //endregion

    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                                  boolean runOnce,
                                  List<String> elements) {
        this(name, type, changeTypes, runOnce, elements, null);
    }

    public BefAftModifyDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                                  boolean runOnce, List<String> elements, List<ChildElementsTuple> childElements) {
        super(name, type);
        this.changeTypes = changeTypes;
        this.runOnce = runOnce;
        this.elements = elements;
        this.childElements = childElements;
    }

    @Override
    public boolean canExecute(IChangeDetail iChangeDetail) {
        if (changeTypes != null && !changeTypes.contains(iChangeDetail.getChangeType())) {
            return false;
        }
        if (elements != null && Util.containsAny(iChangeDetail, elements)) {
            return true;
        }
        if (childElements != null && childElements.stream().anyMatch(childElement -> Util
                .containsChildAny(iChangeDetail, childElement.getItem1(), childElement.getItem2()))) {
            return true;
        }
        return elements == null && childElements == null;
    }

    @Override
    public void execute(ICefDeterminationContext ctx, IChangeDetail change) {
        if (runOnce) {
            IBefCallContext callContext = ((IDeterminationContext) ctx).getBEContext().getCallContext();
            String dataId = ((IDeterminationContext) ctx).getBENodeEntity().getID();
            if (callContext.containsKey(getRunOnceKey(dataId)))
                return;
            callContext.setData(getRunOnceKey(dataId), null);
        }
        super.execute(ctx, change);
    }

    private String getRunOnceKey(String dataId) {
        if (runOnceKey == null) {
            runOnceKey = Long.toString(idGenerator.nextLong());
        }
        return runOnceKey.concat(dataId);
    }
}
