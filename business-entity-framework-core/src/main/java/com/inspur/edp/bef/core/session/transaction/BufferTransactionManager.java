/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.transaction;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSession.SessionItemMap;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.cef.api.session.ICefSessionItem;

import java.util.Map;

/**
 * 缓存事务管理器，用于保证Action执行的原子性
 */
public class BufferTransactionManager {

    private final FuncSession session;
    private int trans;

    // region 构造函数
    public BufferTransactionManager(FuncSession session) {
        this.session = session;
    }
    // endregion

    @Deprecated
    public static BufferTransactionManager getInstance() {
        return new BufferTransactionManager(FuncSessionManager.getCurrentSession());
    }

    public final void Begin() {
        trans++;
        //事务开启时，为保证原子性（单独提交或回滚)，把当前的SessionItems复制一份并加入栈顶
        SessionItemMap currItems = (SessionItemMap) session.getSessionItemsStack().peek();
        SessionItemMap newTranItems = session.newSessionItemMap();

        for (Map.Entry<String, ICefSessionItem> currItem : currItems.entrySet()) {
            //如果当前的currItem为IBefTransactionItem事务参与方，则begin一个新的
            if (currItem.getValue() instanceof IBefTransactionItem) {
                newTranItems.innerPut(currItem.getKey(),
                        (ICefSessionItem) ((IBefTransactionItem) currItem.getValue()).begin());
            } else {
                newTranItems.innerPut(currItem.getKey(), currItem.getValue());
            }
        }
        session.getSessionItemsStack().push(newTranItems);
    }

    public final void SetComplete() {
        CheckTransaction();
        //待提交层级的SessionItems
        Map<String, ICefSessionItem> toCompleteItems = session.getSessionItemsStack().pop();
        //上一层级的SessionItems
        SessionItemMap currItems = (SessionItemMap) session.getSessionItemsStack().peek();

        for (Map.Entry<String, ICefSessionItem> toCompleteItem : toCompleteItems.entrySet()) {
            ICefSessionItem currItem = currItems.get(toCompleteItem.getKey());

            //如果该类型的对象为IBefTransactionItem，则子级向父级提交一下
            if (toCompleteItem.getValue() instanceof IBefTransactionItem) {
                ((IBefTransactionItem) toCompleteItem.getValue()).commit((IBefTransactionItem) currItem);
            }

            //父级上没有同key的ICefSessionItem时，需要将自身放入上一层级（多数为同beType的BEFuncSessionBase）
            if(currItem==null) {
                currItems.innerPut(toCompleteItem.getKey(), toCompleteItem.getValue());
            }
        }
        trans--;
    }

    /**
     * 回滚事务
     */
    public final void SetAbort() {
        CheckTransaction();
        Map<String, ICefSessionItem> upperItems = session.getSessionItemsStack().pop();
        for (Map.Entry<String, ICefSessionItem> upperItem : upperItems.entrySet()) {
            if (upperItem.getValue() instanceof IBefTransactionItem) {
                ((IBefTransactionItem) upperItem.getValue()).rollBack();
            }
        }
        trans--;
    }

    // 验证当前存在事务
    private void CheckTransaction() {
        if (trans <= 0) {
            throw new BefExceptionBase();
        }
    }
}
