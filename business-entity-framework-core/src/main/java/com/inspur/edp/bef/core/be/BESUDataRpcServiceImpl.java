//package com.inspur.edp.bef.core.be;
//
//import com.alibaba.fastjson.JSON;
//import com.inspur.edp.bef.api.be.IBESUDataRpcService;
//import com.inspur.edp.bef.api.lcp.ILcpFactory;
//import com.inspur.edp.bef.api.lcp.IStandardLcp;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
//import com.inspur.edp.bef.api.services.IBefSessionManager;
//import com.inspur.edp.cdp.common.utils.json.JsonUtil;
//import com.inspur.edp.cef.entity.condition.EntityFilter;
//import com.inspur.edp.cef.entity.entity.IEntityData;
//import io.iec.edp.caf.commons.utils.SpringBeanUtils;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class BESUDataRpcServiceImpl implements IBESUDataRpcService {
//    @Override
//    public String rpcRetrieveByBeId(String dataId, RetrieveParam retrieveParam, Map<String, String> variables,
//        String beId) {
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            setLcpRepositoryVariables(variables, standardLcp);
//            String result = standardLcp.serializeData(standardLcp.retrieve(dataId, retrieveParam).getData());
//            return result;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    @Override
//    public Map<String, String> rpcRetrieveByBeIdWithDataIds(List<String> dataIds, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId) {
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            setLcpRepositoryVariables(variables, standardLcp);
//            Map<String, IEntityData> entityDataMap = standardLcp.retrieve(dataIds, retrieveParam).getDatas();
//            Map<String, String> resultMap = dealResultMap(entityDataMap, standardLcp);
//            return resultMap;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    @Override
//    public List<String> rpcQueryByBeId(EntityFilter filter, Map<String, String> variables, String beId) {
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            setLcpRepositoryVariables(variables, standardLcp);
//            List<IEntityData> entityDataList = standardLcp.query(filter);
//            List<String> entityDataJsons = dealEntityDataJson(entityDataList, standardLcp);
//            return entityDataJsons;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    @Override
//    public String rpcRetrieveByNodeIdByBeId(String nodeId, String dataId, RetrieveParam retrieveParam,
//        String beId) {
//        return null;
//        // todo 暂不实现子表
////        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
////        try {
////            iBefSessionManager.createSession();
////            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
////            return standardLcp.retrieve(nodeId, dataId, retrieveParam);
////        } finally {
////            iBefSessionManager.closeCurrentSession();
////        }
//    }
//
//    private Map<String, String> dealResultMap(Map<String, IEntityData> entityDataMap, IStandardLcp lcp) {
//        Map<String, String> resultMap = new HashMap<>(entityDataMap.size());
//        for (Map.Entry<String, IEntityData> entry : entityDataMap.entrySet()) {
//            resultMap.put(entry.getKey(), lcp.serializeData(entry.getValue()));
//        }
//        return resultMap;
//    }
//
//    private List<String> dealEntityDataJson(List<IEntityData> entityDataList, IStandardLcp standardLcp) {
//        List<String> entityDataJsons = new ArrayList<>();
//        for (IEntityData entityData : entityDataList) {
//            entityDataJsons.add(standardLcp.serializeData(entityData));
//        }
//        return entityDataJsons;
//    }
//
//    private void setLcpRepositoryVariables(Map<String, String> variables, IStandardLcp lcp) {
//        if (variables == null || variables.isEmpty()) {
//            return;
//        }
//        for (Map.Entry<String, String> entry : variables.entrySet()) {
//            lcp.setRepositoryVariables(entry.getKey(), entry.getValue());
//        }
//    }
//}
