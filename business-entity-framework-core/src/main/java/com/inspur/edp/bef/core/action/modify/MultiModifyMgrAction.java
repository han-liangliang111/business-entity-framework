/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.modify;


import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
import com.inspur.edp.bef.core.action.base.ActionStack;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.action.delete.DeleteChecker;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

import java.util.*;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class MultiModifyMgrAction extends AbstractManagerAction<VoidActionResult> {

    //region Constructor

    private java.util.ArrayList<IChangeDetail> changeList;

    //endregion


    //region 属性字段

    public MultiModifyMgrAction(IBEManagerContext managerContext, java.util.ArrayList<IChangeDetail> changeList) {
        super(managerContext);
        ActionUtil.requireNonNull(changeList, "changeList");
        this.changeList = changeList;
    }


    //endregion


    //region  Override

    @Override
    public final void execute() {

        getBEManagerContext().checkAuthority("Modify");
//		AuthorityUtil.checkAuthority("Modify");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Modify");
        Set<String> set = new LinkedHashSet(changeList.size());
        for (IChangeDetail item : changeList) {
            String dataID = item.getDataID();
            if (StringUtils.isEmpty(dataID)) {
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5041);
            }
            set.add(dataID);
        }
        List<String> ids = new ArrayList<>(set);
        RetrieveParam para = new RetrieveParam();
        para.setNeedLock(true);
        RetrieveResult result = getBEManagerContext().getBEManager().retrieve(ids, para);

        checkRetrieveResult(ids, result);

        Map<String, IBusinessEntity> beList = getBEManagerContext().getEntitiesMap(ids);
        boolean isLast = ActionStack.isLastNode();
        List<IBusinessEntity> entityList = new ArrayList<>();
        for (IChangeDetail change : changeList) {
            IBusinessEntity be = beList.get(change.getDataID());
            entityList.add(be);
            be.modify(change);
            if (!isLast) {
                ((BusinessEntity) be).getBEContext().getResponse().mergeInnerChange(change);
            }
        }

        DeleteChecker deleteChecker = new DeleteChecker();
        deleteChecker.check(entityList, ((BEManager)getBEManagerContext().getBEManager()).getResponseContext());
    }

    /**
     * 检验Retrieve结果，若未全部获取成功，则抛异常，并返回失败ids
     *
     * @param ids    批量Modify的数据id
     * @param result 获取结果
     */
    private void checkRetrieveResult(List<String> ids, RetrieveResult result) {
        List<Object> resultIds = result.getDatas().keySet().stream().collect(Collectors.toList());
        if (resultIds.size() != ids.size()) {
            java.util.ArrayList<String> retrieveFailIDs = new java.util.ArrayList<String>();
            for (String id : ids) {
                if (!resultIds.contains(id)) {
                    retrieveFailIDs.add(id);
                }
            }
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5042, String.valueOf(retrieveFailIDs));
        }
        if (!LockUtils.needLock(getBEManagerContext().getModelResInfo()))
            return;
        if (!result.getLockFailedIds().isEmpty()) {
            LockUtils.throwLockFailed(result.getLockFailedIds().stream().collect(Collectors.toList()), null);
            //throw new BefException(ErrorCodes.EditingLocked, "数据已被其他用户锁定无法修改");//TODO: 使用统一的消息机制
        }
    }

    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory().getMultiModifyMgrActionAssembler(getBEManagerContext(), changeList);
    }

    //endregion
}
