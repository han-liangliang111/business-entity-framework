/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.core.session.distributed.close.AbnormalClosingUtil;
import com.inspur.edp.commonmodel.core.session.serviceinterface.SessionConfigService;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionCacheInfo;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionConfig;
import com.inspur.edp.commonmodel.core.util.SessionIncrementUtil;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.core.context.BizContextManager;
import io.iec.edp.caf.core.context.ICAFContextService;
import io.iec.edp.caf.core.session.CafSession;
import io.iec.edp.caf.core.session.ICafSessionService;
import io.iec.edp.caf.core.session.SessionType;
import io.iec.edp.caf.core.session.core.CAFSessionThreadHolder;
import io.iec.edp.caf.msu.api.ServiceUnitAwareService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.security.core.context.SecurityContextHolder;

@Slf4j
@DisallowConcurrentExecution
public class SessionValidityCheck implements Job {

    private static HashMap<String, List<Date>> dates = new HashMap<>();
    private static HashMap<String, List<String>> linkInfoRecord = new HashMap<>();

    /**
     * 清理失效的功能会话
     */
    private static synchronized void cleanExpiredFuncSession() {
        try {
            LocalDateTime timeThreshold = LocalDateTime.now().minusMinutes(35);
            BizContextManager bizContextManager = SpringBeanUtils.getBean(BizContextManager.class);
            for (Map<String, FuncSession> sessionMap : FuncSessionManager.getCurrent().getBuckets()
                    .values()) {
                if (sessionMap == null || sessionMap.isEmpty()) {
                    continue;
                }
                List<String> expiredIds = new ArrayList<>();
                Iterator<Map.Entry<String, FuncSession>> iterator = sessionMap.entrySet().iterator();
                while (iterator.hasNext()) {
                    try {
                        Map.Entry<String, FuncSession> item = iterator.next();
                        if (item.getValue().isExpired()) {
                            expiredIds.add(item.getKey());
                        } else if (item.getValue().getLastOn().isBefore(timeThreshold) && bizContextManager
                                .isExpired(item.getKey())) {
                            expiredIds.add(item.getKey());
                        }
                    } catch (Throwable t) {
                        log.error("cleanExpiredFuncSession failed 1", t);
                    }
                }
                if (!expiredIds.isEmpty()) {
                    for (String item : expiredIds) {
                        FuncSessionManager.getCurrent().closeSessionWithDestroy(item);
                    }
                }
            }
        } catch (Throwable r) {
            log.error("cleanExpiredFuncSession failed 9", r);
        }
    }

    private static synchronized void cleanByLocalCache() {
        try {
            AbnormalClosingUtil util = new AbnormalClosingUtil();
            List<String> tokenIds = util.getValidityTokenId();
            if (tokenIds == null || tokenIds.isEmpty()) {
                return;
            }
            int failedCount = 0;
            Throwable lastError = null;
            for (String tokenId : tokenIds) {
                try {
                    List<String> sessionIds = util.clearLocalCache(tokenId);
//          util.clearLocalCache(tokenId);
                    if (sessionIds == null || sessionIds.isEmpty()) {
                        continue;
                    }
                    for (String item : sessionIds) {
                        FuncSessionManager.getCurrent()
                                .closeSessionWithDestroy(FuncSessionUtil.getSessionIdBySplit(item));
                    }
                } catch (Throwable t) {
                    failedCount++;
                    lastError = t;
                }
            }
            if (failedCount > 0) {
                log.error("cleanExpiredFuncSession定时任务内部失败次数" + failedCount, lastError);
            }
        } catch (Throwable r) {
            log.error("cleanExpiredFuncSession定时任务执行失败", r);
        }
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        try {
            cleanExpiredFuncSession();
            cleanByLocalCache();
        } catch (Throwable r) {
            log.error("清理BefSession定时任务执行失败", r);
        }
    }
}
