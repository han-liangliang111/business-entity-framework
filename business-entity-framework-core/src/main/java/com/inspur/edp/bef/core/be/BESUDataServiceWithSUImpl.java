//package com.inspur.edp.bef.core.be;
//
//import com.inspur.edp.bef.api.be.BeDataServiceWithSU;
//import com.inspur.edp.bef.api.lcp.ILcpFactory;
//import com.inspur.edp.bef.api.lcp.IStandardLcp;
//import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
//import com.inspur.edp.bef.api.services.IBefSessionManager;
//import com.inspur.edp.cef.entity.condition.EntityFilter;
//import com.inspur.edp.cef.entity.entity.IEntityData;
//import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
//import com.inspur.edp.metadata.rtcustomization.api.CustomizationService;
//import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataQueryParam;
//import com.inspur.edp.metadata.rtcustomization.api.entity.MetadataScopeEnum;
//import io.iec.edp.caf.businessobject.api.entity.DevBasicBoInfo;
//import io.iec.edp.caf.commons.utils.SpringBeanUtils;
//import io.iec.edp.caf.rpc.api.service.RpcClient;
//import io.iec.edp.caf.rpc.api.support.Type;
//import io.iec.edp.caf.sumgr.api.SUService;
//import io.iec.edp.caf.businessobject.api.service.BusinessObjectServerService;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;
//
//public class BESUDataServiceWithSUImpl implements BeDataServiceWithSU {
//    private RpcClient rpcClient = SpringBeanUtils.getBean(RpcClient.class);
//
//    @Override
//    public RespectiveRetrieveResult retrieveByBeId(String dataId, RetrieveParam retrieveParam, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        return getRetrieveResultByDataId(dataId, retrieveParam, null, beId);
//    }
//
//    @Override public RespectiveRetrieveResult retrieveByBeIdWithParam(String dataId, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        return getRetrieveResultByDataId(dataId, retrieveParam, variables, beId);
//    }
//
//    @Override
//    public RetrieveResult retrieveByBeId(List<String> dataIds, RetrieveParam retrieveParam, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        if (dataIds == null || dataIds.isEmpty()) {
//            throw new RuntimeException("dataIds不应该为控");
//        }
//        return getRetrieveResultByDataIds(dataIds, retrieveParam, null, beId);
//    }
//
//    @Override public RetrieveResult retrieveByBeIdWithParam(List<String> dataIds, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        if (dataIds == null || dataIds.isEmpty()) {
//            throw new RuntimeException("dataIds不应该为控");
//        }
//        return getRetrieveResultByDataIds(dataIds, retrieveParam, variables, beId);
//    }
//
//    private HashMap<String, IEntityData> dealEntityDataMap(Map<String, String> result, IStandardLcp lcp) {
//        HashMap<String, IEntityData> entityDataMap = new HashMap<>(result.size());
//        for (Map.Entry<String, String> entry : result.entrySet()) {
//            entityDataMap.put(entry.getKey(), lcp.deSerializeData(entry.getValue()));
//        }
//        return entityDataMap;
//    }
//
//    // todo 暂不实现子表
////    @Override public RespectiveRetrieveResult retrieveByBeId(String nodeId, String dataId, RetrieveParam retrieveParam,
////        String beId) {
////        String su = getSuInfo(beId);
////        SUService suService = SpringBeanUtils.getBean(SUService.class);
////        if (suService.isLocalInvoke(su)) {
////            // su信息在当前应用，走本地
////            IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
////            try {
////                iBefSessionManager.createSession();
////                IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
////                return standardLcp.retrieve(nodeId, dataId, retrieveParam);
////            } finally {
////                iBefSessionManager.closeCurrentSession();
////            }
////        }
////        // 远程获取数据
////        String serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByNodeIdByBeId";
////        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
////        parameters.put("nodeId", nodeId);
////        parameters.put("dataId", dataId);
////        parameters.put("retrieveParam", retrieveParam);
////        parameters.put("beId", beId);
////        RespectiveRetrieveResult result = rpcClient.invoke(RespectiveRetrieveResult.class, serviceId, su, parameters, null);
////        return result;
////    }
//
//    @Override public List<IEntityData> queryByBeId(EntityFilter filter, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        return getQueryResultByFilter(filter, null, beId);
//    }
//
//    @Override
//    public List<IEntityData> queryByBeIdWithParam(EntityFilter filter, Map<String, String> variables, String beId) {
//        if (beId == null) {
//            throw new RuntimeException("BEId不应该为null");
//        }
//        return getQueryResultByFilter(filter, variables, beId);
//    }
//
//    private List<IEntityData> dealEntityData(List<String> results, String beId) {
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            List<IEntityData> entityDataList = new ArrayList<>();
//            for (String data : results) {
//                entityDataList.add(standardLcp.deSerializeData(data));
//            }
//            return entityDataList;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    // TODO 暂不实现子表
////    @Override
////    public List<IEntityData> queryByBeId(String nodeCode, EntityFilter filter, String beId) {
////        return null;
////    }
//
//    private String getSuInfo(String beId) {
//        MetadataQueryParam metadataQueryParam = new MetadataQueryParam();
//        // 传入需要查询的元数据id信息
//        metadataQueryParam.setMetadataId(beId);
//        /*// 根据需要传入即可，默认是国际化
//        metadataQueryParam.setIsI18n();*/
//        metadataQueryParam.setMetadataScopeEnum(MetadataScopeEnum.RUNTIME);
//        CustomizationService customizationService = SpringBeanUtils.getBean(CustomizationService.class);
//        GspMetadata beInfo = customizationService.getGspMetadata(metadataQueryParam);
//        //  获取业务对象id
//        if (beInfo == null) {
//            throw new RuntimeException("获取BEId对应元数据失败，请确认BEId是否正确，BEId：" + beId);
//        }
//        String bizObjectId = beInfo.getHeader().getBizobjectID();
//        BusinessObjectServerService businessObjectServerService = SpringBeanUtils.getBean(BusinessObjectServerService.class);
//        DevBasicBoInfo devBasicBoInfo = businessObjectServerService.getDevBasicBoInfo(bizObjectId);
//        if (devBasicBoInfo == null) {
//            throw new RuntimeException("业务对象不存在，请检查配置。bizObjectId:" + bizObjectId);
//        }
//        String su = devBasicBoInfo.getSuCode();
//        if (su == null || su.isEmpty()) {
//            throw new RuntimeException("获取su信息失败");
//        }
//        return su;
//    }
//
//    private RetrieveResult getRetrieveResultByDataIds(List<String> dataIds, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId) {
//        String su = getSuInfo(beId);
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        SUService suService = SpringBeanUtils.getBean(SUService.class);
//        if (suService.isLocalInvoke(su)) {
//            try {
//                iBefSessionManager.createSession();
//                IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//                setLcpRepositoryVariables(variables, standardLcp);
//                return standardLcp.retrieve(dataIds, retrieveParam);
//            } finally {
//                iBefSessionManager.closeCurrentSession();
//            }
//        }
//        // 远程获取数据
//        String serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByBeIdWithDataIds";
//        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
//        parameters.put("dataIds", dataIds);
//        parameters.put("retrieveParam", retrieveParam);
//        parameters.put("beId", beId);
//        parameters.put("variables", variables);
//        Type type = new Type(Map.class, String.class, String.class);
//        Map<String, String> result = (Map<String, String>) rpcClient.invoke(type, serviceId, su, parameters, null);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            HashMap<String, IEntityData> entityDataMap = dealEntityDataMap(result, standardLcp);
//            RetrieveResult retrieveResult = new RetrieveResult();
//            retrieveResult.setDatas(entityDataMap);
//            return retrieveResult;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    /**
//     * 检索
//     *
//     * @param dataId 数据ID
//     * @param retrieveParam 检索参数
//     * @param variables 参数
//     * @param beId beID
//     * @return 检索返回值
//     */
//    private RespectiveRetrieveResult getRetrieveResultByDataId(String dataId, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId) {
//        String su = getSuInfo(beId);
//        SUService suService = SpringBeanUtils.getBean(SUService.class);
//        IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//        if (suService.isLocalInvoke(su)) {
//            // su信息在当前应用，走本地
//            try {
//                iBefSessionManager.createSession();
//                IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//                setLcpRepositoryVariables(variables, standardLcp);
//                return standardLcp.retrieve(dataId, retrieveParam);
//            } finally {
//                iBefSessionManager.closeCurrentSession();
//            }
//        }
//        // 远程获取数据
//        String serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByBeId";
//        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
//        parameters.put("dataId", dataId);
//        parameters.put("retrieveParam", retrieveParam);
//        parameters.put("beId", beId);
//        parameters.put("variables", variables);
//        String result = rpcClient.invoke(String.class, serviceId, su, parameters, null);
//        try {
//            iBefSessionManager.createSession();
//            IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//            RespectiveRetrieveResult retrieveResult = new RespectiveRetrieveResult();
//            retrieveResult.setData(standardLcp.deSerializeData(result));
//            return retrieveResult;
//        } finally {
//            iBefSessionManager.closeCurrentSession();
//        }
//    }
//
//    private List<IEntityData> getQueryResultByFilter(EntityFilter filter, Map<String, String> variables, String beId) {
//        String su = getSuInfo(beId);
//        SUService suService = SpringBeanUtils.getBean(SUService.class);
//        if (suService.isLocalInvoke(su)) {
//            IBefSessionManager iBefSessionManager = SpringBeanUtils.getBean(IBefSessionManager.class);
//            try {
//                iBefSessionManager.createSession();
//                IStandardLcp standardLcp = SpringBeanUtils.getBean(ILcpFactory.class).createLcpByBEId(beId);
//                setLcpRepositoryVariables(variables, standardLcp);
//                return standardLcp.query(filter);
//            } finally {
//                iBefSessionManager.closeCurrentSession();
//            }
//        }
//        // 远程获取数据
//        String serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcQueryByBeId";
//        LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
//        parameters.put("filter", filter);
//        parameters.put("beId", beId);
//        parameters.put("variables", variables);
//        Type type = new Type(List.class, String.class);
//        List<String> results = (List<String>) rpcClient.invoke(type, serviceId, su, parameters, null);
//        List<IEntityData> entityDataList = dealEntityData(results, beId);
//        return entityDataList;
//    }
//
//    private void setLcpRepositoryVariables(Map<String, String> variables, IStandardLcp lcp) {
//        if (variables == null || variables.isEmpty()) {
//            return;
//        }
//        for (Map.Entry<String, String> entry : variables.entrySet()) {
//            lcp.setRepositoryVariables(entry.getKey(), entry.getValue());
//        }
//    }
//
//}
