/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.spi.action.determination.IDeterminationExecutor;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors.AbstractBeforeSaveDtmAdaptor;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;

import java.util.Arrays;
import java.util.List;

public class BefB4SaveDtmAdaptor extends BefAftModifyDtmAdaptor {
    //region 第一版构造函数
    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes) {
        super(name, type, changeTypes, null, null);
    }

    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                               List<String> elements) {
        super(name, type, changeTypes, elements, null);
    }

    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                               List<String> elements,
                               List<ChildElementsTuple> childElements) {
        super(name, type, changeTypes, elements, childElements);
    }
    //endregion

    //region 增加是否执行一次构造函数
    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                               boolean runOnce) {
        this(name, type, changeTypes, runOnce, null, null);
    }

    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes,
                               boolean runOnce, List<String> elements) {
        this(name, type, changeTypes, runOnce, elements, null);
    }

    public BefB4SaveDtmAdaptor(String name, Class type, List<ChangeType> changeTypes, boolean runOnce,
                               List<String> elements, List<ChildElementsTuple> childElements) {
        super(name, type, changeTypes, runOnce, elements, childElements);
    }
    //endregion
}
