/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.be.builtinimpls;

import com.inspur.edp.bef.core.tcc.builtinimpls.BefTccHandlerAssembler;
import com.inspur.edp.cef.core.datatype.CefEntityCacheInfo;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;

public class BeEntityCacheInfo extends CefEntityCacheInfo {
    private IEntityRTDtmAssembler onCancelDtmAssembler;
    private IEntityRTValidationAssembler afterSaveValAssembler;
    private IEntityRTDtmAssembler afterLoadingDtmAssembler;
    private IEntityRTDtmAssembler afterQueryDtmAssembler;
    private BefTccHandlerAssembler tccAssembler;

    public IEntityRTDtmAssembler getOnCancelDtmAssembler() {
        return onCancelDtmAssembler;
    }

    public void setOnCancelDtmAssembler(
            IEntityRTDtmAssembler onCancelDtmAssembler) {
        this.onCancelDtmAssembler = onCancelDtmAssembler;
    }

    public IEntityRTDtmAssembler getAfterLoadingDtmAssembler() {
        return afterLoadingDtmAssembler;
    }

    public void setAfterLoadingDtmAssembler(
            IEntityRTDtmAssembler afterLoadingDtmAssembler) {
        this.afterLoadingDtmAssembler = afterLoadingDtmAssembler;
    }

    public IEntityRTDtmAssembler getAfterQueryDtmAssembler() {
        return afterQueryDtmAssembler;
    }

    public void setAfterQueryDtmAssembler(
            IEntityRTDtmAssembler afterQueryDtmAssembler) {
        this.afterQueryDtmAssembler = afterQueryDtmAssembler;
    }

    public IEntityRTValidationAssembler getAfterSaveValAssembler() {
        return this.afterSaveValAssembler;
    }

    public void setAfterSaveValAssembler(IEntityRTValidationAssembler afterSaveValAssembler) {
        this.afterSaveValAssembler = afterSaveValAssembler;
    }

    public BefTccHandlerAssembler getTccAssembler() {
        return tccAssembler;
    }

    public void setTccAssembler(BefTccHandlerAssembler ass) {
        tccAssembler = ass;
    }
}
