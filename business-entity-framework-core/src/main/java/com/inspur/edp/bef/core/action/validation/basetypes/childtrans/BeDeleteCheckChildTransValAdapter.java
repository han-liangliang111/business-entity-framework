package com.inspur.edp.bef.core.action.validation.basetypes.childtrans;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;

/**
 * @author: wangmj
 * @date: 2024/11/1
 * @Description:
 **/
public class BeDeleteCheckChildTransValAdapter extends BeChildTransValAdapter {
    public BeDeleteCheckChildTransValAdapter(String name, String childCode) {
        super(name, childCode);
    }

    @Override
    protected void doExecute(IValidationContext compContext, IBENodeEntity itemEntity) {
        itemEntity.deleteCheckValidate();

    }
}
