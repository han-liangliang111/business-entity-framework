package com.inspur.edp.bef.core.validation;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;

public class DeleteCheckValidationAdaptor implements IValidation {

    private String befConfig;
    private String nodeCode;

    public DeleteCheckValidationAdaptor(String befConfig, String nodeCode) {
        this.befConfig = befConfig;
        this.nodeCode = nodeCode;
    }

    public final boolean canExecute(IChangeDetail change) {
        return change != null && change instanceof DeleteChangeDetail;
    }

    public final void execute(ICefValidationContext context, IChangeDetail change) {
        String dbCode = befConfig + "/" + nodeCode;
        DeleteCheckScopeNodeParameter parameter = new DeleteCheckScopeNodeParameter(
                (IValidationContext) context, dbCode);
        ((IValidationContext) context).getBEContext().addScopeParameter(parameter);
    }
}
