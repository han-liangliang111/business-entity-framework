package com.inspur.edp.bef.core.action.delete;

import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.lcp.ResponseContext;
import com.inspur.edp.bef.core.scope.BefScope;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.cef.api.message.AggregateBizMsgException;
import com.inspur.edp.cef.api.message.BizMessage;
import com.inspur.edp.cef.api.message.IBizMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author: wangmj
 * @date: 2024/11/1
 * @Description: 删除检查器
 **/
public class DeleteChecker {
    public void check(List<IBusinessEntity> entities, ResponseContext responseContext){
        BefScope valScope = new BefScope(FuncSessionManager.getCurrentSession());
        valScope.beginScope();
        try {
            deleteCheckValidate(entities);
            valScope.setComplete();
        } catch (Throwable e) {
            valScope.setAbort();
            throw e;
        }

        List<IBizMessage> msgList = new ArrayList<>();
        if (!responseContext.hasErrorMessage()) {
            return;
        }
        msgList.addAll((responseContext.getErrorMessages().stream()
                .map(msg -> new BizMessage() {{
                    setMessageParams(msg.getMessageParams());
                    setMessageFormat(msg.getMessageFormat());
                    setLevel(msg.getLevel());
                }}).collect(Collectors.toList())));
        msgList.addAll(0, responseContext.getErrorMessages());
        throw new AggregateBizMsgException(msgList);
    }

    public final void deleteCheckValidate(List<IBusinessEntity> entities) {
        entities.forEach(entity -> entity.deleteCheckValidate());
    }
}
