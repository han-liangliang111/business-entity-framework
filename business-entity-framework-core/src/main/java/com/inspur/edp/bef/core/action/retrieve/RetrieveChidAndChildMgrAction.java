/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveChildResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.boot.model.source.spi.IdentifierSourceSimple;
import org.yaml.snakeyaml.events.Event.ID;

public class RetrieveChidAndChildMgrAction extends AbstractManagerAction<RetrieveChildResult> {

    //region 字段属性
    private RetrieveParam para;


    //endregion Consturctor
    private java.util.List<String> nodeCodes;
    private java.util.List<String> hierachyIdList;
    private ArrayList<String> ids;
    public RetrieveChidAndChildMgrAction(IBEManagerContext managerContext, List<String> nodeCodes,
                                         List<String> hierachyIdList, ArrayList<String> ids, RetrieveParam para) {
        super(managerContext);
        ActionUtil.requireNonNull(nodeCodes, "nodeCodes");
        ActionUtil.requireNonNull(hierachyIdList, "hierachyIdList");
        ActionUtil.requireNonNull(ids, "ids");
        this.nodeCodes = nodeCodes;
        this.hierachyIdList = hierachyIdList;
        this.para = para;
        this.ids = ids;
    }

    //endregion 字段属性


    //region Override

    @Override
    public final void execute() {
        if (nodeCodes.size() != hierachyIdList.size()) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5043);
        }

        getBEManagerContext().checkAuthority("Retrieve");
//    AuthorityUtil.checkAuthority("Retrieve");
//    FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Retrieve");
        IBusinessEntity be = getBEManagerContext().getEntity(hierachyIdList.get(0), para.getAddDataToCache());
        be.retrieve(para);
        setResult(be.retrieveChild(nodeCodes, hierachyIdList, ids, para));
        if (!para.getAddDataToCache() && !((BEContext) be.getBEContext()).hasChange()) {
            ((CoreBEContext) be.getBEContext()).clearData();
        }
    }

    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return null;
    }
}
