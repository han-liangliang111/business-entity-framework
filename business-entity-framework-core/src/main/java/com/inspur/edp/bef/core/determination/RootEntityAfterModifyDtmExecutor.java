/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.DtmExecutionHistory;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class RootEntityAfterModifyDtmExecutor extends EntityDeterminationExecutor {
    private DtmExecutionHistory execHistory;

    public RootEntityAfterModifyDtmExecutor(IEntityRTDtmAssembler assembler, ICefEntityContext entityCtx) {
        super(assembler, entityCtx);
        execHistory = new DtmExecutionHistory();
    }

    @Override
    public void execute() {
        CoreBEContext beCtx = (CoreBEContext) getEntityContext();
        setContext(GetContext());
        setChange(beCtx.getCurrentTemplateChange());
        while (getChange() != null) {
            execHistory.beginGroup("AfterModify");

            executeBelongingDeterminations();
            executeDeterminations();
            executeChildDeterminations();
            //在联动计算和校验都执行完后，合并templeChange
            //beCtx.acceptTempChange();
            setChange(beCtx.getListenerChange());
            beCtx.mergeIntoInnerChange();
            beCtx.acceptListenerChange();
        }
    }
}
