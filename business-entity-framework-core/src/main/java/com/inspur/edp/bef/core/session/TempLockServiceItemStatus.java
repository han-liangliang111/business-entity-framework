package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.core.runtimeconfig.RuntimeConfigRepository;
import com.inspur.edp.bef.entity.befruntimeconfig.GspBefRuntimeConfig;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * 由于此前基础BE和扩展BE分别使用不同的LockServiceItem的原因，导致不同用户对于同一条数据分别使用基础和扩展Lcp时，都能获得锁并修改数据。
 * 修改后，原先不同session分别使用基础和扩展lcp分别获得锁的场景将报错（修改前由于是不同的锁，都能成功），
 * 因此添加此开关快速切换从而避免项目报错后难以立即处理。出错后应推动相关功能修改使用场景。
 * 该类建议确认所有项目都更新后未引起问题后删除。
 *
 * @author Kaixuan Shi
 * @see FuncSession#getLockServiceItems()
 * @since 2024/2/4
 */
@Path("")
@Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
@Slf4j
public class TempLockServiceItemStatus {

    private static final String LOCK_SERVICE_ITEM_SHARE_CONFIG_ID = "lock-service-item-share";
    public static boolean isUseBaseBeLockServiceItem = true;
    private final RuntimeConfigRepository runtimeConfigRepository;

    public TempLockServiceItemStatus(RuntimeConfigRepository runtimeConfigRepository) {
        this.runtimeConfigRepository = runtimeConfigRepository;
        try {
            GspBefRuntimeConfig runtimeConfig = runtimeConfigRepository.findById(LOCK_SERVICE_ITEM_SHARE_CONFIG_ID).orElse(null);
            if (runtimeConfig != null && "false".equals(runtimeConfig.getRuntimeConfig())) {
                isUseBaseBeLockServiceItem = false;
            }
        } catch (Throwable e) {
            log.warn("获取数据库值lock-service-item-status失败，使用默认值", e);
        }
    }

    @GET
    @Path("/")
    public String refresh() {
        GspBefRuntimeConfig runtimeConfig = this.runtimeConfigRepository.findById(LOCK_SERVICE_ITEM_SHARE_CONFIG_ID).orElse(null);
        String result = "";
        if (runtimeConfig == null) {
            return "数据库表GspBefRuntimeConfig中未找到id为[" + LOCK_SERVICE_ITEM_SHARE_CONFIG_ID + "]的配置。当前程序设置：" + isUseBaseBeLockServiceItem;
        }
        if ("false".equals(runtimeConfig.getRuntimeConfig())) {
            isUseBaseBeLockServiceItem = false;
        } else if ("true".equals(runtimeConfig.getRuntimeConfig())) {
            isUseBaseBeLockServiceItem = true;
        } else {
            result = "请录入正确的数据库值（\"true\" 或者 \"false\"），当前数据库字段值：" + runtimeConfig.getRuntimeConfig() + "。";
        }
        return result + "当前程序设置：" + isUseBaseBeLockServiceItem;
    }
}
