/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.query;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.determination.BeforeQueryDtmExecutor;
import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.BefRunTimeException;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.bef.spi.action.assembler.query.QueryMgrActionAssembler;
import com.inspur.edp.bef.spi.event.queryactionevent.BefQueryEventBroker;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.repository.IRootRepository;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.ExpressCompareType;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

@Slf4j
public class QueryManagerAction extends AbstractManagerAction<List<IEntityData>> {
    protected String nodeCode;
    protected EntityFilter filter;
    private EntityFilter privateFinalFilter;

    // TODO:wj-Query考虑权限问题
    public QueryManagerAction(
            IBEManagerContext managerContext, String nodeCode, EntityFilter filter) {
        super(managerContext);
        this.nodeCode = nodeCode;
        this.filter = filter;
    }

    public static java.util.ArrayList<AuthorityInfo> getAuthorityInfos(BEManagerContext beManagerContext) {
        if (!AuthorityUtil.hasAuthority(beManagerContext))
            return new ArrayList<>();
        List<AuthorityInfo> rez = AuthorityUtil.getAuthorityInfos(beManagerContext.getSessionItem());
        return rez instanceof ArrayList ? (ArrayList) rez : new ArrayList<>(rez);
    }

    protected final EntityFilter getFinalFilter() {
        return privateFinalFilter;
    }

    private void setFinalFilter(EntityFilter value) {
        privateFinalFilter = value;
    }

    @Override
    public void execute() {
        EntityFilter customEntityFilter = getCustomEntityFilter();
        setFinalFilter(mergeEntityFilter(filter, customEntityFilter));

        // 查询前联动计算自定义过滤条件
        try {
            BefQueryEventBroker.fireBeforeTotalCXQDeterminate(filter, customEntityFilter, getFinalFilter());
            // 查询前联动计算自定义过滤条件
            int count = Determinate();
            BefQueryEventBroker.fireAfterTotalCXQDeterminate(getFinalFilter(), count);
        } catch (Exception e) {
            BefQueryEventBroker.fireQueryUnnormalStop(e);
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5013, e);
        }

        // 执行查询
        IRootRepository repository = getBEManagerContext().getBEManager().getRepository();
        List<IEntityData> datas;
        try {
            BefQueryEventBroker.firebeforeCoreQuery(getFinalFilter());
            datas = repository.query(nodeCode, getFinalFilter(), getAuthorityInfosWithQuery((BEManagerContext) getBEManagerContext())).stream()
                    .map(item -> (IEntityData) item)
                    .collect(Collectors.toList());
            if(log.isInfoEnabled()){
                log.info("查询{}的数据量：{}", this.nodeCode, datas == null ? 0 : datas.size() );
            }
            BefQueryEventBroker.fireafterCoreQuery(getBEManagerContext().getBEManager(), datas);
        } catch (Exception e) {
            BefQueryEventBroker.fireQueryUnnormalStop(e);
            throw new BefRunTimeException(e);
        }

        // 查询后联动计算修改虚拟字段
        try {
            BefQueryEventBroker.fireBeforeTotalCXHDeterminate(getBEManagerContext().getBEManager(), datas);
            if (datas != null) {
                for (IEntityData data : datas) {
                    try {
                        BusinessEntity entity =
                                (BusinessEntity)
                                        getBEManagerContext().getBEManager().createSessionlessEntity(data.getID());
                        entity.getBEContext().setCurrentData(data);
                        entity.afterQueryDeterminate();
                    } catch (Exception e) {
                        BefQueryEventBroker.fireQueryUnnormalStop(e);
                        throw new BefRunTimeException(e);
                    }
                }
            }
            BefQueryEventBroker.fireafterTotalCXHDeterminate(getBEManagerContext().getBEManager(), datas, (datas == null ? 0 : datas.size()));
        } catch (Exception e) {
            BefQueryEventBroker.fireQueryUnnormalStop(e);
            throw new BefRunTimeException(e);
        }
        setResult(datas);
    }

    private int Determinate() {
        //非主节点？？？
        if (!StringUtils.isEmpty(nodeCode) && getBEManagerContext().getBEManager().getRootEntityResInfo() != null
                && !StringUtils.isEmpty(getBEManagerContext().getBEManager().getRootEntityResInfo().getEntityCode())
                && !nodeCode.equalsIgnoreCase(getBEManagerContext().getBEManager().getRootEntityResInfo().getEntityCode())) {
            return 0;
        }
        IEntityRTDtmAssembler queryDtmAss =
                ((BEManager) getBEManagerContext().getBEManager()).getBeforeQueryDtmAssembler();
        if (queryDtmAss == null) {
            return 0;
        }
        BeforeQueryDtmExecutor dtmExecutor =
                new BeforeQueryDtmExecutor(queryDtmAss, getBEManagerContext());
        int count = dtmExecutor.execute();
        if(count > 0 && log.isInfoEnabled()){
            log.info("执行完对象[{}]的查询前联动计算,查询前组织的过滤条件:{}" , this.nodeCode, SerializerUtil.writeObject(dtmExecutor.getFilter()));
        }
        if (dtmExecutor.getFilter() != null) {
            setFinalFilter(mergeEntityFilter(getFinalFilter(), dtmExecutor.getFilter()));
        }
        return count;
    }

    protected ArrayList<AuthorityInfo> getAuthorityInfosWithQuery(BEManagerContext beManagerContext) {
        return getAuthorityInfos(beManagerContext);
    }

    @Override
    protected final IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory().getQueryMgrActionAssembler(getBEManagerContext(), filter);
    }

    /**
     * 通过QueryMgrAction对应的组装器，获取自定义过滤条件
     *
     * @return 自定义过滤条件
     */
    private EntityFilter getCustomEntityFilter() {
        QueryMgrActionAssembler assembler =
                getMgrActionAssemblerFactory().getQueryMgrActionAssembler(getBEManagerContext(), filter);
        return assembler.getNewEntityFilter();
    }

    /**
     * 合并当前过滤条件及装配器中传入的自定义过滤条件 （此处仅考虑过滤和排序的修改，无修改分页的需求）
     *
     * @param currentEntityFilter 构造函数中的过滤条件
     * @param customEntityFilter  组装器中传入的自定义过滤条件
     * @return
     */
    private EntityFilter mergeEntityFilter(
            EntityFilter currentEntityFilter, EntityFilter customEntityFilter) {
        if (customEntityFilter == null) {
            return currentEntityFilter;
        }

        EntityFilter entityFilter = currentEntityFilter;
        if (customEntityFilter.getFilterConditions() != null) {
            entityFilter.addFilterConditions(customEntityFilter.getFilterConditions());
        }
        if (customEntityFilter.getSortConditions() != null) {
            entityFilter.addSortConditions(customEntityFilter.getSortConditions());
        }
        return entityFilter;
    }
}
