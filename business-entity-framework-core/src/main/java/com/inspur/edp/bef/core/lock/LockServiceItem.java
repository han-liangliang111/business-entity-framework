/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.lock;

import com.inspur.edp.bef.api.befruntimeconfig.RuntimeConService;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.lock.LockAdapter.LockEntity;
import com.inspur.edp.bef.core.session.transaction.IBefTransactionItem;
import com.inspur.edp.cdp.common.utils.spring.SpringUtil;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.commonmodel.core.session.distributed.RootEditToken;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

//成功后合并模式
@Slf4j
public class LockServiceItem implements IBefTransactionItem {

    //用到这个beType处理增量了。。。
    private final String beType;//所有扩展be对应的是基础beType，而非扩展beType，因为数据共用，锁也要共用
    private final LockServiceItem upper;

    private List<GroupDataIdsPair> baseLocks;
    private List<GroupDataIdsPair> currentLocks;
    private RootEditToken token;


    public LockServiceItem(String beType) {
        this(beType, null);
    }

    public LockServiceItem(String beType, LockServiceItem upper) {
        this.beType = beType;
        this.upper = upper;
    }

    /**
     * 返回父级Item
     */
    public LockServiceItem getUpper() {
        return this.upper;
    }

    /**
     * 获取批量加锁的所数据（分组编号，数据列表）
     */
    @Nonnull
    private List<GroupDataIdsPair> getLocks() {
        if (currentLocks == null) {
            if (baseLocks == null) {
                baseLocks = new ArrayList<>();
            }
            currentLocks = new ArrayList<>(baseLocks);
        }
        return currentLocks;
    }

    public boolean getLockState(String dataId, RefObject<String> userName) {
        for (GroupDataIdsPair groupLock : getLocks()) {
            if (groupLock.getDataIds().contains(dataId)) {
                userName.argvalue = null;
                return true;
            } else if (groupLock.getFailedDataIds().containsKey(dataId)) {
                userName.argvalue = groupLock.getFailedDataIds().get(dataId);
                return false;
            }
        }

        if (upper != null) {
            return upper.getLockState(dataId, userName);
        } else {
            userName.argvalue = null;
            return false;
        }
    }

    /**
     * 新增锁
     */
    public void addLock(String dataId) {
        ArrayList<String> list = new ArrayList<>(1);
        list.add(dataId);
        addLocks(list);
    }

    public String getLockType() {
        return beType;
    }

    /**
     * 新增锁
     */
    @Transactional
    public boolean addLocks(List<String> dataIds) {
        String lockType = getLockType();
        //获取未加锁的Id列表
        List<String> unlockedDataIds = getUnlockIds(dataIds);
        //如果未加锁的列表为空，则当前已全部加锁
        if (unlockedDataIds.isEmpty()) {
            return true;
        }
        GroupDataIdsPair beLocks = getLocks().stream().findFirst().orElseGet(() -> {
            GroupDataIdsPair rez = new GroupDataIdsPair();
            getLocks().add(rez);
            return rez;
        });
        //调用caf加锁组件
        boolean success;
        List<LockAdapter.LockEntity> lockEntities = null;
        if (LockAdapter.isMobileClient()) {
            success = !LockAdapter.getInstance().isLocked(lockType, unlockedDataIds);
        } else {
            if (DotNetToJavaStringHelper.isNullOrEmpty(beLocks.getGroupId())) {
                RefObject<List<LockEntity>> tempRef_lockEntities = new RefObject<>(
                        lockEntities);
                RefObject<String> groupId = new RefObject<>("");

                success = LockAdapter.getInstance()
                        .addLock(lockType, unlockedDataIds, groupId, tempRef_lockEntities);
                lockEntities = tempRef_lockEntities.argvalue;
                if (success) {
                    beLocks.setGroupId(groupId.argvalue);
                }
            } else {
                RefObject<List<LockEntity>> tempRef_lockEntities2 = new RefObject<>(
                        lockEntities);
                success = LockAdapter.getInstance()
                        .addLock2Group(lockType, unlockedDataIds, beLocks.getGroupId(),
                                tempRef_lockEntities2);
                lockEntities = tempRef_lockEntities2.argvalue;
            }
            //兼容自己锁自己
            if (!success) {
                RuntimeConService service = SpringUtil.getBean(RuntimeConService.class);
                if (service.isUseIgnoreLock()) {
                    success = LockAdapter.getInstance().addIgnoreLock(lockType, unlockedDataIds, beLocks.getGroupId());
                }
            }
        }
        if (success) {
            unlockedDataIds.forEach(id -> beLocks.getDataIds().add(id));
            if (token != null) {
                FuncSessionItemIncrement inc = token.getItemIncrement(beType);
                if (inc.getLockIds() == null) {
                    inc.setLockIds(new ArrayList<>());
                }
                inc.getLockIds().add(beLocks.getGroupId());
            }
        } else if (lockEntities != null) {
            for (LockAdapter.LockEntity entity : lockEntities) {
                beLocks.getFailedDataIds().put(entity.getDataId(), entity.getUserName());
            }
        }
        return success;
    }

    /**
     * 获取未加锁的数据Id列表
     */
    private List<String> getUnlockIds(List<String> dataIds) {
        List<String> rez = new ArrayList<>(dataIds);
        return innerGetUnlockIds(rez);
    }

    /**
     * 获取未加锁的数据Id列表
     */
    private List<String> innerGetUnlockIds(List<String> dataIds) {
        for (GroupDataIdsPair pair : getLocks()) {
            if (pair.getDataIds().isEmpty()) {
                continue;
            }
            dataIds.removeIf(item -> pair.getDataIds().contains(item));
        }
        if (upper != null) {
            return upper.innerGetUnlockIds(dataIds);
        } else {
            return dataIds;
        }
    }

    public void releaseLocks() {
        //删除全部锁, 在保存成功后/cancel/cleanup 调用, 此类行为穿透所有事务.
        releaseCurrentItemLock();
        if (upper != null) {
            upper.releaseLocks();
        } else if (token != null) {
            FuncSessionItemIncrement inc = token.getItemIncrement(beType);
            inc.setLockIds(null);
        }
    }

    private void releaseCurrentItemLock() {
        if (currentLocks != null) {
            for (GroupDataIdsPair item : currentLocks) {
                if (item.getGroupId() != null) {
                    LockAdapter.getInstance().releaseLock(item.getGroupId());
                }
            }
            currentLocks = null;
            baseLocks = null;
        } else if (baseLocks != null) {
            for (GroupDataIdsPair item : baseLocks) {
                if (item.getGroupId() != null) {
                    LockAdapter.getInstance().releaseLock(item.getGroupId());
                }
            }
            baseLocks = null;
        }
    }

    public void rejectCurrentLocks() {
        if (currentLocks == null) {
            return;
        }
        currentLocks.stream().filter(item -> !baseLocks.contains(item)).forEach(item -> {
            if (item.getGroupId() != null) {
                LockAdapter.getInstance().releaseLock(item.getGroupId());
            }
        });
        currentLocks = null;
    }

    public void acceptCurrentLocks() {
        if (currentLocks != null) {
            baseLocks = currentLocks;
            currentLocks = null;
        }
    }

    @Override
    @NotNull
    public void commit(IBefTransactionItem upper) {
        if (upper == null)
            return;

        LockServiceItem up = (LockServiceItem) upper;
        //将自身的锁Id添加到upper之上
        List<GroupDataIdsPair> locks = this.getLocks();
        if (up.token != null) {
            if (!locks.isEmpty()) {
                up.token.getItemIncrement(beType).setLockIds(
                        locks.stream().map(GroupDataIdsPair::getGroupId).filter(Objects::nonNull)
                                .collect(Collectors.toList()));
            }
        }
        up.getLocks().addAll(locks);
    }

    @Override
    public void rollBack() {
        releaseCurrentItemLock();
    }

    /**
     * 增加新的SessionItems层次时会调用begin拷贝一下
     */
    @Override
    public LockServiceItem begin() {
        return new LockServiceItem(beType, this);
    }

    @Override
    public void beginEdit(RootEditToken token) {
        this.token = token;
    }

    @Override
    public void notifySave() {
    }

    @Override
    public void endEdit(RootEditToken token) {
        this.token = null;
    }

    public String upgradeLock4Tcc() {
        // BatchLock锁不支持LockedScope，不必升级
        Optional<GroupDataIdsPair> pair = getLocks().stream().findFirst();
        return pair.map(GroupDataIdsPair::getGroupId).orElse(null);
    }

    public void initLockState4TccSecondPhase(String groupId, List<String> dataIds) {
        getLocks().add(new GroupDataIdsPair() {{
            setGroupId(groupId);
            getDataIds().addAll(dataIds);
        }});
    }

    public void recover(FuncSessionItemIncrement increment) {
        if (increment.getLockIds() != null) {
            if (baseLocks == null) {
                baseLocks = new ArrayList<>();
                ((ArrayList<?>) baseLocks).ensureCapacity(increment.getLockIds().size());
            }
            for (String id : increment.getLockIds()) {
                GroupDataIdsPair pair = new GroupDataIdsPair();
                pair.setGroupId(id);
                pair.getDataIds().addAll(LockAdapter.getInstance().getDataIdsByGroupId(id));
                baseLocks.add(pair);
            }
            currentLocks = null;
        }
    }

    public void reset() {
        if (baseLocks != null) {
            baseLocks = null;
        }
        if (currentLocks != null) {
            currentLocks = null;
        }
    }

    /**
     * region inner class GroupDataIdsPair
     */
    private static class GroupDataIdsPair {

        /**
         * 被加锁的数据编号列表
         */
        private final HashSet<String> dataIds = new HashSet<>();
        /**
         * 失败的数据编号列表
         * key：dataId
         * value:锁定当前数据的用户名
         */
        private final HashMap<String, String> failedDataIds = new HashMap<>();
        /**
         * 批量锁分组编号
         */
        private String groupId;

        public final String getGroupId() {
            return groupId;
        }

        public final void setGroupId(String value) {
            groupId = value;
        }

        public final HashSet<String> getDataIds() {
            return dataIds;
        }

        public final HashMap<String, String> getFailedDataIds() {
            return failedDataIds;
        }

    }
}
