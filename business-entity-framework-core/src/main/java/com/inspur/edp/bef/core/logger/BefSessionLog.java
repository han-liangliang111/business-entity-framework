package com.inspur.edp.bef.core.logger;

import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
import io.iec.edp.caf.rpc.api.annotation.RpcParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface BefSessionLog {
    /**
     * 开启Session日志追踪
     */
    String openLoggerTrace();

    /**
     * 在日志中获取相关堆栈信息
     */
    String openStackTrace();

    String closeLoggerTrace();

    String closeStackTrace();

    String enableSetRedisInfo();

    String disEnableSetRedisInfo();

    /**
     * 获取所有的session跟踪信息
     *
     * @return
     */
    List<Map<String, Object>> getSessionTraceInfo(FilterParam filterParam);


}
