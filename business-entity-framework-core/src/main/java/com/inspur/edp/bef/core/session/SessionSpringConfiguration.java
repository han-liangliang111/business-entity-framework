package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.core.runtimeconfig.RuntimeConfigRepository;
import io.iec.edp.caf.rest.RESTEndpoint;
import org.springframework.context.annotation.Bean;

/**
 * @author Kaixuan Shi
 * @since 2024/2/4
 */
public class SessionSpringConfiguration {

    @Bean("com.inspur.edp.bef.core.session.SessionSpringConfiguration.publishTempLockServiceItemStatus")
    public RESTEndpoint publishTempLockServiceItemStatus(RuntimeConfigRepository runtimeConfigRepository) {
        return new RESTEndpoint("/platform/common/v1.0/lock/service/status",
                new TempLockServiceItemStatus(runtimeConfigRepository));
    }
}
