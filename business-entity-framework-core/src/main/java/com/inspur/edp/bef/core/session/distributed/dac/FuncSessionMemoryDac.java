/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed.dac;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.distributed.close.AbnormalClosingUtil;
import com.inspur.edp.bef.core.session.distributed.common.FuncInstLockProvider;
import com.inspur.edp.bef.core.session.distributed.common.NoLock;
import com.inspur.edp.bef.core.session.distributed.common.SessionLockProvider;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;
import io.iec.edp.caf.boot.context.CAFContext;

import java.util.concurrent.locks.Lock;

import org.springframework.util.StringUtils;

public class FuncSessionMemoryDac implements FuncSessionDac {
    private static FuncSessionMemoryDac instance = new FuncSessionMemoryDac();

    public static FuncSessionMemoryDac getInstance() {
        return instance;
    }

    public Lock getFuncInstanceLock(String funcInstId) {
        return NoLock.getInstance();
//    if (StringUtils.isEmpty(funcInstId)) {
//      return NoLock.getInstance();
//    }
//    return FuncInstLockProvider.get(funcInstId);
    }

//  private final ConcurrentHashMap<String, ReentrantLock> sessionLocks = new ConcurrentHashMap<>(512);

    public Lock getSessionLock(String session) {
        return SessionLockProvider.getNonDistLock(session);
//    return sessionLocks.computeIfAbsent(session, (key) -> new ReentrantLock());
    }

    @Override
    public Lock getLocalSessionLock(String session) {
        return getSessionLock(session);
    }

    @Override
    public boolean isLatest(String sessionId, int version) {
        return true;
    }

    @Override
    public String getSessionListByToken(String token) {
        return AbnormalClosingUtil.getInstance().getSessionId(token, CAFContext.current.getCurrentSU());
    }

    @Override
    public void addSessionWithToken(String userSessionId, String token, String sessionId) {
        AbnormalClosingUtil.getInstance().setSessionRelationCacheInBiz(token, sessionId,
                CAFContext.current.getCurrentSU());
    }

    @Override
    public void removeSessionWithToken(String userSessionId, String token, String sessionId) {
        AbnormalClosingUtil.getInstance().removeSessionRelationCacheInBiz(token, sessionId,
                CAFContext.current.getCurrentSU());
    }

    @Override
    public void removeSession(String sessionId) {
        SessionLockProvider.remove(sessionId);
    }

    @Override
    public FuncSession recover(String id, FuncSession existing) {
        return null;
    }

    @Override
    public void update(FuncSession session, FuncSessionIncrement value) {
    }
}
