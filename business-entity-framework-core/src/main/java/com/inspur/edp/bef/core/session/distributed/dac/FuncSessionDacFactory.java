/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed.dac;

import com.inspur.edp.bef.core.exception.ExceptionCode;
import com.inspur.edp.bef.core.session.distributed.DistributedConfig;
import com.inspur.edp.bef.core.session.distributed.DistributedConfigProvider;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.commonmodel.core.session.tableentity.ConnectType;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionConfig;
import com.inspur.edp.commonmodel.core.session.serviceinterface.SessionConfigService;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

public class FuncSessionDacFactory {

    public static FuncSessionDac getDac() {
//    DistributedConfig config = DistributedConfigProvider.getInstance().getConfig();
        SessionConfig config = SpringBeanUtils.getBean(SessionConfigService.class).getSessionConfig();
        return getDac(config);
    }

    public static FuncSessionDac getDac(SessionConfig config) {
        if (config == null || config.getConnectInfo() == null || !config.getConnectInfo().isUseCache()) {
            return FuncSessionMemoryDac.getInstance();
        } else if (config.getConnectInfo().getConnectType() == ConnectType.DataBase) {
            return FuncSessionDbDac.getInstance();
        }

        throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_5072);
    }
}
