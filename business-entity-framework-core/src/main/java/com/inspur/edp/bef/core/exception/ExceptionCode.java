package com.inspur.edp.bef.core.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/15
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 builtin:3001开头 spi:4001开头 core:5001开头
    public static final String BEF_RUNTIME_5001 = "BEF_RUNTIME_5001";
    /**
     * 要删除的数据：{0}不存在，请检查：１.删除数据的ID是否正确　２.数据是否已被删除　３.如果使用了负载均衡设备，请检查负载均衡策略是否配置为粘性。
     */
    public static final String BEF_RUNTIME_5002 = "BEF_RUNTIME_5002";
    /**
     * BizContext发生变化在{0}
     */
    public static final String BEF_RUNTIME_5003 = "BEF_RUNTIME_5003";
    public static final String BEF_RUNTIME_5004 = "BEF_RUNTIME_5004";
    /**
     * 要删除的数据对应主表数据不存在，请检查：１.删除数据的ID是否正确　２.数据是否已被删除　３.如果使用了负载均衡设备，请检查负载均衡策略是否配置为粘性。
     */
    public static final String BEF_RUNTIME_5005 = "BEF_RUNTIME_5005";
    /**
     * 要复制的数据{0}不存在,请检查：１.复制数据的ID是否正确　２.数据是否已被删除　３.如果使用了负载均衡设备，请检查负载均衡策略是否配置为粘性。
     */
    public static final String BEF_RUNTIME_5006 = "BEF_RUNTIME_5006";
    /**
     * 要编辑的数据：{0}不存在，请检查：１.编辑数据的ID是否正确　２.数据是否已被删除　３.如果使用了负载均衡设备，请检查负载均衡策略是否配置为粘性。
     */
    public static final String BEF_RUNTIME_5007 = "BEF_RUNTIME_5007";
    /**
     * 调用modify方法修改数据时，不能传入新增变更集
     */
    public static final String BEF_RUNTIME_5008 = "BEF_RUNTIME_5008";
    public static final String BEF_RUNTIME_5009 = "BEF_RUNTIME_5009";
    public static final String BEF_RUNTIME_5010 = "BEF_RUNTIME_5010";
    public static final String BEF_RUNTIME_5011 = "BEF_RUNTIME_5011";
    public static final String BEF_RUNTIME_5012 = "BEF_RUNTIME_5012";
    /**
     * BE查询前联动执行异常
     */
    public static final String BEF_RUNTIME_5013 = "BEF_RUNTIME_5013";
    public static final String BEF_RUNTIME_5014 = "BEF_RUNTIME_5014";
    public static final String BEF_RUNTIME_5015 = "BEF_RUNTIME_5015";
    public static final String BEF_RUNTIME_5016 = "BEF_RUNTIME_5016";
    public static final String BEF_RUNTIME_5017 = "BEF_RUNTIME_5017";
    public static final String BEF_RUNTIME_5018 = "BEF_RUNTIME_5018";
    /**
     * 当前不存在ID:{0}的Session,关闭Session操作失败
     */
    public static final String BEF_RUNTIME_5019 = "BEF_RUNTIME_5019";
    public static final String BEF_RUNTIME_5020 = "BEF_RUNTIME_5020";
    /**
     * 不能重复执行保存前联动计算
     */
    public static final String BEF_RUNTIME_5021 = "BEF_RUNTIME_5021";
    public static final String BEF_RUNTIME_5022 = "BEF_RUNTIME_5022";
    /**
     * session不存在, 请先创建BefSession
     */
    public static final String BEF_RUNTIME_5023 = "BEF_RUNTIME_5023";
    /**
     * 用户上下文id为空
     */
    public static final String BEF_RUNTIME_5024 = "BEF_RUNTIME_5024";
    public static final String BEF_RUNTIME_5025 = "BEF_RUNTIME_5025";
    public static final String BEF_RUNTIME_5026 = "BEF_RUNTIME_5026";
    public static final String BEF_RUNTIME_5027 = "BEF_RUNTIME_5027";
    /**
     * 查询数据数量不能带分页条件
     */
    public static final String BEF_RUNTIME_5028 = "BEF_RUNTIME_5028";
    public static final String BEF_RUNTIME_5029 = "BEF_RUNTIME_5029";
    public static final String BEF_RUNTIME_5030 = "BEF_RUNTIME_5030";
    public static final String BEF_RUNTIME_5031 = "BEF_RUNTIME_5031";
    public static final String BEF_RUNTIME_5032 = "BEF_RUNTIME_5032";
    public static final String BEF_RUNTIME_5033 = "BEF_RUNTIME_5033";
    public static final String BEF_RUNTIME_5034 = "BEF_RUNTIME_5034";
    public static final String BEF_RUNTIME_5035 = "BEF_RUNTIME_5035";
    public static final String BEF_RUNTIME_5036 = "BEF_RUNTIME_5036";
    /**
     * 要修改的数据:{0}不存在，请检查：１.编辑数据的ID是否正确　２.数据是否已被删除　３.如果使用了负载均衡设备，请检查负载均衡策略是否配置为粘性。
     */
    public static final String BEF_RUNTIME_5037 = "BEF_RUNTIME_5037";
    public static final String BEF_RUNTIME_5038 = "BEF_RUNTIME_5038";
    public static final String BEF_RUNTIME_5039 = "BEF_RUNTIME_5039";
    public static final String BEF_RUNTIME_5040 = "BEF_RUNTIME_5040";
    public static final String BEF_RUNTIME_5041 = "BEF_RUNTIME_5041";

    /**
     * 批量修改时，ID为以下值的数据不存在：[{0}]，请确认ID是否正确或数据是否已被删除
     */
    public static final String BEF_RUNTIME_5042 = "BEF_RUNTIME_5042";
    public static final String BEF_RUNTIME_5043 = "BEF_RUNTIME_5043";
    public static final String BEF_RUNTIME_5044 = "BEF_RUNTIME_5044";
    public static final String BEF_RUNTIME_5045 = "BEF_RUNTIME_5045";
    public static final String BEF_RUNTIME_5046 = "BEF_RUNTIME_5046";
    public static final String BEF_RUNTIME_5047 = "BEF_RUNTIME_5047";
    public static final String BEF_RUNTIME_5048 = "BEF_RUNTIME_5048";
    public static final String BEF_RUNTIME_5049 = "BEF_RUNTIME_5049";
    public static final String BEF_RUNTIME_5050 = "BEF_RUNTIME_5050";
    public static final String BEF_RUNTIME_5051 = "BEF_RUNTIME_5051";
    public static final String BEF_RUNTIME_5052 = "BEF_RUNTIME_5052";
    public static final String BEF_RUNTIME_5053 = "BEF_RUNTIME_5053";
    public static final String BEF_RUNTIME_5054 = "BEF_RUNTIME_5054";
    public static final String BEF_RUNTIME_5055 = "BEF_RUNTIME_5055";
    public static final String BEF_RUNTIME_5056 = "BEF_RUNTIME_5056";
    public static final String BEF_RUNTIME_5057 = "BEF_RUNTIME_5057";
    public static final String BEF_RUNTIME_5058 = "BEF_RUNTIME_5058";
    public static final String BEF_RUNTIME_5059 = "BEF_RUNTIME_5059";
    public static final String BEF_RUNTIME_5060 = "BEF_RUNTIME_5060";
    public static final String BEF_RUNTIME_5061 = "BEF_RUNTIME_5061";
    public static final String BEF_RUNTIME_5062 = "BEF_RUNTIME_5062";
    public static final String BEF_RUNTIME_5063 = "BEF_RUNTIME_5063";
    public static final String BEF_RUNTIME_5064 = "BEF_RUNTIME_5064";
    public static final String BEF_RUNTIME_5065 = "BEF_RUNTIME_5065";
    public static final String BEF_RUNTIME_5066 = "BEF_RUNTIME_5066";
    public static final String BEF_RUNTIME_5067 = "BEF_RUNTIME_5067";
    public static final String BEF_RUNTIME_5068 = "BEF_RUNTIME_5068";
    public static final String BEF_RUNTIME_5069 = "BEF_RUNTIME_5069";
    public static final String BEF_RUNTIME_5070 = "BEF_RUNTIME_5070";
    public static final String BEF_RUNTIME_5071 = "BEF_RUNTIME_5071";
    public static final String BEF_RUNTIME_5072 = "BEF_RUNTIME_5072";
    public static final String BEF_RUNTIME_5073 = "BEF_RUNTIME_5073";
    public static final String BEF_RUNTIME_5074 = "BEF_RUNTIME_5074";
    public static final String BEF_RUNTIME_5075 = "BEF_RUNTIME_5075";
    public static final String BEF_RUNTIME_5076 = "BEF_RUNTIME_5076";
    public static final String BEF_RUNTIME_5077 = "BEF_RUNTIME_5077";
    public static final String BEF_RUNTIME_5078 = "BEF_RUNTIME_5078";
    public static final String BEF_RUNTIME_5079 = "BEF_RUNTIME_5079";
    public static final String BEF_RUNTIME_5080 = "BEF_RUNTIME_5080";
    public static final String BEF_RUNTIME_5081 = "BEF_RUNTIME_5081";
    public static final String BEF_RUNTIME_5082 = "BEF_RUNTIME_5082";
    public static final String BEF_RUNTIME_5083 = "BEF_RUNTIME_5083";
}
