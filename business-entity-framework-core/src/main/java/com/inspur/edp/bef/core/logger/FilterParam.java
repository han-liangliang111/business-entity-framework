package com.inspur.edp.bef.core.logger;

import lombok.Data;

@Data
public class FilterParam {
    private int minutes;
    private int maxCount;
    private String filePath;

    public FilterParam(int minutes, int maxCount, String filePath) {
        this.minutes = minutes;
        this.maxCount = maxCount;
        this.filePath = filePath;
    }
}
