package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.spi.exception.ExceptionCode;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

public class BefBizContextBuilderTest {
    @Test
    public void getRandomString(){
        String PREFIX = "bef";
//        Random random = new Random();
//        String s1 = PREFIX + digits(random.nextLong()) + digits(random.nextLong());
//        System.out.println(s1);
        String s2;
        try {
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            s2 = PREFIX + digits(secureRandom.nextLong()) + digits(secureRandom.nextLong());
        }catch (NoSuchAlgorithmException nsae) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_4003);
        }
        System.out.println(s2);
    }

    private String digits(long val) {
        String str = Long.toHexString(val);
        return StringUtils.leftPad(str, 16, '0');
    }
}
