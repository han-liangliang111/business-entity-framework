package com.inspur.edp.cef.spi.repository.entity;

import lombok.Data;

/**
 * @Author wangmaojian
 * @create 2023/4/25
 * 表名拼接扩展配置信息
 */
@Data
public class TableNameReposConfigInfo extends BaseReposConfigInfo {
}
