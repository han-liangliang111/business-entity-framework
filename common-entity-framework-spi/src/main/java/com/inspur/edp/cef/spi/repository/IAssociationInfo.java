package com.inspur.edp.cef.spi.repository;

import com.inspur.edp.cef.api.repository.IRootRepository;

/**
 * @Author wangmaojian
 * @create 2023/4/17
 */
public interface IAssociationInfo {
    IRootRepository getRefRepository();
}
