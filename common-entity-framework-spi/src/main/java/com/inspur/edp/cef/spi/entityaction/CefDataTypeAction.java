/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entityaction;

import com.inspur.edp.cef.api.dataType.action.ICefDataTypeAction;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.base.IDataTypeActionAssembler;

public abstract class CefDataTypeAction<TResult> implements ICefDataTypeAction<TResult> {
    private ICefDataTypeContext privateContext;
    /**
     * 获取或设置自定义动作的返回值，返回值类型为TResult
     */
    private TResult privateResult;

    protected CefDataTypeAction() {
    }

    public ICefDataTypeContext getContext() {
        return privateContext;
    }

    public void setContext(ICefDataTypeContext value) {
        privateContext = value;
    }

    public TResult getResult() {
        return privateResult;
    }

    protected void setResult(TResult value) {
        privateResult = value;
    }

    public void internalExecute() {
        execute();
    }

    public abstract void execute();


    public IDataTypeActionAssembler internalGetActionAssembler() {
        return _GetActionAssembler();
    }

    protected IDataTypeActionAssembler _GetActionAssembler() {
        return null;
    }

}
