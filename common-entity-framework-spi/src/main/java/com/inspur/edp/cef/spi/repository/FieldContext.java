package com.inspur.edp.cef.spi.repository;

import com.inspur.edp.udt.entity.IUdtData;

import java.util.HashMap;
import java.util.List;

/**
 * 查询字段上下文
 */
public class FieldContext {
    private BaseDbColumnInfo dbColumnInfo;
    private String propertyName;
    private List<String> alias;
    /**
     * 当前字段对应表别名  考虑字段拼接的 连接符？
     */
    private String tableAlias;
    private Iterable<BaseDbColumnInfo> dbColumnInfos;
    //扩展列
    private Iterable<BaseDbColumnInfo> extendColumnInfos;
    private int index;
    private HashMap<String, String> variableData;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public HashMap<String, String> getVariableData() {
        return variableData;
    }

    public void setVariableData(HashMap<String, String> variableData) {
        this.variableData = variableData;
    }

    //变量Data..
    public Iterable<BaseDbColumnInfo> getDbColumnInfos() {
        return dbColumnInfos;
    }

    public void setDbColumnInfos(Iterable<BaseDbColumnInfo> dbColumnInfos) {
        this.dbColumnInfos = dbColumnInfos;
    }

    public BaseDbColumnInfo getDbColumnInfo() {
        return dbColumnInfo;
    }

    public void setDbColumnInfo(BaseDbColumnInfo dbColumnInfo) {
        this.dbColumnInfo = dbColumnInfo;
    }

    public List<String> getAlias() {
        return alias;
    }

    public void setAlias(List<String> alias) {
        this.alias = alias;
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public Iterable<BaseDbColumnInfo> getExtendColumnInfos() {
        return extendColumnInfos;
    }

    public void setExtendColumnInfos(Iterable<BaseDbColumnInfo> extendColumnInfos) {
        this.extendColumnInfos = extendColumnInfos;
    }
}
