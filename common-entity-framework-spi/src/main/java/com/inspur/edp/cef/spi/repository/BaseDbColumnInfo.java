package com.inspur.edp.cef.spi.repository;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.udt.entity.IUdtData;

import java.util.HashMap;

public class BaseDbColumnInfo implements Cloneable {
    /**
     * 字段别名（Be上字段名称）
     */
    protected String columnName;
    /**
     * 对应数据库字段名称
     */
    protected String dbColumnName;

    /**
     * 表别名
     */
    protected String tableAlias;
    protected GspDbDataType columnType;
    protected int length;
    protected int precision;
    protected Object defaultValue;
    protected boolean isPrimaryKey;
    protected boolean isAssociateRefElement;
    protected boolean isMultiLang;
    protected boolean isParentId;
    protected boolean isUdtElement;
    protected boolean isAssociation;
    protected boolean isEnum;
    /**
     * 是否虚拟字段
     */
    protected boolean isVirtual = false;
    protected boolean isPersistent = true;
    protected DataTypePropertyInfo dataTypePropertyInfo;
    /**
     * 保存关联字段上记录的数据
     */
    protected HashMap<String, String> variableData;
    /**
     * udt字段，关联带出字段所属的字段标签
     */
    protected String belongElementLabel;
    //public Func<FilterCondition, IGSPDatabase, object> typetransprocesser { get; set; }
    protected ITypeTransProcesser typeTransProcesser;
    protected boolean isRef;
    //字段扩展插件ID 后期要通过ID，找到yaml中已注册的插件
    private String fieldReposExtendConfigId;
    //todo 字段扩展实现类
    private String fieldReposExtendConfigClassImpl;
    //字段扩展插件ID 后期要通过ID，找到yaml中已注册的插件
    public BaseDbColumnInfo() {
    }
    public BaseDbColumnInfo(String columnName, String dbColumnName, GspDbDataType columnType, int length, int precision, boolean isPrimaryKey, boolean isAssociateRefElement, boolean isMultiLang, boolean isParentId, boolean isUdtElement
            , boolean isAssociation, boolean isEnum, String belongElementLabel, ITypeTransProcesser typeTransProcesser) {
        this.columnName = columnName;
        this.dbColumnName = dbColumnName;
        this.columnType = columnType;
        this.length = length;
        this.precision = precision;
        this.isPrimaryKey = isPrimaryKey;
        this.isAssociateRefElement = isAssociateRefElement;
        this.isMultiLang = isMultiLang;
        this.isParentId = isParentId;
        this.isUdtElement = isUdtElement;
        this.isAssociation = isAssociation;
        this.isEnum = isEnum;
        this.belongElementLabel = belongElementLabel;
        this.typeTransProcesser = typeTransProcesser;
    }

    public HashMap<String, String> getVariableData() {
        return variableData;
    }

    public void setVariableData(HashMap<String, String> variableData) {
        this.variableData = variableData;
    }

    /**
     * 是否参与持久化
     *
     * @return
     */
    public boolean getIsPersistent() {
        return isPersistent;
    }

    public void setPersistent(boolean persistent) {
        isPersistent = persistent;
    }

    public final String getColumnName() {
        return columnName;
    }

    public final void setColumnName(String value) {
        columnName = value;
    }

    public final String getDbColumnName() {
        return dbColumnName;
    }

    public final void setDbColumnName(String value) {
        dbColumnName = value;
    }

    /**
     * 关联的，运行时才会设置
     *
     * @return
     */
    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String tableAlias) {
        this.tableAlias = tableAlias;
    }

    public final GspDbDataType getColumnType() {
        return columnType;
    }

    public final void setColumnType(GspDbDataType value) {
        columnType = value;
    }

    public final int getLength() {
        return length;
    }

    public final void setLength(int value) {
        length = value;
    }

    public final int getPrecision() {
        return precision;
    }

    public final void setPrecision(int value) {
        precision = value;
    }

    public final Object getDefaultValue() {
        return defaultValue;
    }

    public final void setDefaultValue(Object value) {
        defaultValue = value;
    }

    public final boolean getIsPrimaryKey() {
        return isPrimaryKey;
    }

    public final void setIsPrimaryKey(boolean value) {
        isPrimaryKey = value;
    }

    public final boolean getIsAssociateRefElement() {
        return isAssociateRefElement;
    }

    public final void setIsAssociateRefElement(boolean value) {
        isAssociateRefElement = value;
    }

    public final boolean getIsMultiLang() {
        return isMultiLang;
    }

    public final void setIsMultiLang(boolean value) {
        isMultiLang = value;
    }

    public final boolean getIsParentId() {
        return isParentId;
    }

    public final void setIsParentId(boolean value) {
        isParentId = value;
    }

    public final boolean getIsUdtElement() {
        return isUdtElement;
    }

    public final void setIsUdtElement(boolean value) {
        isUdtElement = value;
    }

    public final boolean getIsAssociation() {
        return isAssociation;
    }

    public final void setIsAssociation(boolean value) {
        isAssociation = value;
    }

    public final boolean getIsEnum() {
        return isEnum;
    }

    public final void setIsEnum(boolean value) {
        isEnum = value;
    }

    public final String getBelongElementLabel() {
        return belongElementLabel;
    }

    public final void setBelongElementLabel(String value) {
        belongElementLabel = value;
    }

    public final ITypeTransProcesser getTypeTransProcesser() {
        return typeTransProcesser;
    }

    public final void setTypeTransProcesser(ITypeTransProcesser value) {
        typeTransProcesser = value;
    }

    public String getAliasName(int index) {
        if (columnName.length() >= 30) {
            return columnName.substring(0, 26) + index;
        } else {
            return columnName;
        }
    }

    public final boolean isRef() {
        return isRef;
    }

    public final void setRef(boolean ref) {
        isRef = ref;
    }

    public final String getFieldReposExtendConfigId() {
        return fieldReposExtendConfigId;
    }

    public final void setFieldReposExtendConfigId(String fieldReposExtendConfigId) {
        this.fieldReposExtendConfigId = fieldReposExtendConfigId;
    }

    public final String getFieldReposExtendConfigClassImpl() {
        return fieldReposExtendConfigClassImpl;
    }

    public final void setFieldReposExtendConfigClassImpl(String fieldReposExtendConfigClassImpl) {
        this.fieldReposExtendConfigClassImpl = fieldReposExtendConfigClassImpl;
    }


    public final String getRealColumnName() {
        if (getIsMultiLang())
            return getDbColumnName() + "@Language@";
        return getDbColumnName();
    }

    public boolean isVirtual() {
        return isVirtual;
    }

    public void setVirtual(boolean virtual) {
        isVirtual = virtual;
    }

    public boolean isUdtRefColumn() {
        return false;
    }

}
