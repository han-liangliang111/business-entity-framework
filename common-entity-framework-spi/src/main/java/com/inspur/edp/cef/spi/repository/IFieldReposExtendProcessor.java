package com.inspur.edp.cef.spi.repository;

/**
 * 字段拼接扩展处理
 */
public interface IFieldReposExtendProcessor {
    String getQueryField(FieldContext fieldContext);
}
