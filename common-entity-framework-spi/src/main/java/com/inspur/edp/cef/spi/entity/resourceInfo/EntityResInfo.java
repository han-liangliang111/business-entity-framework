/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo;

import com.inspur.edp.cef.api.message.CefException;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.exception.ExceptionCode;

import java.util.Map;

/**
 * 对象实体资源信息，作为运行时的主要操作对象，生成型和解析型的公共媒介
 */
public abstract class EntityResInfo extends DataTypeResInfo {

    /**
     * 唯一性约束提示信息
     *
     * @param uniqueConCode 唯一性约束编号
     * @return
     */
    public String getUniqueConstraintMessage(String uniqueConCode) {
        return null;
    }


    public String getEntityCode() {
        return null;
    }


    public EntityResInfo getChildEntityResInfo(String entityCode) {
        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3029);
    }

    public Map<String, EntityResInfo> getChildEntityResInfos() {
        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3029);
    }

}
