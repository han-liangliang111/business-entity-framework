/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.base;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * 动态属性集合序列化构件接口, 请使用AbstractDynamicPropSetSerItem抽象类, 而不要直接实现此接口
 * <see cref="AbstractDynamicPropSetSerItem"/>
 */
public interface IDynamicPropSerItem {
    /**
     * 将动态属性名转换为序列化后的动态属性名
     *
     * @param actualName 动态属性名
     * @return 序列化动态属性名
     */
    String getSerializationItemName(String actualName);

    /**
     * 将序列化后的动态属性名转换为实际动态属性名
     *
     * @param serName 序列化后的动态属性名
     * @return 实际动态属性名
     */
    String getActualItemName(String serName);

    /**
     * 序列化动态属性
     */
    void writeItemValue(String name, Object value, JsonGenerator writer, SerializerProvider serializer);

    /**
     * 反序列化动态属性
     */
    Object readItemValue(String name, JsonParser reader, DeserializationContext serializer);

    /**
     * 序列化动态属性变更集
     */
    void writeItemChange(String name, Object value, JsonGenerator writer, SerializerProvider serializer);

    /**
     * 反序列化动态属性变更集
     */
    Object readItemChange(String name, JsonParser reader, DeserializationContext serializer);
}
