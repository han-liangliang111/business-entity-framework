/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info.propertyinfo;

import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.entity.PropertyDefaultValue;

/**
 * 带出字段：关联带出字段、关联UDT带出字段、多值UDT包含字段
 */
public class RefDataTypePropertyInfo extends DataTypePropertyInfo {
    private final String refPropertyName;
    private boolean isRefInit = true;
    private DataTypePropertyInfo belongPropertyInfo;

    public RefDataTypePropertyInfo(String propertyName, String displayValueKey, boolean required,
                                   boolean enableRtrim, int length, int precision,
                                   FieldType fieldType,
                                   ObjectType objectType,
                                   BasePropertyInfo objectInfo, PropertyDefaultValue defaultValue,
                                   String refPropertyName) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType,
                objectType, objectInfo, defaultValue);
        this.refPropertyName = refPropertyName;
    }

    public RefDataTypePropertyInfo(String propertyName, String displayValueKey, boolean required,
                                   boolean enableRtrim, int length, int precision,
                                   FieldType fieldType, ObjectType objectType,
                                   BasePropertyInfo objectInfo, PropertyDefaultValue defaultValue, boolean isMultiLaguange,
                                   String refPropertyName) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType,
                objectType, objectInfo, defaultValue, isMultiLaguange);
        this.refPropertyName = refPropertyName;
    }

    public RefDataTypePropertyInfo(String propertyName, String displayValueKey, boolean required,
                                   boolean enableRtrim, int length, int precision,
                                   FieldType fieldType, ObjectType objectType,
                                   BasePropertyInfo objectInfo, PropertyDefaultValue defaultValue, boolean isMultiLaguange,
                                   boolean bigNumber, String refPropertyName) {
        super(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType,
                objectType, objectInfo, defaultValue, isMultiLaguange, bigNumber);
        this.refPropertyName = refPropertyName;
    }

    public String getRefPropertyName() {
        return refPropertyName;
    }

    public final DataTypePropertyInfo getBelongPropertyInfo() {
        return belongPropertyInfo;
    }

    public final void setBelongPropertyInfo(DataTypePropertyInfo belongPropertyInfo) {
        this.belongPropertyInfo = belongPropertyInfo;
    }

    public boolean isRefInit() {
        return isRefInit;
    }

    public void setRefInit(boolean refInit) {
        isRefInit = refInit;
    }
}
