/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info.propertyinfo;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.entity.PropertyDefaultValue;
import com.inspur.edp.cef.spi.exception.ExceptionCode;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;
import com.inspur.edp.cef.spi.repository.entity.FieldReposConfigInfo;
import com.inspur.edp.cef.spi.repository.entity.TableNameReposConfigInfo;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 字段在运行时抽象出的对象表示，包含必填、长度、精度、数据类型等属性信息，并且对外提供了数据和变更集的序列化方法
 * 其中BasePropertyInfo是具体的实现对象，按照字段类型有具体的子类重写相关的方法
 */
public class DataTypePropertyInfo {

    /**
     * 字段标签labelId
     */
    private String propertyName;

    private String displayValueKey;

    /**
     * 是否必填属性
     */
    private boolean required = false;

    /**
     * 是否去除空值
     */
    private boolean enableRtrim = true;

    private int length = 0;
    private int precision = 0;

    /**
     * 字段类型:对应BE上的类型
     */
    private FieldType fieldType = FieldType.forValue(0);

    /**
     * 对象类型：正常、关联、枚举、UDT、动态属性
     */
    private ObjectType objectType = ObjectType.Normal;
    private BasePropertyInfo objectInfo;
    private PropertyDefaultValue defaultValue;
    private boolean isMultiLaguange = false;
    private boolean bigNumber = false;

    /**
     * 对应运行时数据类型，类型是从dbo字段类型映射转换而来
     */
    private GspDbDataType dbDataType;
    private String dbColumnName;

    /**
     * 字段扩展配置信息
     */
    private FieldReposConfigInfo fieldReposConfigInfo;

    /**
     * 表名扩展配置信息
     */
    private TableNameReposConfigInfo tableNameReposConfigInfo;
    /**
     * 是否虚拟字段
     */
    private boolean isVirtual = false;

    public DataTypePropertyInfo(
            String propertyName,
            String displayValueKey,
            boolean required,
            boolean enableRtrim,
            int length,
            int precision,
            FieldType fieldType,
            ObjectType objectType,
            BasePropertyInfo objectInfo,
            PropertyDefaultValue defaultValue) {

        this.propertyName = propertyName;
        this.displayValueKey = displayValueKey;
        this.required = required;
        this.enableRtrim = enableRtrim;
        this.length = length;
        this.precision = precision;
        this.fieldType = fieldType;
        this.objectType = objectType;
        this.objectInfo = objectInfo;
        this.defaultValue = defaultValue;
    }

    public DataTypePropertyInfo(
            String propertyName,
            String displayValueKey,
            boolean required,
            boolean enableRtrim,
            int length,
            int precision,
            FieldType fieldType,
            ObjectType objectType,
            BasePropertyInfo objectInfo,
            PropertyDefaultValue defaultValue, boolean isMultiLaguange) {

        this(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType,
                objectType, objectInfo, defaultValue);
        this.isMultiLaguange = isMultiLaguange;
    }

    public DataTypePropertyInfo(
            String propertyName,
            String displayValueKey,
            boolean required,
            boolean enableRtrim,
            int length,
            int precision,
            FieldType fieldType,
            ObjectType objectType,
            BasePropertyInfo objectInfo,
            PropertyDefaultValue defaultValue,
            boolean isMultiLaguange,
            boolean bigNumber) {

        this(propertyName, displayValueKey, required, enableRtrim, length, precision, fieldType,
                objectType, objectInfo, defaultValue, isMultiLaguange);
        this.bigNumber = bigNumber;
    }

    public static void setPropertyInfoByRefed(DataTypePropertyInfo refDataTypePropertyInfo, DataTypePropertyInfo refedDataTypePropInfo) {
        refDataTypePropertyInfo.required = refedDataTypePropInfo.required;
        refDataTypePropertyInfo.enableRtrim = refedDataTypePropInfo.enableRtrim;
        refDataTypePropertyInfo.length = refedDataTypePropInfo.length;
        refDataTypePropertyInfo.precision = refedDataTypePropInfo.precision;
        refDataTypePropertyInfo.fieldType = refedDataTypePropInfo.fieldType;
        refDataTypePropertyInfo.objectType = refedDataTypePropInfo.objectType;
        refDataTypePropertyInfo.objectInfo = refedDataTypePropInfo.objectInfo;
        refDataTypePropertyInfo.defaultValue = refedDataTypePropInfo.defaultValue;
        refDataTypePropertyInfo.isMultiLaguange = refedDataTypePropInfo.isMultiLaguange;
        refDataTypePropertyInfo.bigNumber = refedDataTypePropInfo.bigNumber;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public String getDisplayValueKey() {
        return displayValueKey;
    }

    public boolean getRequired() {
        return required;
    }

//    public boolean getRequiredAsConstraint() {
//        return requiredAsConstraint;
//    }
//
//    public boolean isDefaultNull() {
//        return isDefaultNull;
//    }

    public boolean getEnableRtrim() {
        return enableRtrim;
    }

    public int getLength() {
        return length;
    }

    public int getPrecision() {
        return precision;
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public ObjectType getObjectType() {
        return objectType;
    }

    public Object getObjectInfo() {
        return objectInfo;
    }

    public PropertyDefaultValue getDefaultValue() {
        return defaultValue;
    }

    public void write(JsonGenerator writer, Object value, SerializerProvider serializer,
                      CefSerializeContext serContext) {

        if (objectType != ObjectType.Normal) {
            if (bigNumber) {
                serContext.setBigNumber(bigNumber);
            }
            objectInfo.write(writer, propertyName, value, serializer, serContext);
        } else {
            writeBasicTypeProp(writer, value, serializer, serContext);
        }
    }

    public void write(String prefix, JsonGenerator writer, Object value, SerializerProvider serializer,
                      CefSerializeContext serContext) {

        if (objectType != ObjectType.Normal) {
            if (bigNumber) {
                serContext.setBigNumber(bigNumber);
            }
            String propName = StringUtils.isEmpty(prefix) ? propertyName : prefix + "_" + propertyName;
            objectInfo.write(writer, propName, value, serializer, serContext);
        } else {
            writeBasicTypeProp(prefix, writer, value, serializer, serContext);
        }
    }

    private void writeBasicTypeProp(String prefix, JsonGenerator writer, Object value,
                                    SerializerProvider serializer, CefSerializeContext serContext) {
        String prop = StringUtils.isEmpty(prefix) ? propertyName : prefix + "_" + propertyName;
        switch (fieldType) {
            case String:
            case Text:
                SerializerUtil.writeString(writer, value, prop, serializer);
                break;
            case Integer:
                SerializerUtil.writeInt(writer, value, prop, serializer);
                break;
            case Decimal:
                writeDecimalDataTypeInfo(writer, value, serializer, serContext, prop);
                break;
            case Date:
                SerializerUtil.writeDate(writer, value, prop, serializer);
                break;
            case DateTime:
//                SerializerUtil.writeStdDateTime(writer, value, propertyName, serializer);
                SerializerUtil.writeDateTime(writer, value, prop, serializer, serContext.getEnableStdTimeFormat());
                break;
            case Boolean:
                SerializerUtil.writeBool(writer, value, prop, serializer);
                break;
            case Binary:
                SerializerUtil.writeBytes(writer, value, prop, serializer);
                break;
            default:
                throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3013, this.getPropertyName(), fieldType.name());
        }
    }

    private void writeDecimalDataTypeInfo(JsonGenerator writer, Object value, SerializerProvider serializer, CefSerializeContext serContext, String prop) {
        if (!bigNumber && serContext != null) {
            SerializerUtil.writeDecimal(writer, value, prop, serializer,
                    serContext.isBigNumber());
        } else {
            SerializerUtil.writeDecimal(writer, value, prop, serializer, bigNumber);
        }
        return;
    }

    private void writeBasicTypeProp(JsonGenerator writer, Object value,
                                    SerializerProvider serializer, CefSerializeContext serContext) {
        switch (fieldType) {
            case String:
            case Text:
                SerializerUtil.writeString(writer, value, propertyName, serializer);
                break;
            case Integer:
                SerializerUtil.writeInt(writer, value, propertyName, serializer);
                break;
            case Decimal:
                writeDecimalProp(writer, value, serializer, serContext);
                break;
            case Date:

                SerializerUtil.writeDate(writer, value, propertyName, serializer);
                break;
            case DateTime:
//                SerializerUtil.writeStdDateTime(writer, value, propertyName, serializer);
                SerializerUtil.writeDateTime(writer, value, propertyName, serializer, serContext.getEnableStdTimeFormat());
                break;
            case Boolean:
                SerializerUtil.writeBool(writer, value, propertyName, serializer);
                break;
            case Binary:
                SerializerUtil.writeBytes(writer, value, propertyName, serializer);
                break;
            default:
                throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3013, this.getPropertyName(), fieldType.name());
        }
    }

    protected void writeDecimalProp(JsonGenerator writer, Object value, SerializerProvider serializer, CefSerializeContext serContext) {
        if (!bigNumber && serContext != null) {
            SerializerUtil.writeDecimal(writer, value, propertyName, serializer,
                    serContext.isBigNumber());
        } else {
            SerializerUtil.writeDecimal(writer, value, propertyName, serializer, bigNumber);
        }
    }

    public Object read(JsonParser reader, DeserializationContext serializer,
                       CefSerializeContext serContext) {

        if (objectType != ObjectType.Normal) {
            return objectInfo.read(reader, propertyName, serializer, serContext);
        } else {
            return readBasicTypeProp(reader, serializer, serContext);
        }
    }

    public Object read(JsonNode node, CefSerializeContext serContext) {
        if (objectType != ObjectType.Normal) {
            return objectInfo.read(node, propertyName, serContext);
        } else {
            return readBasicTypeProp(node, serContext);
        }
    }

    private Object readBasicTypeProp(JsonParser reader, DeserializationContext serializer, CefSerializeContext serContext) {

        if (reader.getCurrentToken() == JsonToken.VALUE_NULL)
            return null;
        switch (fieldType) {
            case String:
            case Text:
                return SerializerUtil.readString(reader);
            case Integer:
                return SerializerUtil.readInt(reader);
            case Decimal:
                return SerializerUtil.readDecimal(reader);
            case Date:
                //应按照国际化时区方式读取数据，否则日期型差8小时
                return SerializerUtil.readDateTime(reader, true);
            case DateTime:
                return SerializerUtil.readDateTime(reader, serContext.getEnableStdTimeFormat());
            case Boolean:
                return SerializerUtil.readBool(reader);
            case Binary:
                return SerializerUtil.readBytes(reader);
            default:
                throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3015, fieldType.name());
        }

    }

    private Object readBasicTypeProp(JsonNode node, CefSerializeContext serContext) {

        if (node.isNull())
            return null;
        switch (fieldType) {
            case String:
            case Text:
                return node.textValue();
            case Integer:
                return node.intValue();
            case Decimal:
                return node.decimalValue();
            case Date:
                //应按照国际化时区方式读取数据，否则日期型差8小时
                return SerializerUtil.readDateTime(node, true);
            case DateTime:
                return SerializerUtil.readDateTime(node, serContext.getEnableStdTimeFormat());
            case Boolean:
                if (node.getNodeType() == JsonNodeType.STRING) {
                    String value = node.textValue();
                    if ("0".equals(value))
                        return false;
                    return true;
                }
                return node.booleanValue();
            case Binary:
                try {
                    return node.binaryValue();
                } catch (IOException e) {
                    throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3023, e);
                }
            default:
                ;
                throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3015, fieldType.name());
        }

    }

    public void writeChange(JsonGenerator writer, Object value, SerializerProvider serializer,
                            CefSerializeContext serContext) {

        if (objectType != ObjectType.Normal) {
            if (bigNumber) {
                serContext.setBigNumber(bigNumber);
            }
            objectInfo.writeChange(writer, propertyName, value, serializer, serContext);
        } else {
            writeBasicTypeProp(writer, value, serializer, serContext);
        }
    }

    public Object readChange(JsonParser reader, DeserializationContext serializer,
                             CefSerializeContext serContext) {

        if (objectType != ObjectType.Normal) {
            if (this instanceof RefDataTypePropertyInfo && objectType == ObjectType.Association)
                return objectInfo.readChange(reader, ((RefDataTypePropertyInfo) this).getRefPropertyName(), serializer, serContext);
            return objectInfo.readChange(reader, propertyName, serializer, serContext);
        } else {
            return readBasicTypeProp(reader, serializer, serContext);
        }
    }

    public Object readChange(JsonNode node, CefSerializeContext serContext) {
        if (objectType != ObjectType.Normal) {
            return objectInfo.readChange(node, propertyName, serContext);
        } else {
            return readBasicTypeProp(node, serContext);
        }
    }

    public boolean isMultiLaguange() {
        return isMultiLaguange;
    }

    public boolean getIsBigNumber() {
        return bigNumber;
    }

    public void setIsBigNumber(boolean value) {
        bigNumber = value;
    }

    public Object createPropertyDataValue() {
        switch (objectType) {
            case Normal:
                switch (fieldType) {
                    case String:
                    case Text:
                        return "";
                    case Integer:
                        return 0;
                    case Decimal:
                        return new BigDecimal(0);
                    case Date:
                        return new Date();
                    case DateTime:
                        return new Date();
                    case Boolean:
                        return false;
                    case Binary:
                        return null;
                    default:
                        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3015, fieldType.name());
                }
            case Association:
                AssocationPropertyInfo assocationPropertyInfo = (AssocationPropertyInfo) objectInfo;
                return assocationPropertyInfo.createAssociationDefaultValue();
            case Enum:
                EnumPropertyInfo enumPropertyInfo = (EnumPropertyInfo) objectInfo;
                return enumPropertyInfo.createAssociationDefaultValue();
            case UDT:
            case DynamicProp:
            default:
                throw new UnsupportedOperationException();
        }
    }

    public void setValue(Object data, String propertyName, Object value, CefSerializeContext serContext) {
        if (objectInfo != null)
            objectInfo.setValue(data, propertyName, value, serContext);
    }

    public Object createValue() {
        if (objectInfo != null)
            return objectInfo.createValue();
        throw new UnsupportedOperationException();
    }

    public Object createChange() {
        if (objectInfo != null)
            return objectInfo.createChange();
        throw new UnsupportedOperationException();
    }

    public GspDbDataType getDbDataType() {
        return dbDataType;
    }

    public void setDbDataType(GspDbDataType dbDataType) {
        this.dbDataType = dbDataType;
    }

    public String getDbColumnName() {
        return dbColumnName;
    }

    public void setDbColumnName(String dbColumnName) {
        this.dbColumnName = dbColumnName;
    }

    public FieldReposConfigInfo getFieldReposConfigInfo() {
        return fieldReposConfigInfo;
    }

    public void setFieldReposConfigInfo(FieldReposConfigInfo fieldReposConfigInfo) {
        this.fieldReposConfigInfo = fieldReposConfigInfo;
    }

    public TableNameReposConfigInfo getTableNameReposConfigInfo() {
        return tableNameReposConfigInfo;
    }

    public void setTableNameReposConfigInfo(TableNameReposConfigInfo tableNameReposConfigInfo) {
        this.tableNameReposConfigInfo = tableNameReposConfigInfo;
    }

    public String getPropValue(Object value) {
        String returnvalue = "";
        if (objectType != ObjectType.Normal) {
            returnvalue = objectInfo.getPropValue(value);
        } else {
            returnvalue = getBasicProp(value);
        }
        return returnvalue;
    }


    public String getBasicProp(Object value) {
        String propString = "";
        switch (fieldType) {
            case String:
            case Text:
                propString = "\"" + SerializerUtil.getString(value) + "\"";
                break;
            case Integer:
                propString = String.valueOf(SerializerUtil.getInteger(value));
                break;
            case Decimal:
                propString = String.valueOf(SerializerUtil.getDecimal(value));
                break;
            case Date:
                propString = "\"" + SerializerUtil.getDateProp(value) + "\"";
                break;
            case DateTime:
                propString = "\"" + SerializerUtil.getDatetimeProp(value) + "\"";
                break;
            case Boolean:
                propString = String.valueOf(SerializerUtil.getBoolean(value));
                break;
            case Binary:
                propString = String.valueOf(SerializerUtil.getBinary(value));
                break;
            default:
                throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3013, this.getPropertyName(), fieldType.name());
        }
        return propString;
    }

}
