package com.inspur.edp.cef.spi.repository;


import com.inspur.edp.cef.api.repository.INestedRepository;

/**
 * @Author wangmaojian
 * @create 2023/4/17
 */
public interface IAdaptorItem {
    IAssociationInfo getAssociation(String propertyName);

    INestedRepository getNestedRepo(String configId);
}
