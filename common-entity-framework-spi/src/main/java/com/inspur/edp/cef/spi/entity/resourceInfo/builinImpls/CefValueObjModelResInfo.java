/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls;

import com.inspur.edp.cef.api.CefRtBeanUtil;
import com.inspur.edp.cef.api.message.CefException;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjModelResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjResInfo;
import com.inspur.edp.cef.spi.exception.ExceptionCode;

public abstract class CefValueObjModelResInfo extends ValueObjModelResInfo {
    private final String currentSu;
    private final String metadataId;
    private final String rootNodeCode;
    private final String displayKey;
    CefValueObjResInfo valueObjResInfo;

    public CefValueObjModelResInfo(String currentSu, String metadataId, String rootNodeCode, String displayKey) {

        this.currentSu = currentSu;
        this.metadataId = metadataId;
        this.rootNodeCode = rootNodeCode;
        this.displayKey = displayKey;
        valueObjResInfo = getResourceInfo();
        valueObjResInfo.setModelResInfo(this);
    }

    protected abstract CefValueObjResInfo getResourceInfo();

    @Override
    public ValueObjResInfo getCustomResource(String objCode) {
        return valueObjResInfo;
    }

    @Override
    public String getModelDispalyName() {
        return getResourceItemValue(displayKey);
    }

    @Override
    public String getRootNodeCode() {
        return rootNodeCode;
    }

    @Override
    public String getResourceMetaId() {
        return metadataId;
    }

    public final String getResourceItemValue(String resourceKey) {
        String resValue = "";
        try {
            resValue = CefRtBeanUtil.getResourceItemValue(currentSu, metadataId, resourceKey);
        } catch (Exception ex) {
            String[] params = new String[]{currentSu, metadataId, resourceKey};
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_3033, ex, params);
        }
        return resValue;
    }
}
