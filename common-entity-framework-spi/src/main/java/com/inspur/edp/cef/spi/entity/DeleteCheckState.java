package com.inspur.edp.cef.spi.entity;

/**
 * @author: wangmj
 * @date: 2024/11/2
 * @Description: 关联字段删除检查状态
 **/
public enum DeleteCheckState {
    Enabled(0),
    Disabled(1);

    private static java.util.HashMap<Integer, DeleteCheckState> mappings;
    private int intValue;

    private DeleteCheckState(int value) {
        intValue = value;
        DeleteCheckState.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, DeleteCheckState> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, DeleteCheckState>();
        }
        return mappings;
    }

    public static DeleteCheckState forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
