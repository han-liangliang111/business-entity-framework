/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.mgraction;

import com.inspur.edp.cef.api.manager.ICefManagerContext;
import com.inspur.edp.cef.api.manager.ICefMgrActionAssembler;
import com.inspur.edp.cef.api.manager.action.ICefMgrAction;

public abstract class CefMgrAction<T> implements ICefMgrAction<T> {
    private ICefManagerContext privateContext;
    private T privateResult;

    public final ICefManagerContext getContext() {
        return privateContext;
    }

    public final void setContext(ICefManagerContext value) {
        privateContext = value;
    }

    public final T getResult() {
        return privateResult;
    }

    protected final void setResult(T value) {
        privateResult = value;
    }


    public void internalExecute() {
        execute();
    }

    /**
     * 自定义管理动作的核心逻辑抽象方法，该方法需要开发人员在实现类中复写并添加实现逻辑
     */
    public abstract void execute();


    protected ICefMgrActionAssembler _GetActionAssembler() {
        return null;
    }


    public ICefMgrActionAssembler innerGetActionAssembler() {
        return _GetActionAssembler();
    }
}
