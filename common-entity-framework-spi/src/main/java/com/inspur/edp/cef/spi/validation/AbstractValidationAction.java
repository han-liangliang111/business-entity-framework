/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.validation;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.api.validation.ICefValidationContext;

public abstract class AbstractValidationAction {
    private IChangeDetail change;
    private ICefValidationContext context;

    protected AbstractValidationAction(ICefValidationContext context, IChangeDetail change) {
        this.context = context;
        this.change = change;
    }

    public ICefValidationContext getContext() {
        return context;
    }

    public IChangeDetail getChange() {
        return change;
    }

    public final void Do() {
        execute();
    }

    protected abstract void execute();
}
