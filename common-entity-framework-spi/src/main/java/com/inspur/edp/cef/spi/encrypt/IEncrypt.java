package com.inspur.edp.cef.spi.encrypt;

public interface IEncrypt {
    String encrypt(String encryptContent);

    String decrypt(String decryptContent);

}
