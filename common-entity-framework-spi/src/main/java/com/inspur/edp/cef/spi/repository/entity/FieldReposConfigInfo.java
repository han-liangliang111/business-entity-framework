package com.inspur.edp.cef.spi.repository.entity;

import lombok.Data;

/**
 * @Author wangmaojian
 * @create 2023/4/25
 * 字段拼接扩展配置信息
 */
@Data
public class FieldReposConfigInfo extends BaseReposConfigInfo {
}
