/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

import com.inspur.edp.cef.entity.entity.FieldType;

import java.lang.reflect.Type;

/**
 * 属性信息
 */
public abstract class AbstractPropertyInfo {

    private boolean privateRequired;

    //public abstract bool ReadOnly { get; }
    private boolean enableRtrim;
    private boolean privateRequiredAsConstraint;
    /**
     * 是否去空格约束
     */
    private boolean EnableRtrimAsConstraint;
    private boolean privateIsUdt;
    private String privateUdtConfigId;
    /**
     * 是否默认存null
     */
    private boolean isDefaultNull;
    /**
     * 长度
     */
    private int privateLength;
    /**
     * 精度
     */
    private int privatePrecision;
    /**
     * 类型
     */
    private FieldType privateType = FieldType.forValue(0);
    private boolean privateIsAssociation;
    private AssociationInfo privateAssociationInfo;
    /**
     * 是否枚举
     */
    private boolean privateIsEnum;
    /**
     * 枚举信息
     */
    private java.util.HashMap<String, Class> privateEnumValueInfos;
    /**
     * 关联信息
     */
    private java.lang.Class privateAssociationInfoType;
    /**
     * 属性_国际化项的前缀 e.g. (commonField.I18nResourcePrefix).Name
     */
    private String privateDisplayValueKey;
    private PropertyDefaultValue privateDefaultValue;

    public abstract String getName();

    public boolean getRequired() {
        return privateRequired;
    }

    public boolean getRequiredAsConstraint() {
        return privateRequiredAsConstraint;
    }

    public boolean getEnableRtrimAsConstraint() {
        return EnableRtrimAsConstraint;
    }

    public boolean getIsUdt() {
        return privateIsUdt;
    }

    public String getUdtConfigId() {
        return privateUdtConfigId;
    }

    public boolean getIsDefaultNull() {
        return isDefaultNull;
    }

    public void setIsDefaultNull(boolean value) {
        isDefaultNull = value;
    }

    public int getLength() {
        return privateLength;
    }

    public void setLength(int value) {
        privateLength = value;
    }

    public int getPrecision() {
        return privatePrecision;
    }

    public void setPrecision(int value) {
        privatePrecision = value;
    }

    public FieldType getType() {
        return privateType;
    }

    public boolean getIsAssociation() {
        return privateIsAssociation;
    }

    public AssociationInfo getAssociationInfo() {
        return privateAssociationInfo;
    }

    public boolean getIsEnum() {
        return privateIsEnum;
    }

    public java.util.HashMap<String, Class> getEnumValueInfos() {
        return privateEnumValueInfos;
    }

    public java.lang.Class getAssociationInfoType() {
        return privateAssociationInfoType;
    }

    public String getDisplayValueKey() {
        return privateDisplayValueKey;
    }

    public PropertyDefaultValue getDefaultValue() {
        return privateDefaultValue;
    }


    @Override
    public boolean equals(Object obj) {
        return (obj instanceof AbstractPropertyInfo && (((AbstractPropertyInfo) obj).getName()
                == getName()));
    }


    @Override
    public int hashCode() {
        if (getName() == null) {
            return 0;
        }
        return getName().hashCode();
    }

    /**
     * 去空格属性
     *
     * @return
     */
    public boolean getEnableRtrim() {
        return enableRtrim;
    }

    public void setEnableRtrim(boolean enableRtrim) {
        this.enableRtrim = enableRtrim;
    }
}
