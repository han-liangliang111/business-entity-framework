package com.inspur.edp.cef.spi.manager;

import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.commonmodel.api.ICMManager;

/**
 * 模型国际化资源信息服务接口
 * 仅为解耦cef-spi和bef-api，增加此接口，具体实现在bef中提供，其内部转调IStandardLcp。
 */
public interface ModelResInfoService {
    /**
     * 获取模型相关信息
     * @param configId
     * @return 模型信息类
     */
    ModelResInfo getModelInfo(String configId);
}
