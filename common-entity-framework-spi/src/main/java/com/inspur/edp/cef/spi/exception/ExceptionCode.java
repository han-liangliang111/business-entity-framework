package com.inspur.edp.cef.spi.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/19
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 spi:3001开头 core:4001开头 repository:5001
    public static final String CEF_RUNTIME_3001 = "CEF_RUNTIME_3001";
    public static final String CEF_RUNTIME_3002 = "CEF_RUNTIME_3002";
    /**
     * 找不到关联带出字段：[{0}]的信息，请检查补丁是否部署。对应BE:[{1}],对象[{2}],字段[{3}]
     */
    public static final String CEF_RUNTIME_3003 = "CEF_RUNTIME_3003";
    /**
     * 转换值为枚举时出错，枚举类型为[{0}],传入的值为[{1}]，值类型为[{2}]，请检查调用参数传递
     */
    public static final String CEF_RUNTIME_3004 = "CEF_RUNTIME_3004";
    public static final String CEF_RUNTIME_3005 = "CEF_RUNTIME_3005";
    /**
     * 当前字段标签为:{0},业务字段字段标签为:{1},业务字段configId为:{2}
     */
    public static final String CEF_RUNTIME_3006 = "CEF_RUNTIME_3006";
    /**
     * 实例化字段对应的关联对象出错，关联类型为[{0}]，请确认元数据是否部署
     */
    public static final String CEF_RUNTIME_3007 = "CEF_RUNTIME_3007";
    /**
     * json结构中关联字段[{0}]的值应为对象类型, 当前类型为[{1}], 请检查前端网络请求内容
     */
    public static final String CEF_RUNTIME_3008 = "CEF_RUNTIME_3008";
    public static final String CEF_RUNTIME_3009 = "CEF_RUNTIME_3009";
    /**
     * UDT字段[{0}]进行序列化时出错,无法序列化非ICefData类型数据,当前值类型[{1}]
     */
    public static final String CEF_RUNTIME_3010 = "CEF_RUNTIME_3010";
    /**
     * 枚举字段对应的枚举项为空，请检查元数据。
     */
    public static final String CEF_RUNTIME_3011 = "CEF_RUNTIME_3011";
    public static final String CEF_RUNTIME_3012 = "CEF_RUNTIME_3012";
    /**
     * 字段[{0}]类型为[{1}],暂不支持序列化
     */
    public static final String CEF_RUNTIME_3013 = "CEF_RUNTIME_3013";
    public static final String CEF_RUNTIME_3014 = "CEF_RUNTIME_3014";
    /**
     * 字段类型为[{1}],暂不支持反序列化
     */
    public static final String CEF_RUNTIME_3015 = "CEF_RUNTIME_3015";
    /**
     * 序列化字段[{0}]失败
     */
    public static final String CEF_RUNTIME_3016 = "CEF_RUNTIME_3016";
    public static final String CEF_RUNTIME_3017 = "CEF_RUNTIME_3017";
    public static final String CEF_RUNTIME_3018 = "CEF_RUNTIME_3018";
    /**
     * 序列变更失败，属性名称为{0}
     */
    public static final String CEF_RUNTIME_3019 = "CEF_RUNTIME_3019";
    /**
     * 序列化字段失败，属性名称为{0}
     */
    public static final String CEF_RUNTIME_3020 = "CEF_RUNTIME_3020";
    /**
     * 找不到加密实现类:{0}
     */
    public static final String CEF_RUNTIME_3021 = "CEF_RUNTIME_3021";
    public static final String CEF_RUNTIME_3022 = "CEF_RUNTIME_3022";
    public static final String CEF_RUNTIME_3023 = "CEF_RUNTIME_3023";
    /**
     * 数据库中的值{0}不在枚举列表中
     */
    public static final String CEF_RUNTIME_3024 = "CEF_RUNTIME_3024";
    /**
     * 未识别的枚举持久化类型{0}
     */
    public static final String CEF_RUNTIME_3025 = "CEF_RUNTIME_3025";
    /**
     * 关联类型{0}实例化失败
     */
    public static final String CEF_RUNTIME_3026 = "CEF_RUNTIME_3026";
    /**
     * 反序列化ChangeInfo失败，请联系管理员
     */
    public static final String CEF_RUNTIME_3027 = "CEF_RUNTIME_3027";
    public static final String CEF_RUNTIME_3028 = "CEF_RUNTIME_3028";
    public static final String CEF_RUNTIME_3029 = "CEF_RUNTIME_3029";
    /**
     * 编号为[{0}]的节点上不存在编号为[{1}]的子节点.
     */
    public static final String CEF_RUNTIME_3030 = "CEF_RUNTIME_3030";
    /**
     * 无效节点编号[{0}],请检查编号是否正确或者修改是否部署。
     */
    public static final String CEF_RUNTIME_3031 = "CEF_RUNTIME_3031";

    /**
     * 获取资源值报错:su=[{0}] beid=[{1}] resMetaDataId=[{2}] resourceKey=[{3}]
     */
    public static final String CEF_RUNTIME_3032 = "CEF_RUNTIME_3032";
    /**
     * 获取资源值报错:su=[{0}] resMetaDataId=[{1}] resourceKey=[{2}]
     */
    public static final String CEF_RUNTIME_3033 = "CEF_RUNTIME_3033";
    /**
     * json结构异常
     */
    public static final String CEF_RUNTIME_3034 = "CEF_RUNTIME_3034";
    /**
     * 输入的变更集类型不正确：{0}
     */
    public static final String CEF_RUNTIME_3035 = "CEF_RUNTIME_3035";
    public static final String CEF_RUNTIME_3036 = "CEF_RUNTIME_3036";
    public static final String CEF_RUNTIME_3037 = "CEF_RUNTIME_3037";
    public static final String CEF_RUNTIME_3038 = "CEF_RUNTIME_3038";
    public static final String CEF_RUNTIME_3039 = "CEF_RUNTIME_3039";
    public static final String CEF_RUNTIME_3040 = "CEF_RUNTIME_3040";
    public static final String CEF_RUNTIME_3041 = "CEF_RUNTIME_3041";
    public static final String CEF_RUNTIME_3042 = "CEF_RUNTIME_3042";
    public static final String CEF_RUNTIME_3043 = "CEF_RUNTIME_3043";
    public static final String CEF_RUNTIME_3044 = "CEF_RUNTIME_3044";
    public static final String CEF_RUNTIME_3045 = "CEF_RUNTIME_3045";
    public static final String CEF_RUNTIME_3046 = "CEF_RUNTIME_3046";
    public static final String CEF_RUNTIME_3047 = "CEF_RUNTIME_3047";
    public static final String CEF_RUNTIME_3048 = "CEF_RUNTIME_3048";
    public static final String CEF_RUNTIME_3049 = "CEF_RUNTIME_3049";
    public static final String CEF_RUNTIME_3050 = "CEF_RUNTIME_3050";
    public static final String CEF_RUNTIME_3051 = "CEF_RUNTIME_3051";
    public static final String CEF_RUNTIME_3052 = "CEF_RUNTIME_3052";
    public static final String CEF_RUNTIME_3053 = "CEF_RUNTIME_3053";
    public static final String CEF_RUNTIME_3054 = "CEF_RUNTIME_3054";
    public static final String CEF_RUNTIME_3055 = "CEF_RUNTIME_3055";
    public static final String CEF_RUNTIME_3056 = "CEF_RUNTIME_3056";
    public static final String CEF_RUNTIME_3057 = "CEF_RUNTIME_3057";
    public static final String CEF_RUNTIME_3058 = "CEF_RUNTIME_3058";
    public static final String CEF_RUNTIME_3059 = "CEF_RUNTIME_3059";
    public static final String CEF_RUNTIME_3060 = "CEF_RUNTIME_3060";
    public static final String CEF_RUNTIME_3061 = "CEF_RUNTIME_3061";
    public static final String CEF_RUNTIME_3062 = "CEF_RUNTIME_3062";
    public static final String CEF_RUNTIME_3063 = "CEF_RUNTIME_3063";
    public static final String CEF_RUNTIME_3064 = "CEF_RUNTIME_3064";
    public static final String CEF_RUNTIME_3065 = "CEF_RUNTIME_3065";
    public static final String CEF_RUNTIME_3066 = "CEF_RUNTIME_3066";
    public static final String CEF_RUNTIME_3067 = "CEF_RUNTIME_3067";
}
