package com.inspur.edp.cef.spi.entity;

public class LogicDeleteResInfo {
    private boolean enableLogicDelete = false;
    private String labelId;

    public LogicDeleteResInfo(boolean enableLogicDelete, String labelId) {
        this.enableLogicDelete = enableLogicDelete;
        this.labelId = labelId;
    }

    public boolean isEnableLogicDelete() {
        return enableLogicDelete;
    }

    public void setEnableLogicDelete(boolean enableLogicDelete) {
        this.enableLogicDelete = enableLogicDelete;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }
}
