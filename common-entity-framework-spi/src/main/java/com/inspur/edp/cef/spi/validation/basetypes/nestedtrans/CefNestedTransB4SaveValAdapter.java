/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.validation.basetypes.nestedtrans;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;
import com.inspur.edp.cef.spi.common.UdtManagerUtil;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.udt.api.Udt.IUnifiedDataType;

public class CefNestedTransB4SaveValAdapter extends CefNestedTransValAdapter {
    private DataTypePropertyInfo dataTypePropertyInfo;

    public CefNestedTransB4SaveValAdapter(String name, String propertyName, String udtConfig) {
        super(name, propertyName, udtConfig);
    }

    public CefNestedTransB4SaveValAdapter(String name, String propertyName, String udtConfig, DataTypePropertyInfo dataTypePropertyInfo) {
        super(name, propertyName, udtConfig);
        this.dataTypePropertyInfo = dataTypePropertyInfo;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        if (Util.containsAny(change, new String[]{propertyName})) {
            return true;
        }
        return false;
    }

    @Override
    protected void doExecute(ICefValueObject valueObject, ICefValidationContext context, IChangeDetail change) {
        IUnifiedDataType valueObj = (IUnifiedDataType) valueObject;
        if (this.dataTypePropertyInfo != null) {
            valueObj.getTemplateValues().setRequired(this.dataTypePropertyInfo.getRequired());
        } else {
            valueObj.getTemplateValues().setRequired(false);
        }
        valueObj.setParentContext(context);
        valueObj.setPropertyName(propertyName);
        valueObj.beforeSaveValidate(Util.getNestedPropChange(change, propertyName));

    }

}
