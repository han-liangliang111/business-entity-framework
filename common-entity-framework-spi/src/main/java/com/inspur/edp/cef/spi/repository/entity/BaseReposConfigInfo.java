package com.inspur.edp.cef.spi.repository.entity;

import lombok.Data;

/**
 * @Author wangmaojian
 * @create 2023/4/25
 * 基本扩展信息(字段扩展、表名扩展、数据转换)
 */
@Data
public class BaseReposConfigInfo {
    private String configId;
    private String configCode;
    private String configName;
    private String configImpl;
}
