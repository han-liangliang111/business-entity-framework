/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo;

import com.inspur.edp.cef.api.determination.NestedTransmitType;

/**
 * 国际化模型信息
 */
public abstract class ValueObjModelResInfo extends BaseModelResInfo {
    private NestedTransmitType transType = NestedTransmitType.ValueChanged;

    /**
     * 节点的国际化资源
     *
     * @param objCode 节点编号
     * @return
     */
    public ValueObjResInfo getCustomResource(String objCode) {
        return null;
    }

    public NestedTransmitType getTransmitType() {
        return transType;
    }

    public void setTransmitType(NestedTransmitType type) {
        this.transType = type;
    }
}
