package com.inspur.edp.bef.builtincomponents.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/18
 **/
public class ExceptionCode {
    /**
     * 用户密级权限不足，请修改密级级别！
     */
    public static final String BEF_RUNTIME_1001 = "BEF_RUNTIME_1001";

    /**
     * 未获取到被删除数据的密级级别
     */
    public static final String BEF_RUNTIME_1002 = "BEF_RUNTIME_1002";

    /**
     * 不支持的过滤条件关系类型{0}
     */
    public static final String BEF_RUNTIME_1003 = "BEF_RUNTIME_1003";

    /**
     * 没有获取到权限对象
     */
    public static final String BEF_RUNTIME_1006 = "BEF_RUNTIME_1006";
    /**
     * 获取规则权限结果报错
     */
    public static final String BEF_RUNTIME_1007 = "BEF_RUNTIME_1007";
    /**
     * 没有找到VO动作{0}对应的权限操作信息
     */
    public static final String BEF_RUNTIME_1008 = "BEF_RUNTIME_1008";

    /**
     * 不支持的比较符{0}
     */
    public static final String BEF_RUNTIME_1009 = "BEF_RUNTIME_1009";
}
