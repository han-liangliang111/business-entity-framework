/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.FKConstraints;

import com.inspur.edp.bef.api.BefRtBeanUtil;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.BizMessageException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.cef.api.exceptions.ErrorCodes;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.api.validation.IValueObjValidationContext;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.changeset.AbstractModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import com.inspur.edp.cef.spi.validation.IValidation;
import io.iec.edp.caf.boot.context.CAFContext;
import org.springframework.util.StringUtils;

public class FKCheckValidationAdaptor implements IValidation {
    private String sourcePropName;
    private String propName;
    private String befConfig;
    private String nodeCode;
    private String mainCode;

    public FKCheckValidationAdaptor(String propName, String befConfig, String nodeCode, String mainCode) {
        this.propName = propName;
        this.befConfig = befConfig;
        this.nodeCode = nodeCode;
        this.mainCode = mainCode;
    }

    public FKCheckValidationAdaptor(String propName, String befConfig, String nodeCode, String mainCode, String sourcePropName) {
        this.propName = propName;
        this.befConfig = befConfig;
        this.nodeCode = nodeCode;
        this.mainCode = mainCode;
        this.sourcePropName = sourcePropName;
    }

    public final boolean canExecute(IChangeDetail change) {
        if (change == null) {
            return false;
        }
        AbstractModifyChangeDetail modifyChange = (AbstractModifyChangeDetail) ((change instanceof AbstractModifyChangeDetail) ? change : null);
        if (modifyChange != null) {
            return modifyChange.getPropertyChanges() != null && modifyChange.getPropertyChanges().containsKey(propName);
        }
        return change.getChangeType() == ChangeType.Added;
    }

    public final void execute(ICefValidationContext context, IChangeDetail change) {
        String language = CAFContext.current.getLanguage();

        if (context.getData() == null) {
            return;
        }
        Object tempVar = context.getData().getValue(propName);
        IAuthFieldValue info = (IAuthFieldValue) ((tempVar instanceof IAuthFieldValue) ? tempVar : null);
        if (info == null) {
            return;
        }
        String id = info.getValue();
        if (id == null || "".equals(id)) {
            return;
        }

        if (context instanceof IValidationContext) {
            if (sourcePropName == null || sourcePropName.isEmpty()) {
                FKConstraintValidateParam parameter = new FKConstraintValidateParam(
                        ((IValidationContext) context).getBEContext(), propName, id, befConfig, nodeCode);
                ((IValidationContext) context).getBEContext().addScopeParameter(parameter);
            } else {
                FKConstraintValidateParam parameter = new FKConstraintValidateParam(
                        ((IValidationContext) context).getBEContext(), propName, id, befConfig, nodeCode, sourcePropName);
                ((IValidationContext) context).getBEContext().addScopeParameter(parameter);
            }
        } else if (context instanceof IValueObjValidationContext
                && ((IValueObjValidationContext) context).getParentContext() != null
                && ((IValueObjValidationContext) context).getParentContext() instanceof IValidationContext
                && !StringUtils.isEmpty(((IValueObjValidationContext) context).getPropertyName())) {
            IBENodeEntityContext beCtx = ((IValidationContext) ((IValueObjValidationContext) context).getParentContext()).getBEContext();
            FKConstraintValidateParam parameter = new FKConstraintValidateParam(beCtx,
                    ((IValueObjValidationContext) context).getPropertyName(), id, befConfig, nodeCode);
            beCtx.addScopeParameter(parameter);
        } else {
            String exceptionCode = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Bef_FKVal_0001");
            if (language.equals("zh-CHS")) {
                exceptionCode = exceptionCode + id + "。";
            }
            if (language.equals("en")) {
                exceptionCode = exceptionCode + id + ".";
            }
            //Java版临时屏蔽
            if (!BefRtBeanUtil.getLcpFactory().createLcp(befConfig).isEffective(nodeCode, id)) {
                IBizMessage message = null;
                if (context instanceof IValidationContext) {
                    message = ((IValidationContext) context).createMessageWithLocation(MessageLevel.Error, context.getPropertyI18nName(propName) + exceptionCode, null);
                    throw new BizMessageException(ErrorCodes.ForeignKey, message);
                }
                throw new BefException(ErrorCodes.ForeignKey, context.getPropertyI18nName(propName) + exceptionCode, null, ExceptionLevel.Warning);
            }
        }


    }
}
