/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.UQConstraints;

import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.scope.SingleBETypeExtension;
import com.inspur.edp.cef.api.message.BizMessage;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.api.message.MessageLocation;
import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.UQConstraintConfig;
import com.inspur.edp.cef.entity.UQConstraintMediate;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import com.inspur.edp.udt.entity.ISimpleUdtData;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

public class QUConstraintValScopeExtension extends SingleBETypeExtension {

    private String defaultDisplayMsg;

    @Override
    public void onExtendSetComplete() {
        BEManager beManager = (BEManager) getSessionItem().getBEManager();

        HashMap<String, UQConstraintItem> items = new HashMap<>();
        for (ICefScopeNodeParameter parameter : super.getParameters()) {
            AddParamToItems(items, (UQConstraintValParameter) parameter);
        }
        HashMap<String, ArrayList<UQConstraintMediate>> dict = new HashMap<>();
        for (Map.Entry<String, ArrayList<UQConstraintConfig>> item : beManager.getConstraintInfo()
                .entrySet()) {
            UQConstraintItem uqItem = items.get(item.getKey());
            if (uqItem == null) {
                continue;
            }
            for (UQConstraintConfig config : item.getValue()) {
                UQConstraintMediate mediate = GetUQConstraintMediate(uqItem.getAddedDatas(),
                        uqItem.getModifiedDatas(), uqItem.getModifiedChanges(), uqItem.getDeletedDatas(),
                        config, item.getKey());
                tryToAddConstraintMediateToDict(dict, mediate, item.getKey());
            }
        }
        beManager.getRepository().checkUniqueness(dict, false);

        for (Map.Entry<String, ArrayList<UQConstraintMediate>> pair : dict.entrySet()) {
            for (UQConstraintMediate mediate : pair.getValue()) {
                if (!mediate.getCheckFailed()) {
                    continue;
                }
                ArrayList<String> duplicateDataIds = getDuplicateDataIds(mediate);
                addMessage(createMessage(pair.getKey(), buildI18nMessage(mediate.getMessage()),
                        duplicateDataIds,
                        new ArrayList<>(mediate.getParametersInfo().values().iterator().next().keySet())));
            }
        }
    }

    private void tryToAddConstraintMediateToDict(
            HashMap<String, ArrayList<UQConstraintMediate>> dict,
            UQConstraintMediate mediate, String nodeCode) {
        ArrayList<UQConstraintMediate> list = dict.computeIfAbsent(nodeCode, k -> new ArrayList<>());
        list.add(mediate);
    }

    private void AddParamToItems(HashMap<String, UQConstraintItem> items,
                                 UQConstraintValParameter parameter) {
        UQConstraintItem item = items.get(parameter.getNodeCode());
        if (item == null) {
            items.put(parameter.getNodeCode(), (item = new UQConstraintItem()));
        }
        switch (parameter.getChangeType()) {
            case Added:
                item.getAddedDatas().add(parameter.getChangeData());
                break;
            case Deleted:
                if (parameter.getChangeData() != null) {
                    item.getDeletedDatas().add(parameter.getChangeData().getID());
                }
                break;
            case Modify:
                item.getModifiedDatas().add(parameter.getChangeData());
                break;
        }
    }

    private UQConstraintMediate GetUQConstraintMediate(ArrayList<IEntityData> add,
                                                       ArrayList<IEntityData> modify, ArrayList<ModifyChangeDetail> modifyChange,
                                                       ArrayList<String> deleteDataIds, UQConstraintConfig config, String nodeCode) {
        //排除没有任何约束字段修改的变更数据
        ArrayList<ModifyChangeDetail> changeList = new ArrayList<>();
        for (ModifyChangeDetail change : modifyChange) {
            boolean notContain = true;
            for (int i = 0; i < config.getConstraintFields().size(); i++) {
                if (change.getPropertyChanges().containsKey(config.getConstraintFields().get(i))) {
                    notContain = false;
                    break;
                }
            }
            if (notContain) {
                changeList.add(change);
                for (int i = 0; i < modify.size(); i++) {
                    if (modify.get(i).getID().equals(change.getID())) {
                        modify.remove(modify.get(i));
                        break;
                    }
                }
            }

        }
        for (ModifyChangeDetail item : changeList) {
            modifyChange.remove(item);
        }
        //将新增和修改的数据进行比较，如果有冲突则无需与数据库校验
        ArrayList<IEntityData> check = new ArrayList<>(add.size() + modify.size());
        check.addAll(add);
        check.addAll(modify);

        UQConstraintMediate result = new UQConstraintMediate();
        this.constraintCheck(check, config, nodeCode, result);

        result.getExceptDeleteIds().addAll(deleteDataIds);
        result.getExceptModifyIds().ensureCapacity(modify.size());
        for (IEntityData data : modify) {
            result.getExceptModifyIds().add(data.getID());
        }
        result.setMessage(config.getMessage());
        return result;
    }

    /**
     * 校验数据是否满足唯一性约束
     */
    private void constraintCheck(ArrayList<IEntityData> checkData, UQConstraintConfig constraintConfig,
                                 String nodeCode, UQConstraintMediate result) {
        ArrayList<String> constraintFields = constraintConfig.getConstraintFields();
        //将数据持久化值拼接成String作为unionDataKey借助HashMap比较是否有重复
        HashMap<String, IEntityData> uniqueValues = new HashMap<>(checkData.size() * 4 / 3 + 1);
        HashMap<String, LinkedList<String>> duplicateDataIds = new HashMap<>();
        for (IEntityData currentData : checkData) {
            StringBuilder unionDataKeyBuilder = new StringBuilder();
            for (String field : constraintFields) {
                unionDataKeyBuilder.append(getStrPersistentValue(currentData, field));
            }
            String unionDataKey = unionDataKeyBuilder.toString();
            if (uniqueValues.containsKey(unionDataKey)) {
                String firstDataId = uniqueValues.get(unionDataKey).getID();
                duplicateDataIds.computeIfAbsent(firstDataId, key -> new LinkedList<>());
                duplicateDataIds.get(firstDataId).add(currentData.getID());
            } else {
                uniqueValues.put(unionDataKey, currentData);
            }
        }
        //生成检查结果
        generateCheckResult(checkData, duplicateDataIds, nodeCode, constraintConfig, result);
    }

    /**
     * 获取指定约束，与数据库比较得到的重复数据ID列表
     * @param uqConstraintMediate
     * @return
     */
    private ArrayList<String> getDuplicateDataIds(UQConstraintMediate uqConstraintMediate){
        ArrayList<String> duplicateDtaIds = new ArrayList<>();
        //循环
        for(Map.Entry<String, HashMap<String, Object>> entry : uqConstraintMediate.getParametersInfo().entrySet()){
            String dataId = entry.getKey();
            HashMap<String, Object> itemValue = entry.getValue();
            //todo 比较一次后，从待比较的数据中删除掉。。
            Iterator<IEntityData> dataIterator = uqConstraintMediate.getDuplicateDbDatas().iterator();
            while (dataIterator.hasNext()){
                IEntityData data = dataIterator.next();
                boolean isEqual = compareData(itemValue, data);
                if(isEqual){
                    duplicateDtaIds.add(dataId);
                    dataIterator.remove();
                    //继续下一条数据的比较
                    break;
                }
            }
        }
        return duplicateDtaIds;
    }

    /**
     * 比较数据库值和要比较数据
     * @param hashMap
     * @param entityData
     * @return
     */
    private boolean compareData(HashMap<String, Object> hashMap, IEntityData entityData){
        boolean isEqual = true;
        for(Map.Entry<String, Object> entry : hashMap.entrySet()){
            String fieldCode = entry.getKey();
            Object fieldValue = entry.getValue();

            Object fValue = getPersistentValue(fieldValue);
            Object dataValue = getPersistentValue(entityData, fieldCode);
            if(fValue == null && fValue == null){
                continue;
            }
            if(fValue!=null && fValue.equals(dataValue)){
                continue;
            }
            if(entityData.getValue(fieldCode) instanceof BigDecimal && fieldValue instanceof  BigDecimal){
               if(((BigDecimal) entityData.getValue(fieldCode)).compareTo((BigDecimal) fieldValue)!=0){
                   return false;
               }
            }else {
                return false;
            }
        }
        return isEqual;
    }

    /**
     * 生成检查结果
     */
    private void generateCheckResult(ArrayList<IEntityData> check, HashMap<String, LinkedList<String>> duplicateDataIds,
                                     String nodeCode, UQConstraintConfig config, UQConstraintMediate result) {
        HashSet<String> allDuplicatedDataIds = new HashSet<>();
        if (duplicateDataIds != null && !duplicateDataIds.isEmpty()) {
            duplicateDataIds.forEach(
                    (dataId, duplicatedDataIds) -> {
                        duplicatedDataIds.add(0, dataId);
                        String exceptionCode = StringUtils.isEmpty(config.getMessage()) ?
                                getDefaultDisplayMessage() : config.getMessage();
                        addMessage(
                                createMessage(nodeCode, exceptionCode, duplicatedDataIds,
                                        config.getConstraintFields()));
                        allDuplicatedDataIds.addAll(duplicatedDataIds);
                    });
        }
        for (IEntityData currentData : check) {
            if (!allDuplicatedDataIds.contains(currentData.getID())) {
                result.getParametersInfo().put(currentData.getID(),
                        buildConstraintValues(config, currentData));
            }
        }
    }

    /**
     * 获取持久化的值，不支持多值UDT数据
     */
    private String getStrPersistentValue(IEntityData data, String field) {
        return String.valueOf(getPersistentValue(data, field));
    }

    private Object getPersistentValue(IEntityData data, String field) {
        Object tempVar = data.getValue(field);
        Object result;
        if (tempVar instanceof ISimpleUdtData) {
            //单值UDT数据
            ISimpleUdtData<?> simpleUdtData = (ISimpleUdtData<?>) tempVar;
            if (simpleUdtData.getValue() instanceof IAuthFieldValue) {
                //UDT对象类型为”关联“
                result = ((IAuthFieldValue) simpleUdtData.getValue()).getValue();
            } else {
                result = simpleUdtData.getValue();
            }
        } else if (tempVar instanceof IAuthFieldValue) {
            //关联数据
            result = ((IAuthFieldValue) tempVar).getValue();
        } else {
            //基本类型数据
            result = tempVar;
        }
        return result;
    }

    /**
     * 获取
     * @param fieldValue
     * @return
     */
    private Object getPersistentValue(Object fieldValue) {
        Object result;
        if (fieldValue instanceof ISimpleUdtData) {
            //单值UDT数据
            ISimpleUdtData<?> simpleUdtData = (ISimpleUdtData<?>) fieldValue;
            if (simpleUdtData.getValue() instanceof IAuthFieldValue) {
                //UDT对象类型为”关联“
                result = ((IAuthFieldValue) simpleUdtData.getValue()).getValue();
            } else {
                result = simpleUdtData.getValue();
            }
        } else if (fieldValue instanceof IAuthFieldValue) {
            //关联数据
            result = ((IAuthFieldValue) fieldValue).getValue();
        } else {
            //基本类型数据
            result = fieldValue;
        }
        return result;
    }

    private HashMap<String, Object> buildConstraintValues(UQConstraintConfig config, IEntityData data) {
        HashMap<String, Object> value = new HashMap<>((int) (config.getConstraintFields().size() / 0.75 + 1));
        for (String field : config.getConstraintFields()) {
            value.put(field, data.getValue(field));
        }
        return value;
    }

    private synchronized String getDefaultDisplayMessage() {
        if (defaultDisplayMsg == null) {
            defaultDisplayMsg = I18nResourceUtil.getResourceItemValue(
                    "pfcommon", "cef_exception.properties", "Gsp_Bef_UQVal_0001");
        }
        return defaultDisplayMsg;
    }

    private String buildI18nMessage(String message) {
        return StringUtils.isEmpty(message) ? getDefaultDisplayMessage() : message;
    }

    public IBizMessage createMessage(String nodeCode, String msg, List<String> dataIds,
                                     List<String> columns) {
        return new BizMessage() {{
            setMessageFormat(msg);
            setLevel(MessageLevel.Error);
            setLocation(new MessageLocation() {{
                setDataIds(dataIds);
                setNodeCode(nodeCode);
                setColumnNames(columns);
            }});
        }};
    }
}
