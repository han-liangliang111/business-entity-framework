//package com.inspur.edp.bef.builtincomponents;
//
//import com.inspur.edp.bef.builtincomponents.FKConstraints.FKConstraintValScopeExtension;
//import com.inspur.edp.bef.builtincomponents.UQConstraints.QUConstraintValScopeExtension;
//import com.inspur.edp.bef.core.action.save.SaveWithScopeExtension;
//import com.inspur.edp.cef.spi.scope.AbstractCefScopeExtension;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//@EnableTransactionManagement
//@Configuration
//public class BuiltInComponentsAutoConfiguration {
//    @Bean("PlatformCommon_Bef_UQConstraintValNodeParameter")
//    @Scope(value="prototype")
//    public QUConstraintValScopeExtension getSaveExt() {
//        return new QUConstraintValScopeExtension();
//    }
//
//    @Bean("PlatformCommon_Bef_FKConstraint")
//    @Scope(value="prototype")
//    public FKConstraintValScopeExtension getFKConstraintExtension() {
//        return new FKConstraintValScopeExtension();
//    }
//}
