/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.idgen;

import com.inspur.edp.bef.spi.entity.IdGenerator;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

//智慧粮食需求, 32位长度Id
public class UUID32 implements IdGenerator {

    private static String getString(UUID uid) {
        return digits(uid.getMostSignificantBits()) + digits(uid.getLeastSignificantBits());
    }

    private static String digits(long val) {
        String str = Long.toHexString(val);
        return StringUtils.leftPad(str, 16, '0');
    }

    @Override
    public String generateId() {
        return getString(UUID.randomUUID());
    }
}
