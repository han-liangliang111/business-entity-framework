package com.inspur.edp.bef.builtincomponents.auth;

import com.inspur.edp.bef.api.BefRtBeanUtil;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.exceptions.BefFuncPermissionDeniedException;
import com.inspur.edp.bef.api.lcp.AuthInfo;
import com.inspur.edp.bef.builtincomponents.exception.ExceptionCode;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.cef.api.ValueGetterConfig;
import com.inspur.edp.cef.api.authority.AuthFilter;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.authority.AuthorityInfoType;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.entity.condition.ExpressRelationType;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import com.inspur.edp.udt.entity.ISimpleUdtData;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.permission.api.data.designtime.datapermission.permrule.GPermRuleArgTypeEnum;
import io.iec.edp.caf.rpc.api.service.RpcClient;
import io.iec.edp.caf.securityentry.api.common.AuthType;
import io.iec.edp.caf.securityentry.api.data.AuthOperationEntry;
import io.iec.edp.caf.securityentry.api.data.SecurityEntry;
import io.iec.edp.caf.securityentry.api.data.SecurityRuleEntry;
import io.iec.edp.caf.securityentry.api.manager.SecurityEntryService;
import io.iec.edp.gpermrule.runtime.api.entity.*;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Priority;
import java.util.*;

/**
 * 规则权限控制器
 */
@Priority(100)
public class PermRuleDataPermissionController implements
        com.inspur.edp.bef.spi.auth.DataPermissionController<PermRuleDataPermissionCache> {

    private static SecurityEntryService securityEntryService = null;
    private static RpcClient client = SpringBeanUtils.getBean(RpcClient.class);

    /**
     * 将权限结果转换为BEF过滤条件
     * @param ruleResult 权限分配结果
     * @return
     */
    private List<AuthorityInfo> convertExpress2Filter(GPermResultEntity ruleResult) {
        if (ruleResult.getPermResultExpressList() == null || ruleResult.getPermResultExpressList().isEmpty()) {
            return Collections.emptyList();
        }
        AuthorityInfo authInfo = new AuthorityInfo();
        authInfo.setAuthType(AuthorityInfoType.Filter);
        List<AuthFilter> filters = new ArrayList<>(ruleResult.getPermResultExpressList().size());
        for (GPermResultExpress permResultExpress : ruleResult.getPermResultExpressList()) {
            filters.add(convertExpress2Condition(permResultExpress));
        }
        authInfo.setFilter(filters);
        if (ruleResult.getParameters() != null && !ruleResult.getParameters().isEmpty()) {
            authInfo.setFilterParameters(convertAuthParameters(ruleResult.getParameters()));
        }
        List<AuthorityInfo> rez = new ArrayList<>(1);
        rez.add(authInfo);
        return rez;
    }

    /**
     * 绑定参数映射转换
     *
     * @param pars
     * @return
     */
    private List<DbParameter> convertAuthParameters(Map<Integer, GPermParamEntity> pars) {
        List<DbParameter> rez = new ArrayList<>(pars.size());
        for (Map.Entry<Integer, GPermParamEntity> item : pars.entrySet()) {
            GPermParamEntity par = item.getValue();
            if (par.getFieldType() != GPermFieldType.String) {
                throw new BefRunTimeBaseException("Unsupported fieldtype:" + par.getFieldType());
            }
            //in类型值存在inValuesStr中
            if (par.isInValues()) {
                rez.add(new DbParameter(par.getFieldName(), GspDbDataType.VarChar, par.getInValuesStr()));
            } else {
                rez.add(new DbParameter(par.getFieldName(), GspDbDataType.VarChar, par.getValueStr()));
            }
        }
        return rez;
    }

    private AuthFilter convertExpress2Condition(GPermResultExpress permResultExpress) {
        AuthFilter rez = new AuthFilter();
        rez.setFilterBody(convert2ExpressBody(permResultExpress));
        rez.setColumnName(permResultExpress.getRealField());
        rez.setRelationType(convert2ExpressRelation(permResultExpress.getRelation()));
        return rez;
    }

    private String convert2ExpressBody(GPermResultExpress permResultExpress) {
        String filterValue = permResultExpress.getValue();
        switch (permResultExpress.getCompare()) {
            case Equal:
                return String.format(" = %s ", filterValue);
            case In:
                return String.format("in (%s)", filterValue);
            default:
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_1009, String.valueOf(permResultExpress.getCompare()));
        }
    }

    private ExpressRelationType convert2ExpressRelation(GPermExpressRelationType relationType) {
        switch (relationType) {
            case And:
                return ExpressRelationType.And;
            case Or:
                return ExpressRelationType.Or;
            case Empty:
                return ExpressRelationType.Empty;
            default:
                throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_1003, relationType.toString());
        }
    }

    @Override
    public String getName() {
        return "VMRule";
    }

    @Override
    public PermRuleDataPermissionCache buildCache() {
        return new PermRuleDataPermissionCache();
    }

    /**
     * 检查数据权限
     *
     * @param actionCode
     * @param beContext
     * @param authInfo
     * @param cache
     * @return
     */
    @Override
    public boolean checkDataAuthority(String actionCode, IBEContext beContext,
                                      AuthInfo authInfo, PermRuleDataPermissionCache cache) {
        SecurityEntry securityEntry = getAuthSecurityEntry(authInfo);
        AuthOperationEntry authOperationEntry = securityEntry.getAuthOpRelations().stream().filter(item -> item.getActionId().equals(actionCode)).findFirst().orElse(null);
        if (authOperationEntry == null) {
            return true;
        }
        LinkedHashMap<String, Object> paramMap = buildCheckDataAuthParam(actionCode, beContext, securityEntry);
        String serviceName = "io.iec.edp.gpermrule.runtime.api.manager.GPermRuleRunTimeManager.isHasDataPermission";
        Boolean hasPermission;
        try{
            hasPermission = client.invoke(Boolean.class, serviceName, "Sys", paramMap, null);
        }
        catch (Throwable throwable){
            throw new BefRunTimeBaseException("", new Exception(throwable));
        }
        return hasPermission;
    }

    /**
     * 获取数据权限信息
     *
     * @param authInfo
     * @param cache
     * @return
     */
    @Override
    public List<AuthorityInfo> getQueryAuthInfo(AuthInfo authInfo, PermRuleDataPermissionCache cache) {
        SecurityEntry entryResult = getAuthSecurityEntry(authInfo);
        //权限对象ID
        if (StringUtils.isEmpty(entryResult.getAuthObjIds())) {
            throw new BefRunTimeBaseException(ExceptionCode.BEF_RUNTIME_1006);
        }

        //数据操作与单据操作映射关系
        List<AuthOperationEntry> opList = entryResult.getAuthOpRelations();

        if (opList == null || opList.size() == 0) {
            return Collections.emptyList();
        }
        AuthOperationEntry authOperationEntry = entryResult.getAuthOpRelations().stream().filter(item -> item.getActionId().equals("Query")).findFirst().orElse(null);
        if (authOperationEntry == null) {
            return Collections.emptyList();
        }
        GPermResultEntity resultEntity = getPermResultEntity(entryResult);
        switch (resultEntity.getPermResultType()) {
            case ALLPerm:
                return Collections.emptyList();
            case NOPerm:
                throw new BefFuncPermissionDeniedException();
            case Express:
                return convertExpress2Filter(resultEntity);
            default:
                throw new BefRunTimeBaseException("Unrecognized permission result type");
        }
    }

    /**
     * 获取权限信息
     *
     * @param authInfo
     * @return
     */
    private SecurityEntry getAuthSecurityEntry(AuthInfo authInfo) {
        SecurityEntry entry = createSecurityEntry(authInfo);
        SecurityEntry securityEntry = getSecurityEntryService().getAuthSecurityEntry(entry);
        return securityEntry;
    }

    /**
     * 根据AuthInfo构造SecurityEntry
     *
     * @param authInfo
     * @return
     */
    private SecurityEntry createSecurityEntry(AuthInfo authInfo) {
        SecurityEntry entry = new SecurityEntry();
        entry.setEx1(authInfo.getExtend1());//VOID
        entry.setExtType("VMRule");//必须所有规则类表单都是固定值，类似VM，VOHelp、VMRule
        entry.setAuthType(AuthType.PermRule);//必须,规则权限固定用AuthType.PermRule即可。
        return entry;
    }

    /**
     * 构造检查数据权限参数信息
     *
     * @param securityEntry
     * @return
     */
    private LinkedHashMap<String, Object> buildCheckDataAuthParam(String actionCode, IBEContext beContext, SecurityEntry securityEntry) {
        LinkedHashMap<String, Object> paramMap = new LinkedHashMap<>();
        paramMap.put("authorizationId", securityEntry.getAuthObjIds());
        AuthOperationEntry authOperationEntry = securityEntry.getAuthOpRelations().stream().filter(item -> item.getActionId().equals(actionCode)).findFirst().orElse(null);
        //数据操作ID
        paramMap.put("opId", authOperationEntry.getOperationId());

        //参数映射关系转换
        Map<String, Map<String, String>> mapArgValues = new HashMap<>();
        Map<String, Map<String, String>> mapArgEntityValues = new HashMap<>();
        for (SecurityRuleEntry ruleEntry : securityEntry.getSecurityRuleEntryList()) {
            if (StringUtils.isEmpty(ruleEntry.getPermRuleId())) {
                continue;
            }
            if (StringUtils.isEmpty(ruleEntry.getPermRuleArgId())) {
                continue;
            }
            if (StringUtils.isEmpty(ruleEntry.getPermRuleArgValue())) {
                continue;
            }
            String permRuleId = ruleEntry.getPermRuleId();
            //etc:CreatorField
            String argId = ruleEntry.getPermRuleArgId();
            //etc:createUser VO上的字段标签
            String argValue = ruleEntry.getPermRuleArgValue();
            if (mapArgValues.containsKey(permRuleId)) {
                mapArgValues.get(permRuleId).putIfAbsent(argId, argValue);
            } else {
                mapArgValues.put(permRuleId, new HashMap<>());
                mapArgValues.get(permRuleId).put(argId, argValue);
            }

            //构造字段实际值，进行数据过滤用，排除非数据绑定参数(是否系统用户、是否系统组织等属性)
            if (ruleEntry.getPermRuleArgType() == GPermRuleArgTypeEnum.DynamicBind) {
                String argEntityValue = getFieldValue(beContext, argValue);
                if (mapArgEntityValues.containsKey(permRuleId)) {
                    mapArgEntityValues.get(permRuleId).putIfAbsent(argValue, argEntityValue);
                } else {
                    mapArgEntityValues.put(permRuleId, new HashMap<>());
                    mapArgEntityValues.get(permRuleId).put(argValue, argEntityValue);
                }
            }
        }

        paramMap.put("ruleArgValueMap", mapArgValues);
        //key为上面的argValue,value为对应字段在data中的值
        paramMap.put("ruleArgEntityValueMap", mapArgEntityValues);
        return paramMap;
    }

    /**
     * 获取规则权限结果
     *
     * @param securityEntry
     * @return
     */
    private GPermResultEntity getPermResultEntity(SecurityEntry securityEntry) {
        LinkedHashMap<String, Object> paramMap = buildGetPermRuleResParam(securityEntry);
        String serviceName = "io.iec.edp.gpermrule.runtime.api.manager.GPermRuleRunTimeManager.getPermRuleResult";
        GPermResultEntity  permResultEntity;
        try{
            permResultEntity = client.invoke(GPermResultEntity.class, serviceName, "Sys", paramMap, null);
        }
        catch (Throwable throwable){
            throw new BefRunTimeBaseException("", new Exception(throwable));
        }

        return permResultEntity;
    }

    /**
     * 构造获取数据权限的参数信息
     *
     * @param securityEntry
     * @return
     */
    private LinkedHashMap<String, Object> buildGetPermRuleResParam(SecurityEntry securityEntry) {
        LinkedHashMap<String, Object> paramMap = new LinkedHashMap<>();
        //权限对象ID
        paramMap.put("authorizationId", securityEntry.getAuthObjIds());

        AuthOperationEntry authOperationEntry = securityEntry.getAuthOpRelations().stream().filter(item -> item.getActionId().equals("Query")).findFirst().orElse(null);
        paramMap.put("opId", authOperationEntry.getOperationId());

        //参数映射关系转换
        Map<String, Map<String, String>> mapArgValues = new HashMap<>();
        for (SecurityRuleEntry ruleEntry : securityEntry.getSecurityRuleEntryList()) {
            if (StringUtils.isEmpty(ruleEntry.getPermRuleId())) {
                continue;
            }
            if (StringUtils.isEmpty(ruleEntry.getPermRuleArgId())) {
                continue;
            }
            if (StringUtils.isEmpty(ruleEntry.getPermRuleArgValue())) {
                continue;
            }
            String permRuleId = ruleEntry.getPermRuleId();
            String argId = ruleEntry.getPermRuleArgId();
            String argValue = ruleEntry.getPermRuleArgValue();
            if (mapArgValues.containsKey(permRuleId)) {
                mapArgValues.get(permRuleId).putIfAbsent(argId, argValue);
                continue;
            }
            mapArgValues.put(permRuleId, new HashMap<>());
            mapArgValues.get(permRuleId).put(argId, argValue);
        }

        paramMap.put("ruleArgValueMap", mapArgValues);
        return paramMap;
    }

    /**
     * 获取指定字段的值
     *
     * @param beContext
     * @param fieldCode
     * @return
     */
    private String getFieldValue(IBEContext beContext, String fieldCode) {
        String data = null;
        ValueGetterConfig valueGetterConfig = new ValueGetterConfig();
        valueGetterConfig.setAssociationValue(false);
        valueGetterConfig.setSingleValueUdt(true);

        Object tempVar = BefRtBeanUtil
                .getValueGetter()
                .getValue(beContext.getCurrentData(), fieldCode, valueGetterConfig);
        IAuthFieldValue authFieldValue = (IAuthFieldValue) ((tempVar instanceof IAuthFieldValue)
                ? tempVar : null);
        if (tempVar != null && tempVar instanceof ISimpleUdtData) {
            ISimpleUdtData data1 = (ISimpleUdtData) tempVar;
            if (data1.getValue() != null && data1.getValue() instanceof IAuthFieldValue) {
                data = ((IAuthFieldValue) data1.getValue()).getValue();
            } else if (data1.getValue() == null) {
                data = null;
            } else {
                data = String.valueOf(data1.getValue());
            }
        } else if (authFieldValue != null) {
            data = authFieldValue.getValue();
        } else {
            if (tempVar != null) {
                data = String.valueOf(tempVar);
            }
        }
        return data;
    }

    private SecurityEntryService getSecurityEntryService() {
        if (securityEntryService == null) {
            securityEntryService = SpringBeanUtils.getBean(SecurityEntryService.class);
        }
        return securityEntryService;
    }
}
