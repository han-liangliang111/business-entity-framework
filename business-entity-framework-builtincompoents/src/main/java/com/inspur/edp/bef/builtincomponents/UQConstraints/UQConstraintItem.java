/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.UQConstraints;


import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

public class UQConstraintItem {
    private java.util.ArrayList<IEntityData> addedDatas = new java.util.ArrayList<IEntityData>();
    private java.util.ArrayList<IEntityData> modifiedDatas = new java.util.ArrayList<IEntityData>();
    private java.util.ArrayList<String> deletedDatas = new java.util.ArrayList<String>();
    private java.util.ArrayList<ModifyChangeDetail> modifiedChanges = new java.util.ArrayList<ModifyChangeDetail>();

    public java.util.ArrayList<IEntityData> getAddedDatas() {
        return addedDatas;
    }

    public java.util.ArrayList<IEntityData> getModifiedDatas() {
        return modifiedDatas;
    }

    public java.util.ArrayList<String> getDeletedDatas() {
        return deletedDatas;
    }

    public java.util.ArrayList<ModifyChangeDetail> getModifiedChanges() {
        return modifiedChanges;
    }
}
