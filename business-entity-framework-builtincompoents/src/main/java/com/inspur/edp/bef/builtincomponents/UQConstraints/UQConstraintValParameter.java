/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.UQConstraints;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.core.scope.ScopeNodeParameter;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.entity.IEntityData;

public class  UQConstraintValParameter extends ScopeNodeParameter {
    /**
     * 对象编号
     */
    private String privateNodeCode;
    /**
     * 数据ID
     */
    private String privateDataID;
    /**
     * 变更集类型
     */
    private ChangeType privateChangeType;
    /**
     * 变更数据
     */
    private IEntityData privateChangeData;

    public UQConstraintValParameter(IBEContext beContext, String nodeCode, String dataID, ChangeType changeType, IEntityData changeData) {
        super(beContext);
        privateNodeCode = nodeCode;
        privateDataID = dataID;
        privateChangeType = changeType;
        privateChangeData = changeData;
    }

    @Override
    public String getParameterType() {
        return "PlatformCommon_Bef_UQConstraintValNodeParameter";
    }

    public final String getNodeCode() {
        return privateNodeCode;
    }

    public final String getDataID() {
        return privateDataID;
    }

    public final ChangeType getChangeType() {
        return privateChangeType;
    }

    public final IEntityData getChangeData() {
        return privateChangeData;
    }

    /**
     * 是否可以跨类型合并执行
     * @return
     */
    @Override
    public boolean isMergableCrossType() {
        return true;
    }
}
