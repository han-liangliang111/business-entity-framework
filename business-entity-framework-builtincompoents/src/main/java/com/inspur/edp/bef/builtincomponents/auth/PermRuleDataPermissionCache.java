package com.inspur.edp.bef.builtincomponents.auth;

import lombok.Data;

@Data
public class PermRuleDataPermissionCache implements com.inspur.edp.bef.spi.auth.DataPermissionCache {

    @Override
    public void clear() {
    }
}
