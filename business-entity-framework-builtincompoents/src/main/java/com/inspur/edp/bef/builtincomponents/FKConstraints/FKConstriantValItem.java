/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.FKConstraints;

import java.util.ArrayList;
import java.util.List;
import javax.print.DocFlavor.STRING;

public class FKConstriantValItem {

    private final String befConfig;
    private final List<String> dataIds;
    private final String targetNodeCode;
    private String propName;
    private List<String> sourceDataIds = new ArrayList<>();

    FKConstriantValItem(String befConfig, List<String> dataIds, String targetNodeCode) {
        this.befConfig = befConfig;
        this.dataIds = dataIds;
        this.targetNodeCode = targetNodeCode;
    }

    FKConstriantValItem(String befConfig, List<String> dataIds, String targetNodeCode, String propName) {
        this.befConfig = befConfig;
        this.dataIds = dataIds;
        this.targetNodeCode = targetNodeCode;
        this.propName = propName;
    }

    public String getPropName() {
        return propName;
    }

    public String getBefConfig() {
        return befConfig;
    }

    public List<String> getDataIds() {
        return dataIds;
    }

    public String getTargetNodeCode() {
        return targetNodeCode;
    }

    public List<String> getSourceDataIds() {
        return sourceDataIds;
    }
}
