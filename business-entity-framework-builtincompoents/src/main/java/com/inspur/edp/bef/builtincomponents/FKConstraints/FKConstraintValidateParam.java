/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.FKConstraints;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.scope.ScopeNodeParameter;

public class FKConstraintValidateParam extends ScopeNodeParameter {

    static String parameterType = "PlatformCommon_Bef_FKConstraint";
    private final String befConfig;
    private final String targetNodeCode;
    private String sourcePropName;
    private String propName;
    private String nodeCode;
    private String dataId;

    public FKConstraintValidateParam(IBENodeEntityContext beContext, String propName, String dataId,
                                     String befConfig, String targetNodeCode) {
        super(getRootContext(beContext), beContext);
        this.propName = propName;
        this.nodeCode = beContext.getCode();
        this.dataId = dataId;
        this.befConfig = befConfig;
        this.targetNodeCode = targetNodeCode;
    }

    @Deprecated
    public FKConstraintValidateParam(IBEContext beContext, String propName, String nodeCode,
                                     String dataId, String befConfig, String targetNodeCode) {
        super(beContext);
        this.propName = propName;
        this.nodeCode = nodeCode;
        this.dataId = dataId;
        this.befConfig = befConfig;
        this.targetNodeCode = targetNodeCode;
    }

    public FKConstraintValidateParam(IBENodeEntityContext beContext, String propName, String dataId,
                                     String befConfig, String targetNodeCode, String sourcePropName) {
        super(getRootContext(beContext), beContext);
        this.propName = propName;
        this.nodeCode = beContext.getCode();
        this.dataId = dataId;
        this.befConfig = befConfig;
        this.targetNodeCode = targetNodeCode;
        this.sourcePropName = sourcePropName;
    }

    private static IBEContext getRootContext(IBENodeEntityContext context) {
        if (context == null)
            return null;
        if (context instanceof IBEContext)
            return (IBEContext) context;
        if (context.getParentContext() != null)
            return getRootContext(context.getParentContext());
        return null;
    }

    public String getSourcePropName() {
        return sourcePropName;
    }

    public String getPropName() {
        return propName;
    }

    public String getNodeCode() {
        return nodeCode;
    }

    public String getDataId() {
        return dataId;
    }

    @Override
    public String getParameterType() {
        return parameterType;
    }

    public String getBefConfig() {
        return befConfig;
    }

    public String getTargetNodeCode() {
        return targetNodeCode;
    }

    @Override
    public boolean isMergableCrossType() {
        return true;
    }
}
