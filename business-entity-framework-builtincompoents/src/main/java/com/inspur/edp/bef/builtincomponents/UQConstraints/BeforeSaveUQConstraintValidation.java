/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.UQConstraints;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.UQConstraintConfig;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.entity.info.UniqueConstraintInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.validation.IValidation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BeforeSaveUQConstraintValidation implements IValidation {
    private static final String UQKeyPrefix = "pfcommon_UQConfigKey";
    private CefEntityResInfoImpl cefEntityResInfo;

    public BeforeSaveUQConstraintValidation(CefEntityResInfoImpl cefEntityResInfo) {
        this.cefEntityResInfo = cefEntityResInfo;
    }

    public boolean canExecute(IChangeDetail change) {
        return true;
    }

    public void execute(ICefValidationContext compContext, IChangeDetail change) {
        //key值为对象编号
        HashMap<String, ArrayList<UQConstraintConfig>> hashMap = getConstraintConfig((IValidationContext) compContext);
        if (hashMap == null || hashMap.isEmpty()) {
            return;
        }
        new com.inspur.edp.bef.builtincomponents.UQConstraints.UQConstraintValidator((com.inspur.edp.bef.api.action.validation.IValidationContext) compContext, change, hashMap).Do();
    }

    //TODO:getUniqueConstraintMessage调用与EngineBEManager.getUniqueConstraintsInfo是重复的, 需要梳理确认
    private HashMap<String, ArrayList<UQConstraintConfig>> getConstraintConfig(com.inspur.edp.bef.api.action.validation.IValidationContext context) {
        Map<String, UniqueConstraintInfo> constraintInfoFromRes = cefEntityResInfo
                .getEntityTypeInfo().getUniqueConstraintInfos();
        if (constraintInfoFromRes == null || constraintInfoFromRes.isEmpty()) {
            return null;
        }

        String uqKey = UQKeyPrefix.concat(context.getNodeCode());
        IBefCallContext callContext = context.getBEContext().getCallContext();
        if (!callContext.containsKey(uqKey)) {
            HashMap<String, ArrayList<UQConstraintConfig>> hashMap = new HashMap<>();
            ArrayList<UQConstraintConfig> uqConstraintConfigs = new ArrayList<>(constraintInfoFromRes.size());
            String nodeCode = "";
            for (Map.Entry<String, UniqueConstraintInfo> entry : constraintInfoFromRes.entrySet()) {
                nodeCode = entry.getValue().getNodeCode();
                UQConstraintConfig config = new UQConstraintConfig();
                config.setConstraintFields(entry.getValue().getFields());
                config.setMessage(cefEntityResInfo.getUniqueConstraintMessage(entry.getKey()));
                uqConstraintConfigs.add(config);
            }
            hashMap.put(nodeCode, uqConstraintConfigs);
            callContext.setData(uqKey, hashMap);
        }
        return callContext.getData(uqKey);
    }
}
