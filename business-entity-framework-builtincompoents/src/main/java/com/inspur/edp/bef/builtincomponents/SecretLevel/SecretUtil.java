/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.SecretLevel;

import io.iec.edp.caf.sys.security.api.data.SecLevelObjSet;

public class SecretUtil {
    public static SecetLevelObjSetInfo getSecretLevelObjSetInfo(SecLevelObjSet secLevelObjSet) {
        SecetLevelObjSetInfo secertInfo = new SecetLevelObjSetInfo();
        String fieldName = "";
        String fieldType = "";//secLevelIdField,secLevelField,secLevelNameField
        if (secLevelObjSet.getSecLevelIdField() != null && secLevelObjSet.getSecLevelIdField().length() > 0) {
            fieldType = "secLevelIdField";
            fieldName = secLevelObjSet.getSecLevelIdField();
        }
        if (fieldName.isEmpty()) {
            if (secLevelObjSet.getSecLevelField() != null && secLevelObjSet.getSecLevelField().length() > 0) {
                fieldType = "secLevelField";
                fieldName = secLevelObjSet.getSecLevelField();
            }
        }
        if (fieldName.isEmpty()) {
            if (secLevelObjSet.getSecLevelNameField() != null && secLevelObjSet.getSecLevelNameField().length() > 0) {
                fieldType = "secLevelNameField";
                fieldName = secLevelObjSet.getSecLevelField();
            }
        }
        secertInfo.setFieldName(fieldName);
        secertInfo.setFieldType(fieldType);
        return secertInfo;
    }
}
