/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.SecretLevel;

import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;
import io.iec.edp.caf.sys.security.api.manager.SecurityLevelManager;

public class SecretValidation implements IValidation {
    private String beid;
    private SecurityLevelManager securityLevelManager;

    @Override
    public boolean canExecute(IChangeDetail iChangeDetail) {
        //因为有缓存，运行时启用密级，这样判断不合适。
//        if(securityLevelManager.getIsEnableByBE(beid)){
//            return true;
//        }
        return true;
    }

    @Override
    public void execute(ICefValidationContext cefValidationContext, IChangeDetail changeDetail) {
        new SecretValidator((IValidationContext) cefValidationContext, changeDetail).execute();
    }
}
