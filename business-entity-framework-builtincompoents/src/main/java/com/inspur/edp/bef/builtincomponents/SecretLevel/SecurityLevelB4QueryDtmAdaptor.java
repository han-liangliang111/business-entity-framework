/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.builtincomponents.SecretLevel;

import com.inspur.edp.bef.api.action.determination.IQueryDeterminationContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;

public class SecurityLevelB4QueryDtmAdaptor implements IDetermination {
    @Override
    public String getName() {
        return "SecurityLevelB4QueryDtm";
    }

    @Override
    public boolean canExecute(IChangeDetail iChangeDetail) {
        return true;
    }

    @Override
    public void execute(ICefDeterminationContext context, IChangeDetail iChangeDetail) {
        new SecurityLevelB4QueryDetermination((IQueryDeterminationContext) context).Do();
    }
}
