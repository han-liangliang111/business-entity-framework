package com.inspur.edp.bef.builtincomponents.UQConstraints;

import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.session.BEFuncSessionBase;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.repository.IRootRepository;
import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import com.inspur.edp.cef.entity.UQConstraintConfig;
import com.inspur.edp.cef.entity.UQConstraintMediate;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import com.inspur.edp.udt.entity.ISimpleUdtData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * 运行说明：该测试用例classpath过长，IDEA运行时需要设置运行Configurations中项："Shorten command line：classpath file"
 */
public class QUConstraintValScopeExtensionTest {

    private static final String CONSTRAINT_FIELD_PROJECT = "project";
    private static final String CONSTRAINT_FIELD_PHONE = "phone";
    private static final String NODE_CODE = "nodeCode";
    private final List<IBizMessage> messages = new ArrayList<>();
    private UQConstraintMediate mediate;

    @Test
    public void testOnGetUQConstraintMediate() {
        //构造数据
        UQConstraintConfig constraintConfig = new UQConstraintConfig();
        constraintConfig.setConstraintFields(new ArrayList<>(Arrays.asList(CONSTRAINT_FIELD_PROJECT, CONSTRAINT_FIELD_PHONE)));
        constraintConfig.setMessage("编号和手机号不能重复");
        HashMap<String, ArrayList<UQConstraintConfig>> constraintInfo = new HashMap<>();
        constraintInfo.put(NODE_CODE, new ArrayList<>(Collections.singletonList(constraintConfig)));
        //mockBEManager
        try (MockedStatic<FuncSessionManager> sessionManager = mockStatic(FuncSessionManager.class)) {
            FuncSession mockedFuncSession = mock(FuncSession.class);
            when(FuncSessionManager.getCurrentSession()).thenReturn(mockedFuncSession);
            BEFuncSessionBase mockedFuncSessionItem = mock(BEFuncSessionBase.class);
            when(mockedFuncSession.getFuncSessionItem(any())).thenReturn(mockedFuncSessionItem);
            BEManager mockedBEManager = mock(BEManager.class);
            when(mockedFuncSessionItem.getBEManager()).thenReturn(mockedBEManager);
            //mock constraintInfo
            when(mockedBEManager.getConstraintInfo()).thenReturn(constraintInfo);
            //mock Repository
            IRootRepository mockedRepository = mock(IRootRepository.class);
            when(mockedBEManager.getRepository()).thenReturn(mockedRepository);
            //记录传递给checkUniqueness的参数，用来验证结果是否正确
            doAnswer((invocation) -> {
                HashMap<String, ArrayList<UQConstraintMediate>> result = invocation.getArgument(0);
                Assertions.assertEquals(1, result.size());
                ArrayList<UQConstraintMediate> uqConstraintMediates = result.get(NODE_CODE);
                Assertions.assertEquals(1, uqConstraintMediates.size());
                mediate = uqConstraintMediates.get(0);
                return null;
            }).when(mockedRepository).checkUniqueness(any(), eq(false));
            //spy QUConstraintValScopeExtension，由各子测试设置mock的数据
            QUConstraintValScopeExtension mockedQUConstraintValScopeExtension = spy(QUConstraintValScopeExtension.class);
            doNothing().when(mockedQUConstraintValScopeExtension).addMessage(any());
            doAnswer((invocation) -> {
                Object result = invocation.callRealMethod();
                messages.add((IBizMessage) result);
                return result;
            }).when(mockedQUConstraintValScopeExtension).createMessage(any(), any(), any(), any());
            //测试各种数据组合下唯一约束场景
            messages.clear();
            testOnCheckBasicAndSimpleAssociationUDTNoDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnCheckBasicAndSimpleAssociationUDTDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnCheckBasicAndAssociationNoDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnCheckBasicAndAssociationDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnCheckSimpleUDTAndAssociationNoDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnCheckSimpleUDTAndAssociationDuplicatedValues(mockedQUConstraintValScopeExtension);
            messages.clear();
            testOnSingleDataCheck(mockedQUConstraintValScopeExtension);
            messages.clear();
            constraintConfig.setConstraintFields(new ArrayList<>());
            testOnNoConstraintFieldCheck(mockedQUConstraintValScopeExtension);
        }
    }

    /**
     * mock构造新增的数据
     *
     * @param addData 新增的数据
     */
    private void mockAddData(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension, ArrayList<IEntityData> addData) {
        List<ICefScopeNodeParameter> parameters = new ArrayList<>();
        for (IEntityData data : addData) {
            UQConstraintValParameter mockedParameter = mock(UQConstraintValParameter.class);
            when(mockedParameter.getNodeCode()).thenReturn(NODE_CODE);
            when(mockedParameter.getChangeType()).thenReturn(ChangeType.Added);
            when(mockedParameter.getChangeData()).thenReturn(data);
            parameters.add(mockedParameter);
        }
        when(mockedQUConstraintValScopeExtension.getParameters()).thenReturn(parameters);
    }

    /**
     * 测试没有约束字段的场景但仍然运行到重复数据校验逻辑处（旧版本这种情况所有数据都会标记为违反唯一性约束）
     */
    private void testOnNoConstraintFieldCheck(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //数据1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("11");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData1);
        //数据1
        IEntityData mockData2 = new EntityData();
        mockData2.setID("22");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project2"));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("132"));
        checkData.add(mockData2);
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(0, mediate.getParametersInfo().size());
        //校验重复数据结果个数
        Assertions.assertEquals(1, messages.size());
    }

    /**
     * 测试单条数据新增场景
     */
    private void testOnSingleDataCheck(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //数据1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("11");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData1);
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(1, mediate.getParametersInfo().size());
        //校验重复数据结果个数
        Assertions.assertEquals(0, messages.size());
    }

    /**
     * 测试单值UDT、关联数据类型的数据重复场景
     */
    private void testOnCheckSimpleUDTAndAssociationDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1，重复数据1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("11");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData1);
        //2,不重复
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("133"));
        checkData.add(mockData2);
        //3，重复数据3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("31");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("133"));
        checkData.add(mockData3);
        //4，与3重复
        IEntityData mockData4 = new EntityData();
        mockData4.setID("32");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("133"));
        checkData.add(mockData4);
        //5，与1重复
        IEntityData mockData5 = new EntityData();
        mockData5.setID("12");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData5);
        //6，与1重复
        IEntityData mockData6 = new EntityData();
        mockData6.setID("13");
        mockData6.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData6.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData6);
        //7,不重复
        IEntityData mockData7 = new EntityData();
        mockData7.setID("4");
        mockData7.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project4"));
        mockData7.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("134"));
        checkData.add(mockData7);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(2, mediate.getParametersInfo().size());
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("2"));
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("4"));
        //校验重复数据结果个数
        Assertions.assertEquals(2, messages.size());
        //校验重复数据1结果
        List<String> duplicatedDataIds1 = messages.get(0).getLocation().getDataIds();
        Assertions.assertEquals(3, duplicatedDataIds1.size());
        for (int i = 1; i < 4; i++) {
            Assertions.assertEquals("1" + i, duplicatedDataIds1.get(i - 1));
        }
        //校验重复数据3结果
        List<String> duplicatedDataIds2 = messages.get(1).getLocation().getDataIds();
        Assertions.assertEquals(2, duplicatedDataIds2.size());
        for (int i = 1; i < 3; i++) {
            Assertions.assertEquals("3" + i, duplicatedDataIds2.get(i - 1));
        }
    }

    /**
     * 测试单值UDT、关联数据类型的数据不重复场景
     */
    private void testOnCheckSimpleUDTAndAssociationNoDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("1");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, null);
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData1);
        //2
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("132"));
        checkData.add(mockData2);
        //3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("3");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("131"));
        checkData.add(mockData3);
        //4
        IEntityData mockData4 = new EntityData();
        mockData4.setID("4");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project4"));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("134"));
        checkData.add(mockData4);
        //5
        IEntityData mockData5 = new EntityData();
        mockData5.setID("5");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, new SimpleUDTPhoneNumber("133"));
        checkData.add(mockData5);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(5, mediate.getParametersInfo().size());
        for (int i = 1; i < 6; i++) {
            Assertions.assertTrue(mediate.getParametersInfo().containsKey(Integer.toString(i)));
        }
        //校验重复数据结果个数
        Assertions.assertEquals(0, messages.size());
    }

    /**
     * 测试基本、关联数据类型的数据重复场景
     */
    private void testOnCheckBasicAndAssociationDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1，重复数据1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("11");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData1);
        //2,不重复
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData2);
        //3，重复数据3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("31");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData3);
        //4，与3重复
        IEntityData mockData4 = new EntityData();
        mockData4.setID("32");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData4);
        //5，与1重复
        IEntityData mockData5 = new EntityData();
        mockData5.setID("12");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData5);
        //6，与1重复
        IEntityData mockData6 = new EntityData();
        mockData6.setID("13");
        mockData6.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData6.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData6);
        //7,不重复
        IEntityData mockData7 = new EntityData();
        mockData7.setID("4");
        mockData7.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project4"));
        mockData7.setValue(CONSTRAINT_FIELD_PHONE, "134");
        checkData.add(mockData7);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(2, mediate.getParametersInfo().size());
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("2"));
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("4"));
        //校验重复数据结果个数
        Assertions.assertEquals(2, messages.size());
        //校验重复数据1结果
        List<String> duplicatedDataIds1 = messages.get(0).getLocation().getDataIds();
        Assertions.assertEquals(3, duplicatedDataIds1.size());
        for (int i = 1; i < 4; i++) {
            Assertions.assertEquals("1" + i, duplicatedDataIds1.get(i - 1));
        }
        //校验重复数据3结果
        List<String> duplicatedDataIds2 = messages.get(1).getLocation().getDataIds();
        Assertions.assertEquals(2, duplicatedDataIds2.size());
        for (int i = 1; i < 3; i++) {
            Assertions.assertEquals("3" + i, duplicatedDataIds2.get(i - 1));
        }
    }

    /**
     * 测试基本、关联数据类型的数据不重复场景
     */
    private void testOnCheckBasicAndAssociationNoDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("1");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData1);
        //2
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, "132");
        checkData.add(mockData2);
        //3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("3");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project3"));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData3);
        //4
        IEntityData mockData4 = new EntityData();
        mockData4.setID("4");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project4"));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, "134");
        checkData.add(mockData4);
        //5
        IEntityData mockData5 = new EntityData();
        mockData5.setID("5");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new ProjectInfo("project1"));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData5);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(5, mediate.getParametersInfo().size());
        for (int i = 1; i < 6; i++) {
            Assertions.assertTrue(mediate.getParametersInfo().containsKey(Integer.toString(i)));
        }
        //校验重复数据结果个数
        Assertions.assertEquals(0, messages.size());
    }

    /**
     * 测试基本、单值UDT(关联)的数据重复场景
     */
    private void testOnCheckBasicAndSimpleAssociationUDTDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1，重复数据1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("11");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData1);
        //2,不重复
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData2);
        //3，重复数据3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("31");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project3")));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData3);
        //4，与3重复
        IEntityData mockData4 = new EntityData();
        mockData4.setID("32");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project3")));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData4);
        //5，与1重复
        IEntityData mockData5 = new EntityData();
        mockData5.setID("12");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData5);
        //6，与1重复
        IEntityData mockData6 = new EntityData();
        mockData6.setID("13");
        mockData6.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData6.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData6);
        //7,不重复
        IEntityData mockData7 = new EntityData();
        mockData7.setID("4");
        mockData7.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project4")));
        mockData7.setValue(CONSTRAINT_FIELD_PHONE, "134");
        checkData.add(mockData7);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(2, mediate.getParametersInfo().size());
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("2"));
        Assertions.assertTrue(mediate.getParametersInfo().containsKey("4"));
        //校验重复数据结果个数
        Assertions.assertEquals(2, messages.size());
        //校验重复数据1结果
        List<String> duplicatedDataIds1 = messages.get(0).getLocation().getDataIds();
        Assertions.assertEquals(3, duplicatedDataIds1.size());
        for (int i = 1; i < 4; i++) {
            Assertions.assertEquals("1" + i, duplicatedDataIds1.get(i - 1));
        }
        //校验重复数据3结果
        List<String> duplicatedDataIds2 = messages.get(1).getLocation().getDataIds();
        Assertions.assertEquals(2, duplicatedDataIds2.size());
        for (int i = 1; i < 3; i++) {
            Assertions.assertEquals("3" + i, duplicatedDataIds2.get(i - 1));
        }
    }

    /**
     * 测试基本、单值UDT(关联)的数据不重复场景
     */
    private void testOnCheckBasicAndSimpleAssociationUDTNoDuplicatedValues(QUConstraintValScopeExtension mockedQUConstraintValScopeExtension) {
        //region 构造数据
        ArrayList<IEntityData> checkData = new ArrayList<>();
        //1
        IEntityData mockData1 = new EntityData();
        mockData1.setID("1");
        mockData1.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData1.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData1);
        //2
        IEntityData mockData2 = new EntityData();
        mockData2.setID("2");
        mockData2.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData2.setValue(CONSTRAINT_FIELD_PHONE, "132");
        checkData.add(mockData2);
        //3
        IEntityData mockData3 = new EntityData();
        mockData3.setID("3");
        mockData3.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project3")));
        mockData3.setValue(CONSTRAINT_FIELD_PHONE, "131");
        checkData.add(mockData3);
        //4
        IEntityData mockData4 = new EntityData();
        mockData4.setID("4");
        mockData4.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project4")));
        mockData4.setValue(CONSTRAINT_FIELD_PHONE, "134");
        checkData.add(mockData4);
        //5
        IEntityData mockData5 = new EntityData();
        mockData5.setID("5");
        mockData5.setValue(CONSTRAINT_FIELD_PROJECT, new SimpleAssociationUDTProjectInfo(new ProjectInfo("project1")));
        mockData5.setValue(CONSTRAINT_FIELD_PHONE, "133");
        checkData.add(mockData5);
        //endregion
        //调用方法
        mockAddData(mockedQUConstraintValScopeExtension, checkData);
        mockedQUConstraintValScopeExtension.onExtendSetComplete();
        //校验结果
        //校验非重复数据结果
        Assertions.assertEquals(5, mediate.getParametersInfo().size());
        for (int i = 1; i < 6; i++) {
            Assertions.assertTrue(mediate.getParametersInfo().containsKey(Integer.toString(i)));
        }
        //校验重复数据结果个数
        Assertions.assertEquals(0, messages.size());
    }

    /**
     * IEntityData实现类，用于构造测试数据
     */
    private static class EntityData implements IEntityData {

        private final HashMap<String, Object> values = new HashMap<>();
        private String id;

        @Override
        public Object getValue(String field) {
            return this.values.get(field);
        }

        @Override
        public void setValue(String field, Object value) {
            this.values.put(field, value);
        }

        @Override
        public String getID() {
            return id;
        }

        @Override
        public void setID(String id) {
            this.id = id;
        }

        @Override
        public HashMap<String, IEntityDataCollection> getChilds() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData createChild(String s) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void mergeChildData(String s, ArrayList<IEntityData> arrayList) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copySelf() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copy() {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<String> getPropertyNames() {
            throw new UnsupportedOperationException();
        }

    }

    /**
     * 手机号码类，模拟BE中的关联数据类型，用于构造测试数据
     */
    private static class ProjectInfo implements IAuthFieldValue {
        private final String projectId;

        public ProjectInfo(String projectId) {
            this.projectId = projectId;
        }

        public String getProjectId() {
            return projectId;
        }

        @Override
        public String getValue() {
            return projectId;
        }

        @Override
        public String toString() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            return Objects.equals(this.projectId, (((ProjectInfo) obj).getProjectId()));
//            throw new UnsupportedOperationException();
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * 手机号码类，模拟BE中单值UDT，用于构造测试数据
     */
    private static class SimpleUDTPhoneNumber implements ISimpleUdtData<String> {
        private String phoneNumber;

        public SimpleUDTPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override
        public String getValue() {
            return phoneNumber;
        }

        @Override
        public void setValue(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            return Objects.equals(this.phoneNumber, ((SimpleUDTPhoneNumber) obj).getValue());
//            throw new UnsupportedOperationException();
        }

        @Override
        public String toString() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object getValue(String s) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copySelf() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copy() {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<String> getPropertyNames() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setValue(String s, Object o) {
            throw new UnsupportedOperationException();
        }
    }

    private static class SimpleAssociationUDTProjectInfo implements ISimpleUdtData<ProjectInfo> {
        private ProjectInfo projectInfo;

        public SimpleAssociationUDTProjectInfo(ProjectInfo projectInfo) {
            this.projectInfo = projectInfo;
        }

        @Override
        public ProjectInfo getValue() {
            return projectInfo;
        }

        @Override
        public void setValue(ProjectInfo projectInfo) {
            this.projectInfo = projectInfo;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            return this.projectInfo.equals(((SimpleAssociationUDTProjectInfo) obj).projectInfo);
//            throw new UnsupportedOperationException();
        }

        @Override
        public String toString() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int hashCode() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object getValue(String s) {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copySelf() {
            throw new UnsupportedOperationException();
        }

        @Override
        public ICefData copy() {
            throw new UnsupportedOperationException();
        }

        @Override
        public List<String> getPropertyNames() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setValue(String s, Object o) {
            throw new UnsupportedOperationException();
        }
    }
}
