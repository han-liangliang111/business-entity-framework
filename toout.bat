@echo off

for /f "tokens=*" %%i in ('CALL .\xpath0.1.bat pom.xml "/project/version"') do set version=%%i
ECHO version=%version%

DEL /S/Q .\out

MKDIR .\out\server\platform\common\libs
MKDIR .\out\tools\deploy\metadata\runtime\libs
MKDIR .\out\server\platform\common\resources

COPY .\business-entity-framework-api\target\business-entity-framework-api-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.api.jar
COPY .\business-entity-framework-builtincompoents\target\business-entity-framework-builtincompoents-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.builtincomponents.jar
COPY .\business-entity-framework-core\target\business-entity-framework-core-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.core.jar
COPY .\business-entity-framework-entity\target\business-entity-framework-entity-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.entity.jar
COPY .\business-entity-framework-entity\target\business-entity-framework-entity-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.bef.entity.jar
COPY .\business-entity-framework-spi\target\business-entity-framework-spi-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.bef.spi.jar
COPY .\common-entity-framework-api\target\common-entity-framework-api-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.api.jar
COPY .\common-entity-framework-core\target\common-entity-framework-core-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.core.jar
COPY .\common-entity-framework-entity\target\common-entity-framework-entity-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.entity.jar
COPY .\common-entity-framework-repository\target\common-entity-framework-repository-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.repository.jar
COPY .\common-entity-framework-spi\target\common-entity-framework-spi-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.cef.spi.jar
COPY .\cm-entity-framework-api\target\cm-entity-framework-api-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.commonmodel.api.jar
COPY .\cm-entity-framework-spi\target\cm-entity-framework-spi-%version%.jar .\out\server\platform\common\libs\com.inspur.edp.commonmodel.spi.jar
COPY .\common-entity-framework-entity\target\common-entity-framework-entity-%version%.jar .\out\tools\deploy\metadata\runtime\libs\com.inspur.edp.cef.entity.jar

::XCOPY .\i18n\server\platform\common\resources  .\out\server\platform\common\resources /S
::XCOPY .\i18n\server\platform\runtime\trace\resources  .\out\server\platform\runtime\trace\resources /S
::pause