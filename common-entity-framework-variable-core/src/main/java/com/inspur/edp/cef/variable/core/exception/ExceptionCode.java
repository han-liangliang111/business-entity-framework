package com.inspur.edp.cef.variable.core.exception;

/**
 * @className: ExceptionCode
 * @author: sure
 * @date: 2024/2/18
 **/
public class ExceptionCode {
    //错误代码四位数：core 1001开头
    public static final String VARIABLE_RUNTIME_1001 = "VARIABLE_RUNTIME_1001";
    public static final String VARIABLE_RUNTIME_1002 = "VARIABLE_RUNTIME_1002";

}
