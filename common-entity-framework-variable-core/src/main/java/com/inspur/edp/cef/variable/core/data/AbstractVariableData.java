package com.inspur.edp.cef.variable.core.data;


import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.cef.variable.core.exception.ExceptionCode;
import com.inspur.edp.cef.variable.core.exception.VariableRunTimeException;

public abstract class AbstractVariableData implements IVariableData {
    //C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#region IValueObjData Impl
    public final ICefData Copy() {
        throw new VariableRunTimeException(ExceptionCode.VARIABLE_RUNTIME_1001);
    }

    public final ICefData CopySelf() {
        try {
            return (ICefData) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new VariableRunTimeException(e);
        }
    }

    public abstract java.util.List<String> GetPropertyNames();

    public abstract Object GetValue(String propName);

    public abstract void SetValue(String propName, Object value);
//C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
    ///#endregion
}