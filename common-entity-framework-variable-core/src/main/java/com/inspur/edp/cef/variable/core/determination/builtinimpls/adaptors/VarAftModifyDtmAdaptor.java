package com.inspur.edp.cef.variable.core.determination.builtinimpls.adaptors;

import com.inspur.edp.cef.core.action.CompReflector;
import com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors.BaseDtmAdaptor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Util;
import com.inspur.edp.cef.variable.api.determination.IVarDeterminationContext;

import java.util.List;

public class VarAftModifyDtmAdaptor extends BaseDtmAdaptor {

    private final List<String> elements;

    public VarAftModifyDtmAdaptor(String name, Class type) {
        this(name, type, null);
    }

    public VarAftModifyDtmAdaptor(String name, Class type, List<String> elements) {
        super(name, type, new CompReflector(type, IVarDeterminationContext.class, IChangeDetail.class));
        this.elements = elements;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return elements == null || Util.containsAny(change, elements);
    }
}
