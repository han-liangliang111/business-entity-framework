package com.inspur.edp.commonmodel.api.exception;

/**
 * @className: ExceptionCode
 * @author: sure
 * @date: 2024/2/18
 **/
public class ExceptionCode {
    //错误代码四位数：core 1001开头
    public static final String CM_RUNTIME_1001 = "CM_RUNTIME_1001";
    public static final String CM_RUNTIME_1002 = "CM_RUNTIME_1002";
    public static final String CM_RUNTIME_1003 = "CM_RUNTIME_1003";
    public static final String CM_RUNTIME_1004 = "CM_RUNTIME_1004";
    public static final String CM_RUNTIME_1005 = "CM_RUNTIME_1005";
    public static final String CM_RUNTIME_1006 = "CM_RUNTIME_1006";
    public static final String CM_RUNTIME_1007 = "CM_RUNTIME_1007";
    public static final String CM_RUNTIME_1008 = "CM_RUNTIME_1008";
    public static final String CM_RUNTIME_1009 = "CM_RUNTIME_1009";
    public static final String CM_RUNTIME_1010 = "CM_RUNTIME_1010";
    public static final String CM_RUNTIME_1011 = "CM_RUNTIME_1011";
    public static final String CM_RUNTIME_1012 = "CM_RUNTIME_1012";
}
