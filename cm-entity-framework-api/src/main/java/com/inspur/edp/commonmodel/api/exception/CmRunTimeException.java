package com.inspur.edp.commonmodel.api.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @className: CmRunTimeException
 * @author: sure
 * @date: 2024/2/18
 **/
public class CmRunTimeException extends CAFRuntimeException {
    private static final String suCode = "pfcommon";
    private static final String resFile = "cm_runtime_core.properties";

    public CmRunTimeException() {
        super(suCode, "CmRuntime001", "", null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error);
    }

    public CmRunTimeException(String exceptionCode, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public CmRunTimeException(String exceptionCode, Exception innerException, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public CmRunTimeException(String exceptionCode, Exception innerException, ExceptionLevel level, boolean isBiz, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), isBiz);
    }

    public CmRunTimeException(String exceptionCode, Exception innerException, ExceptionLevel level, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), true);
    }

    private static io.iec.edp.caf.commons.exception.ExceptionLevel getLevel(ExceptionLevel level) {
        io.iec.edp.caf.commons.exception.ExceptionLevel level1 = null;
        if (level == ExceptionLevel.Error) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Error;
        }
        if (level == ExceptionLevel.Warning) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Warning;
        }
        if (level == ExceptionLevel.Info) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Info;
        }
        return level1;

    }
}
