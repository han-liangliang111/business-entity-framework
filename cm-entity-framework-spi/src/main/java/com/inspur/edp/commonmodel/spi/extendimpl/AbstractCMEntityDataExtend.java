/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.spi.extendimpl;

import com.inspur.edp.cef.spi.extend.entity.ICefEntityDataExtend;
import com.inspur.edp.cef.spi.extendimpl.datatype.AbstractCefDataExtend;
import com.inspur.edp.cef.spi.extendimpl.entity.AbstractEntityDataExtend;
import com.inspur.edp.commonmodel.spi.extend.ICMEntityDataExtend;

import java.util.List;

public abstract class AbstractCMEntityDataExtend extends AbstractEntityDataExtend implements
        ICMEntityDataExtend {

    @Override
    public List<String> getPropertyNames() {
        return createData().getPropertyNames();
    }
}
