package com.inspur.edp.bef.spi.auth;

/**
 * 缓存实例以功能为粒度, 生命周期由bef框架管理
 */
public interface DataPermissionCache {

    /**
     * 清空缓存
     */
    void clear();
}
