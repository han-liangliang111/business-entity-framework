/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.query;


import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.spi.action.assembler.MgrActionAssembler;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.entity.condition.EntityFilter;

public class QueryMgrActionAssembler extends MgrActionAssembler {
    private com.inspur.edp.cef.entity.condition.EntityFilter entityFilter;

    public QueryMgrActionAssembler(IBEManagerContext managerContext, EntityFilter filter) {
        super(managerContext);
        this.entityFilter = filter;
    }

    protected EntityFilter getEntityFilter() {
        return entityFilter;
    }

    /**
     * 允许子类重写查询参数（注意：此处仅考虑过滤和排序的修改，无修改分页的需求）
     *
     * @return
     */
    public EntityFilter getNewEntityFilter() {
        return null;
    }

    public java.util.ArrayList<AuthorityInfo> getDataPermissionFilters() {
        return null;
    }
}
