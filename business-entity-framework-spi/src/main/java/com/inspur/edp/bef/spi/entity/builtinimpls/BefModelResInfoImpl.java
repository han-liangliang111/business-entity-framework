/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity.builtinimpls;

import static com.inspur.edp.bef.api.be.BefLockType.None;

import com.inspur.edp.bef.api.be.BefLockType;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefModelResInfoImpl;

public class BefModelResInfoImpl extends CefModelResInfoImpl {
    private BefLockType lockType = BefLockType.distributeLock;
    //是否支持在tcc事务中调用
    private boolean tccSupported;
    /**
     * 是否在启用TCC时自动处理锁
     * 1.必须在BE模型启用TCC时，此属性才可以使用
     */
    private boolean autoTccLock = true;
    //是否自动处理tcc回滚
    private boolean autoCancel = true;
    private boolean autoConfirm = true;

    public BefModelResInfoImpl(String currentSu, String resMetadataId, String displayKey) {
        super(currentSu, resMetadataId, displayKey);
    }

    public BefModelResInfoImpl(String currentSu, String resMetadataId, String metaDataId, String displayKey) {
        super(currentSu, resMetadataId, metaDataId, displayKey);
    }

    public BefModelResInfoImpl(String currentSu, String resMetadataId, String metaDataId, String displayKey, BefLockType lockType) {
        super(currentSu, resMetadataId, metaDataId, displayKey);
        this.lockType = lockType;
    }

    public BefLockType getLockType() {
        return lockType;
    }

    public boolean getTccSupported() {
        return tccSupported;
    }

    public void setTccSupported(boolean value) {
        this.tccSupported = value;
    }

    public boolean getAutoTccLock() {
        return autoTccLock;
    }

    public void setAutoTccLock(boolean value) {
        this.autoTccLock = value;
    }

    public boolean getAutoCancel() {
        return autoCancel;
    }

    public void setAutoCancel(boolean value) {
        this.autoCancel = value;
    }

    public boolean getAutoConfirm() {
        return autoConfirm;
    }

    public void setAutoConfirm(boolean value) {
        this.autoConfirm = value;
    }
}
