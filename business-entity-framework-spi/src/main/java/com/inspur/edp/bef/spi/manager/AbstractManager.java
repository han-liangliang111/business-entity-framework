/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.manager;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.cef.api.manager.action.IMgrActionExecutor;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.manager.AbstractEntityManager;
import com.inspur.edp.cef.spi.mgraction.CefMgrAction;

public abstract class AbstractManager extends AbstractEntityManager {
    public IBEManagerContext getBEManagerContext() {
        return (IBEManagerContext) getContext();
    }

    // region ExecuteAction
    @Override
    protected <TResult> TResult execute(CefMgrAction<TResult> action) {
        DataValidator.checkForNullReference(action, "com.inspur.edp.bef.core.action");
        IMgrActionExecutor<TResult> tempVar = getBEManagerContext().<TResult>getActionExecutor();
        tempVar.setAction(action);
        tempVar.setContext(this.getBEManagerContext());
        return tempVar.execute();
    }
}
