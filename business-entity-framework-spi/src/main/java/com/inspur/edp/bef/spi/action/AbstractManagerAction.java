/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.spi.action.assembler.mgrAssemblerFactory.IDefaultMgrActionAssFactory;
import com.inspur.edp.cef.api.manager.ICefMgrActionAssembler;
import com.inspur.edp.cef.spi.mgraction.CefMgrAction;

import static java.security.AccessController.getContext;

/**
 * 自定义动作的编排类，与AbstractAction相比，AbstractManagerAction可以批量执行一组实体数据的自定义动作。
 * TResult指定了自定义动作管理类的返回值类型，需要在实现类中指定为具体类型
 */
public abstract class AbstractManagerAction<TResult> extends CefMgrAction<TResult> {
    protected boolean isReadOnly = false;

    public AbstractManagerAction() {
    }

    public AbstractManagerAction(IBEManagerContext context) {
        setBEManagerContext(context);
    }

    /**
     * 获取该自定义动作管理类的业务实体管理类上下文
     */
    public IBEManagerContext getBEManagerContext() {
        return (IBEManagerContext) getContext();
    }

    // region 组装器工厂

    public void setBEManagerContext(IBEManagerContext value) {
        setContext(value);
    }

    /**
     * 获取组装器工厂
     *
     * @return 动作管理类组装器工厂
     */
    protected final IDefaultMgrActionAssFactory getMgrActionAssemblerFactory() {
        return (IDefaultMgrActionAssFactory) getBEManagerContext().getMgrActionAssemblerFactory();
    }

    protected final ICefMgrActionAssembler _getActionAssembler() {
        return getMgrAssembler();
    }

    /**
     * 获取BE管理动作组装器，默认情况下获取MgrActionAssembler;该方法可复写，可指定扩展的组装器
     *
     * @return BE管理动作组装器接口
     */
    protected IMgrActionAssembler getMgrAssembler() {
        return getMgrActionAssemblerFactory().getMgrActionAssembler(getBEManagerContext());
    }

    public final boolean isActionReadonly() {
        return isReadOnly;
    }

    // endregion
}
