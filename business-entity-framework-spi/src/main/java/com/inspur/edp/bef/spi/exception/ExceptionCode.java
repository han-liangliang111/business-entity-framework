package com.inspur.edp.bef.spi.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/18
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 builtin:3001开头 spi:4001开头 core:5001开头
    /**
     * 未找到子对象[{0}]对应的序列化器，请检查元数据[{1}]是否部署。
     */
    public static final String BEF_RUNTIME_4001 = "BEF_RUNTIME_4001";
    public static final String BEF_RUNTIME_4002 = "BEF_RUNTIME_4002";
    public static final String BEF_RUNTIME_4003 = "BEF_RUNTIME_4003";
    public static final String BEF_RUNTIME_4004 = "BEF_RUNTIME_4004";
    public static final String BEF_RUNTIME_4005 = "BEF_RUNTIME_4005";
    public static final String BEF_RUNTIME_4006 = "BEF_RUNTIME_4006";
    public static final String BEF_RUNTIME_4007 = "BEF_RUNTIME_4007";
    public static final String BEF_RUNTIME_4008 = "BEF_RUNTIME_4008";
    //    public static final String BEF_RUNTIME_4009 = "BEF_RUNTIME_4009";
    public static final String BEF_RUNTIME_4010 = "BEF_RUNTIME_4010";
    public static final String BEF_RUNTIME_4011 = "BEF_RUNTIME_4011";
    public static final String BEF_RUNTIME_4012 = "BEF_RUNTIME_4012";
}
