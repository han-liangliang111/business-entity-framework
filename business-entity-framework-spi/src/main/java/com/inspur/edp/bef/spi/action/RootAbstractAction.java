/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;

public abstract class RootAbstractAction<TResult> extends AbstractAction<TResult> {
    public RootAbstractAction(IBENodeEntityContext context) {
        setContext(context);
    }

    /**
     * 获取该自定义动作的业务实体上下文
     */
    @Override
    public IBEContext getBEContext() {
        return (IBEContext) getContext();
    }

    public void setBEContext(IBEContext value) {
        setContext(value);
    }
}
