package com.inspur.edp.bef.spi.auth;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.lcp.AuthInfo;
import com.inspur.edp.cef.api.authority.AuthorityInfo;

import java.util.List;

/**
 * bef数据权限Spi接口
 * <p>可在实现类上使用{@link javax.annotation.Priority}标记优先级顺序, 如遇同名类({@link #getName()}), Priority值较小者生效.
 */
public interface DataPermissionController<CacheT extends DataPermissionCache> {

    /**
     * 名称, 非特殊场景建议使用类全名防止意外的冲突
     */
    String getName();

    /**
     * 构造缓存实例, 如不需要缓存机制可返回{@code null}.
     * <p>缓存实例以功能为粒度, 生命周期由bef框架管理, 可用于在当前类的其他方法中缓存权限相关数据.
     *
     * @see com.inspur.edp.bef.spi.auth.DataPermissionCache
     */
    CacheT buildCache();

    /**
     * 检查指定数据是否具有执行指定动作的权限
     *
     * @return 返回指定数据是否具有执行指定动作的权限, true代表有权限, false代表无权限
     */
    boolean checkDataAuthority(String actionCode, IBEContext beContext, AuthInfo authInfo, CacheT cache);

    /**
     * 获取查询数据的权限信息, 权限信息中的sql片段将会作为列表取数sql的一部分被执行
     */
    List<AuthorityInfo> getQueryAuthInfo(AuthInfo authInfo, CacheT cache);
}
