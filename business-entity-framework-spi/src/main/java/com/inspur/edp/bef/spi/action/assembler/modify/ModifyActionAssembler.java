/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.modify;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.spi.action.assembler.BEActionAssembler;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class ModifyActionAssembler extends BEActionAssembler {
    private IChangeDetail privateChangeDetail;

    public ModifyActionAssembler(IBEContext beContext, IChangeDetail changeDetail) {
        super(beContext);
        this.privateChangeDetail = changeDetail;
    }

    protected final IChangeDetail getChangeDetail() {
        return privateChangeDetail;
    }
}
