/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package io.iec.edp.caf.runtime.config;

import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

//todo 2011 EntityManager的获取方式修改
public class CefBeanUtil {

    protected static volatile EntityManager staticEntityManager;

    public static ApplicationContext getAppCtx() {
        return SpringBeanUtils.getApplicationContext();
    }

    public static EntityManager getEntityManager() {
        if (staticEntityManager == null) {
            staticEntityManager = SpringBeanUtils.getBean(EntityManager.class);
        }
        return staticEntityManager;
    }
}
