package com.inspur.edp.cef.entity.exception;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/19
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 spi:3001开头 core:4001开头 repository:5001
    public static final String CEF_RUNTIME_1001 = "CEF_RUNTIME_1001";
    public static final String CEF_RUNTIME_1002 = "CEF_RUNTIME_1002";
    /**
     * 动态属性不支持合并类型为：[{0}]的变更集，只支持ValueObjModifyChangeDetail
     */
    public static final String CEF_RUNTIME_1003 = "CEF_RUNTIME_1003";
    /**
     * 字段[{0}]对应的过滤条件，设置左括号时，只能使用"("不能使用字符：[{1}]，请检查设置。
     */
    public static final String CEF_RUNTIME_1004 = "CEF_RUNTIME_1004";
    /**
     * 字段[{0}]对应的过滤条件，设置右括号时，只能使用")"不能使用字符：[{1}]，请检查设置。
     */
    public static final String CEF_RUNTIME_1005 = "CEF_RUNTIME_1005";
    public static final String CEF_RUNTIME_1006 = "CEF_RUNTIME_1006";
    /**
     * 字段[{0}]对应的过滤条件，右括号数不能小于0，请检查FilterCondition的setRBracketCount方法的传入参数
     */
    public static final String CEF_RUNTIME_1007 = "CEF_RUNTIME_1007";
    public static final String CEF_RUNTIME_1008 = "CEF_RUNTIME_1008";
    public static final String CEF_RUNTIME_1009 = "CEF_RUNTIME_1009";
    public static final String CEF_RUNTIME_1010 = "CEF_RUNTIME_1010";
    public static final String CEF_RUNTIME_1011 = "CEF_RUNTIME_1011";
    public static final String CEF_RUNTIME_1012 = "CEF_RUNTIME_1012";
    public static final String CEF_RUNTIME_1013 = "CEF_RUNTIME_1013";
    public static final String CEF_RUNTIME_1014 = "CEF_RUNTIME_1014";
    public static final String CEF_RUNTIME_1015 = "CEF_RUNTIME_1015";
    public static final String CEF_RUNTIME_1016 = "CEF_RUNTIME_1016";
    public static final String CEF_RUNTIME_1017 = "CEF_RUNTIME_1017";
    public static final String CEF_RUNTIME_1018 = "CEF_RUNTIME_1018";
    public static final String CEF_RUNTIME_1019 = "CEF_RUNTIME_1019";
    public static final String CEF_RUNTIME_1020 = "CEF_RUNTIME_1020";
    public static final String CEF_RUNTIME_1021 = "CEF_RUNTIME_1021";
    public static final String CEF_RUNTIME_1022 = "CEF_RUNTIME_1022";
    public static final String CEF_RUNTIME_1023 = "CEF_RUNTIME_1023";
    public static final String CEF_RUNTIME_1024 = "CEF_RUNTIME_1024";
    public static final String CEF_RUNTIME_1025 = "CEF_RUNTIME_1025";
    public static final String CEF_RUNTIME_1026 = "CEF_RUNTIME_1026";
    public static final String CEF_RUNTIME_1027 = "CEF_RUNTIME_1027";
    public static final String CEF_RUNTIME_1028 = "CEF_RUNTIME_1028";
    public static final String CEF_RUNTIME_1029 = "CEF_RUNTIME_1029";
    public static final String CEF_RUNTIME_1030 = "CEF_RUNTIME_1030";
}
