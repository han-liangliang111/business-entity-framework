/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractModifyChangeDetail implements IChangeDetail, IMultiLanguageData {

    private java.util.HashMap<String, Object> changes;
    private String privateDataID;
    private Map<String, MultiLanguageInfo> multiLanguageInfos;

    public java.util.HashMap<String, Object> getPropertyChanges() {
        return (changes != null) ? changes : (changes = new java.util.HashMap<String, Object>());
    }

    public final Object getItem(String key) {
        return getPropertyChanges().get(key);
    }

    public void setItem(String key, Object value) {
        getPropertyChanges().put(key, value);
    }

    public AbstractModifyChangeDetail putItem(String key, Object value) {
        setItem(key, value);
        return this;
    }

    public AbstractModifyChangeDetail putItem(String key, String nestedKey, Object value) {
        Object existing = getItem(key);
        if (existing == null) {
            return putItem(key, new ValueObjModifyChangeDetail().putItem(nestedKey, value));
        } else if (existing instanceof ValueObjModifyChangeDetail) {
            ((ValueObjModifyChangeDetail) existing).setItem(nestedKey, value);
            return this;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public com.inspur.edp.cef.entity.changeset.ChangeType getChangeType() {
        return ChangeType.Modify;
    }

    public String getDataID() {
        return privateDataID;
    }

    public void setDataID(String value) {
        privateDataID = value;
    }

    @Deprecated
    public Map<String, MultiLanguageInfo> getMultiLanguageInfos() {
        if (this.multiLanguageInfos == null) {
            this.multiLanguageInfos = new HashMap<>();
        }
        return multiLanguageInfos;
    }


    public ValueObjModifyChangeDetail getNestedChange(String propName) {
        HashMap<String, Object> propertyChanges = getPropertyChanges();
        if (propertyChanges.containsKey(propName)) {
            ValueObjModifyChangeDetail changeValue = (ValueObjModifyChangeDetail) propertyChanges
                    .get(propName);
            if (changeValue != null) {
                return InnerUtil.buildNestedChange(changeValue);
            }
        }
        return null;
    }

    public abstract IChangeDetail clone();
}
