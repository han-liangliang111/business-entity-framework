/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.event;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Objects;
import java.util.function.Consumer;

public class EventHandlerList {

    private HashMap<Object, LinkedList<Object>> listeners;

    public void addListener(Object key, Object listener) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(listener);

        findOrCreate(key).add(listener);
    }

    public void removeListener(Object key, Object listener) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(listener);

        findOrCreate(key).remove(listener);
    }

    public <T> void Fire(Object key, Consumer<? super T> consumer) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(consumer);

        for (Object listener : findOrCreate(key)) {
            consumer.accept((T) listener);
        }
    }

    private LinkedList<Object> findOrCreate(Object key) {
        if (listeners == null) {
            listeners = new HashMap<>();
        }
        LinkedList<Object> rez = listeners.get(key);
        if (rez == null) {
            rez = new LinkedList<Object>();
            listeners.put(key, rez);
        }
        return rez;
    }
}
