/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

/**
 * 检索实体数据时，查询条件中的排序信息
 */
public class SortCondition implements Cloneable {
    /**
     * 获取或设置排序类型
     */
    private SortType sortType = SortType.forValue(0);
    /**
     * 获取或设置进行排序的实体属性名称
     */
    private String sortField;

    public SortCondition() {
        setSortType(SortType.Asc);
    }

    /**
     * 新实例初始化SortCondition排序条件具有给定的实体属性名称，并默认升序
     *
     * @param fieldName 指定的实体属性名称
     */
    public SortCondition(String fieldName) {
        setSortField(fieldName);
        setSortType(SortType.Asc);
    }

    /**
     * 新实例初始化SortCondition排序条件具有给定的实体属性名称，以及排序方式
     */
    public SortCondition(String fieldName, SortType type) {
        setSortField(fieldName);
        setSortType(type);
    }

    public final SortType getSortType() {
        return sortType;
    }

    public final void setSortType(SortType value) {
        sortType = value;
    }

    public final String getSortField() {
        return sortField;
    }

    public final void setSortField(String value) {
        sortField = value;
    }

    /**
     * 生成指定实体属性排序的Sql片段
     *
     * @param filedName 指定的实体属性名称
     * @return sql片段
     */
    public final String trans2Sql(String filedName) {
        String sortBuilder = " " +
                filedName +
                " " +
                getSortType();
        return sortBuilder;
    }

    @Override
    public SortCondition clone() {
        try {
            SortCondition clone = (SortCondition) super.clone();
            // copy mutable state here, so the clone can't change the internals of the original
            return clone;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
    }
}
