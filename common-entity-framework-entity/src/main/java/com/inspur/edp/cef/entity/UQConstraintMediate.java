/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity;

import com.inspur.edp.cef.entity.entity.IEntityData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 持久化层用于构造唯一性约束SQL语句的中介类
 */
public class UQConstraintMediate {
    /**
     * 需要排除的修改ID字段集合
     */
    private java.util.ArrayList<String> privateExceptModifyIds = new ArrayList<>();
    /**
     * 需要排除的删除ID字段集合
     */
    private java.util.ArrayList<String> privateExceptDeleteIds = new ArrayList<>();
    /**
     * key为dataId, value分别对应：字段编号 字段值
     */
    private java.util.HashMap<String, java.util.HashMap<String, Object>> privateParametersInfo = new HashMap<>();
    /**
     * 节点编号
     */
    private String privateNodeCode;
    /**
     * 唯一性约束提示信息
     */
    private String privateMessage;
    private boolean checkFailed;

    /**
     * 从数据库中查询出的重复数据
     */
    private final List<IEntityData> duplicateDbDatas = new ArrayList<>();

    public final java.util.ArrayList<String> getExceptModifyIds() {
        return privateExceptModifyIds;
    }

    public final java.util.ArrayList<String> getExceptDeleteIds() {
        return privateExceptDeleteIds;
    }

    public final java.util.HashMap<String, java.util.HashMap<String, Object>> getParametersInfo() {
        return privateParametersInfo;
    }

    public final String getNodeCode() {
        return privateNodeCode;
    }

    public final void setNodeCode(String value) {
        privateNodeCode = value;
    }

    public final String getMessage() {
        return privateMessage;
    }

    public final void setMessage(String value) {
        privateMessage = value;
    }

    public final boolean getCheckFailed() {
        return checkFailed;
    }

    public final void setCheckFailed(boolean value) {
        checkFailed = value;
    }

    public List<IEntityData> getDuplicateDbDatas() {
        return duplicateDbDatas;
    }
}
