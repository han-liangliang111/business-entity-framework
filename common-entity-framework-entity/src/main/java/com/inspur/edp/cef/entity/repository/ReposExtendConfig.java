package com.inspur.edp.cef.entity.repository;

import lombok.Data;

/**
 * @Author wangmaojian
 * @create 2023/5/9
 */
@Data
public class ReposExtendConfig {
    private String id;
    private String code;
    private String name;
    private String implClassName;
    private String scopeType;
    private String scope;
}
