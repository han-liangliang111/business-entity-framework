package com.inspur.edp.cef.entity.condition;

import com.inspur.edp.caf.db.dbaccess.GspDbDataType;
import lombok.Data;

/**
 * @className: FilterConditionParameter
 * @author: wangmj
 * @date: 2024/8/23
 **/
@Data
public class FilterConditionParameter {
    private GspDbDataType gspDbDataType;
    private Object value;
}
