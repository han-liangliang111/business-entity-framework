/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;

import java.util.ArrayList;

/**
 * 查询实体数据的查询条件。
 * 此类上可以对查询数据时使用的过滤、排序、分页进行设置。
 */
public class EntityFilter implements Cloneable {
    /**
     * 获取或设置是否启用分页
     */
    private boolean isUsePagination;
    /**
     * 获取或设置过滤条件集合
     */
    private ArrayList<FilterCondition> filterConditions;
    private String enableLanguage;
    /**
     * 获取或设置排序条件集合
     */
    private ArrayList<SortCondition> sortConditions;
    /**
     * 获取或设置分页信息
     */
    private Pagination pagination;
    private FieldsFilter fieldsFilter;

    /**
     * 获取是否启用分页
     *
     * @return 是否启用分页
     */
    public final boolean getIsUsePagination() {
        return isUsePagination;
    }

    /**
     * 设置是否启用分页
     *
     * @param value 是否启用分页
     */
    public final void setIsUsePagination(boolean value) {
        isUsePagination = value;
    }

    /**
     * 获取过滤条件集合
     * <p>关于FilterCondition的使用可以<a href="https://open.inspuronline.com/iGIX/#/document/mddoc/igix-2103%2Fdev-guide-beta%2Fspecial-subject%2Fbusiness-object%2FFilterConditions_Example.md">访问这里</a>查看
     *
     * @return 过滤条件集合
     * @see FilterCondition
     */
    public final ArrayList<FilterCondition> getFilterConditions() {
        if (filterConditions == null)
            filterConditions = new ArrayList<>();
        return filterConditions;
    }

    /**
     * 设置过滤条件集合
     *
     * @param value 过滤条件集合
     * @see FilterCondition
     */
    public final void setFilterConditions(ArrayList<FilterCondition> value) {
        filterConditions = value;
    }

    public String getEnableLanguage() {
        return enableLanguage;
    }

    public void setEnableLanguage(String enableLanguage) {
        this.enableLanguage = enableLanguage;
    }

    public final ArrayList<SortCondition> getSortConditions() {
        return sortConditions;
    }

    public final void setSortConditions(ArrayList<SortCondition> value) {
        sortConditions = value;
    }

    public final Pagination getPagination() {
        return pagination;
    }

    public final void setPagination(Pagination value) {
        pagination = value;
    }

    /**
     * 向已定义的过滤条件集合中追加一组过滤条件集合。
     * 该方法不用于过滤条件的定义，一般用于在不同地方定义的过滤条件的合并，且为并集合并。
     *
     * @param newFilterConditions 追加的过滤条件集合
     */
    public final void addFilterConditions(ArrayList<FilterCondition> newFilterConditions) {
        DataValidator.checkForNullReference(newFilterConditions, "newFilterConditions");

        if (getFilterConditions() == null) {
            setFilterConditions(new ArrayList<>(newFilterConditions.size()));
        }

        int count = getFilterConditions().size();
        int newListCount = newFilterConditions.size();

        if (newListCount == 0)
            return;

        if (count == 0) {
            getFilterConditions().addAll(newFilterConditions);
            return;
        }

        getFilterConditions().get(0).setlBracketCount(getFilterConditions().get(0).getLBracketCount() + 1);
        getFilterConditions().get(count - 1).setRBracketCount(getFilterConditions().get(count - 1).getRBracketCount() + +1);
        getFilterConditions().get(count - 1).setRelation(ExpressRelationType.And);
        newFilterConditions.get(0).setlBracketCount(newFilterConditions.get(0).getLBracketCount() + 1);
        newFilterConditions.get(newListCount - 1).setRBracketCount(newFilterConditions.get(newListCount - 1).getRBracketCount() + 1);
        getFilterConditions().addAll(newFilterConditions);
    }

    public final void addFilterCondition(FilterCondition filterCondition) {
        if (getFilterConditions() == null) {
            setFilterConditions(new ArrayList<>());
        }
        getFilterConditions().add(filterCondition);

    }

    /**
     * 向已定义的排序条件集合中追加一组排序条件集合。
     * 该方法不用于排序条件的定义，一般用于在不同地方定义的排序条件的合并，且为并集合并。
     *
     * @param newSortConditions 追加的排序条件集合
     */
    public final void addSortConditions(ArrayList<SortCondition> newSortConditions) {
        DataValidator.checkForNullReference(newSortConditions, "newSortConditions");

        if (getSortConditions() == null) {
            setSortConditions(new ArrayList<>());
        }
        getSortConditions().addAll(newSortConditions);
    }

    public FieldsFilter getFieldsFilter() {
        return fieldsFilter;
    }

    public void setFieldsFilter(FieldsFilter fieldsFilter) {
        this.fieldsFilter = fieldsFilter;
    }

    /**
     * clone（深拷贝）当前对象
     */
    @Override
    public EntityFilter clone() {
        try {
            EntityFilter clonedEntityFilter = (EntityFilter) super.clone();
            // copy mutable state here, so the clone can't change the internals of the original
            if (fieldsFilter != null) {
                clonedEntityFilter.setFieldsFilter(fieldsFilter.clone());
            }
            if (pagination != null) {
                clonedEntityFilter.setPagination(pagination.clone());
            }
            // clone filterConditions，注意集合中null对象
            if (filterConditions != null) {
                ArrayList<FilterCondition> clonedFilterConditions =
                        new ArrayList<>(filterConditions.size());
                clonedEntityFilter.setFilterConditions(clonedFilterConditions);
                for (FilterCondition filterCondition : filterConditions) {
                    if (filterCondition == null) {
                        clonedFilterConditions.add(null);
                    } else {
                        clonedFilterConditions.add(filterCondition.clone());
                    }
                }
            }
            // clone sortConditions，注意集合中null对象
            if (sortConditions != null) {
                ArrayList<SortCondition> clonedSortConditions =
                        new ArrayList<>(sortConditions.size());
                clonedEntityFilter.setSortConditions(clonedSortConditions);
                for (SortCondition sortCondition : sortConditions) {
                    if (sortCondition == null) {
                        clonedSortConditions.add(null);
                    } else {
                        clonedSortConditions.add(sortCondition.clone());
                    }
                }
            }
            return clonedEntityFilter;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are all Cloneable
            throw new InternalError(e);
        }
    }

}
