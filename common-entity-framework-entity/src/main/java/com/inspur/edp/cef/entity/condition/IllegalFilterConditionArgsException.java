/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @author Kaixuan Shi
 * @since 2023/8/3
 */
public class IllegalFilterConditionArgsException extends CAFRuntimeException {

    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RES_File = "cef_runtime_core.properties";


    public IllegalFilterConditionArgsException(String exceptionCode, String... messageParams) {
        super(SERVICE_UNIT_CODE, RES_File, exceptionCode, messageParams, null, ExceptionLevel.Error, false);
    }


}
