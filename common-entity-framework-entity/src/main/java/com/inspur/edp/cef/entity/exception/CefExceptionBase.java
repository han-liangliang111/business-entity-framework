package com.inspur.edp.cef.entity.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @className: CefExceptionBase
 * @author: wangmj
 * @date: 2023/7/22
 **/
public class CefExceptionBase extends CAFRuntimeException {
    private static final String resFile = "cef_runtime_core.properties";
    private static String serviceUnitCode = "pfcommon";

    public CefExceptionBase(String exceptionCode, String... messageParams) {
        super(serviceUnitCode, resFile, exceptionCode, messageParams, null, ExceptionLevel.Error, true);
    }

    public CefExceptionBase(String exceptionCode, boolean isBizException, String... messageParams) {
        super(serviceUnitCode, resFile, exceptionCode, messageParams, null, ExceptionLevel.Error, isBizException);
    }

    public CefExceptionBase(String exceptionCode, Exception innerException, String... messageParams) {
        super(serviceUnitCode, resFile, exceptionCode, messageParams, innerException);
    }

    public CefExceptionBase(String exceptionCode, Exception innerException, ExceptionLevel level, boolean bizException, String... messageParams) {
        super(serviceUnitCode, resFile, exceptionCode, messageParams, innerException, level, bizException);
    }

    public CefExceptionBase(String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(serviceUnitCode, resourceFile, exceptionCode, messageParams, innerException, level, bizException);
    }

    public CefExceptionBase(String exceptionCode, String message, Exception innerException, ExceptionLevel level) {
        super(serviceUnitCode, exceptionCode, message, innerException, level);
    }

    public CefExceptionBase(String message, String exceptionCode) {
        super(serviceUnitCode, exceptionCode, message, null, ExceptionLevel.Error);
    }

    public CefExceptionBase() {
        super(serviceUnitCode, "CEFRuntime001", "", null, ExceptionLevel.Error);
    }

//    public CefExceptionBase(String message){
//        super(serviceUnitCode,"CEFRuntime001",message,null,ExceptionLevel.Error);
//    }

//    public CefExceptionBase(Exception e,String message){
//        super(serviceUnitCode, "CEFRuntime001",message,e,ExceptionLevel.Error);
//    }
}
