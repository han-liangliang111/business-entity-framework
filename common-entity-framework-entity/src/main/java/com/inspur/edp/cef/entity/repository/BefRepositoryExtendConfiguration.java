package com.inspur.edp.cef.entity.repository;

import io.iec.edp.caf.commons.event.config.CompositePropertySourceFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.List;

/**
 * @Author wangmaojian
 * @create 2023/5/9
 */
@Configuration
@PropertySource(
        value = {"application.yml"},
        factory = CompositePropertySourceFactory.class
)
@ConfigurationProperties(
        prefix = "bef-repositoryextend-configuration"
)
public class BefRepositoryExtendConfiguration {
    private List<ReposExtendConfig> fieldExtendConfigs;
    private List<ReposExtendConfig> tableNameExtendConfigs;
    private List<ReposExtendConfig> dataConvertExtendConfigs;

    public List<ReposExtendConfig> getFieldExtendConfigs() {
        return fieldExtendConfigs;
    }

    public void setFieldExtendConfigs(List<ReposExtendConfig> fieldExtendConfigs) {
        this.fieldExtendConfigs = fieldExtendConfigs;
    }

    public List<ReposExtendConfig> getTableNameExtendConfigs() {
        return tableNameExtendConfigs;
    }

    public void setTableNameExtendConfigs(List<ReposExtendConfig> tableNameExtendConfigs) {
        this.tableNameExtendConfigs = tableNameExtendConfigs;
    }

    public List<ReposExtendConfig> getDataConvertExtendConfigs() {
        return dataConvertExtendConfigs;
    }

    public void setDataConvertExtendConfigs(List<ReposExtendConfig> dataConvertExtendConfigs) {
        this.dataConvertExtendConfigs = dataConvertExtendConfigs;
    }
}
