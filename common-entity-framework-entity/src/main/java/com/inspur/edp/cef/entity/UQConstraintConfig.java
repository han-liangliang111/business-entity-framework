/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity;

import java.util.Iterator;
import java.util.Map;

/**
 * 约束信息配置类
 */
public class UQConstraintConfig {

    /**
     * 约束字段集合
     */
    private java.util.ArrayList<String> privateConstraintFields;
    /**
     * 唯一性约束编号
     */
    private String uqConstraintCode;
    /**
     * 约束提示信息
     */
    private String privateMessage;

    public final java.util.ArrayList<String> getConstraintFields() {
        return privateConstraintFields;
    }

    public final void setConstraintFields(java.util.ArrayList<String> value) {
        privateConstraintFields = value;
    }

    public final String getUQConstraintCode() {
        return uqConstraintCode;
    }

    public final void setUQConstraintCode(String value) {
        uqConstraintCode = value;
    }

    public final String getMessage() {
        return privateMessage;
    }

    public final void setMessage(String value) {
        privateMessage = value;
    }
}
