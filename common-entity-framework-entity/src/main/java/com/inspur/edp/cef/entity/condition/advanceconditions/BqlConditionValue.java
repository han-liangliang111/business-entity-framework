/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition.advanceconditions;

import java.util.ArrayList;
import java.util.List;

public class BqlConditionValue extends AbsConditionValue {

    private List<String> refBEIds;
    private final String bql;

    public BqlConditionValue(List<String> refBEIds, String bql) {
        this.refBEIds = refBEIds;
        this.bql = bql;
    }

    public List<String> getRefBEIds() {
        return refBEIds;
    }

    public String getBql() {
        return bql;
    }

    @Override
    public BqlConditionValue clone() {
        BqlConditionValue clone = (BqlConditionValue) super.clone();
        // copy mutable state here, so the clone can't change the internals of the original
        if (refBEIds != null) {
            clone.refBEIds = new ArrayList<>(refBEIds);
        }
        return clone;
    }
}
