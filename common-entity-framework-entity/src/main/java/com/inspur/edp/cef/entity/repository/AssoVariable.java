package com.inspur.edp.cef.entity.repository;

import lombok.Data;

/**
 * @Author wangmaojian
 * @create 2023/4/26
 */
@Data
public class AssoVariable {
    private String varCode;
    private String varValue;
}
