/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.base;

//import com.inspur.edp.cef.entity.accessor.IAccessor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValuesContainer;
import com.inspur.edp.cef.entity.event.EventHandlerList;

import java.util.function.Consumer;

public abstract class AccessorBase implements IValuesContainer, ICefData, IAccessor {

    private static final Object propertyChanging = new Object();
    private static final Object multiLangChanging = new Object();
    //region event
    private EventHandlerList events;
    private ICefData inner;
    private ICefData clonedDataType;
    private int suspendCount = 0;

    protected AccessorBase() {
    }

    protected AccessorBase(ICefData inner) {
        this();
        this.inner = inner;
    }

    private EventHandlerList getEventListeners() {
        if (events == null) {
            events = new EventHandlerList();
        }
        return events;
    }

    protected <T> void fire(Object key, Consumer<? super T> consumer) {
        if (events == null) {
            return;
        }
        events.Fire(key, consumer);
    }

    protected void addHandler(Object key, Object value) {
        getEventListeners().addListener(key, value);
    }
    //endregion

    protected void removeHandler(Object key, Object value) {
        getEventListeners().removeListener(key, value);
    }

    @Override
    public void addPropertyChangingListener(OnPropertyChangingListener listener) {
        addHandler(propertyChanging, listener);
    }

    @Override
    public void removePropertyChangingListener(OnPropertyChangingListener listener) {
        removeHandler(propertyChanging, listener);
    }

    public void addMultiLangChangingListener(OnMultiLangChangingListener listener) {
        addHandler(multiLangChanging, listener);
    }

    public void removeMultiLangChangingListener(OnMultiLangChangingListener listener) {
        removeHandler(multiLangChanging, listener);
    }

    @Override
    public ICefData getInnerData() {
        if (clonedDataType != null) {
            return clonedDataType;
        }
        return inner;
    }

    @Override
    public void setInnerData(ICefData value) {
        inner = value;
        clonedDataType = null;
        onInnerDataChange();
    }

    protected void onInnerDataChange() {
    }

    @Override
    public ICefData copySelf() {
        return getInnerData().copySelf();
    }

    @Override
    @JsonIgnore
    public java.util.List<String> getPropertyNames() {
        return getInnerData().getPropertyNames();
    }

    protected final void tryCopy() {
        if (clonedDataType != null) {
            return;
        }
        if (inner == null) {
            throw new UnsupportedOperationException();
        }
        clonedDataType = inner.copySelf();
        inner = null;
    }

    //region ChangeListener

    @Override
    public final void acceptChange(IChangeDetail change) {
        DataValidator.checkForNullReference(change, "change");

        tryCopy();
        acceptChangeCore(change);
    }

    protected boolean getIsSuspended() {
        return suspendCount > 0;
    }

    public void suspendChangeEvent() {
        suspendCount++;
    }

    public void resumeChangeEvent() {
        suspendCount--;
    }

    public final void raisePropertyChanging(String propName, Object value, Object orgValue) {
        firePropertyChanging(this, propName, value, orgValue);
    }

    public final void firePropertyChanging(IAccessor sender, String propName, Object value, Object orgValue) {
        if (getIsSuspended()) {
            return;
        }
        AccessorBase root = (AccessorBase) getRoot();
        if (root == null || root.events == null) {
            return;
        }
        root.fire(propertyChanging, (OnPropertyChangingListener listener) -> listener
                .OnPropertyChanging(sender, propName, value, orgValue, root));
    }

    public final void raiseMultiLangChanging(String propName, String langCode, Object value, Object orgValue) {
        fireMultiLangChanging(this, langCode, propName, value, orgValue);
    }

    private final void fireMultiLangChanging(IAccessor sender, String langCode, String propName, Object value, Object orgValue) {
        if (getIsSuspended()) {
            return;
        }
        AccessorBase root = (AccessorBase) getRoot();
        if (root == null || root.events == null) {
            return;
        }
        root.fire(multiLangChanging, (OnMultiLangChangingListener listener) -> listener
                .OnMultiLangChanging(sender, propName, langCode, value, orgValue, root));
    }
    //endregion

    //#region Abstract
    @Override
    public abstract IAccessor getRoot();

    //[JsonIgnore]
    @Override
    public abstract boolean getIsReadonly();

    protected abstract void acceptChangeCore(IChangeDetail change);

    @Override
    public abstract Object getValue(String propName);

    @Override
    public abstract void setValue(String propName, Object value);

    protected void copyCore(AccessorBase accessor) {
    }

    protected abstract AccessorBase createNewObject();

    @Override
    public ICefData copy() {
        AccessorBase result = createNewObject();
//		result.Events = new EventHandlerList();

        if (inner != null) {
            result.inner = inner.copy();
        }
        if (clonedDataType != null) {
            result.clonedDataType = clonedDataType.copy();
        }
        //TODO: reset运行时生成会用InnerData重新创建udtAccessor和子表的AccessorCollection, 这样原来的accessorCollection里的增删丢失了,
        //但是现在新增必然不带子表暴露不出来问题
        copyCore(result);
        return result;
    }

    @Override
    public Object createValue(String propName) {
        return ((IValuesContainer) getInnerData()).createValue(propName);
    }

    //#endregion

    //#region util
    protected void checkReadonly() {
        if (getIsReadonly()) {
            throw new ReadonlyDataException();
        }
    }

    protected void setAccNormalValue(String propName, Object value) {
        checkReadonly();
        raisePropertyChanging(propName, value, getInnerData().getValue(propName));
        if (!AccessorComparer.equals(getInnerData().getValue(propName), value)) {
            tryCopy();
            getInnerData().setValue(propName, value);
        }
    }
    //#endregion
}
