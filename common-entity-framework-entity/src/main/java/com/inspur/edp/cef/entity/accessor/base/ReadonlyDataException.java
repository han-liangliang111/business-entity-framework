/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.base;

import com.inspur.edp.cef.entity.exception.ExceptionCode;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * 如果发生此异常，说明你修改了不支持修改的数据，请根据报错堆栈查找相关代码检查并修改。
 */
public class ReadonlyDataException extends CAFRuntimeException {
    private static final String exceptionCode = ExceptionCode.CEF_RUNTIME_1029;
    private static final String SERVICE_UNIT_CODE = "pfcommon";
    private static final String RES_File = "cef_runtime_core.properties";

    public ReadonlyDataException(String... messageParams) {
        super(SERVICE_UNIT_CODE, RES_File, exceptionCode, messageParams, null, ExceptionLevel.Error, false);
    }
}
