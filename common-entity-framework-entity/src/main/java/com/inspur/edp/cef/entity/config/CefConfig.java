/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.config;

public class CefConfig {
    /**
     * 节点Id（带命名空间）
     */
    private String privateID;
    /**
     * BE元数据Id
     */
    private String privateBEID = "";
    private String privateDotnetID;
    private String privateSourceID;
    private String privateDefaultNamespace;
    private java.util.List<CefExtendConfig> privateExtendConfigs;
    private MgrConfig privateMgrConfig;
    private RepositoryConfig privateRepositoryConfig;

    public final String getID() {
        return privateID;
    }

    public final void setID(String value) {
        privateID = value;
    }

    public final String getBEID() {
        return privateBEID;
    }

    public final void setBEID(String value) {
        privateBEID = value;
    }

    public final String getDotnetID() {
        return privateDotnetID;
    }

    public final void setDotnetID(String value) {
        privateDotnetID = value;
    }

    public final String getSourceID() {
        return privateSourceID;
    }

    public final void setSourceID(String value) {
        privateSourceID = value;
    }

    public final String getDefaultNamespace() {
        return privateDefaultNamespace;
    }

    public final void setDefaultNamespace(String value) {
        privateDefaultNamespace = value;
    }

    public final java.util.List<CefExtendConfig> getExtendConfigs() {
        return privateExtendConfigs;
    }

    public final void setExtendConfigs(java.util.List<CefExtendConfig> value) {
        privateExtendConfigs = value;
    }

    public final MgrConfig getMgrConfig() {
        return privateMgrConfig;
    }

    public final void setMgrConfig(MgrConfig value) {
        privateMgrConfig = value;
    }

    public final RepositoryConfig getRepositoryConfig() {
        return privateRepositoryConfig;
    }

    public final void setRepositoryConfig(RepositoryConfig value) {
        privateRepositoryConfig = value;
    }
}
