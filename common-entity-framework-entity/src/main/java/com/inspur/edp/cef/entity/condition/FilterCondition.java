/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.caf.db.dbaccess.GspDbDataType;
import com.inspur.edp.cef.entity.condition.advanceconditions.AbsConditionValue;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import com.inspur.edp.cef.entity.exception.ExceptionCode;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 检索实体数据时，查询条件中过滤条件描述类
 * <p>关于FilterCondition的使用可以访问<a href="https://open.inspures.com/openplat/#/doc/md/iGIX%2FiGIX_2305%2Fdevelop%2Flowcode-dev%2Fdev-sub%2Fbusiness-entity%2Ffeature-introduction%2Fcustomaction%2Fbuiltinactions%2F%E8%BF%87%E6%BB%A4%E6%9D%A1%E4%BB%B6%E4%BD%BF%E7%94%A8%E8%8C%83%E4%BE%8B.md">这里</a>查看
 */
public class FilterCondition implements Cloneable {
    /**
     * 获取或设置进行过滤的属性名称
     * <see cref="string"/>
     */
    private String filterField;
    private String filterNode;
    /**
     * 获取或设置过滤条件的值
     * 针对in条件，可以结合下面的extenalParameter传递参数，占位符使用?placeHolder，框架自动替换
     * <see cref="string"/>
     */
    private String value;
    /**
     * 针对SQL片段，支持传递参数
     */
    private List<FilterConditionParameter> extenalParameter = new ArrayList<>();
    /**
     * 针对in类型 value范围值集合
     */
    private List inValues;
    private int lBracketCount = 0;
    /**
     * 排除的转义字符列表，与enableEscape配合使用（暂时只使用enableEscape控制）
     */
    private List<String> excludeEscapeCharacter = new ArrayList<>();
    /**
     * 是否启用转义，与excludeEscapeCharacter配合使用
     */
    private boolean enableEscape = true;
    private int rBracketCount = 0;
    /**
     * 获取或设置过滤条件的关系符
     * <see cref="ExpressRelationType"/>
     */
    private ExpressRelationType relation = ExpressRelationType.forValue(0);
    /**
     * 获取或设置过滤条件的值类型
     * <see cref="ExpressValueType"/>
     */
    private ExpressValueType expresstype = ExpressValueType.forValue(0);
    /**
     * 获取或设置过滤条件的比较符类型
     * <see cref="ExpressCompareType"/>
     */
    private ExpressCompareType compare = ExpressCompareType.forValue(0);

    //TODO Value能否用Object
    private AbsConditionValue customConditionValue = null;
    //针对表达式类型，表达式值是否被解析过
    private boolean isParsed = false;

    /**
     * 新实例初始化AddChangeDetail
     */
    public FilterCondition() {

    }

    /**
     * 新实例初始化AddChangeDetail具有给定的表达式内容
     *
     * @param lBracketCount 左括号
     * @param fieldName     属性名称
     * @param compare       比较符
     * @param value         值
     * @param rBracketCount 右括号
     * @param relation      关系符
     * @param expresstype   表达式值类型
     */
    public FilterCondition(int lBracketCount, String fieldName, ExpressCompareType compare, String value
            , int rBracketCount, ExpressRelationType relation, ExpressValueType expresstype) {
        setlBracketCount(lBracketCount);
        setFilterField(fieldName);
        setCompare(compare);
        setValue(value);
        setRBracketCount(rBracketCount);
        setRelation(relation);
        setExpresstype(expresstype);
    }

    @Deprecated
    public FilterCondition(int lBracketCount, String fieldName, ExpressCompareType compare, List<String> inValues
            , int rBracketCount, ExpressRelationType relation, ExpressValueType expresstype) {
        setlBracketCount(lBracketCount);
        setFilterField(fieldName);
        setCompare(compare);
        if (inValues == null) {
            setValue(null);
        } else {
            setInValues(inValues);
        }
        setRBracketCount(rBracketCount);
        setRelation(relation);
        setExpresstype(expresstype);
    }

    /**
     * 初始化过滤条件建造器
     */
    public static FilterConditionBuilder builder() {
        return FilterConditionBuilder.builder();
    }

    /**
     * 初始化过滤条件建造器，并将已有过滤条件添加到建造器中
     * <p>例：filterConditions 为 "age > 13 and age < 20"时，FilterConditionBuilder.builder(filterConditions).orEqual("age", 15).build()表示"age > 13 and age < 20 or age = 15"
     */
    public static FilterConditionBuilder builder(List<FilterCondition> filterConditions) {
        return FilterConditionBuilder.builder(filterConditions);
    }

    public final String getFilterField() {
        return filterField;
    }

    public final void setFilterField(String value) {
        filterField = value;
    }

    public String getFilterNode() {
        return filterNode;
    }

    public void setFilterNode(String filterNode) {
        this.filterNode = filterNode;
    }

    public final String getValue() {
        return value;
    }

    public final void setValue(String value) {
        this.value = value;
    }

    public List getInValues() {
        return inValues;
    }

    public void setInValues(List inValues) {
        this.inValues = inValues;
    }

    public List<FilterConditionParameter> getExtenalParameter() {
        return extenalParameter;
    }

    public void setExtenalParameter(List<FilterConditionParameter> extenalParameter) {
        this.extenalParameter = extenalParameter;
    }

    /**
     * 获取或设置过滤条件的左括号
     * <see cref="string"/>
     */

    public final String getLbracket() {
        if (lBracketCount <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lBracketCount; i++) {
            sb.append("(");
        }
        return sb.toString();
    }

    public final void setLbracket(String value) {
        if (value == null || "".equals(value)) {
            setlBracketCount(0);
            return;
        } else if ("null".equals(value.toLowerCase())) {
            setlBracketCount(0);
            return;
        }
        int count = 0;
        for (char item : value.toCharArray()) {
            if (item == '(') {
                count++;
                continue;
            }
            if (item == ' ')
                continue;
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_1004, this.filterField, String.valueOf(item));
        }
        setlBracketCount(count);
    }

    @JsonIgnore
    public final int getLBracketCount() {
        return lBracketCount;
    }

    public final void setlBracketCount(int count) {
        lBracketCount = count;
    }

    public List<String> getExcludeEscapeCharacter() {
        if (excludeEscapeCharacter == null) {
            excludeEscapeCharacter = new ArrayList<>();
        }
        return excludeEscapeCharacter;
    }

    public void setExcludeEscapeCharacter(List<String> excludeEscapeCharacter) {
        this.excludeEscapeCharacter = excludeEscapeCharacter;
    }

    public boolean isEnableEscape() {
        return enableEscape;
    }

    public void setEnableEscape(boolean enableEscape) {
        this.enableEscape = enableEscape;
    }

    /**
     * 获取或设置过滤条件的右括号
     * <see cref="string"/>
     */
    public final String getRbracket() {
        if (rBracketCount <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rBracketCount; i++) {
            sb.append(")");
        }
        return sb.toString();
    }

    public final void setRbracket(String value) {
        if (value == null || "".equals(value)) {
            setRBracketCount(0);
            return;
        } else if ("null".equals(value.toLowerCase())) {
            setRBracketCount(0);
            return;
        }
        int count = 0;
        for (char item : value.toCharArray()) {
            if (item == ')') {
                count++;
                continue;
            }
            if (item == ' ')
                continue;
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_1005, this.filterField, String.valueOf(item));
        }
        setRBracketCount(count);
    }

    @JsonIgnore
    public final int getRBracketCount() {
        return rBracketCount;
    }

    public final void setRBracketCount(int value) {
        if (value < 0) {
            throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_1007, this.filterField);
        }
        rBracketCount = value;
    }

    public final ExpressRelationType getRelation() {
        return relation;
    }

    public final void setRelation(ExpressRelationType value) {
        relation = value;
    }

    public final ExpressValueType getExpresstype() {
        return expresstype;
    }

    public final void setExpresstype(ExpressValueType value) {
        expresstype = value;
    }

    public final ExpressCompareType getCompare() {
        return compare;
    }

    public final void setCompare(ExpressCompareType value) {
        compare = value;
    }

    public AbsConditionValue getCustomConditionValue() {
        return customConditionValue;
    }

    public void setCustomConditionValue(
            AbsConditionValue customConditionValue) {
        this.customConditionValue = customConditionValue;
    }

    public boolean isParsed() {
        return isParsed;
    }

    public void setParsed(boolean parsed) {
        isParsed = parsed;
    }

    @Override
    public FilterCondition clone() {
        try {
            FilterCondition clone = (FilterCondition) super.clone();
            // copy mutable state here, so the clone can't change the internals of the original
            if (this.inValues != null) {
                clone.setInValues(new ArrayList<>(this.inValues));
            }
            if(this.getExtenalParameter() != null){
                clone.setExtenalParameter(new ArrayList<>(this.extenalParameter));
            }
            if (this.excludeEscapeCharacter != null) {
                clone.setExcludeEscapeCharacter(new ArrayList<>(this.excludeEscapeCharacter));
            }
            if (this.customConditionValue != null) {
                clone.setCustomConditionValue(this.customConditionValue.clone());
            }
            return clone;
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
    }
}
