package com.inspur.edp.cef.entity.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @className: CefRuntimeBaseException
 * @author: wangmj
 * @date: 2023/11/19
 **/
public class CefRuntimeBaseException extends CAFRuntimeException {
    private static final String suCode = "pfcommon";
    private static final String resFile = "cef_runtime_core.properties";

    public CefRuntimeBaseException(String exceptionCode, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public CefRuntimeBaseException(String exceptionCode, Exception innerException, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public CefRuntimeBaseException(String exceptionCode, Exception innerException, ExceptionLevel level, boolean isBiz, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, level, isBiz);
    }

    public CefRuntimeBaseException(String exceptionCode, Exception innerException, ExceptionLevel level, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, level, true);
    }
}
