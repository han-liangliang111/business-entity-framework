/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

public enum FieldType {
    /**
     * 字符串
     */
    String(0),
    /**
     * 长文本
     */
    Text(1),
    /**
     * 整形
     */
    Integer(2),
    /**
     * 十进制
     */
    Decimal(3),
    /**
     * 布尔型
     */
    Boolean(4),
    /**
     * 日期型
     */
    Date(5),
    /**
     * 日期时间型
     */
    DateTime(6),
    /**
     * 二进制
     */
    Binary(7),
    /**
     * 业务字段
     */
    Udt(8);

    private static java.util.HashMap<Integer, FieldType> mappings;
    private int intValue;

    private FieldType(int value) {
        intValue = value;
        FieldType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, FieldType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, FieldType>();
        }
        return mappings;
    }

    public static FieldType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
