/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.base;

import java.math.BigDecimal;

import org.springframework.util.Assert;

public class AccessorComparer {
    public static boolean equals(Object v1, Object v2) {
        if (v1 != null) {
            //应该判断java.lang.Comparable接口, 发版期间暂不进行影响过大的修改
            if (v1 instanceof BigDecimal && v2 instanceof BigDecimal) {
                return equals((BigDecimal) v1, (BigDecimal) v2);
            } else {
                return v1.equals(v2);
            }
        }
        return v2 == null;
    }

    public static boolean equals(BigDecimal v1, BigDecimal v2) {
        if (v1 != null) {
            return v2 != null ? v1.compareTo(v2) == 0 : false;
        } else {
            return v2 == null;
        }
    }

    public static boolean equals(byte[] array1, byte[] array2) {
        if (array1 == null) {
            if (array2 == null) {
                return true;
            } else {
                return false;
            }
        }
        if (array2 == null) {
            return false;
        }
        if (array1.length != array2.length) {
            return false;
        }
        for (int i = 0; i < array1.length; i++) {
            if (array1[i] != array2[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Assert.isTrue(!equals(new java.util.Date(0), null));
        Assert.isTrue(!equals(null, new java.util.Date(0)));
        Assert.isTrue(!equals(new java.util.Date(0), new java.util.Date(1)));
        Assert.isTrue(equals(new java.util.Date(1), new java.util.Date(1)));

        Assert.isTrue(!equals(new Boolean(true), new Boolean(false)));
        Assert.isTrue(equals(new Boolean(true), new Boolean(true)));
        Assert.isTrue(!equals(new Boolean(true), null));

        Assert.isTrue(!equals("", null));
        Assert.isTrue(!equals(null, ""));
        Assert.isTrue(!equals(" ", ""));
        Assert.isTrue(equals("", ""));

        Assert.isTrue(equals(new byte[]{}, new byte[]{}));
        Assert.isTrue(equals(new byte[]{0}, new byte[]{0}));
        Assert.isTrue(!equals(null, new byte[]{}));
        Assert.isTrue(!equals(new byte[]{}, null));
        Assert.isTrue(!equals(new byte[]{0}, new byte[]{1}));
        Assert.isTrue(!equals(new byte[]{0}, new byte[]{0, 1}));

        Assert.isTrue(equals(BigDecimal.ZERO, new BigDecimal("0.0")));
        Assert.isTrue(!equals((BigDecimal) null, new BigDecimal("0.0")));
        Assert.isTrue(equals((BigDecimal) null, (BigDecimal) null));
        Assert.isTrue(!equals(new BigDecimal("0.0"), (BigDecimal) null));
        Assert.isTrue(equals(BigDecimal.ZERO, BigDecimal.ZERO));
        Assert.isTrue(!equals(BigDecimal.ONE, BigDecimal.ZERO));

    }
}
