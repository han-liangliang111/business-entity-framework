package com.inspur.edp.cef.entity.condition;

import com.inspur.edp.cef.entity.condition.advanceconditions.BqlConditionValue;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

class FilterConditionTest {

    @Test
    void testClone() {
        // 创建 FilterCondition 实例 并设置基础属性
        FilterCondition original = new FilterCondition();
        original.setFilterField("testField");
        original.setValue("testValue");
        original.setFilterNode("filterNode");
        original.setCompare(ExpressCompareType.Is);
        original.setRelation(ExpressRelationType.And);
        original.setExpresstype(ExpressValueType.Expression);
        original.setEnableEscape(true);
        original.setParsed(true);
        original.setInValues(Arrays.asList("value1", "value2"));
        original.setlBracketCount(1);
        original.setRBracketCount(1);
        original.setExcludeEscapeCharacter(Arrays.asList("\\", "'"));

        // 创建 AbsConditionValue 实例并赋值给原始 FilterCondition
        List<String> refBEIds = Arrays.asList("be1", "be2", "be3");
        BqlConditionValue customConditionValue = new BqlConditionValue(refBEIds, "SELECT * FROM TABLE");
        original.setCustomConditionValue(customConditionValue);

        // 克隆 FilterCondition
        FilterCondition clone = original.clone();

        // 验证克隆对象不是同一个对象
        assertNotSame(original, clone);

        // 验证克隆对象与原始对象的属性相同
        assertEquals(original.getFilterField(), clone.getFilterField());
        assertEquals(original.getValue(), clone.getValue());
        assertEquals(original.getFilterNode(), clone.getFilterNode());
        assertEquals(original.getCompare(), clone.getCompare());
        assertEquals(original.getRelation(), clone.getRelation());
        assertEquals(original.getExpresstype(), clone.getExpresstype());
        assertEquals(original.isEnableEscape(), clone.isEnableEscape());
        assertEquals(original.isParsed(), clone.isParsed());
        assertNotSame(original.getInValues(), clone.getInValues());
        assertEquals(original.getInValues(), clone.getInValues());
        assertEquals(original.getLBracketCount(), clone.getLBracketCount());
        assertEquals(original.getRBracketCount(), clone.getRBracketCount());
        assertNotSame(original.getExcludeEscapeCharacter(), clone.getExcludeEscapeCharacter());
        assertEquals(original.getExcludeEscapeCharacter(), clone.getExcludeEscapeCharacter());

        // 验证 AbsConditionValue 的克隆情况
        BqlConditionValue clonedCustomConditionValue = (BqlConditionValue) clone.getCustomConditionValue();
        assertNotNull(clonedCustomConditionValue);
        assertNotSame(customConditionValue, clonedCustomConditionValue);
        assertNotSame(customConditionValue.getRefBEIds(), clonedCustomConditionValue.getRefBEIds());
        assertEquals(customConditionValue.getRefBEIds(), clonedCustomConditionValue.getRefBEIds());
        assertEquals(customConditionValue.getBql(), clonedCustomConditionValue.getBql());
    }
}