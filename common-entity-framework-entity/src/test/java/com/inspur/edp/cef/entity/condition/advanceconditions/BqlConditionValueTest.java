package com.inspur.edp.cef.entity.condition.advanceconditions;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

class BqlConditionValueTest {

    @Test
    void testClone() {
        // Arrange
        List<String> refBEIds = Arrays.asList("be1", "be2", "be3");
        BqlConditionValue original = new BqlConditionValue(refBEIds, "SELECT * FROM TABLE");

        // Act
        BqlConditionValue clone = original.clone();

        // Assert
        assertNotNull(clone, "Clone should not be null");
        assertNotSame(original, clone, "Clone should not be the same instance as original");
        assertEquals(original.getRefBEIds(), clone.getRefBEIds(), "Clone should have the same reference IDs as the original");
        assertEquals(original.getBql(), clone.getBql(), "Clone should have the same BQL query as the original");

        // Ensure that the clone's refBEIds is a separate list instance from the original
        clone.getRefBEIds().remove(0);
        assertNotEquals(original.getRefBEIds(), clone.getRefBEIds(), "Clone's refBEIds should not affect the original's refBEIds");

    }
}