package com.inspur.edp.cef.entity.condition;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

class SortConditionTest {

    @Test
    void testClone() {
        // 创建一个SortCondition实例
        SortCondition original = new SortCondition("testField", SortType.Asc);

        // 调用clone()方法创建一个副本
        SortCondition clone = original.clone();

        // 验证副本和原始对象不是同一个对象
        assertNotSame(original, clone);
        assertNotNull(clone);

        // 验证副本是否正确复制了原始对象的状态
        assertEquals(original.getSortField(), clone.getSortField());
        assertEquals(original.getSortType(), clone.getSortType());

    }
}