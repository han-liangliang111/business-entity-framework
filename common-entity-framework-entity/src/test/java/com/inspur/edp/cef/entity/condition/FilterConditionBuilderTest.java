/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import com.inspur.edp.cef.entity.condition.advanceconditions.AbsConditionValue;
import com.inspur.edp.cef.entity.condition.advanceconditions.BqlConditionValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kaixuan Shi
 * @since 2023/7/24
 */
public class FilterConditionBuilderTest {
    private static final String beginTime = "2023-01-01";
    private static final String overTime = "2023-02-02";

    /**
     * 对基本方法测试（构造器、.build()、.get(int index)、.getLast()）
     */
    @Test
    public void baseMethodTest() {
        FilterConditionBuilder builder = FilterCondition.builder();
        List<FilterCondition> builderResult = builder
                .orEqual("age0", "0")
                .orNotEqual("age1", "1")
                .andGreater("age2", "2")
                .build();
        //每个的字段、值、值类型、左右括号个数正确
        for (int i = 0; i < 3; i++) {
            FilterCondition condition = builderResult.get(i);
            Assertions.assertEquals(("age" + i), condition.getFilterField());
            Assertions.assertEquals(String.valueOf(i), condition.getValue());
            Assertions.assertEquals(condition.getLBracketCount(), 0);
            Assertions.assertEquals(condition.getRBracketCount(), 0);
            Assertions.assertEquals(condition.getExpresstype(), ExpressValueType.Value);
        }
        //每个的关系符正确
        Assertions.assertEquals(builderResult.get(0).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(builderResult.get(1).getRelation(), ExpressRelationType.And);
        Assertions.assertEquals(builderResult.get(2).getRelation(), ExpressRelationType.Empty);
        //FilterConditionBuilder.build方法正确返回所有对象并把最后一个关系符置为ExpressRelationType.Empty
        Assertions.assertEquals(builderResult.size(), 3);
        Assertions.assertEquals(builderResult.get(2).getRelation(), ExpressRelationType.Empty);
        //再次调用FilterConditionBuilder.build后返回空数组（非null）
        List<FilterCondition> emptyConditions = builder.build();
        Assertions.assertNotNull(emptyConditions);
        Assertions.assertEquals(emptyConditions.size(), 0);
        //调用FilterConditionBuilder.build后再次添加，正确返回新添加的内容，build之前的内容不再保留
        builder.orEqual("age4", "4");
        Assertions.assertEquals(builder.build().size(), 1);
        Assertions.assertEquals(builder.build().size(), 0);
        //构造函数直接将conditions添加进来后调用build方法，返回结果与conditions一致
        FilterConditionBuilder builder2 = FilterConditionBuilder.builder(builderResult);
        checkFilterConditionsIsSame(builder2.build(), builderResult);
    }

    /**
     * 测试自行构造FilterCondition与使用FilterConditionBuilder构造结果是否一致，遍历所有“orIs”、“andEqual”格式的方法（不包含值为表达式、in、notIn的方法）
     */
    @Test
    public void singleAddTest() {
        //自行构造FilterCondition过滤条件
        List<FilterCondition> originalResult = new ArrayList<>();
        originalResult.add(new FilterCondition(2, "BeginTime", ExpressCompareType.LessOrEqual, beginTime, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.LessOrEqual, beginTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.LessOrEqual, beginTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.GreaterOrEqual, beginTime, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.GreaterOrEqual, beginTime, 1,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(1, "BeginTime", ExpressCompareType.Less, overTime, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.Less, overTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.Less, overTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.Greater, overTime, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.Greater, overTime, 1,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(1, "BeginTime", ExpressCompareType.Equal, beginTime, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.Equal, beginTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "BeginTime", ExpressCompareType.Equal, beginTime, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.IsNot, (String) null, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "OverTime", ExpressCompareType.IsNot, (String) null, 2,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingRoom", ExpressCompareType.Is, (String) null, 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingRoom", ExpressCompareType.Is, (String) null, 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingRoom", ExpressCompareType.Like, "305", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingRoom", ExpressCompareType.Like, "305", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "DocumentType", ExpressCompareType.LikeEndWith, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "DocumentType", ExpressCompareType.LikeEndWith, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.In, Arrays.asList("1", "2", "4"), 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.In, Arrays.asList("1", "2", "4"), 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotIn, Arrays.asList("1", "2", "4"), 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotIn, Arrays.asList("1", "2", "4"), 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.LikeStartWith, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.LikeStartWith, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.LikeIgnoreCase, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.LikeIgnoreCase, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLike, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLike, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLikeStartWith, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLikeStartWith, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLikeEndWith, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotLikeEndWith, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotEqual, "cancel", 0,
                ExpressRelationType.And, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotEqual, "cancel", 0,
                ExpressRelationType.Or, ExpressValueType.Value));
        originalResult.add(new FilterCondition(0, "MeetingState", ExpressCompareType.NotEqual, "cancel", 0,
                ExpressRelationType.Empty, ExpressValueType.Value));
        //通过FilterConditionBuilder构造过滤条件
        List<FilterCondition> builderResult = FilterCondition.builder()
                .and(() -> FilterCondition.builder()
                        .and(() -> FilterCondition.builder()
                                .andLessOrEqual("BeginTime", beginTime)
                                .orLessOrEqual("BeginTime", beginTime)
                                .andLessOrEqual("BeginTime", beginTime)
                                .andGreaterOrEqual("OverTime", beginTime)
                                .orGreaterOrEqual("OverTime", beginTime)
                                .build()
                        )
                        .or(() -> FilterCondition.builder()
                                .andLess("BeginTime", overTime)
                                .orLess("BeginTime", overTime)
                                .andLess("BeginTime", overTime)
                                .andGreater("OverTime", overTime)
                                .orGreater("OverTime", overTime)
                                .build()
                        )
                        .or(() -> FilterCondition.builder()
                                .andEqual("BeginTime", beginTime)
                                .orEqual("BeginTime", beginTime)
                                .andEqual("BeginTime", beginTime)
                                .andIsNotNull("OverTime")
                                .orIsNotNull("OverTime")
                                .build()
                        ).build()
                )
                .orIsNull("MeetingRoom")
                .andIsNull("MeetingRoom")
                .orLike("MeetingRoom", "305")
                .andLike("MeetingRoom", "305")
                .orLikeEndWith("DocumentType", "cancel")
                .andLikeEndWith("DocumentType", "cancel")
                .orIn("MeetingState", Arrays.asList("1", "2", "4"))
                .andIn("MeetingState", Arrays.asList("1", "2", "4"))
                .orNotIn("MeetingState", Arrays.asList("1", "2", "4"))
                .andNotIn("MeetingState", Arrays.asList("1", "2", "4"))
                .orLikeStartWith("MeetingState", "cancel")
                .andLikeStartWith("MeetingState", "cancel")
                .orLikeIgnoreCase("MeetingState", "cancel")
                .andLikeIgnoreCase("MeetingState", "cancel")
                .orNotLike("MeetingState", "cancel")
                .andNotLike("MeetingState", "cancel")
                .orNotLikeStartWith("MeetingState", "cancel")
                .andNotLikeStartWith("MeetingState", "cancel")
                .orNotLikeEndWith("MeetingState", "cancel")
                .andNotLikeEndWith("MeetingState", "cancel")
                .orNotEqual("MeetingState", "cancel")
                .andNotEqual("MeetingState", "cancel")
                .orNotEqual("MeetingState", "cancel").build();
        //测试两种方式构造出来的结果含义一致
        checkFilterConditionsIsSame(originalResult, builderResult);
        //测试所有过滤条件的ExpressValueType为Value类型
        for (FilterCondition filterCondition : originalResult) {
            Assertions.assertEquals(filterCondition.getExpresstype(), ExpressValueType.Value);
        }
    }

    /**
     * 测试添加多个过滤条件组成一个子条件的方法
     * ((BeginTime<=0 and OverTime>=1 ) or (OverTime>2 or BeginTime<3 or (OverTime isNot 4 or BeginTime=5) and (OverTime>6 and BeginTime<7) )) or age!=8
     */
    @Test
    public void batchAddTest() {
        List<FilterCondition> lambdaConstructConditions = FilterCondition.builder()
                .and(() -> FilterCondition.builder()
                        .and(() -> FilterCondition.builder()
                                .orLessOrEqual("BeginTime", "0")
                                .andGreaterOrEqual("OverTime", "1")
                                .build()
                        )
                        .or(() -> FilterCondition.builder()
                                .andGreater("OverTime", "2")
                                .orLess("BeginTime", "3")
                                .or(() -> FilterCondition.builder()
                                        .andIsNotNull("OverTime")
                                        .orEqual("BeginTime", "5")
                                        .build()
                                ).and(() -> FilterCondition.builder()
                                        .orGreater("OverTime", "6")
                                        .andLess("BeginTime", "7")
                                        .build()
                                )
                                .build()
                        )
                        .build()
                ).orNotEqual("age", "8").build();
        //region 测试lambda表达式构造结果是否正确
        //测试每个过滤条件的左右括号数正确
        Assertions.assertEquals(lambdaConstructConditions.size(), 9);
        Assertions.assertEquals(lambdaConstructConditions.get(0).getLBracketCount(), 2);
        Assertions.assertEquals(lambdaConstructConditions.get(0).getRBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(1).getLBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(1).getRBracketCount(), 1);
        Assertions.assertEquals(lambdaConstructConditions.get(2).getLBracketCount(), 1);
        Assertions.assertEquals(lambdaConstructConditions.get(2).getRBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(3).getLBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(3).getRBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(4).getLBracketCount(), 1);
        Assertions.assertEquals(lambdaConstructConditions.get(4).getRBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(5).getLBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(5).getRBracketCount(), 1);
        Assertions.assertEquals(lambdaConstructConditions.get(6).getLBracketCount(), 1);
        Assertions.assertEquals(lambdaConstructConditions.get(6).getRBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(7).getLBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(7).getRBracketCount(), 3);
        Assertions.assertEquals(lambdaConstructConditions.get(8).getLBracketCount(), 0);
        Assertions.assertEquals(lambdaConstructConditions.get(8).getRBracketCount(), 0);
        //测试过滤条件之间关系符正确
        Assertions.assertEquals(lambdaConstructConditions.get(0).getRelation(), ExpressRelationType.And);
        Assertions.assertEquals(lambdaConstructConditions.get(1).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(lambdaConstructConditions.get(2).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(lambdaConstructConditions.get(3).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(lambdaConstructConditions.get(4).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(lambdaConstructConditions.get(5).getRelation(), ExpressRelationType.And);
        Assertions.assertEquals(lambdaConstructConditions.get(6).getRelation(), ExpressRelationType.And);
        Assertions.assertEquals(lambdaConstructConditions.get(7).getRelation(), ExpressRelationType.Or);
        Assertions.assertEquals(lambdaConstructConditions.get(8).getRelation(), ExpressRelationType.Empty);
        //endregion
        List<FilterCondition> listConstructConditions = FilterCondition.builder()
                .and(FilterCondition.builder()
                        .and(FilterCondition.builder()
                                .orLessOrEqual("BeginTime", "0")
                                .andGreaterOrEqual("OverTime", "1")
                                .build()
                        )
                        .or(FilterCondition.builder()
                                .andGreater("OverTime", "2")
                                .orLess("BeginTime", "3")
                                .or(FilterCondition.builder()
                                        .andIsNotNull("OverTime")
                                        .orEqual("BeginTime", "5")
                                        .build()
                                )
                                .and(FilterCondition.builder()
                                        .orGreater("OverTime", "6")
                                        .andLess("BeginTime", "7")
                                        .build()
                                )
                                .build()
                        ).build()
                ).orNotEqual("age", "8").build();
        checkFilterConditionsIsSame(lambdaConstructConditions, listConstructConditions);
    }

    /**
     * 测试表达式值类型是否正确设置
     */
    @Test
    public void expressValueTest() {
        List<FilterCondition> builderResult = FilterCondition.builder()
                .andLessOrEqual("BeginTime", beginTime, ExpressValueType.Expression)
                .orLessOrEqual("BeginTime", beginTime, ExpressValueType.Expression)
                .andGreaterOrEqual("OverTime", beginTime, ExpressValueType.Expression)
                .orGreaterOrEqual("OverTime", beginTime, ExpressValueType.Expression)
                .andLess("BeginTime", overTime, ExpressValueType.Expression)
                .orLess("BeginTime", overTime, ExpressValueType.Expression)
                .andGreater("OverTime", overTime, ExpressValueType.Expression)
                .orGreater("OverTime", overTime, ExpressValueType.Expression)
                .andEqual("BeginTime", beginTime, ExpressValueType.Expression)
                .orEqual("BeginTime", beginTime, ExpressValueType.Expression)
                .andEqual("BeginTime", beginTime, ExpressValueType.Expression)
                .orLike("MeetingRoom", "305", ExpressValueType.Expression)
                .andLike("MeetingRoom", "305", ExpressValueType.Expression)
                .orLikeEndWith("DocumentType", "cancel", ExpressValueType.Expression)
                .andLikeEndWith("DocumentType", "cancel", ExpressValueType.Expression)
                .orLikeStartWith("MeetingState", "cancel", ExpressValueType.Expression)
                .andLikeStartWith("MeetingState", "cancel", ExpressValueType.Expression)
                .orLikeIgnoreCase("MeetingState", "cancel", ExpressValueType.Expression)
                .andLikeIgnoreCase("MeetingState", "cancel", ExpressValueType.Expression)
                .orNotLike("MeetingState", "cancel", ExpressValueType.Expression)
                .andNotLike("MeetingState", "cancel", ExpressValueType.Expression)
                .orNotLikeStartWith("MeetingState", "cancel", ExpressValueType.Expression)
                .andNotLikeStartWith("MeetingState", "cancel", ExpressValueType.Expression)
                .orNotLikeEndWith("MeetingState", "cancel", ExpressValueType.Expression)
                .andNotLikeEndWith("MeetingState", "cancel", ExpressValueType.Expression)
                .orNotEqual("MeetingState", "cancel", ExpressValueType.Expression)
                .andNotEqual("MeetingState", "cancel", ExpressValueType.Expression)
                .orNotEqual("MeetingState", "cancel", ExpressValueType.Expression)
                .build();
        for (int i = 0; i < builderResult.size(); i++) {
            Assertions.assertEquals(builderResult.get(i).getExpresstype(), ExpressValueType.Expression);
            if (i == builderResult.size() - 1) {
                Assertions.assertEquals(builderResult.get(i).getRelation(), ExpressRelationType.Empty);
            } else if (i % 2 == 0) {
                Assertions.assertEquals(builderResult.get(i).getRelation(), ExpressRelationType.Or);
            } else {
                Assertions.assertEquals(builderResult.get(i).getRelation(), ExpressRelationType.And);
            }
        }
    }

    /**
     * 测试in、notIn的bql值是否正确设置
     */
    @Test
    public void bqlValueTest() {
        String fieldName = "age";
        BqlConditionValue bqlCondition = new BqlConditionValue(Arrays.asList("1", "2", "3"), "select age from gsp");
        List<FilterCondition> builderResult = FilterCondition.builder()
                .orIn(fieldName, bqlCondition)
                .andIn(fieldName, bqlCondition)
                .orNotIn(fieldName, bqlCondition)
                .andNotIn(fieldName, bqlCondition)
                .build();
        for (int i = 0; i < builderResult.size(); i++) {
            FilterCondition condition = builderResult.get(i);
            Assertions.assertEquals(condition.getExpresstype(), ExpressValueType.Bql);
            Assertions.assertEquals(condition.getFilterField(), fieldName);
            Assertions.assertEquals(condition.getCustomConditionValue(), bqlCondition);
            if (i == builderResult.size() - 1) {
                Assertions.assertEquals(condition.getRelation(), ExpressRelationType.Empty);
            } else if (i % 2 == 0) {
                Assertions.assertEquals(condition.getRelation(), ExpressRelationType.And);
            } else {
                Assertions.assertEquals(condition.getRelation(), ExpressRelationType.Or);
            }
        }
    }

    /**
     * 测试between方法
     */
    @Test
    public void betweenTest() {
        //测试值类型为ExpressValueType.Value时
        List<FilterCondition> conditions = new ArrayList<>();
        conditions.add(
                new FilterCondition(0, "name", ExpressCompareType.Equal, "张飞", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Or, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Empty, ExpressValueType.Value));
        List<FilterCondition> builderConditions = FilterCondition.builder()
                .orEqual("name", "张飞")
                .andBetween("age", "18", "30")
                .orBetween("age", "18", "30")
                .build();
        checkFilterConditionsIsSame(conditions, builderConditions);

        //测试值类型为ExpressValueType.Expression时
        List<FilterCondition> conditions2 = new ArrayList<>();
        conditions2.add(
                new FilterCondition(0, "name", ExpressCompareType.Equal, "张飞", 0,
                        ExpressRelationType.And, ExpressValueType.Expression));
        conditions2.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Expression));
        conditions2.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Or, ExpressValueType.Expression));
        conditions2.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Expression));
        conditions2.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Empty, ExpressValueType.Expression));
        List<FilterCondition> builderConditions2 = FilterCondition.builder()
                .orEqual("name", "张飞", ExpressValueType.Expression)
                .andBetween("age", "18", "30", ExpressValueType.Expression)
                .orBetween("age", "18", "30", ExpressValueType.Expression)
                .build();
        checkFilterConditionsIsSame(conditions2, builderConditions2);
    }

    @Test
    public void argsCheckTest() {
        //属性校验
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().orIn("age", (List<?>) null));
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().orIn("age", (BqlConditionValue) null));
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().andEqual(" ", "12"));
        Assertions.assertDoesNotThrow(
                () -> FilterCondition.builder().andEqual("age", null));
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().andEqual("age", "", ExpressValueType.Expression));
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().andEqual("age", "", null));
        //括号数校验
        List<FilterCondition> conditions = new ArrayList<>();
        conditions.add(
                new FilterCondition(0, "name", ExpressCompareType.Equal, "张飞", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Or, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(1, "age", ExpressCompareType.GreaterOrEqual, "18", 0,
                        ExpressRelationType.And, ExpressValueType.Value));
        conditions.add(
                new FilterCondition(0, "age", ExpressCompareType.LessOrEqual, "30", 1,
                        ExpressRelationType.Empty, ExpressValueType.Value));
        Assertions.assertDoesNotThrow(() -> FilterCondition.builder(conditions));
        Assertions.assertDoesNotThrow(() -> FilterCondition.builder().and(conditions));
        //测试多一个右括号
        conditions.get(0).setRBracketCount(conditions.get(0).getRBracketCount() + 1);
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().or(conditions));
        //测试多一个左括号
        conditions.get(0).setlBracketCount(conditions.get(0).getLBracketCount() + 1);
        Assertions.assertDoesNotThrow(
                () -> FilterCondition.builder().or(conditions)
        );
        conditions.get(1).setlBracketCount(conditions.get(1).getLBracketCount() + 1);
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().or(conditions));
        //测试左括号数为负数
        conditions.get(2).setlBracketCount(-1);
        Assertions.assertThrows(IllegalFilterConditionArgsException.class,
                () -> FilterCondition.builder().or(conditions));
    }

    /**
     * 检测两个过滤条件集合含义是否完全一致
     */
    private void checkFilterConditionsIsSame(List<FilterCondition> conditions1, List<FilterCondition> conditions2) {
        if (conditions1 == null) {
            Assertions.assertNull(conditions2);
            return;
        }
        Assertions.assertNotNull(conditions2);
        Assertions.assertEquals(conditions1.size(), conditions2.size());
        for (int i = 0; i < conditions1.size(); i++) {
            checkFilterConditionIsSame(conditions1.get(i), conditions2.get(i));
        }
    }

    /**
     * 测试两个FilterCondition类型的对象是否属性值相同
     */
    private void checkFilterConditionIsSame(FilterCondition condition1, FilterCondition condition2) {
        Assertions.assertEquals(condition1.getCompare(), condition2.getCompare());
        Assertions.assertEquals(condition1.isParsed(), condition2.isParsed());
        checkAbsConditionValueIsSame(condition1.getCustomConditionValue(), condition2.getCustomConditionValue());
        Assertions.assertEquals(condition1.getFilterField(), condition2.getFilterField());
        Assertions.assertEquals(condition1.getFilterNode(), condition2.getFilterNode());
        Assertions.assertEquals(condition1.getLbracket(), condition2.getLbracket());
        Assertions.assertEquals(condition1.getExpresstype(), condition2.getExpresstype());
        Assertions.assertEquals(condition1.getLBracketCount(), condition2.getLBracketCount());
        Assertions.assertEquals(condition1.getRBracketCount(), condition2.getRBracketCount());
        Assertions.assertEquals(condition1.getRbracket(), condition2.getRbracket());
        Assertions.assertEquals(condition1.getRelation(), condition2.getRelation());
        Assertions.assertEquals(condition1.getValue(), condition2.getValue());
        if (condition1.getInValues() == null) {
            Assertions.assertNull(condition2.getInValues());
        } else {
            Assertions.assertNotNull(condition2.getInValues());
            Assertions.assertEquals(condition1.getInValues().size(), condition2.getInValues().size());
            for (int j = 0; j < condition1.getInValues().size(); j++) {
                Assertions.assertEquals(condition1.getInValues().get(j), condition2.getInValues().get(j));
            }
        }
    }

    /**
     * 测试两个AbsConditionValue类型的对象是否属性值相同，当前仅支持AbsConditionValue抽象类的BqlConditionValue实现类
     */
    private void checkAbsConditionValueIsSame(AbsConditionValue conditionValue1, AbsConditionValue conditionValue2) {
        if (conditionValue1 == null) {
            Assertions.assertNull(conditionValue2);
            return;
        }
        Assertions.assertNotNull(conditionValue2);
        Assertions.assertTrue(conditionValue1 instanceof BqlConditionValue);
        Assertions.assertTrue(conditionValue2 instanceof BqlConditionValue);
        BqlConditionValue bql1 = (BqlConditionValue) conditionValue1;
        BqlConditionValue bql2 = (BqlConditionValue) conditionValue2;
        Assertions.assertEquals(bql1.getBql(), bql2.getBql());
        if (bql1.getRefBEIds() == null) {
            Assertions.assertNull(bql2.getRefBEIds());
        } else {
            Assertions.assertNotNull(bql2.getRefBEIds());
            Assertions.assertEquals(bql1.getRefBEIds().size(), bql2.getRefBEIds().size());
            for (int j = 0; j < bql1.getRefBEIds().size(); j++) {
                Assertions.assertEquals(bql1.getRefBEIds().get(j), bql2.getRefBEIds().get(j));
            }
        }
    }

}
