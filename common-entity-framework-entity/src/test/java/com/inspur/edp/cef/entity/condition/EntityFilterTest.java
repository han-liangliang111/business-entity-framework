package com.inspur.edp.cef.entity.condition;

import com.inspur.edp.cef.entity.condition.advanceconditions.BqlConditionValue;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EntityFilterTest {

    @Test
    void testClone() {
        // 创建原始的EntityFilter对象并配置它的状态
        EntityFilter originalFilter = new EntityFilter();
        originalFilter.setEnableLanguage("zh");
        originalFilter.setIsUsePagination(true);
        originalFilter.setPagination(new Pagination(10, 1));

        FieldsFilter fieldsFilter = new FieldsFilter();
        fieldsFilter.setUseFieldsCondition(true);
        originalFilter.setFieldsFilter(fieldsFilter);

        FilterCondition filterCondition1 = new FilterCondition();
        filterCondition1.setlBracketCount(1);
        List<String> refBEIds = new ArrayList<>();
        refBEIds.add("be1");
        BqlConditionValue bqlConditionValue = new BqlConditionValue(refBEIds, "select * from be1");
        filterCondition1.setCustomConditionValue(bqlConditionValue);
        ArrayList<FilterCondition> filterConditions = new ArrayList<>();
        filterConditions.add(filterCondition1);
        filterConditions.add(null);
        originalFilter.setFilterConditions(filterConditions);

        SortCondition sortCondition1 = new SortCondition("testField", SortType.Asc);
        ArrayList<SortCondition> sortConditions = new ArrayList<>();
        sortConditions.add(sortCondition1);
        sortConditions.add(null);
        originalFilter.setSortConditions(sortConditions);

        // 克隆EntityFilter对象
        EntityFilter clonedFilter = originalFilter.clone();

        // 验证克隆对象不是同一个对象
        assertNotSame(originalFilter, clonedFilter);

        // 验证克隆对象的属性与原始对象的属性相同
        assertTrue(clonedFilter.getIsUsePagination());
        assertEquals(originalFilter.getEnableLanguage(), clonedFilter.getEnableLanguage());

        // 验证克隆对象的Pagination对象的属性与原始对象的Pagination对象的属性相同，但不是同一个对象
        assertNotNull(clonedFilter.getPagination());
        assertNotSame(originalFilter.getPagination(), clonedFilter.getPagination());
        assertEquals(originalFilter.getPagination().getPageIndex(), clonedFilter.getPagination().getPageIndex());
        assertEquals(originalFilter.getPagination().getPageSize(), clonedFilter.getPagination().getPageSize());

        // 验证克隆对象的FieldsFilter对象的属性与原始对象的FieldsFilter对象的属性相同，但不是同一个对象
        assertNotNull(clonedFilter.getFieldsFilter());
        assertNotSame(originalFilter.getFieldsFilter(), clonedFilter.getFieldsFilter());
        assertEquals(originalFilter.getFieldsFilter().isUseFieldsCondition(), clonedFilter.getFieldsFilter().isUseFieldsCondition());

        // 验证克隆对象的FilterCondition对象的属性与原始对象的FilterCondition对象的属性相同，但不是同一个对象
        assertNotNull(clonedFilter.getFilterConditions());
        assertNotSame(originalFilter.getFilterConditions(), clonedFilter.getFilterConditions());
        assertEquals(originalFilter.getFilterConditions().size(), clonedFilter.getFilterConditions().size());
        assertEquals(originalFilter.getFilterConditions().get(0).getLBracketCount(),
                clonedFilter.getFilterConditions().get(0).getLBracketCount()
        );
        assertNull(clonedFilter.getFilterConditions().get(1));
        assertEquals(originalFilter.getFilterConditions().get(1), clonedFilter.getFilterConditions().get(1));

        // 验证克隆对象的SortCondition对象的属性与原始对象的SortCondition对象的属性相同，但不是同一个对象
        assertNotNull(clonedFilter.getSortConditions());
        assertNotSame(originalFilter.getSortConditions(), clonedFilter.getSortConditions());
        assertEquals(originalFilter.getSortConditions().size(), clonedFilter.getSortConditions().size());
        assertEquals(originalFilter.getSortConditions().get(0).getSortField(),
                clonedFilter.getSortConditions().get(0).getSortField()
        );
        assertEquals(originalFilter.getSortConditions().get(0).getSortType(),
                clonedFilter.getSortConditions().get(0).getSortType()
        );
        assertNull(clonedFilter.getSortConditions().get(1));
        assertEquals(originalFilter.getSortConditions().get(1), clonedFilter.getSortConditions().get(1));

    }

}
