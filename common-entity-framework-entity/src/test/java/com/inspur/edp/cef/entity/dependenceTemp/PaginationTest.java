package com.inspur.edp.cef.entity.dependenceTemp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;

class PaginationTest {

    @Test
    void testClone() {
        // 创建一个Pagination实例
        Pagination original = new Pagination(10, 1);
        original.setTotalCount(100); // 假设方法设置总记录数
        original.setPageCount(10); // 假设方法设置总页数

        // 克隆该实例
        Pagination clone = original.clone();

        // 验证克隆对象不是同一个对象
        assertNotSame(original, clone);
        assertNotNull(clone);

        // 验证克隆对象与原始对象的属性相同
        assertEquals(original.getPageSize(), clone.getPageSize());
        assertEquals(original.getPageIndex(), clone.getPageIndex());
        assertEquals(original.getTotalCount(), clone.getTotalCount());
        assertEquals(original.getPageCount(), clone.getPageCount());
    }
}