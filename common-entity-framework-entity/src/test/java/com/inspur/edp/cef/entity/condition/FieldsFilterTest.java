package com.inspur.edp.cef.entity.condition;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FieldsFilterTest {

    @Test
    void testCloneWithFields() {
        // 创建FieldsFilter实例并设置条件
        FieldsFilter original = new FieldsFilter();
        original.setUseFieldsCondition(true);
        original.getFilterFields().add("field1");
        original.getFilterFields().add("field2");

        // 克隆FieldsFilter实例
        FieldsFilter clone = original.clone();

        // 验证克隆结果
        assertNotNull(clone, "Clone should not be null.");
        assertTrue(clone.isUseFieldsCondition());
        assertEquals(original.getFilterFields(), clone.getFilterFields(), "Filter fields should be the same in original and clone.");
        assertNotSame(original.getFilterFields(), clone.getFilterFields(), "Filter fields list should not be the same reference in original and clone.");
    }

    @Test
    void testCloneWithoutFields() {
        // 创建FieldsFilter实例不设置条件
        FieldsFilter original = new FieldsFilter();

        // 克隆FieldsFilter实例
        FieldsFilter clone = original.clone();

        // 验证克隆结果
        assertNotNull(clone, "Clone should not be null.");
        assertFalse(clone.isUseFieldsCondition(), "Clone should have 'useFieldsCondition' set to false.");
        assertNotSame(original.getFilterFields(), clone.getFilterFields(), "Filter fields list should not be the same reference in original and clone.");
        assertTrue(clone.getFilterFields().isEmpty(), "Filter fields should be empty in the clone.");
    }
}