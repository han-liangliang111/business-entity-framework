/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.dataType.valueObj;

import com.inspur.edp.cef.api.dataType.base.ICefDataType;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;

public interface ICefValueObject extends ICefDataType {
    void afterModifyDeterminate(ICefDeterminationContext parent, ValueObjModifyChangeDetail change);

    void retrieveDefaultDeterminate(ICefDeterminationContext parent);

    void beforeSaveDeterminate(ICefDeterminationContext parent, ValueObjModifyChangeDetail change);

    void afterModifyDeterminate(ValueObjModifyChangeDetail change);

    void retrieveDefaultDeterminate();

    void beforeSaveDeterminate(ValueObjModifyChangeDetail change);


    void afterModifyValidate(ValueObjModifyChangeDetail change);

    void beforeSaveValidate(ValueObjModifyChangeDetail change);
}
