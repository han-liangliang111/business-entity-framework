package com.inspur.edp.cef.api.exceptions;

/**
 * @className: ExceptionCode
 * @author: wangmj
 * @date: 2023/11/18
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 spi:3001开头 core:4001开头 repository:5001

    /**
     * 当前数据:[{0}]处于删除状态，无法执行添加子表数据操作。
     */
    public static final String CEF_RUNTIME_2001 = "CEF_RUNTIME_2001";
    /**
     * 未找到类:[{0}]
     */
    public static final String CEF_RUNTIME_2002 = "CEF_RUNTIME_2002";
    /**
     * 构造[{0}]的对象出错
     */
    public static final String CEF_RUNTIME_2003 = "CEF_RUNTIME_2003";
    /**
     * 解析日期格式出错：[{0}]
     */
    public static final String CEF_RUNTIME_2004 = "CEF_RUNTIME_2004";

    /**
     * 被监听的data不能为空
     */
    public static final String CEF_RUNTIME_2005 = "CEF_RUNTIME_2005";
    public static final String CEF_RUNTIME_2006 = "CEF_RUNTIME_2006";
}
