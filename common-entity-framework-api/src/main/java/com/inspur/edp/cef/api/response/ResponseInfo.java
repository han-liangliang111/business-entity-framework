/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.response;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.request.RequestInfo;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseInfo {
    private Object privateReturnValue;
    private HashMap<String, IChangeDetail> privateInnerDataChange;
    private List<IBizMessage> privateMessage;
    private IChangeDetail privateInnerVariableChange;

    /**
     * 自定义信息，用于后端返回前端所需额外信息，仅低代码系统内部使用，不放开给低代码用户
     *
     * @see RequestInfo#getCustom
     */
    private Map<String, String> customResult;

    public final Object getReturnValue() {
        return privateReturnValue;
    }

    public final void setReturnValue(Object value) {
        privateReturnValue = value;
    }

    public final java.util.HashMap<String, IChangeDetail> getInnerDataChange() {
        return privateInnerDataChange;
    }

    public final void setInnerDataChange(java.util.HashMap<String, IChangeDetail> value) {
        privateInnerDataChange = value;
    }

    public final java.util.List<IBizMessage> getMessage() {
        return privateMessage;
    }

    public final void setMessage(java.util.List<IBizMessage> value) {
        privateMessage = value;
    }

    public final IChangeDetail getInnerVariableChange() {
        return privateInnerVariableChange;
    }

    public final void setInnerVariableChange(IChangeDetail value) {
        privateInnerVariableChange = value;
    }

    public final Map<String, String> getCustomResult() {
        return customResult;
    }

    public final void setCustomResult(Map<String, String> customResult) {
        this.customResult = customResult;
    }

}
