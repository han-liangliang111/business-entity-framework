/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.manager;

public interface ICefMgrActionAssembler {
    /**
     * 自定义管理动作执行后的扩展时机
     */
    void afterExecute();

    /**
     * 自定义管理动作执行前的扩展时机
     */
    void beforeExecute();

    /**
     * 自定义管理动作执行时抛异常的扩展时机
     *
     * @param e 异常信息
     * @return <see cref="bool"/>
     */
    boolean handleException(RuntimeException e);
}
