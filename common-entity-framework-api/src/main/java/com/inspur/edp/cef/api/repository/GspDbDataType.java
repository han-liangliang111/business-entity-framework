/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.repository;

/**
 * BEF使用的字段数据类型
 */
public enum GspDbDataType {
    Char(0),
    NChar(1),
    VarChar(2),
    NVarChar(3),
    Int(4),
    Decimal(5),
    DateTime(6),
    Blob(7),
    Clob(8),
    NClob(9),
    Date(10),
    Boolean(11),
    Jsonb(12),
    Long(13),
    SmallInt(14),
    UnKnown(255);

    private static java.util.HashMap<Integer, GspDbDataType> mappings;
    private final int intValue;

    GspDbDataType(int value) {
        intValue = value;
        GspDbDataType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, GspDbDataType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<>();
        }
        return mappings;
    }

    public static GspDbDataType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
