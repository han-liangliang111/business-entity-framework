/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.repository;

import com.inspur.edp.cef.api.exceptions.ExceptionCode;
import com.inspur.edp.cef.entity.exception.CefRuntimeBaseException;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DbParameter {
    private final String paramName;
    private final GspDbDataType dataType;
    private final Object value;

    public DbParameter(String paramName, GspDbDataType dataType, Object value) {
        this.paramName = paramName;
        this.dataType = dataType;
        if (dataType == GspDbDataType.Date) {
            if (CAFContext.current.getDbType() == DbType.OceanBase) {
                if (value != null && value instanceof Date) {
                    SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
                    String format = sf.format(value);
                    try {
                        value = sf.parse(format);
                    } catch (ParseException e) {
                        throw new CefRuntimeBaseException(ExceptionCode.CEF_RUNTIME_2004, e, format);
                    }
                }
            }
        }

        this.value = value;
    }


    public String getParamName() {
        return paramName;
    }

    public GspDbDataType getDataType() {
        return dataType;
    }

    public Object getValue() {
        return value;
    }

    public DbParameter Clone() {
        DbParameter newValue = new DbParameter(getParamName(), getDataType(), getValue());
        return newValue;
    }
}
