/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.manager.serialize;

public class CefSerializeContext {

    private static CefSerializeContext serContextKeepAssoPropertyForExpression;
    private Boolean enableStdTimeFormat = false;
    private boolean bigNumber = false;
    private JsonFormatType jsonFormatType = JsonFormatType.Normal;
    private boolean keepAssoPropertyForExpression = false;

    public static CefSerializeContext getSerContextKeepAssoPropertyForExpression() {
        if (serContextKeepAssoPropertyForExpression == null) {
            serContextKeepAssoPropertyForExpression = new CefSerializeContext();
            serContextKeepAssoPropertyForExpression.setKeepAssoPropertyForExpression(true);
        }
        return serContextKeepAssoPropertyForExpression;
    }

    public Boolean getEnableStdTimeFormat() {
        return enableStdTimeFormat;
    }

    public void setEnableStdTimeFormat(Boolean enableStdTimeFormat) {
        this.enableStdTimeFormat = enableStdTimeFormat;
    }

    public boolean isBigNumber() {
        return bigNumber;
    }

    public void setBigNumber(boolean bigNumber) {
        this.bigNumber = bigNumber;
    }

    public JsonFormatType getJsonFormatType() {
        return jsonFormatType;
    }

    public void setJsonFormatType(JsonFormatType jsonFormatType) {
        this.jsonFormatType = jsonFormatType;
    }

    public final boolean isKeepAssoPropertyForExpression() {
        return keepAssoPropertyForExpression;
    }

    public final void setKeepAssoPropertyForExpression(boolean keepAssoPropertyForExpression) {
        this.keepAssoPropertyForExpression = keepAssoPropertyForExpression;
    }
}
