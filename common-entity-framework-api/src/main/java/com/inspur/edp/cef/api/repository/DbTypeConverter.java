package com.inspur.edp.cef.api.repository;

import io.iec.edp.caf.commons.dataaccess.DbType;

public class DbTypeConverter {
    public static GspDbType transGspDbType(DbType dbType) {
        GspDbType gspDbType;
        switch (dbType) {
            case Oracle:
                gspDbType = GspDbType.Oracle;
                break;
            case SQLServer:
                gspDbType = GspDbType.SQLServer;
                break;
            case DM:
                gspDbType = GspDbType.DM;
                break;
            case PgSQL:
                gspDbType = GspDbType.PgSQL;
                break;
            case HighGo:
                gspDbType = GspDbType.HighGo;
                break;
            case OpenGauss:
                gspDbType = GspDbType.OpenGauss;
                break;
            case GaussDB:
                gspDbType = GspDbType.GaussDB;
                break;
            case Gbase:
                gspDbType = GspDbType.GBase;
                break;
            case MySQL:
                gspDbType = GspDbType.MySQL;
                break;
            case Oscar:
                gspDbType = GspDbType.Oscar;
                break;
            case Kingbase:
                gspDbType = GspDbType.Kingbase;
                break;
            case DB2:
                gspDbType = GspDbType.DB2;
                break;
            case OceanBase:
                gspDbType = GspDbType.OceanBase;
                break;
            default:
                gspDbType = GspDbType.Unknown;
        }
        return gspDbType;
    }
}
