/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.attr;

import com.inspur.edp.cef.entity.config.CefConfig;
import com.inspur.edp.cef.entity.config.CefExtendConfig;

import java.util.ArrayList;

public class CefConfigCollection implements ICefConfigCollection {

    private final String id;
    private final String defaultNameSpace;
    private final String configType;

    public CefConfigCollection(String id, String defaultNameSpace, String configType) {

        this.id = id;
        this.defaultNameSpace = defaultNameSpace;
        this.configType = configType;
    }

    @Override
    public ArrayList<CefConfig> getConfigs() {
        java.util.ArrayList<CefConfig> list = new java.util.ArrayList<CefConfig>();
        com.inspur.edp.cef.entity.config.CefConfig config = new com.inspur.edp.cef.entity.config.CefConfig();
        config.setID(id);
        config.setDefaultNamespace(defaultNameSpace);
        config.setMgrConfig(new com.inspur.edp.cef.entity.config.MgrConfig());
        list.add(config);
        return list;
    }

    @Override
    public ArrayList<CefExtendConfig> getExtendConfigs() {
        return new ArrayList<>();
    }

    @Override
    public String getConfigType() {
        return configType;
    }
}
