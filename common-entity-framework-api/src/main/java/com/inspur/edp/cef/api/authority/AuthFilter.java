package com.inspur.edp.cef.api.authority;

import com.inspur.edp.cef.entity.condition.ExpressRelationType;
import lombok.Data;

/**
 * 内部接口, 随时删除, 请勿使用.
 */
@Data
public class AuthFilter {

    //如: ID
    private String columnName;
    //如: in (select authdata where xxx)
    private String filterBody;

    private ExpressRelationType relationType = ExpressRelationType.Empty;
}
