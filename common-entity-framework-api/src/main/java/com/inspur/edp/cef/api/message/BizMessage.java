/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.message;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BizMessage implements IBizMessage {

    @JsonIgnore
    private String privateMessageFormat;
    @JsonIgnore
    private String[] privateMessageParams;
    private MessageLevel privateLevel = MessageLevel.forValue(0);
    private MessageLocation privateLocation;
    @JsonIgnore
    private RuntimeException privateException;

    @Override
    @JsonIgnore
    public final String getMessageFormat() {
        return privateMessageFormat;
    }

    @Override
    public final void setMessageFormat(String value) {
        privateMessageFormat = value;
    }

    @Override
    @JsonIgnore
    public final String[] getMessageParams() {
        return privateMessageParams;
    }

    @Override
    public final void setMessageParams(String[] value) {
        privateMessageParams = value;
    }

    @Override
    public final MessageLevel getLevel() {
        return privateLevel;
    }

    @Override
    public final void setLevel(MessageLevel value) {
        privateLevel = value;
    }

    @Override
    public final MessageLocation getLocation() {
        return privateLocation;
    }

//  private List<IBizMessage> details;
//  public List<IBizMessage> getDetailMessages() {
//    return details == null ? (details = new ArrayList<IBizMessage>()) : details;
//  }

    @Override
    public final void setLocation(MessageLocation value) {
        privateLocation = value;
    }

    @Override
    @JsonIgnore
    public final RuntimeException getException() {
        return privateException;
    }

    @Override
    public final void setException(RuntimeException value) {
        privateException = value;
    }

    @Override
    public String getMessage() {
        if (getMessageParams() == null || getMessageParams().length == 0) {
            return getMessageFormat();
        } else {
            return String.format(getMessageFormat(), getMessageParams());
        }
    }
}
