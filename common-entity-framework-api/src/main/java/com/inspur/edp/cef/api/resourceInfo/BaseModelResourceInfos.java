/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.resourceInfo;

/**
 * 国际化模型信息(20191205废弃，挪至spi)
 */
@Deprecated
public abstract class BaseModelResourceInfos {
    /**
     * 模型名称
     *
     * @return
     */
    public abstract String getModelDispalyName();

    /**
     * 主节点编号
     */
    public abstract String getRootNodeCode();

    /**
     * 资源元数据ID
     */
    public abstract String getResourceMetaId();

}
