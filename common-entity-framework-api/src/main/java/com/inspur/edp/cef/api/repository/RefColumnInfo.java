/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.repository;

public class RefColumnInfo {
    /**
     * 带出字段的数据库字段编号(别名.编号)
     */
    private String privateColumnName;
    private ITypeTransProcesser privateTransProcesser;
    private GspDbDataType privateColumnType;
    private RefColumnType refColumnType = RefColumnType.Normal;
    private String fieldReposExtendConfigClassImpl;
    private String fieldReposExtendConfigId;
    private boolean isVirtual = false;

    public RefColumnInfo() {
    }

    public final String getColumnName() {
        return privateColumnName;
    }

    public final void setColumnName(String value) {
        privateColumnName = value;
    }

    public final ITypeTransProcesser getTransProcesser() {
        return privateTransProcesser;
    }

    public final void setTransProcesser(ITypeTransProcesser value) {
        privateTransProcesser = value;
    }

    public final GspDbDataType getColumnType() {
        return privateColumnType;
    }

    public final void setColumnType(GspDbDataType value) {
        privateColumnType = value;
    }

    public RefColumnType getRefColumnType() {
        return refColumnType;
    }

    public void setRefColumnType(RefColumnType value) throws Exception {
        refColumnType = value;
    }

    public String getFieldReposExtendConfigClassImpl() {
        return fieldReposExtendConfigClassImpl;
    }

    public void setFieldReposExtendConfigClassImpl(String fieldReposExtendConfigClassImpl) {
        this.fieldReposExtendConfigClassImpl = fieldReposExtendConfigClassImpl;
    }

    public String getFieldReposExtendConfigId() {
        return fieldReposExtendConfigId;
    }

    public void setFieldReposExtendConfigId(String fieldReposExtendConfigId) {
        this.fieldReposExtendConfigId = fieldReposExtendConfigId;
    }

    public boolean isVirtual() {
        return isVirtual;
    }

    public void setVirtual(boolean virtual) {
        isVirtual = virtual;
    }

}
