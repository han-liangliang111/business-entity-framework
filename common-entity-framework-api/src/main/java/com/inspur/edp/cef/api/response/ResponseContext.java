/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.response;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageCollector;
import com.inspur.edp.cef.entity.changeset.ChangeDetailMerger;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResponseContext {

    // 消息
    public MessageCollector messageCollector = new MessageCollector();
    // 内部变更
    private HashMap<String, IChangeDetail> innerChangeset;
    private IChangeDetail privateVariableInnerChange;

    /**
     * 自定义信息，用于后端返回前端所需额外信息，仅低代码系统内部使用，不放开给低代码用户
     *
     * @see ResponseInfo#getCustomResult
     */
    private Map<String, String> customResult;

    public HashMap<String, IChangeDetail> getInnerChangeset() {
        return (innerChangeset != null)
                ? innerChangeset
                : (innerChangeset = new HashMap<>());
    }

    public final void mergeInnerChange(IChangeDetail... details) {
        for (IChangeDetail detail : details) {
            IChangeDetail existing = null;
            existing = getInnerChangeset().get(detail.getDataID());
            if (existing == null) {
                getInnerChangeset().put(detail.getDataID(), detail);
                return;
            }
            // 新增变更集合并一个修改变更集在ChangeDetailMerger中是异常情况, 但是在这里是正常的.
            if (existing.getChangeType() == ChangeType.Added && detail.getChangeType() == ChangeType.Modify) {
                return;
            }
            ChangeDetailMerger.mergeChangeDetail(detail, existing);
        }
    }

    public void clearInnerChange() {
        getInnerChangeset().clear();
    }

    public final IChangeDetail getVariableInnerChange() {
        return privateVariableInnerChange;
    }

    public final void setVariableInnerChange(IChangeDetail value) {
        privateVariableInnerChange = value;
    }

    public List<IBizMessage> getMessages() {
        return this.messageCollector.getMessages();
    }

    public final void addMessage(IBizMessage... msgList) {
        for (IBizMessage msg : msgList) {
            this.messageCollector.addMessage(msg);
        }
    }

    public final void addMessage(List<IBizMessage> msgList) {
        addMessage(msgList.toArray(new IBizMessage[]{}));
    }

    public void clearMessage() {
        this.messageCollector.clear();
    }

    public void setCustomResult(Map<String, String> customResult) {
        this.customResult = customResult;
    }

    public Map<String, String> getCustomResult() {
        return customResult;
    }

}
