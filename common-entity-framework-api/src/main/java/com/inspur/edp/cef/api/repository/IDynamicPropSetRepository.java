//package com.inspur.gsp.edp.api.repository;
//
//import Inspur.Gsp.Cef.entity.*;
//import Inspur.Gsp.Cef.entity.Changeset.*;
//
//public interface IDynamicPropSetRepository
//{
//	//获取需要的持久化字段
//	java.util.List<DynamicColumnInfo> GetPersistenceFields();
//
//	//从持久化读属性
//	IDynamicPropSet ReadValue(ICefReader reader, ICefData host);
//
//	//写到持久化
//	void WriteValues(ICefWriter writer, ICefData host);
//
//	//修改
//	void Modify(ValueObjModifyChangeDetail change, ICefData host);
//}