package com.inspur.edp.cef.api.repository;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.RefColumnInfo;
import lombok.Data;

/**
 * 关联表名拼接上下文
 * @Author wangmaojian
 * @create 2023/4/20
 */
@Data
public class TableNameRefContext {
    /**
     * 带出字段信息:key为带出字段在关联实体上的标签， value:为字段的基本信息，返回给外层对象
     */
    private java.util.HashMap<String, RefColumnInfo> columns;

    /**
     * 关联的字段编号信息，一般为关联对象的ID主键列
     */
    private String keyColumnName;

    /**
     * 关联的字段对应数据库字段编号
     */
    private RefObject<String> keyDbColumnName;

    /**
     * 别名集合
     */
    private java.util.ArrayList<String> tableAlias;
    /**
     * 关联字段指定关联对象的别名（IDP上可以指定）
     */
    private String refTableAlias;
}
