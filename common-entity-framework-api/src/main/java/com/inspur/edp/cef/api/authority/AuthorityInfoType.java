package com.inspur.edp.cef.api.authority;

/**
 * 内部接口, 随时删除, 请勿使用.
 */
public enum AuthorityInfoType {

    /**
     * join条件，类似 JOIN (select id from xxx where ...) authTable ON 业务表.xx = authTable.xx";
     */
    Join,

    /**
     * 过滤条件，类似 creator=‘’ and dept=''
     */
    Filter


//  private Integer num;
//
//  AuthorityInfoType(Integer num) {
//    this.num = num;
//  }
//
//  public int getNum() {
//    return num;
//  }
}
