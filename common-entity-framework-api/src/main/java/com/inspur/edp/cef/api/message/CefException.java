/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.message;

import io.iec.edp.caf.commons.exception.BaseException;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

public class CefException extends CAFRuntimeException {
    public CefException(String exceptionCode, String message, Exception innerException, ExceptionLevel level) {
        super("pfcommon", exceptionCode, message, innerException, level);
    }

    public CefException(String message, String exceptionCode) {
        super("pfcommon", exceptionCode, message, null, ExceptionLevel.Error);
    }

    public CefException(String exceptionCode) {
        super("pfcommon", exceptionCode, null, null, ExceptionLevel.Error);
    }

    public CefException(Exception e, String exceptionCode) {
        super("pfcommon", exceptionCode, null, e, ExceptionLevel.Error);
    }

}
