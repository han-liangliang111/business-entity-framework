/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.authority;

import com.inspur.edp.cef.api.repository.DbParameter;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class AuthorityInfo {

    /**
     * 内部接口, 随时删除, 请勿使用.
     */
    @Getter
    @Setter
    List<AuthFilter> Filter;

    //region join形式查询数据权限条件, 形如: 业务表 JOIN (权限sql) authTable ON 业务表.xx = authTable.xx";
    /**
     * 内部接口, 随时删除, 请勿使用.
     */
    @Getter
    @Setter
    List<DbParameter> filterParameters;

    /**
     * 权限信息类型
     * Join、Filter
     */
    @Getter
    @Setter
    private AuthorityInfoType authType = AuthorityInfoType.Join;
    /**
     * 权限字段名称
     */
    private String privateFieldName;
    /**
     * 权限关联字段
     */
    private String privateSourceFieldName;
    /**
     * 权限sql片段
     */
    private String privateAuthoritySql;

    public final String getFieldName() {
        return privateFieldName;
    }

    public final void setFieldName(String value) {
        privateFieldName = value;
    }

    public final String getSourceFieldName() {
        return privateSourceFieldName;
    }

    public final void setSourceFieldName(String value) {
        privateSourceFieldName = value;
    }
    //endregion

    //region 过滤形式查询数据权限条件, 形如:  creator=?0 and deptID=?1

    //过滤条件sql

    public final String getAuthoritySql() {
        return privateAuthoritySql;
    }

    public final void setAuthoritySql(String value) {
        privateAuthoritySql = value;
    }
    //endregion
}
