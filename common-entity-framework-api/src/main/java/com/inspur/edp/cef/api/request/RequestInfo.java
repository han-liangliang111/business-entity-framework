/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.request;

import com.inspur.edp.cef.api.response.ResponseInfo;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

import java.util.ArrayList;
import java.util.Map;

public class RequestInfo {
    private final java.util.ArrayList<IChangeDetail> dataChange =
            new ArrayList<>();
    private IChangeDetail variableChange;

    /**
     * 自定义信息，用于前端向后端传递信息，仅低代码系统内部使用，不放开给低代码用户
     *
     * @see ResponseInfo#getCustomResult
     */
    private Map<String, String> custom;

    public final java.util.ArrayList<IChangeDetail> getDataChange() {
        return dataChange;
    }

    public final IChangeDetail getVariableChange() {
        return variableChange;
    }

    public final void setVariableChange(IChangeDetail value) {
        variableChange = value;
    }

    public final Map<String, String> getCustom() {
        return custom;
    }

    public final void setCustom(Map<String, String> custom) {
        this.custom = custom;
    }
}
