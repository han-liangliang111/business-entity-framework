/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.repository.adaptor;

import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;

import com.inspur.edp.cef.entity.repository.DataSaveParameter;

import java.sql.SQLException;

public interface IEntityAdaptor extends IRepositoryAdaptor {
    void initParams(java.util.Map<String, Object> pars);

    void initVars(java.util.HashMap<String, String> vars);

    //IEntityAdaptor ParentAdaptor { get; }

    //IList<IEntityAdaptor> ChildAdaptors { get; }

//	int modify(ModifyChangeDetail change, DataSaveParameter par) throws SQLException;
//
//	int insert(IEntityData data, DataSaveParameter par) throws SQLException;
//
//	int delete(String id, DataSaveParameter par) throws SQLException;
//
//	void delete(java.util.List<String> ids, DataSaveParameter par) throws SQLException;

    java.util.List<IEntityData> query(EntityFilter filter,
                                      java.util.ArrayList<AuthorityInfo> authorities);

    java.util.List<IEntityData> getDataByIDs(java.util.List<String> dataIds) throws SQLException;
}
