package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.sql.Connection;

/**
 * SamllInt类型处理器
 * @author wangmj
 * @since 2024/6/28
 */
public class SmallIntTransProcesser implements ITypeTransProcesser {

    public static SmallIntTransProcesser getInstance() {
        return Singleton.INSTANCE;
    }

    @Override
    public Object transType(FilterCondition filter, Connection db) {
        return this.transType(filter.getValue());
    }

    @Override
    public Object transType(Object value) {
        return this.transType(value, true);
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        if (value == null || "".equals(value)) {
            return null;
        }
        if (value instanceof String) {
            return Short.parseShort(value.toString());
        }
        if(value instanceof Integer){
            return ((Integer)value).shortValue();
        }
        if (value instanceof Short) {
            return value;
        }
        throw new IllegalArgumentException(value.getClass().toString());
    }

    private static class Singleton {
        static SmallIntTransProcesser INSTANCE = new SmallIntTransProcesser();
    }
}
