package com.inspur.edp.cef.repository.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;

/**
 * @className: CefRunTimeException
 * @author: wangmj
 * @date: 2023/9/25
 **/
public class CefRunTimeException extends RuntimeException {
    public CefRunTimeException(Throwable e) {
        super(e);
    }

    public CefRunTimeException(String message, Throwable e) {
        super(message, e);
    }
}
