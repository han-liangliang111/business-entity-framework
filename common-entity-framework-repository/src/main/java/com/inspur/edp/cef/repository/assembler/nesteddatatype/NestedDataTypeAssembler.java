/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.nesteddatatype;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.INestedDataReader;
import com.inspur.edp.cef.api.repository.INestedPersistenceValueReader;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.repository.assembler.DataTypeAssembler;

public abstract class NestedDataTypeAssembler extends DataTypeAssembler {
    private java.util.HashMap<String, NestedColumnInfo> columnInfos = new java.util.HashMap<String, NestedColumnInfo>();

    private java.util.ArrayList<INestedDataReader> dataReaders = new java.util.ArrayList<INestedDataReader>();

    public NestedDataTypeAssembler() {
        super();
        init();
    }

    public java.util.HashMap<String, NestedColumnInfo> getColumnInfos() {
        return columnInfos;
    }

    public java.util.ArrayList<INestedDataReader> getDataReaders() {
        return dataReaders;
    }

    private void init() {
        initColumnInfo();
        //initReaders();
    }

    public abstract void initColumnInfo();

    //public abstract void initReaders();
    @Override
    public void readData(ICefData data, ICefReader reader) {
        super.readData(data, reader);
    }

    @Override
    public void writeData() {
        super.writeData();
    }

    protected final void addColumn(String columnName, INestedPersistenceValueReader nestedDataProcessor, boolean isMultiLang, boolean isUdt, boolean isAssociation, boolean isEnum, String belongElementLabel, GspDbDataType columnType) {
        NestedColumnInfo tempVar = new NestedColumnInfo();
        tempVar.setColumnName(columnName);
        tempVar.setBelongElementLabel(belongElementLabel);
        tempVar.setIsAssociation(isAssociation);
        tempVar.setIsEnum(isEnum);
        tempVar.setIsAssociateRefElement(false);
        tempVar.setIsMultiLang(isMultiLang);
        tempVar.setIsUdtElement(isUdt);
        tempVar.setPersistenceValueReader(nestedDataProcessor);
        tempVar.setColumnType(columnType);
        columnInfos.put(columnName, tempVar);
    }

    protected final void addColumn(String columnName, INestedPersistenceValueReader nestedDataProcessor, boolean isMultiLang, boolean isUdt, boolean isAssociation, boolean isEnum, String belongElementLabel) {
        NestedColumnInfo tempVar = new NestedColumnInfo();
        tempVar.setColumnName(columnName);
        tempVar.setBelongElementLabel(belongElementLabel);
        tempVar.setIsAssociation(isAssociation);
        tempVar.setIsEnum(isEnum);
        tempVar.setIsAssociateRefElement(false);
        tempVar.setIsMultiLang(isMultiLang);
        tempVar.setIsUdtElement(isUdt);
        tempVar.setPersistenceValueReader(nestedDataProcessor);
        columnInfos.put(columnName, tempVar);
    }

    protected final void AddDataReader(INestedDataReader reader) {
        dataReaders.add(reader);
    }

    public NestedDataColumnMapType getColumnMapType() {
        return NestedDataColumnMapType.MultiColumns;
    }
}
