package com.inspur.edp.cef.repository.extend;

import com.inspur.edp.cef.repository.adaptor.KeyWordsManager;
import com.inspur.edp.cef.spi.repository.BaseDbColumnInfo;
import com.inspur.edp.cef.spi.repository.FieldContext;
import com.inspur.edp.cef.spi.repository.IFieldReposExtendProcessor;

/**
 * @Author wangmaojian
 * @create 2023/4/17
 */
public class TestFieldProcessor implements IFieldReposExtendProcessor {
    @Override
    public String getQueryField(FieldContext fieldContext) {
        String type = "";
        if (fieldContext.getVariableData() != null && fieldContext.getVariableData().size() > 0) {
            //打印下值
            type = fieldContext.getVariableData().get("Type");
        }
        if (type == null || "".equalsIgnoreCase("")) {
            return fieldContext.getTableAlias() + ".code as " + fieldContext.getDbColumnInfo().getColumnName();
        }

        if (!fieldContext.getDbColumnInfo().getColumnName().toLowerCase().contains("displayname")) {
            return "";
        }
        if (type.equalsIgnoreCase("1")) {
            return fieldContext.getTableAlias() + ".code as " + fieldContext.getDbColumnInfo().getColumnName();
        } else if (type.equalsIgnoreCase("2")) {
            return fieldContext.getTableAlias() + ".shortname as " + fieldContext.getDbColumnInfo().getColumnName();
        }

        return fieldContext.getTableAlias() + ".code as " + fieldContext.getDbColumnInfo().getColumnName();
    }
}
