/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.bef.api.lcp.LcpFactoryManagerUtils;
import com.inspur.edp.cef.api.association.Many2OneAssociation;
import com.inspur.edp.cef.repository.assembler.AssociationInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class AssociationUtils {
    public static List<AssociationInfo> getAssocationInfosByMany2OneAssoInfo(List<Many2OneAssociation> many2OneAssociations) {
        if (many2OneAssociations == null || many2OneAssociations.size() == 0)
            return null;

        List<AssociationInfo> associationInfos = new ArrayList<>();
        for (Many2OneAssociation many2OneAssociation : many2OneAssociations) {
            associationInfos.add(convertMany2OneAssoInfoToAssoInfo(many2OneAssociation));
        }

        return associationInfos;
    }

    private static AssociationInfo convertMany2OneAssoInfoToAssoInfo(Many2OneAssociation many2OneAssociation) {
        AssociationInfo associationInfo = new AssociationInfo();
        associationInfo.setNodeCode(many2OneAssociation.getAssociationRefInfo().getNodeCode());
        associationInfo.setSourceColumn(many2OneAssociation.getPrivateTargetColumn());
        associationInfo.setTargetColumn(many2OneAssociation.getPrivateSourceColumn());

        associationInfo.setRefRepository(LcpFactoryManagerUtils.getBefRepositoryFactory()
                .createRepository(many2OneAssociation.getAssociationRefInfo().getConfigID()));
        associationInfo.setRefColumns((HashMap<String, String>) many2OneAssociation.getRefProperties());
        associationInfo.setConfigId(many2OneAssociation.getAssociationRefInfo().getConfigID());
        return associationInfo;
    }

}
