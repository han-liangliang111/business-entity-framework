/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.association.AssociationRefProperty;
import com.inspur.edp.cef.api.association.Many2OneAssociation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class QueryContext {
    private final List<Many2OneAssociation> extendAssociations;
    private final Map<String, List<AssociationRefProperty>> extendRefPropeties;
    private EntityRelationalReposAdaptor belongAdaptor;
    private StringBuilder queryFields;
    private StringBuilder queryTables;
    private StringBuilder conditions;
    private StringBuilder orders;
    private HashMap<String, Integer> mapping = new HashMap<>();

    QueryContext(EntityRelationalReposAdaptor belongAdaptor, List<Many2OneAssociation> extendAssociations, Map<String, List<AssociationRefProperty>> extendRefPropeties) {
        this.belongAdaptor = belongAdaptor;

        this.extendAssociations = extendAssociations;
        this.extendRefPropeties = extendRefPropeties;
    }

    private StringBuilder innerGetQueryFields() {
        if (queryFields == null)
            queryFields = new StringBuilder(128);
        return queryFields;
    }

    final String getQueryFields() {
        if (queryFields == null)
            return "";
        return queryFields.toString();
    }

    final QueryContext appendFields(String appendingValue) {
        innerGetQueryFields().append(appendingValue);
        return this;
    }

    final void appendFields(StringBuilder appendingValue) {
        innerGetQueryFields().append(appendingValue);
    }

    private StringBuilder innerGetQueryTables() {
        if (queryTables == null)
            queryTables = new StringBuilder(128);
        return queryTables;
    }

    final String getQueryTables() {
        if (queryTables == null)
            return "";
        return queryTables.toString();
    }

    final void appendQueryTables(String queryTables) {
        innerGetQueryTables().append(queryTables);
    }

    final void appendQueryTables(StringBuilder queryTables) {
        innerGetQueryTables().append(queryTables);
    }

    private StringBuilder innerGetQueryConditions() {
        if (conditions == null)
            conditions = new StringBuilder(32);
        return conditions;
    }

    final String getQueryCondition() {
        if (conditions == null)
            return "";
        return conditions.toString();
    }

    final void appendConditions(String queryConditions) {
        innerGetQueryConditions().append(queryConditions);
    }

    final void appendConditions(StringBuilder conditions) {
        innerGetQueryConditions().append(conditions);
    }

    private StringBuilder innerGetSortConditions() {
        if (orders == null)
            orders = new StringBuilder();
        return orders;
    }

    final String getSortConditions() {
        if (orders == null)
            return "";
        return orders.toString();
    }

    final QueryContext appendOrders(String orderConditions) {
        innerGetSortConditions().append(orderConditions);
        return this;
    }

    final QueryContext appendOrders(StringBuilder orderConditions) {
        innerGetSortConditions().append(orderConditions);
        return this;
    }

    final HashMap<String, Integer> getMapping() {
        return mapping;
    }

    final void setMapping(HashMap<String, Integer> mapping) {
        this.mapping = mapping;
    }

    public String getQuerySql() {
        StringBuilder stringBuilder = new StringBuilder(200);
        stringBuilder.append(String.format("SELECT %1$s FROM %2$s ", getQueryFields(), getQueryTables()));
        if (conditions != null && conditions.length() > 0)
            stringBuilder.append(" Where ").append(conditions);
        if (orders != null && orders.length() > 0)
            stringBuilder.append(" ORDER BY ").append(orders);
        return stringBuilder.toString();
    }

    public List<Many2OneAssociation> getExtendAssociations() {
        return extendAssociations;
    }

    public Map<String, List<AssociationRefProperty>> getExtendRefPropeties() {
        return extendRefPropeties;
    }
}
