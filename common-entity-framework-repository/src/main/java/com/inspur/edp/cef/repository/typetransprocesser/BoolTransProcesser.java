/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;

import java.sql.Connection;

public class BoolTransProcesser implements ITypeTransProcesser {

    private static BoolTransProcesser instance;

    private BoolTransProcesser() {

    }

    public static BoolTransProcesser getInstacne() {
        if (instance == null) {
            instance = new BoolTransProcesser();
        }
        return instance;
    }

    @Override
    public Object transType(FilterCondition filter, Connection connection) {
        if (filter.getValue().toUpperCase().equals("FALSE") || filter.getValue().equals("0")) {
            filter.setValue("0");
        } else if (filter.getValue().toUpperCase().equals("TRUE") || filter.getValue().equals("1")) {
            filter.setValue("1");
        } else {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5038);
        }

        return filter.getValue().equals("1");
    }

    @Override
    public Object transType(Object value) {
        return this.transType(value, false);
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        if (value == null) {
            return null;
        }
        if (!(value instanceof Boolean)) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5036, false, value.getClass().getTypeName(), value.toString());
        }

        return value;
    }
}
