/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.*;
import com.inspur.edp.cef.entity.condition.advanceconditions.BqlConditionValue;
import com.inspur.edp.cef.repository.adaptor.KeyWordsManager;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.repository.filter.FilterContext;
import com.inspur.edp.qdp.bql.api.IBqlExecuter;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.math.BigDecimal;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public final class FilterUtil {
    /**
     * 根据表达式关系符枚举转换为数据库连接符（and、or）
     * @param relation
     * @return
     */
    public static String trans2DbRelation(ExpressRelationType relation) {
        switch (relation) {
            case And:
                return " AND ";
            case Or:
                return " OR ";
            case Empty:
                return "";
        }
        return "";
    }

    public static String buildCompareAndParam(FilterCondition filter, int number) {
        return buildCompareAndParam(filter, number, true);
    }

    /**
     * 构造比较符、占位符，类似 =？
     * @param filter
     * @param number
     * @param useNumberBinding 参数绑定是否使用数字占位符
     * @return
     */
    public static String buildCompareAndParam(FilterCondition filter, int number, boolean useNumberBinding) {
        String param =  useNumberBinding ?  " ?" + (number) + " " : " ? ";
        //in条件是否使用参数绑定的变量
        boolean userBindingParam = false;
        switch (filter.getCompare()) {
            case Equal:
                return " = " + param;
            case NotEqual:
                return "<>" + param;
            case Greater:
                return ">" + param;
            case GreaterOrEqual:
                return ">=" + param;
            case Less:
                return "<" + param;
            case LessOrEqual:
                return "<=" + param;
            case Like:
            case LikeStartWith:
            case LikeEndWith:
            case LikeIgnoreCase:
                return " like " + param;
            case NotLike:
            case NotLikeStartWith:
            case NotLikeEndWith:
                return " not like " + param;
            case In: {
                userBindingParam = filter.getInValues() != null && filter.getInValues().size() > 0 ? true : false;
                return userBindingParam ? " IN" + param : " IN";
            }
            case NotIn: {
                userBindingParam = filter.getInValues() != null && filter.getInValues().size() > 0 ? true : false;
                return userBindingParam ? " NOT IN" + param : " NOT IN";
            }
            case Is:
                return " IS NULL";
            case IsNot:
                return " IS NOT NULL";
            default:
                return "";
        }
    }

    public static String parseExpression(String expression) {
        //TODO 表达式解析集成
        return ExpressionUtil.getExpressionValue(expression) + "";
    }

    public static List getBuildInCondition(String[] values, GspDbDataType dataType) {
        List list = null;
        if (dataType == GspDbDataType.Int) {
            list = new ArrayList<Integer>();
        } else if (dataType == GspDbDataType.Long) {
            list = new ArrayList<Long>();
        } else if (dataType == GspDbDataType.Decimal) {
            list = new ArrayList<BigDecimal>();
        } else {
            list = new ArrayList<String>();
        }
        if (values == null || values.length == 0)
            return list;
        for (int i = 0; i < values.length; i++) {
            if (dataType == GspDbDataType.Int) {
                list.add(Integer.parseInt(values[i].trim()));
            } else if (dataType == GspDbDataType.Long) {
                list.add(Long.parseLong(values[i].trim()));
            } else if (dataType == GspDbDataType.Decimal) {
                list.add(new BigDecimal(values[i].trim()));
            } else {
                list.add(values[i]);
            }
        }
        return list;
    }

    public static String buildInCondition(String[] values, boolean isNum, String propName) {
        return buildInCondition(Arrays.asList(values),isNum,propName);
    }

    public static String buildInCondition(List values, boolean isNum, String propName) {
        //当values为null 或 values个数为0 时，拼接出的sql都不正确，所以此时应该直接抛错
        if(values==null || values.size() == 0){
            throw new IllegalFilterConditionArgsException(com.inspur.edp.cef.entity.exception.ExceptionCode.CEF_RUNTIME_1026);
        }

        StringBuilder dataIdsCondition = new StringBuilder("(");

        //每批次数据个数
        int batchCount = 999;
        int groupCount = (values.size() % batchCount == 0) ? values.size() / batchCount : (values.size() / batchCount + 1);
        int stratNum = 0;
        int endNum = 0;

        for (int i = 1; i <= groupCount; i++) {
            int searchCount;
            if (i == groupCount) {
                searchCount = values.size() - (i - 1) * batchCount;
            } else {
                searchCount = batchCount;
            }

            if (i > 1) {
                dataIdsCondition.append(" OR " + propName + " IN (");
            }
            endNum = endNum + searchCount;
            for (int j = stratNum; j < endNum; j++) {
                if (j > stratNum) {
                    dataIdsCondition.append(",");
                }
                if (isNum) {
                    dataIdsCondition.append(values.get(j));
                } else {
                    dataIdsCondition.append("'");
                    dataIdsCondition.append(values.get(j));
                    dataIdsCondition.append("'");
                }
            }
            dataIdsCondition.append(")");
            stratNum = stratNum + searchCount;
        }
        return String.valueOf(dataIdsCondition);
    }

    public static void checkInParameterForSqlInjection(String value) {
        if (value == null || value.length() == 0) {
            return;
        }
        if (value.contains("'")) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5046, value);
        }
    }

    public static List buildInCondition(FilterCondition filter, GspDbDataType dataType) {
        //todo ()这种写法得搞搞
        //直接传了('1','2','3')这种
        String[] values = null;
        if (filter.getValue().indexOf("(") == 0 && filter.getValue().lastIndexOf(")") == filter.getValue().length() - 1) {
            String innerValue = filter.getValue().substring(1, filter.getValue().length() - 1);
            values = innerValue.split(",");
            for (int i = 0; i < values.length; i++) {
                if (values[i].indexOf("'") == 0 && values[i].lastIndexOf("'") == values[i].length() - 1 && values[i].length() >= 2) {
                    values[i] = values[i].substring(1, values[i].length() - 1);
                }
                values[i] = values[i].trim();
            }
        } else {
            String value = filter.getValue().trim();
            boolean isNum = dataType == GspDbDataType.Int || dataType == GspDbDataType.Long || dataType == GspDbDataType.Decimal;
            if (isNum) {
                //int类型的直接传递 1,2,3也支持一下
                values = value.indexOf("\r\n") > 0 ? value.split("\r\n") : value.split(",");
            } else {
                values = value.split("\r\n");
            }
        }
        List list = getBuildInCondition(values, dataType);
        return list;
    }

    public static String buildInCondition(FilterCondition filter,RefObject<Integer> refNum, GspDbDataType dataType, String propName) {
        if (filter.getValue().indexOf("(") == 0 && filter.getValue().lastIndexOf(")") == filter.getValue().length() - 1) {
            return filter.getValue();
        }
        if (filter.getValue().toUpperCase().contains("SELECT")) {
            //替换占位符
            while (filter.getValue().contains("?placeHolder")){
                filter.setValue(filter.getValue().replaceFirst("\\?placeHolder", "?" +  refNum.argvalue++));
            }
            return "(" + filter.getValue() + ")";
        }
        String value = filter.getValue().trim();
        String[] values = value.split("\r\n");

        boolean isNum = dataType == GspDbDataType.SmallInt || dataType == GspDbDataType.Int || dataType == GspDbDataType.Long || dataType == GspDbDataType.Decimal;
        return buildInCondition(values, isNum, propName);
    }

    //TODO parameters类型修改后，如何获取构造parameters的方法
    public static String parseFilterCondition(FilterContext filterContext) {
        FilterCondition filter = filterContext.getFilterCondition();
        List<DbParameter> parameters = filterContext.getParameters();
        RefObject<Integer> paramNum = filterContext.getParamNum();
        ITypeTransProcesser processer = filterContext.getProcesser();
        GspDbDataType dataType = filterContext.getDataType();
        String columnName = KeyWordsManager.getColumnAlias(filterContext.getDbColumnName());
        if (filter.getExpresstype() == ExpressValueType.Expression && !filter.isParsed()) {
            filter.setValue(parseExpression(filter.getValue()));
            filter.setParsed(true);
        }

        if (filter.getExpresstype() == ExpressValueType.Bql) {
            if (filter.getCompare() == ExpressCompareType.In || filter.getCompare() == ExpressCompareType.NotIn) {
                if (filter.getCustomConditionValue() == null || ((filter
                        .getCustomConditionValue() instanceof BqlConditionValue)) == false)
                    throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5042);
                BqlConditionValue bqlConditionValue = (BqlConditionValue) filter.getCustomConditionValue();
                String bqlString = getBqlExecutor().translateBql(bqlConditionValue.getBql(), bqlConditionValue.getRefBEIds());
                return String.format(" %1$s " + columnName + " %2$s (%3$s)  %4$s  %5$s",
                        filter.getLbracket(), buildCompareAndParam(filter, 0, filterContext.isUseNumberBinding()),
                        bqlString, filter.getRbracket(),
                        trans2DbRelation(filter.getRelation()));
            }
        }
        if (filter.getCompare() == ExpressCompareType.In || filter.getCompare() == ExpressCompareType.NotIn) {
            //如果条件是通过集合的方式进来
            if (filter.getInValues() != null && filter.getInValues().size() > 0) {
                return buildInConditionWithInValue(filter, paramNum, parameters, dataType, columnName);
            } else {
                return buildInConditionWithValue(filter,paramNum, parameters, dataType, columnName);
            }
        }

        if (filter.getCompare() == ExpressCompareType.Is || filter.getCompare() == ExpressCompareType.IsNot) {
            return String.format(" %1$s " + columnName + " %2$s %3$s %4$s", filter.getLbracket(),
                    buildCompareAndParam(filter, 0, filterContext.isUseNumberBinding()), filter.getRbracket(), trans2DbRelation(filter.getRelation()));
        }

        Object value = null;
        try{
            value = processer.transType(filter, null);
        }
        catch (Exception ex){
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5078, ex, ExceptionLevel.Error, false, filter.getFilterField(), filter.getValue());
        }

        if (isLikeCondition(filter)) {
            String valueFormat = buildCompareAndParam(filter, paramNum.argvalue, filterContext.isUseNumberBinding());
            //同时包含是否会有问题
            String filterValue = filter.getValue();
            //是否需要转义
            boolean needEscape = needEscape(filter,filterValue);
            if(needEscape) {
                //增加自定义转义符 ？0 escape '\'
                valueFormat = handleEscapeSql(valueFormat);
                //字符转义
                filterValue = escapeFilterValue(filterValue);
            }
            if (filter.getCompare() == ExpressCompareType.Like || filter.getCompare() == ExpressCompareType.NotLike) {
                value = "%" + filterValue + "%";
            } else if (filter.getCompare() == ExpressCompareType.LikeStartWith || filter.getCompare() == ExpressCompareType.NotLikeStartWith) {
                value = filterValue + "%";
            } else if (filter.getCompare() == ExpressCompareType.LikeEndWith || filter.getCompare() == ExpressCompareType.NotLikeEndWith) {
                value = "%" + filterValue;
            }
            if (filter.getCompare() == ExpressCompareType.LikeIgnoreCase) {
                if (value == null || value instanceof String == false)
                    throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5043, false, filter.getFilterField(), value == null ? "null" : value.getClass().getTypeName());
                //sqlserver lower函数
                if (!(CAFContext.current.getDbType() == DbType.SQLServer && (dataType == GspDbDataType.Clob || dataType == GspDbDataType.NClob))) {
                    value = "%" + filterValue.toLowerCase() + "%";
                } else {
                    value = "%" + filterValue + "%";
                }
            }
            DbParameter paramItem = buildParam(filter.getFilterField() + paramNum.argvalue, dataType, value);
            parameters.add(paramItem);
            paramNum.argvalue++;
            if (filter.getCompare() == ExpressCompareType.LikeIgnoreCase) {
                if (CAFContext.current.getDbType() == DbType.SQLServer && (dataType == GspDbDataType.Clob || dataType == GspDbDataType.NClob)) {
                    return String.format(" %1$s " + columnName + " %2$s %3$s %4$s", filter.getLbracket(), valueFormat, filter.getRbracket(), trans2DbRelation(filter.getRelation()));
                } else {
                    return String.format(" %1$s  lower(" + columnName + ")  %2$s %3$s %4$s", filter.getLbracket(), valueFormat, filter.getRbracket(), trans2DbRelation(filter.getRelation()));
                }
            } else {
                return String.format(" %1$s " + columnName + " %2$s %3$s %4$s", filter.getLbracket(), valueFormat, filter.getRbracket(), trans2DbRelation(filter.getRelation()));
            }
        }


        //region 06 TODO 这块逻辑需要调整，判断结构比较混乱
        StringBuilder conditionBuilder = new StringBuilder();
        if (CAFContext.current.getDbType() == DbType.Oracle || CAFContext.current.getDbType() == DbType.DM || CAFContext.current.getDbType() == DbType.OceanBase) {
            if (dataType == GspDbDataType.Clob) {
                conditionBuilder.append(" ").append(filter.getLbracket()).append("to_char(").append("substr(").append(columnName).append(",0,4000))").append(" ").append(buildCompareAndParam(filter, paramNum.argvalue, filterContext.isUseNumberBinding()))
                        .append(filter.getRbracket()).append(" ").append(trans2DbRelation(filter.getRelation()));
            } else {
                conditionBuilder.append(" ").append(filter.getLbracket()).append(columnName).append(" ").append(buildCompareAndParam(filter, paramNum.argvalue, filterContext.isUseNumberBinding()))
                        .append(filter.getRbracket()).append(" ").append(trans2DbRelation(filter.getRelation()));
            }

        } else if (CAFContext.current.getDbType() == DbType.SQLServer) {
            if (dataType == GspDbDataType.Clob) {
                conditionBuilder.append(" ").append(filter.getLbracket()).append("convert(").append("nvarchar(max)").append(",").append(columnName).append(")").append(" ").append(buildCompareAndParam(filter, ((Integer) paramNum.argvalue).intValue(), filterContext.isUseNumberBinding()))
                        .append(filter.getRbracket()).append(" ").append(trans2DbRelation(filter.getRelation()));//处理SQL Sever数据库中的clob过滤
            } else {
                conditionBuilder.append(" ").append(filter.getLbracket()).append(columnName).append(" ").append(buildCompareAndParam(filter, paramNum.argvalue, filterContext.isUseNumberBinding()))
                        .append(filter.getRbracket()).append(" ").append(trans2DbRelation(filter.getRelation()));
            }
        } else {
            conditionBuilder.append(" ").append(filter.getLbracket()).append(columnName).append(" ").append(buildCompareAndParam(filter, paramNum.argvalue, filterContext.isUseNumberBinding()))
                    .append(filter.getRbracket()).append(" ").append(trans2DbRelation(filter.getRelation()));
        }

        //IDbDataParameter paramItem = db.MakeInParam(filter.FilterField + paramNum, dataType, filter.Value.Length, value);
        if (filter.getCompare() == ExpressCompareType.Is || filter.getCompare() == ExpressCompareType.IsNot)
            return conditionBuilder.toString();
        DbParameter paramItem = buildParam(filter.getFilterField() + paramNum.argvalue, dataType, value);
        parameters.add(paramItem);
        paramNum.argvalue++;
        return conditionBuilder.toString();
    }

    /**
     * 根据筛选值，返回处理后的sql片段
     * @param value 为sql片段，如  like ?
     * @return
     */
    private final static String handleEscapeSql(String value) {
        if (value == null || value.length() ==0) {
            return value;
        }
        if(CAFContext.current.getDbType()== DbType.MySQL){
            //mysql数据库必须为 escape '\\' 形式，否则报错
            return value + " escape '\\\\'";
        }
        //采用通用的反斜杠作为转义字符串
        return value + " escape '\\'";
    }


    /**
     * 是否需要转义
     *
     * @param value       待检查字符串
     * @return
     */
    private static boolean needEscape(FilterCondition filterCondition, String value) {
        //过滤条件启用转义
        if(filterCondition.isEnableEscape()) {
             //需要转义的字符通常为 _ 和 %，但MySql、pg必须将\也作为转义符，
            List<String> escapeCharacters = new ArrayList<>(Arrays.asList("_", "%", "\\"));

            //如果value中存在需要转义的字符，则返回true
            for (String escapeChar : escapeCharacters) {
                if (value.contains(escapeChar))
                    return true;
            }
        }
        return false;
    }

    /**
     * 返回转义后的字符串
     *
     * @param filterValue 过滤字符串
     * @return
     */
    private final static String escapeFilterValue(String filterValue) {
        if (filterValue == null) {
            return null;
        }
        //需要转义的字符除通配符 _ 和 %外，需要把转义字符\加上
        List<String> escapeCharacters = new ArrayList<>(Arrays.asList("\\","_", "%"));

        for (String replacechar : escapeCharacters) {
            filterValue = filterValue.replace(replacechar, "\\" + replacechar);
        }
        return filterValue;
    }

    /**
     * 构造使用inValue的in条件
     * @param filterCondition 过滤条件
     * @param paramNum 序号
     * @param parameters 参数
     * @param dataType 数据类型
     * @param columnName 字段编号
     * @return
     */
    private static String buildInConditionWithInValue(FilterCondition filterCondition, RefObject<Integer> paramNum, List<DbParameter> parameters, GspDbDataType dataType, String columnName) {
        int inSize = filterCondition.getInValues().size();
        int batchCount = 999;
        //如果是SQLServer超过2000，就直接拼接字符串？
        if (CAFContext.current.getDbType() == DbType.SQLServer) {
            if (inSize > 2000 && (dataType == GspDbDataType.VarChar || dataType == GspDbDataType.NVarChar)) {
                StringBuilder inValue = new StringBuilder();
                inValue.append("(");
                for (Object id : filterCondition.getInValues()) {
                    inValue.append("'");
                    inValue.append(id.toString());
                    inValue.append("',");
                }
                //todo 是不是效率不行？
                String strValue = inValue.substring(0, inValue.length() - 1);
                strValue += ")";
                return String.format(" %1$s " + columnName + " %2$s %3$s%4$s %5$s ",
                        filterCondition.getLbracket(), filterCondition.getCompare(), strValue, filterCondition.getRbracket(), trans2DbRelation(filterCondition.getRelation()));
            }
        }
        if (inSize <= batchCount) {
            String inValueFormat = buildCompareAndParam(filterCondition, paramNum.argvalue);
            DbParameter paramItem = buildParam(filterCondition.getFilterField() + paramNum.argvalue, dataType, filterCondition.getInValues());
            parameters.add(paramItem);
            paramNum.argvalue++;
            return String.format(" %1$s " + columnName + " %2$s%3$s  %4$s  ",
                    filterCondition.getLbracket(), inValueFormat, filterCondition.getRbracket(), trans2DbRelation(filterCondition.getRelation()));
        }

        StringBuilder sbInFormat = new StringBuilder();
        int batchSize = inSize % batchCount == 0 ? inSize / batchCount : (inSize / batchCount + 1);
        String operator = filterCondition.getCompare() == ExpressCompareType.In ? "in" : " not in";
        sbInFormat.append(" (");
        for (int i = 0; i < batchSize; i++) {
            if (i == 0) {
                sbInFormat.append(String.format("%s %s (", columnName, operator));
            } else {
                sbInFormat.append(String.format(" or %s %s(", columnName, operator));
            }
            sbInFormat.append(String.format("?%s ", paramNum.argvalue));
            sbInFormat.append(") ");

            int fromIndex = i * batchCount;
            int toIndex = (i + 1) * batchCount;
            toIndex = toIndex > filterCondition.getInValues().size() ? filterCondition.getInValues().size() : toIndex;
            if (inSize > 15000 && (dataType == GspDbDataType.VarChar || dataType == GspDbDataType.NVarChar || dataType == GspDbDataType.Char || dataType == GspDbDataType.NChar || dataType == GspDbDataType.Clob || dataType == GspDbDataType.NClob)) {
                StringBuilder sbTemp = new StringBuilder();
                List tempList = filterCondition.getInValues().subList(fromIndex, toIndex);
                for (int i1 = 0; i1 < tempList.size(); i1++) {
                    if (i1 == 0) {
                        sbTemp.append(String.format("'%1$s'", tempList.get(i1)));
                    } else {
                        sbTemp.append(String.format(",'%1$s'", tempList.get(i1)));
                    }
                }
                DbParameter paramItem = buildParam(filterCondition.getFilterField() + paramNum.argvalue, dataType, sbTemp.toString());
                parameters.add(paramItem);
                paramNum.argvalue++;
            } else {
                DbParameter paramItem = buildParam(filterCondition.getFilterField() + paramNum.argvalue, dataType, filterCondition.getInValues().subList(fromIndex, toIndex));
                parameters.add(paramItem);
                paramNum.argvalue++;
            }
        }
        sbInFormat.append(") ");
        return String.format(" %1$s %2$s%3$s  %4$s  ",
                filterCondition.getLbracket(), sbInFormat.toString(), filterCondition.getRbracket(), trans2DbRelation(filterCondition.getRelation()));
    }

    /**
     * 兼容早期使用的in条件
     * @param filter
     * @param paramNum
     * @param parameters
     * @param dataType
     * @param columnName
     * @return
     */
    private static String buildInConditionWithValue(FilterCondition filter, RefObject<Integer> paramNum, List<DbParameter> parameters, GspDbDataType dataType, String columnName) {
        //条件格式：
        String inCondi = buildInCondition(filter, paramNum, dataType, columnName);
        if(filter.getExtenalParameter() != null && filter.getExtenalParameter().size() > 0){
            int Param = 0;
            for(FilterConditionParameter parameter : filter.getExtenalParameter()){
                GspDbDataType gspDbDataType = getGspDbDataType(parameter.getGspDbDataType());
                DbParameter dbParameter = new DbParameter("param" + (Param++), gspDbDataType, parameter.getValue());
                parameters.add(dbParameter);
            }
        }
        return String.format("( %1$s " + columnName + " %2$s%3$s  %4$s ) %5$s ",
                filter.getLbracket(), buildCompareAndParam(filter, 0, true),
                inCondi, filter.getRbracket(),
                trans2DbRelation(filter.getRelation()));
    }

    /**
     * 转换数据类型
     * @param dataType
     * @return
     */
    private static GspDbDataType getGspDbDataType(com.inspur.edp.caf.db.dbaccess.GspDbDataType dataType){
        switch (dataType){
            case Decimal:
                return GspDbDataType.Decimal;
            case Int:
                return GspDbDataType.Int;
            case Long:
                return GspDbDataType.Long;
            case VarChar:
                return GspDbDataType.VarChar;
            case NVarChar:
                return GspDbDataType.NVarChar;
            case DateTime:
                return GspDbDataType.DateTime;
            case Clob:
                return GspDbDataType.Clob;
            case NClob:
                return GspDbDataType.NClob;

        }
        return GspDbDataType.VarChar;
    }
    /**
     * 是否like条件
     *
     * @param filter
     * @return
     */
    private static boolean isLikeCondition(FilterCondition filter) {
        boolean result = false;
        result = filter.getCompare() == ExpressCompareType.Like || filter.getCompare() == ExpressCompareType.LikeStartWith
                || filter.getCompare() == ExpressCompareType.LikeEndWith
                || filter.getCompare() == ExpressCompareType.NotLikeStartWith
                || filter.getCompare() == ExpressCompareType.NotLikeEndWith
                || filter.getCompare() == ExpressCompareType.NotLike
                || filter.getCompare() == ExpressCompareType.LikeIgnoreCase;
        return result;
    }

    private static IBqlExecuter getBqlExecutor() {
        return SpringBeanUtils.getBean(IBqlExecuter.class);
    }

    //TODO 增加静态字典，记录字段对应的解析方法（model中是bool，DO是int的方法，model中是bool，DO是char的方法）
    private static String parseValue(Connection db, FilterCondition filter, GspDbDataType dataType) throws ParseException {
        switch (dataType) {
            case DateTime:
                return processDateTimeValue(filter.getValue());
            case VarChar:
            case NVarChar:
            case Char:
            case NChar:
            case Clob:
            case NClob:
                return processStringValue(filter);
            default:
                return filter.getValue();
        }

    }

    private static String processDateTimeValue(String value) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyyMMdd HHmmss");
        return format.parse(value).toString();
    }

    public static java.util.Date transDateTime(java.util.Date dateTime) {
        //TODO 根据国际化信息转换时间格式
        return dateTime;
    }

    ///#warning 这个地方需要完善
    public static String processStringValue(FilterCondition filter) {
        if ("".equals(filter.getValue()) &&
                (CAFContext.current.getDbType() == DbType.Oracle || CAFContext.current.getDbType() == DbType.OceanBase ||
                 CAFContext.current.getDbType() == DbType.Kingbase || CAFContext.current.getDbType() == DbType.GaussDB )) {
            if (filter.getCompare() == ExpressCompareType.IsNot || filter.getCompare() == ExpressCompareType.NotEqual) {
                filter.setCompare(ExpressCompareType.IsNot);
                filter.setValue("NUll");
            } else {
                filter.setCompare(ExpressCompareType.Is);
                filter.setValue("NULL");
            }
        }
        return filter.getValue();
    }

    public static DbParameter buildParam(String paramName, GspDbDataType dataType, Object paramValue) {
        paramName = KeyWordsManager.getColumnAlias(paramName);
        switch (dataType) {
            case SmallInt:
            case Int:
            case Long:
            case Decimal:
            case DateTime:
            case Date:
            case Boolean:
            case Jsonb:
                return new DbParameter(paramName, dataType, paramValue);
            case Clob:
            case NClob:
            case Char:
            case NChar:
            case VarChar:
            case NVarChar:
                if (paramValue == null)
                    return new DbParameter(paramName, dataType, paramValue);
                if (paramValue instanceof List) {
                    return new DbParameter(paramName, dataType, paramValue);
                }
                return new DbParameter(paramName, dataType, paramValue.toString());
            case Blob:

                byte[] byteAry = (byte[]) ((paramValue instanceof byte[]) ? paramValue : null);
                int length = 0;
                if (byteAry != null) {
                    length = byteAry.length;
                }
                Object value = byteAry;
                if (value == null) {
                    value = null;
                }
                return new DbParameter(paramName, dataType, value);
            //case GSPDbDataType.UnKnown:
            //case GSPDbDataType.Cursor:
            //case GSPDbDataType.Default:
            default:
                throw new UnsupportedOperationException("dataType:" + dataType);
        }
    }

    public static DbParameter buildParam(String paramName, GspDbDataType dataType, DbColumnInfo columnInfo, Object paramValue) {
        paramName = KeyWordsManager.getColumnAlias(paramName);
        switch (dataType) {
            case SmallInt:
            case Int:
            case Long:
            case Decimal:
            case DateTime:
            case Date:
            case Boolean:
            case Jsonb:
                return new DbParameter(paramName, dataType, paramValue);
            case Clob:
            case NClob:
            case Char:
            case NChar:
            case VarChar:
            case NVarChar:
                if (paramValue == null)
                    return new DbParameter(paramName, dataType, paramValue);
                if (paramValue instanceof List) {
                    return new DbParameter(paramName, dataType, paramValue);
                }
                return new DbParameter(paramName, dataType, paramValue.toString());
            case Blob:

                byte[] byteAry = (byte[]) ((paramValue instanceof byte[]) ? paramValue : null);
                int length = 0;
                if (byteAry != null) {
                    length = byteAry.length;
                }
                Object value = byteAry;
                if (value == null) {
                    value = null;
                }
                return new DbParameter(paramName, dataType, value);
            //case GSPDbDataType.UnKnown:
            //case GSPDbDataType.Cursor:
            //case GSPDbDataType.Default:
            default:
                throw new UnsupportedOperationException("dataType");
        }
    }

    public String handleInfCondition(FilterCondition filter, RefObject<Integer> paramNum, List<DbParameter> parameters, Object value, String columnName, GspDbDataType dataType) {
        String valueFormat = buildCompareAndParam(filter, paramNum.argvalue);
        if (filter.getCompare() == ExpressCompareType.Like || filter.getCompare() == ExpressCompareType.NotLike) {
            value = "%" + value + "%";
        } else if (filter.getCompare() == ExpressCompareType.LikeStartWith || filter.getCompare() == ExpressCompareType.NotLikeStartWith) {
            value = value + "%";
        } else if (filter.getCompare() == ExpressCompareType.LikeEndWith || filter.getCompare() == ExpressCompareType.NotLikeEndWith) {
            value = "%" + value;
        }
        DbParameter paramItem = buildParam(filter.getFilterField() + paramNum.argvalue, dataType, value);
        parameters.add(paramItem);
        paramNum.argvalue++;
        return String.format(" %1$s " + columnName + " %2$s %3$s %4$s", filter.getLbracket(), valueFormat, filter.getRbracket(), trans2DbRelation(filter.getRelation()));
    }


}
