/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.entity;

/**
 * 基础持久化组装器：
 * 每一个be元数据节点，对应一个组装器，记录了对应的数据库表的相关信息，
 * 通过标签ChildAssemblerAttribute记录子表组装器的类。
 * <p>
 * <p>
 * 每一个be元数据节点，对应一个组装器，记录了对应的数据库表的相关信息
 * 通过标签<see cref="ChildAssemblerAttribute"/>记录子表组装器的类
 */
public abstract class MainEntityAssembler extends EntityAssembler {
    private DbTableInfo tableInfo;
    //public assembler(string nodeCode) : base()
    //{
    //    NodeCode = nodeCode;
    //}

    public final DbTableInfo getTableInfo() {
        DbTableInfo tempVar = new DbTableInfo();
        tempVar.setTableName(getTableName());
        tempVar.setAlias(getTableAlias());
        tempVar.setIsFiscal(getIsFiscalTable());
        tempVar.setDboId(getDboId());
        return tempVar;
    }

    /**
     * 数据库表名
     *
     * <see cref="string"/>
     * <permission cref="protected"></permission>
     */
    protected abstract String getTableName();

    /**
     * 数据库表别名
     *
     * <see cref="string"/>
     * <permission cref="protected"></permission>
     */
    protected abstract String getTableAlias();

    protected abstract boolean getIsFiscalTable();

    protected abstract String getDboId();

    public boolean getUseDataCache() {
        return false;
    }

    public String getCacheConfigID() {
        return "";
    }

    /**
     * 节点编号
     *
     * <see cref="string"/>
     */
    public abstract String getNodeCode();

    public abstract MainEntityAssembler getParentAssembler();

    public final void initTableAlias(java.util.ArrayList<String> tableAlias) {
        tableAlias.add(getTableAlias());
        if (getParentAssembler() != null) {
            getParentAssembler().initTableAlias(tableAlias);
        }
    }

}
