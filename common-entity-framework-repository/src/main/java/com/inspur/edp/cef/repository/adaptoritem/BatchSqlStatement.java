package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.DbTypeConverter;
import com.inspur.edp.cef.api.repository.GspDbType;
import com.inspur.edp.cef.repository.adaptoritem.jdbcprocessor.*;
import io.iec.edp.caf.boot.context.CAFContext;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * 批量执行的Sql信息，记录当前的PrepareStatement及batchSize
 */
public class BatchSqlStatement {
    /**
     * 应合理设置批量大小（Oracle建议在50-100间），避免内存中累积大量参数化变量
     * https://docs.oracle.com/cd/E11882_01/java.112/e16548/oraperf.htm#JJDBC28754
     */
    private static final Integer BATCH_SIZE = 50;

    private int batchSize;

    private PreparedStatement statement;

    private DbProcessor dbProcessor;

    /**
     * 构造函数
     * @param statement
     */
    public BatchSqlStatement(PreparedStatement statement){
        this.batchSize = 0;
        this.statement = statement;
    }

    /**
     * 返回当前Sql对应的PreparedStatement
     * @return
     */
    public PreparedStatement getStatement() {
        return statement;
    }

    /**
     * 返回当前已添加的批次数
     * @return
     */
    public int getBatchSize() {
        return batchSize;
    }
    /**
     * 添加
     * @return
     */
    public void addBatch(int num) {
        this.batchSize += num;
    }

    public void addBatch(List<DbParameter> pars, int num) throws SQLException {
        PreparedStatement stmt = this.getStatement();
        //处理变量
        if (pars != null && !pars.isEmpty()) {
            DbProcessor dbProc = getDBProc();
            for (int i = 0; i < pars.size(); ++i) {
                //参数化的下标从1开始
                dbProc.setPar(stmt, i+1, pars.get(i));
            }
        }
        //加入batch
        stmt.addBatch();
        this.batchSize += num;

        // 每隔batchSize条执行批处理
        if (this.getBatchSize() % BATCH_SIZE == 0) {
            // 执行批处理并重置size
            this.executeBatch();
        }
    }

    public int[] executeBatch() throws SQLException {
        int[] affectCount = null;
        if (this.batchSize > 0) {
            PreparedStatement stmt = this.getStatement();
            // 执行批处理
            affectCount = stmt.executeBatch();
            // 清理批处理数据
            stmt.clearBatch();
            // 重置size
            this.resetBatchSize();
        }
        return affectCount;
    }

    public void close() throws SQLException {
        if(this.statement!=null) {
            this.statement.close();
        }
    }

    /**
     * 重置批次数
     * @return
     */
    public void resetBatchSize(){
        this.batchSize = 0;
    }

    private DbProcessor getDBProc() {
        if (dbProcessor == null) {
            GspDbType gspDbType = DbTypeConverter.transGspDbType(CAFContext.current.getDbType());
            dbProcessor = DbJdbcProcessorFactory.getDbProcessor(gspDbType);
        }
        return dbProcessor;
    }
}
