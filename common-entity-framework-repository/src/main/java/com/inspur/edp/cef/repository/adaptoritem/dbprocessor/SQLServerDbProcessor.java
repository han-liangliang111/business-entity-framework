/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem.dbprocessor;

import com.inspur.edp.cef.api.repository.DbParameter;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.StandardBasicTypes;

import javax.persistence.Query;

public class SQLServerDbProcessor extends DbProcessor {

    @Override
    protected void buildNClobParameter(Query query, int i, DbParameter param) {
        if (CAFContext.current.getDbType() == DbType.SQLServer) {
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.NTEXT, param.getValue()));
        } else {
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
        }
    }

    @Override
    protected void buildNVarcharParmeter(Query query, int i, DbParameter param) {
        if (CAFContext.current.getDbType() == DbType.SQLServer) {//其余的驱动可能没有实现对应参数绑定方法
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.NSTRING, param.getValue()));
        } else {
            buildParameter(query, i, param);
        }
    }

}
