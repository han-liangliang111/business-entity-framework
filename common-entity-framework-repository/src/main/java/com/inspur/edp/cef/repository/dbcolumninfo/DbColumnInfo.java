/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo;

import com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor.*;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.*;
import com.inspur.edp.cef.spi.repository.BaseDbColumnInfo;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptor.KeyWordsManager;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;

public class DbColumnInfo extends BaseDbColumnInfo {
    private AbstractRepoPrcessor repoPrcessor;
    //public Func<FilterCondition, IGSPDatabase, object> typetransprocesser { get; set; }

    public DbColumnInfo() {
    }

    DbColumnInfo(String columnName, String dbColumnName, GspDbDataType columnType, int length, int precision, boolean isPrimaryKey, boolean isAssociateRefElement, boolean isMultiLang, boolean isParentId, boolean isUdtElement
            , boolean isAssociation, boolean isEnum, String belongElementLabel, ITypeTransProcesser typeTransProcesser) {
        super(columnName, dbColumnName, columnType, length, precision, isPrimaryKey, isAssociateRefElement, isMultiLang, isParentId, isUdtElement, isAssociation, isEnum, belongElementLabel, typeTransProcesser);
    }

    /**
     * 处理Oracle字段超长
     *
     * @param index
     * @return
     */
    public final String getAliasName(int index) {
        if (columnName.length() >= 30) {
            return columnName.substring(0, 26) + index;
        } else {
            return KeyWordsManager.getColumnAlias(columnName);
        }
    }

    public final Object clone() {
        DbColumnInfo columnInfo = new DbColumnInfo();
        columnInfo.columnName = columnName;
        columnInfo.dbColumnName = dbColumnName;
        columnInfo.columnType = columnType;
        columnInfo.length = length;
        columnInfo.precision = precision;
        columnInfo.defaultValue = defaultValue;
        columnInfo.isPrimaryKey = isPrimaryKey;
        columnInfo.isAssociateRefElement = isAssociateRefElement;
        columnInfo.isMultiLang = isMultiLang;
        columnInfo.isParentId = isParentId;
        columnInfo.isUdtElement = isUdtElement;
        columnInfo.isAssociation = isAssociation;
        columnInfo.isEnum = isEnum;
        columnInfo.belongElementLabel = belongElementLabel;
        columnInfo.typeTransProcesser = typeTransProcesser;
        columnInfo.isRef = isRef;
        columnInfo.isPersistent = isPersistent;
        return columnInfo;
    }


    private AbstractRepoPrcessor getRepoPrcessor() {
        if (repoPrcessor != null)
            return repoPrcessor;
        if (dataTypePropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo)
            repoPrcessor = new SimpleAssoUdtRepoProcessor();
        else if (dataTypePropertyInfo.getObjectInfo() instanceof SimpleEnumUdtPropertyInfo)
            repoPrcessor = new SimpleEnumUdtRepoProcessor();
        else if (dataTypePropertyInfo.getObjectInfo() instanceof ComplexUdtPropertyInfo)
            repoPrcessor = new ComplexUdtRepoProcessor();
        else if (dataTypePropertyInfo.getObjectInfo() instanceof SimpleUdtPropertyInfo)
            repoPrcessor = new SimpleUdtRepoProcessor();
        else {
            if (dataTypePropertyInfo.getObjectType() == ObjectType.Association)
                repoPrcessor = new AssociationRepoProcessor();
            else if (dataTypePropertyInfo.getObjectType() == ObjectType.Enum)
                repoPrcessor = new EnumRepoDbProcessor();
            else {
                switch (dataTypePropertyInfo.getFieldType()) {
                    case String:
                        repoPrcessor = new StringRepoProcessor();
                        break;
                    case Text:
                        repoPrcessor = new ClobRepoProcessor();
                        break;
                    case Integer:
                        repoPrcessor = new IntRepoProcessor();
                        break;
                    case Decimal:
                        repoPrcessor = new BigDecimalRepoProcessor();
                        break;
                    case Boolean:
                        repoPrcessor = new BooleanRepoProccessor();
                        break;
                    case Date:
                    case DateTime:
                        repoPrcessor = new DateTimeRepoProcessor();
                        break;
                    case Binary:
                        repoPrcessor = new BlobRepoProcessor();
                        break;
                    case Udt:
                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5056);
                }
            }
        }
        return repoPrcessor;
    }

    public final Object readProperty(ICefReader reader, BaseAdaptorItem baseAdaptorItem,
                                     DbProcessor dbProcessor) {
        Object obj = null;
        try {
            obj = getRepoPrcessor().readProperty(this, dbProcessor, baseAdaptorItem, reader);
        } catch (Exception ex) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5054, ex, this.getColumnName());
        }
        return obj;
    }

    /**
     * 对指定值进行转换
     * 1.只对关联和udt进行了特殊处理
     * 2.关联返回IValueContainer.getValue
     * 3.udt返回null,实际调用的时候，udt会单独处理，不会走到这个方法
     * 4.因为其余普通字段都是直接返回，所以一般调用此方法就是为了处理：关联， 其余类型要在调用此方法之前调用transType,
     * 其中关联类型在transType或者其他方法的时候，没有转换成实际的字段数据库类型值。是否可以优化？？？
     * @param value
     * @return
     */
    public final Object getPropertyChangeValue(Object value) {
        return getRepoPrcessor().getPropertyChangeValue(this, value);
    }

    public final Object getPersistenceValue(Object value) {
        return getRepoPrcessor().getPersistenceValue(this, value);
    }

    public DataTypePropertyInfo getDataTypePropertyInfo() {
        return dataTypePropertyInfo;
    }

    public void setDataTypePropertyInfo(
            DataTypePropertyInfo dataTypePropertyInfo) {
        this.dataTypePropertyInfo = dataTypePropertyInfo;
    }
}
