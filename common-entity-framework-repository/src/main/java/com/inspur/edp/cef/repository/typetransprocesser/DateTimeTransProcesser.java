/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.repository.utils.FilterUtil;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.BefDateSerUtil;

import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeTransProcesser implements ITypeTransProcesser {
    private static DateTimeTransProcesser instance;

    private DateTimeTransProcesser() {

    }

    public static DateTimeTransProcesser getInstacne() {
        if (instance == null) {
            instance = new DateTimeTransProcesser();
        }
        return instance;
    }

    public final Object transType(FilterCondition filter, Connection db) {
        try {
            return BefDateSerUtil.getInstance().readDateTime(filter.getValue());

//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//			Date dateVal = sdf.parse(filter.getValue());
//			return filter.getValue();
        } catch (RuntimeException e) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5044, e);
        }
//		catch (ParseException e) {
//			e.printStackTrace();
//			throw new RuntimeException("时间格式转换错误", e);
//		}
    }

    public final Object transType(Object value) {
        if (value == null) {
//			return new Date(0);
            return null;
        }

        java.util.Date date = (java.util.Date) value;
        if (date == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5045);
        }
//		if (date.compareTo(new Date(0)) <= 0) {
//			return null;
//		}
        return FilterUtil.transDateTime(date);
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        return transType(value);
    }
}
