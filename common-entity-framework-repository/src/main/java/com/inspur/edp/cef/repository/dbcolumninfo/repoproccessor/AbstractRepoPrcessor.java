/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.repository.IAdaptorItem;

public abstract class AbstractRepoPrcessor {

    public AbstractRepoPrcessor() {
    }

    /**
     * 从数据库返回值中，读取指定字段值，并转换为IEntityData中对应字段的值类型
     * @param dbColumnInfo
     * @param dbProcessor
     * @param adaptorItem
     * @param reader
     * @return
     */
    public abstract Object readProperty(DbColumnInfo dbColumnInfo, DbProcessor dbProcessor,
                                        IAdaptorItem adaptorItem, ICefReader reader);

    /**
     * 对指定值进行转换
     * 1.只对关联和udt进行了特殊处理
     * 2.关联返回IValueContainer.getValue
     * 3.udt返回null,实际调用的时候，udt会单独处理，不会走到这个方法
     * 4.因为其余普通字段都是直接返回，所以一般调用此方法就是为了处理：关联， 其余类型要在调用此方法之前调用transType,
     * 其中关联类型在transType或者其他方法的时候，没有转换成实际的字段数据库类型值。是否可以优化？？？
     * @param dbColumnInfo
     * @param value
     * @return
     */
    public Object getPropertyChangeValue(DbColumnInfo dbColumnInfo, Object value){
        return value;
    }

    public abstract Object getPersistenceValue(DbColumnInfo dbColumnInfo, Object value);
}
