/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.UQConstraintMediate;
import com.inspur.edp.cef.entity.condition.*;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.BefDateSerUtil;
import com.inspur.edp.udt.entity.IUdtData;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import java.util.*;

public class AdapterUQChecker {

    private boolean throwException;
    private EntityRelationalAdaptor adaptor;

    public AdapterUQChecker(EntityRelationalAdaptor adaptor, boolean throwException) {
        this(adaptor);
        this.throwException = throwException;
    }

    public AdapterUQChecker(EntityRelationalAdaptor adaptor) {
        this.adaptor = adaptor;
    }


    public final void checkUniqueness(ArrayList<UQConstraintMediate> mediates) {
        for (UQConstraintMediate mediate : mediates) {
            checkMediateUniqueness(mediate);
        }
    }

    private void checkMediateUniqueness(UQConstraintMediate mediate) {
        checkUniquenessFromDb(mediate);
    }

    private void checkUniquenessInMemory(UQConstraintMediate mediate) {
        if (mediate == null || mediate.getParametersInfo() == null || mediate.getParametersInfo().size() < 2)
            return;
        ArrayList<String> keys = new ArrayList<>(mediate.getParametersInfo().keySet());
        for (int i = 0; i < keys.size() - 1; i++) {
            for (int j = i + 1; j < keys.size(); j++) {
                checkMediateItemUQ(mediate, mediate.getParametersInfo().get(keys.get(i)), mediate.getParametersInfo().get(keys.get(j)));
            }
        }
    }

    private void checkMediateItemUQ(UQConstraintMediate mediate, HashMap<String, Object> source, HashMap<String, Object> target) {
        if (source == null || target == null)
            return;
        if (isQqueals(source, target) == false)
            return;
        throwUQException(mediate);
    }

    private void throwUQException(UQConstraintMediate mediate) {
        String message = "";
        if (throwException) {
            if (mediate.getMessage() == null || "".equals(mediate.getMessage())) {
                message = I18nResourceUtil
                        .getResourceItemValue("pfcommon", "cef_exception.properties",
                                "Gsp_Cef_UQVal_0001");

            } else {
                message = mediate.getMessage();

            }
            throw new CefRepositoryException("0001", message, null, ExceptionLevel.Warning);
        } else {
            mediate.setCheckFailed(true);
        }
    }

    private boolean isQqueals(HashMap<String, Object> source, HashMap<String, Object> target) {
        for (Map.Entry<String, Object> item : source.entrySet()) {
            if (target.containsKey(item.getKey()) == false)
                return false;

            Object targetValue = target.get(item.getKey());
            if (item.getValue() == null) {
                if (targetValue != null)
                    return false;
            } else {
                if (targetValue == null)
                    return false;
                if (item.getValue().equals(targetValue) == false)
                    return false;
            }
        }
        return true;
    }

    private void checkUniquenessFromDb(UQConstraintMediate mediate) {
        //判断是否有数据，需要检查
        if(mediate.getParametersInfo() == null || mediate.getParametersInfo().size() == 0){
            return;
        }
        ArrayList<EntityFilter> entityFilters = getUniqueConstraintCheckEntityFilters(mediate);

        FieldsFilter fieldsFilter = getFilterFields(mediate);
        for (EntityFilter entityFilter : entityFilters) {
            entityFilter.setFieldsFilter(fieldsFilter);
            List<IEntityData> datas = adaptor.query(entityFilter, null);
            mediate.getDuplicateDbDatas().addAll(datas);
            if(datas != null && datas.size() > 0){
                throwUQException(mediate);
            }
        }
    }

    private FieldsFilter getFilterFields(UQConstraintMediate mediate){
        FieldsFilter fieldsFilter = new FieldsFilter();
        fieldsFilter.setUseFieldsCondition(true);
        Iterator<Map.Entry<String, HashMap<String, Object>>> iterator = mediate.getParametersInfo().entrySet().iterator();
        HashMap<String, Object> value = iterator.next().getValue();
        //主键必选，防止只有单个字段，数据结果集转换为数组的时候报错
        fieldsFilter.getFilterFields().add(adaptor.getContainColumns().getPrimaryKey().getColumnName());
        for(String field : value.keySet()){
            fieldsFilter.getFilterFields().add(field);
        }
        return fieldsFilter;
    }

    /**
     * 构造要排除的ID条件 notInCondition
     * @param exceptIds 要排除的数据集合
     * @return
     */
    private ArrayList<FilterCondition> getUQExceptCondition(List<String> exceptIds) {
        ArrayList<FilterCondition> filterConditions = new ArrayList<>();
        //注意这个地方的分批次执行没有意义了，前面的条件批次是200，进来的数据不会超过200
        //每次批量执行个数
        int batchSize = 900;
        //批次数
        int batchCount = 0;
        int count = exceptIds.size() % batchSize;
        batchCount = count == 0 ? (exceptIds.size() / batchSize) : (exceptIds.size() / batchSize + 1);
        for (int i = 0; i < batchCount; i++) {
            int startIndex = batchSize * i;
            int endIndex = (i + 1) * batchSize - 1;
            if (endIndex >= exceptIds.size()) {
                endIndex = exceptIds.size() - 1;
            }
            List<String> notInValues = new ArrayList<>();
            for (int index = startIndex; index <= endIndex; index++) {
                notInValues.add(exceptIds.get(index));
            }
            FilterCondition filterCondition = new FilterCondition();
            filterCondition.setFilterField(adaptor.getContainColumns().getPrimaryKey().getColumnName());
            filterCondition.setCompare(ExpressCompareType.NotIn);
            filterCondition.setInValues(notInValues);
            filterCondition.setRelation(ExpressRelationType.And);
            filterConditions.add(filterCondition);
        }
        //将最后一个条件的关系符设为空
        filterConditions.get(filterConditions.size() - 1).setRelation(ExpressRelationType.Empty);
        return filterConditions;
    }


    /**
     * 剔除修改和删除的数据(比较的时候，已存在数据库中的数据不参与比较查询)
     * @param mediate
     * @return
     */
    private List<String> getListExceptIds(UQConstraintMediate mediate) {
        List<String> exceptIds = new ArrayList<>();
        exceptIds.addAll(mediate.getExceptDeleteIds());
        exceptIds.addAll(mediate.getExceptModifyIds());
        //剔除重复元素
        HashSet h = new HashSet(exceptIds);
        exceptIds.clear();
        exceptIds.addAll(h);
        return exceptIds;
    }


    /**
     * 获取当前唯一性约束对应的过滤条件 （a=1 and b=2) or (a=2 and b=3)的格式
     * @param mediate
     * @return
     */
    private ArrayList<EntityFilter> getUniqueConstraintCheckEntityFilters(UQConstraintMediate mediate) {
        ArrayList<EntityFilter> entityFilters = new ArrayList<>();
        ArrayList<FilterCondition> filterConditions = new ArrayList<>();
        List<String> listDataIds = new ArrayList<>();
        //要排除的数据ID集合（删除的、修改的数据ID）,每次查询都要全量的排除修改和删除的数据，否则判断会不正确
        List<String> listExceptIds = getListExceptIds(mediate);
        int batchCount = 200;
        //用来记录当前批次的处理位置
        int batchIndex = 1;
        Iterator<Map.Entry<String, HashMap<String, Object>>> iterator = mediate.getParametersInfo().entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, HashMap<String, Object>> item = iterator.next();
            listDataIds.add(item.getKey());
            //key为字段编号， value为字段值
            HashMap<String, Object> fieldsInfos = item.getValue();
            Iterator<Map.Entry<String, Object>> fieldIterator = fieldsInfos.entrySet().iterator();
            boolean isFirst = true;
            //构造单条数据的过滤条件
            while (fieldIterator.hasNext()) {
                Map.Entry<String, Object> field = fieldIterator.next();
                FilterCondition filterCondition = new FilterCondition();
                if(isFirst){
                    filterCondition.setlBracketCount(1);
                    isFirst = false;
                }
                filterCondition.setFilterField(field.getKey());
                Object value = null;
                DbColumnInfo columnInfo = adaptor.getContainColumns().getItem(field.getKey());
                if (field.getValue() instanceof IUdtData) {
                    value = field.getValue();
                } else {
                    value = columnInfo.getTypeTransProcesser().transType(field.getValue());
                }
                value = adaptor.getPropertyChangeValue(field.getKey(), field.getValue());
                if (value == null) {
                    filterCondition.setCompare(ExpressCompareType.Is);
                    filterCondition.setValue(null);
                } else {
                    if(columnInfo.getColumnType() == GspDbDataType.DateTime || columnInfo.getColumnType() == GspDbDataType.Date){
                        value = BefDateSerUtil.getInstance().writeDateTime((Date) value);
                    }
                    filterCondition.setCompare(ExpressCompareType.Equal);
                    filterCondition.setValue(value.toString());
                }


                filterConditions.add(filterCondition);
                if (fieldIterator.hasNext()) {
                    filterCondition.setRelation(ExpressRelationType.And);
                }else{
                    filterCondition.setRelation(ExpressRelationType.Or);
                    filterCondition.setRBracketCount(1);
                }
            }
            if (batchIndex == batchCount) {//本批次结束
                buildBatchEntityFilter(entityFilters, filterConditions, listExceptIds);
                batchIndex = 1;
                listDataIds.clear();
                continue;
            }
            if (iterator.hasNext()) {
                filterConditions.get(filterConditions.size() - 1).setRelation(ExpressRelationType.Or);
            } else {
                //最后一个批次
                buildBatchEntityFilter(entityFilters, filterConditions, listExceptIds);
            }
            batchIndex++;
        }
        return entityFilters;
    }

    /**
     * 构造批次过滤条件
     * @param entityFilters
     * @param filterConditions
     */
    private void buildBatchEntityFilter(ArrayList<EntityFilter> entityFilters, ArrayList<FilterCondition> filterConditions, List<String> exceptIds){
        EntityFilter entityFilter = new EntityFilter();
        entityFilter.setFilterConditions(cloneFilterCondition(filterConditions));
        entityFilters.add(entityFilter);
        FilterCondition firstCondition = entityFilter.getFilterConditions().get(0);
        FilterCondition lastCondition = entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1);
        firstCondition.setlBracketCount(firstCondition.getLBracketCount() + 1);
        lastCondition.setRBracketCount(lastCondition.getRBracketCount() + 1);
        filterConditions.clear();
        //构造NotIn条件
        if(exceptIds != null && exceptIds.size() > 0){
            ArrayList<FilterCondition> notInFilterConditions = getUQExceptCondition(exceptIds);
            lastCondition.setRelation(ExpressRelationType.And);
            entityFilter.getFilterConditions().addAll(notInFilterConditions);
        }
        else{
            lastCondition.setRelation(ExpressRelationType.Empty);
        }
    }

    private ArrayList<FilterCondition> cloneFilterCondition(ArrayList<FilterCondition> filterConditions){
        ArrayList<FilterCondition> clonedFilterConditions = new ArrayList<>(filterConditions.size());
        for (FilterCondition filterCondition : filterConditions) {
            if (filterCondition == null) {
                clonedFilterConditions.add(null);
            } else {
                clonedFilterConditions.add(filterCondition.clone());
            }
        }
        return clonedFilterConditions;
    }
}

class SqlInfo {
    private String sql;
    private ArrayList<DbParameter> parameters;

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public ArrayList<DbParameter> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<DbParameter> parameters) {
        this.parameters = parameters;
    }
}
