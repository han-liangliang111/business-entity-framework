/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.entity;


import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;

public class DbTableInfo {

    private String privateDboId;
    private String privateTableName;
    private String privateAlias;
    private boolean privateIsFiscal;

    public final String getDboId() {
        return privateDboId;
    }

    public final void setDboId(String value) {
        privateDboId = value;
    }

    public final String getTableName() {
        return privateTableName;
    }

    public final void setTableName(String value) {
        privateTableName = value;
    }

    public final String getAlias() {
        return privateAlias;
    }

    public final void setAlias(String value) {
        privateAlias = value;
    }

    public final boolean getIsFiscal() {
        return privateIsFiscal;
    }

    public final void setIsFiscal(boolean value) {
        privateIsFiscal = value;
    }

    public final String getTableName(String year) {
        if (getIsFiscal()) {
            DataValidator.checkForEmptyString("Fiscal", year);
            return getTableName() + year;
        }
        return getTableName();
    }
}
