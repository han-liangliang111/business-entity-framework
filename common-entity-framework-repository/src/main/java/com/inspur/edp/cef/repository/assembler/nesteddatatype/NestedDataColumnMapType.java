/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.nesteddatatype;

/**
 * 数据库值存储方式
 */
public enum NestedDataColumnMapType {
    /**
     * 存为一列
     */
    SingleColumn(0),

    /**
     * 存为多列
     */
    MultiColumns(1);

    private static java.util.HashMap<Integer, NestedDataColumnMapType> mappings;
    private int intValue;

    private NestedDataColumnMapType(int value) {
        intValue = value;
        NestedDataColumnMapType.getMappings().put(value, this);
    }

    private synchronized static java.util.HashMap<Integer, NestedDataColumnMapType> getMappings() {
        if (mappings == null) {
            mappings = new java.util.HashMap<Integer, NestedDataColumnMapType>();
        }
        return mappings;
    }

    public static NestedDataColumnMapType forValue(int value) {
        return getMappings().get(value);
    }

    public int getValue() {
        return intValue;
    }
}
