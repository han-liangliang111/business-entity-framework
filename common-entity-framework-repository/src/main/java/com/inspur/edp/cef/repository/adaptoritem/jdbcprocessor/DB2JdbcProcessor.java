package com.inspur.edp.cef.repository.adaptoritem.jdbcprocessor;


import com.inspur.edp.cef.api.repository.DbParameter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * DB2Jdbc处理器
 */
public class DB2JdbcProcessor extends DbProcessor {

    @Override
    protected void buildNClobParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setString(i, null);
        } else {
            stmt.setNString(i, (String) param.getValue());
        }
    }

    @Override
    protected void buildNVarcharParmeter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setNString(i, (String) param.getValue());
    }
}
