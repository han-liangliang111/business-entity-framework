package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.cef.repository.adaptor.EntityRelationalAdaptor;
import com.inspur.edp.cef.repository.assembler.AssociationInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.repository.repo.BaseRootRepository;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.*;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 关联字段映射工具类
 * @author: wangmj
 * @date: 2024/10/17
 **/
public class AssociationMappingUtil {
    /**
     * 初始化关联字段的mapping映射关系
     * @param entityResInfo
     */
    public static void initAssociationDbMappings(
            CefEntityResInfoImpl entityResInfo, List<AssociationInfo> associationInfos) {
        if (entityResInfo.isInitedAssMapping())
            return;
        synchronized (entityResInfo.getAssociationMapping()){
            if (entityResInfo.isInitedAssMapping())
                return;
            for (Map.Entry<String, DataTypePropertyInfo> dataTypePropertyInfo : entityResInfo
                    .getEntityTypeInfo().getPropertyInfos().entrySet()) {
                if (dataTypePropertyInfo.getValue().getObjectInfo() instanceof AssocationPropertyInfo) {
                    if(!entityResInfo.getAssociationMapping().containsKey(dataTypePropertyInfo.getKey())){
                        buildAssociationMapping(dataTypePropertyInfo, associationInfos);
                        entityResInfo.getAssociationMapping().put(dataTypePropertyInfo.getKey(), true);
                    }
                } else if (dataTypePropertyInfo.getValue().getObjectInfo() instanceof SimpleAssoUdtPropertyInfo) {
                    if(!entityResInfo.getAssociationMapping().containsKey(dataTypePropertyInfo.getKey())){
                        buildAssoUdtMapping(dataTypePropertyInfo);
                        entityResInfo.getAssociationMapping().put(dataTypePropertyInfo.getKey(), true);
                    }
                }
            }
            entityResInfo.setInitedAssMapping(true);
        }
    }

    /**
     * 初始化指定关联字段的mapping关系
     * @param entityResInfo
     * @param propertyName
     * @param associationInfos
     */
    public static void initAssociationDbMapping(
            CefEntityResInfoImpl entityResInfo, String propertyName, List<AssociationInfo> associationInfos) {
        if (entityResInfo.isInitedAssMapping())
            return;
        if(entityResInfo.getAssociationMapping().containsKey(propertyName)){
            return;
        }
        for (Map.Entry<String, DataTypePropertyInfo> dataTypePropertyInfo : entityResInfo
                .getEntityTypeInfo().getPropertyInfos().entrySet()) {
            if(!dataTypePropertyInfo.getKey().equals(propertyName)){
                continue;
            }
            if (dataTypePropertyInfo.getValue().getObjectInfo() instanceof AssocationPropertyInfo) {
                buildAssociationMapping(dataTypePropertyInfo, associationInfos);
            } else if (dataTypePropertyInfo.getValue().getObjectInfo() instanceof SimpleAssoUdtPropertyInfo) {
                buildAssoUdtMapping(dataTypePropertyInfo);
            }
            entityResInfo.getAssociationMapping().put(propertyName, true);
        }
    }

    /**
     * 构造关联字段的mapping映射关系
     * @param dataTypePropertyInfo
     */
    private static void buildAssociationMapping(Map.Entry<String, DataTypePropertyInfo> dataTypePropertyInfo, List<AssociationInfo> associationInfos){
        AssocationPropertyInfo associationInfo = (AssocationPropertyInfo) dataTypePropertyInfo
                .getValue().getObjectInfo();

        HashMap<String, String> map = new HashMap<>();
        //处理普通带出字段的映射关系
        List<RefDataTypePropertyInfo> associationRefProperties = new ArrayList<>();
        for (Map.Entry<String, DataTypePropertyInfo> refProperty : associationInfo.getAssociationInfo().getRefPropInfos().entrySet()) {
            RefDataTypePropertyInfo refDataTypePropertyInfo = (RefDataTypePropertyInfo) refProperty
                    .getValue();
            //将关联（普通关联字段和关联UDT字段）放到集合中，后面处理带出字段的映射
            if (refDataTypePropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo || refDataTypePropertyInfo.getObjectInfo() instanceof AssocationPropertyInfo){
                associationRefProperties.add(refDataTypePropertyInfo);
            }
            map.put(refDataTypePropertyInfo.getRefPropertyName(),
                    refDataTypePropertyInfo.getPropertyName());
            //多值udt 是否判断单列和多列？
            if (refDataTypePropertyInfo.getObjectInfo() instanceof ComplexUdtPropertyInfo) {
                for (Map.Entry<String, String> mappingEntry : ((ComplexUdtPropertyInfo) refDataTypePropertyInfo.getObjectInfo()).getPropAndRefMapping().entrySet()) {
                    map.put(mappingEntry.getValue(), dataTypePropertyInfo.getKey() + "_" + mappingEntry.getValue());
                }
            }
        }
        map.put(associationInfo.getAssociationInfo().getPrivateSourceColumn(),
                associationInfo.getAssociationInfo().getPrivateTargetColumn());
        //处理带出字段是关联和关联UDT的场景,有场景带出字段是两一个实体的关联带出普通字段，延迟到后面readProperty再初始化字段mapping
        if(associationRefProperties == null || associationRefProperties.size() == 0){
            associationInfo.setAssDbMapping(map);
            return;
        }
        //如果BE出现循环依赖时，起始的adapter一定不能引用它的关联字段，这会造成真正的循环依赖，所以这个地方的adapter，我们可以利用它的普通字段，
        //不处理它的关联字段
        EntityRelationalAdaptor adapter = ((BaseRootRepository) getAssociation(dataTypePropertyInfo.getValue().getPropertyName(), associationInfos)
                .getRefRepository())
                .getEntityDac(associationInfo.getAssociationInfo().getNodeCode())
                .getEntityAdaptor();
        for(RefDataTypePropertyInfo refDataTypePropertyInfo : associationRefProperties){
            if (refDataTypePropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo) {
                HashMap<String, String> refPropMap = adapter
                        .getAssosPropDBMapping(refDataTypePropertyInfo.getRefPropertyName());
                if (refPropMap != null) {
                    for (Map.Entry<String, String> item : refPropMap.entrySet()) {
                        if (map.containsKey(item.getValue()) == false) {
                            map.put(item.getValue(),
                                    dataTypePropertyInfo.getValue().getPropertyName() + "_" + item
                                            .getValue());
                        }
                    }
                }
            } else if (refDataTypePropertyInfo.getObjectInfo() instanceof AssocationPropertyInfo) {
                HashMap<String, String> refPropMap = adapter
                        .getAssosPropDBMapping(refDataTypePropertyInfo.getRefPropertyName());
                if (refPropMap != null) {
                    for (Map.Entry<String, String> item : refPropMap.entrySet()) {
                        if (map.containsKey(item.getValue()) == false) {
                            map.put(item.getValue(),
                                    dataTypePropertyInfo.getValue().getPropertyName() + "_" + item
                                            .getValue());
                        }
                    }
                }
            }
        }
        //用来设置关联的mapping关系，取数会用到这个
        associationInfo.setAssDbMapping(map);
    }

    private static AssociationInfo getAssociation(String propertyName, List<AssociationInfo> associationInfos) {
        for (AssociationInfo item : associationInfos) {
            if (propertyName.equals(item.getSourceColumn())) {
                return item;
            }
        }
        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5026, false, "", propertyName);
    }


    /**
     * 构造关联UDT的mapping映射关系
     * @param dataTypePropertyInfo
     */
    private static void buildAssoUdtMapping(Map.Entry<String, DataTypePropertyInfo> dataTypePropertyInfo){
        SimpleAssoUdtPropertyInfo simpleAssoUdtPropertyInfo = (SimpleAssoUdtPropertyInfo) dataTypePropertyInfo.getValue().getObjectInfo();
        AssocationPropertyInfo associationInfo = simpleAssoUdtPropertyInfo.getAssoInfo();

        HashMap<String, String> map = new HashMap<>();

        for (Map.Entry<String, DataTypePropertyInfo> refProperty : associationInfo
                .getAssociationInfo()
                .getRefPropInfos().entrySet()) {
            RefDataTypePropertyInfo refDataTypePropertyInfo = (RefDataTypePropertyInfo) refProperty
                    .getValue();
            if (refDataTypePropertyInfo.getObjectInfo() instanceof ComplexUdtPropertyInfo) {
                for (Map.Entry<String, String> mappingEntry : ((ComplexUdtPropertyInfo) refDataTypePropertyInfo.getObjectInfo()).getPropAndRefMapping().entrySet()) {
                    map.put(simpleAssoUdtPropertyInfo.getUdtConfigId().substring(simpleAssoUdtPropertyInfo.getUdtConfigId().lastIndexOf(".") + 1) + "_" + mappingEntry.getValue(), dataTypePropertyInfo.getKey() + "_" + mappingEntry.getValue());
                }
            }
        }
        map.putAll(simpleAssoUdtPropertyInfo.getMapping());
        associationInfo.setAssDbMapping(map);
    }
}
