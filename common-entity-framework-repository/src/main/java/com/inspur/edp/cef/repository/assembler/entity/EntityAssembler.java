/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.entity;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.entity.condition.SortCondition;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.repository.assembler.DataTypeAssembler;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public abstract class EntityAssembler extends DataTypeAssembler {
    private java.util.ArrayList<ColumnInfo> containColumns;

    public EntityAssembler() {
        super();
        init();
    }

    public java.util.ArrayList<ColumnInfo> getContainColumns() {
        if (containColumns == null)
            containColumns = new ArrayList<>();
        return containColumns;
    }

    private void init() {
        initColumnInfo();
    }

    /**
     * 初始化列信息，使用<see cref="addColumn"/>AddColumn方法添加列信息
     * <p>
     * 使用<see cref="addColumn"/>方法添加列信息
     */
    public abstract void initColumnInfo();

    /**
     * 获取be元数据上设置的默认过滤条件
     *
     * @return 过滤条件实体集合
     * <see cref="List{T}" />
     * <see cref="FilterCondition" />
     *
     * <permission cref="protected"></permission>
     */
    protected abstract java.util.ArrayList<FilterCondition> getDefaultFilterCondition();

    public final java.util.ArrayList<FilterCondition> getDefaultFilter() {
        return getDefaultFilterCondition();
    }

    /**
     * 获取be元数据上设置的排序
     *
     * @return 排序信息实体集合
     * <see cref="List{T}" />
     * <see cref="SortCondition" />
     *
     * <permission cref="protected"></permission>
     */
    protected abstract java.util.ArrayList<SortCondition> getDefaultSortCondition();

    public final java.util.ArrayList<SortCondition> GetDefaultSort() {
        return getDefaultSortCondition();
    }

    /**
     * 导入数据，将数据库查询结果，写入到be实例中
     *
     * @param entityDefinition be实例
     * @param resultSet        数据库查询结果
     *                         将数据库查询结果，写入到be实例中
     *                         <permission cref="protected"></permission>
     *                         <permission cref="internal"></permission>
     */
    protected void importData(IEntityData entityDefinition, ResultSet resultSet) {
    }


    ///#region 初始化：工具方法

    /**
     * 添加列
     *
     * @param columnName            列名称
     * @param dboColumnId           dbo字段ID
     * @param isPrimaryKey          是否主键
     * @param isAssociateRefElement 是否关联引用字段
     * @param isMultiLang           是否多语字段
     * @param isEnum                是否枚举
     * @param belongElementLabel    所属字段标签
     * @param isParentId            是否父节点ID字段
     * @param typeTransProcesser    转换方法
     * @param isUdt                 是否业务字段
     * @param isAssociation         是否关联
     */
    protected final void addColumn(String columnName, String dboColumnId, ITypeTransProcesser typeTransProcesser, boolean isPrimaryKey, boolean isMultiLang, boolean isUdt, boolean isAssociation, boolean isEnum, String belongElementLabel, boolean isParentId) {
        ColumnInfo tempVar = new ColumnInfo();
        tempVar.setColumnName(columnName);
        tempVar.setDboColumnId(dboColumnId);
        tempVar.setIsPrimaryKey(isPrimaryKey);
        tempVar.setIsMultiLang(isMultiLang);
        tempVar.setIsParentId(isParentId);
        tempVar.setIsUdtElement(isUdt);
        tempVar.setIsAssociation(isAssociation);
        tempVar.setIsEnum(isEnum);
        tempVar.setBelongElementLabel(belongElementLabel);
        tempVar.setTypeTransProcesser(typeTransProcesser);
        ColumnInfo columnInfo = tempVar;
        //DbColumnName = isAssociateRefElement ? string.Empty : dbColumnName,
        //ColumnType = columnType,
        //Length = length,
        //Precision = precision,
        //DefaultValue = defaultValue,
        getContainColumns().add(columnInfo);
    }


    ///#endregion

    /**
     * 获取变更集中属性值（对于关联字段的处理）
     *
     * @param propertyName  属性名称
     * @param propertyValue 属性值
     * @return
     */
    protected Object getPropertyChangeValue(String propertyName, Object propertyValue) {
        return null;
    }

    public List<String> getAdapterExtendClasses() {
        return null;
    }
}
