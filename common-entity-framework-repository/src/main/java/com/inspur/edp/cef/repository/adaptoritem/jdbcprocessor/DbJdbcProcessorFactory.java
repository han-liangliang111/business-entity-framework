package com.inspur.edp.cef.repository.adaptoritem.jdbcprocessor;

import com.inspur.edp.cef.api.repository.GspDbType;

/**
 * Jdbc数据库处理器工厂
 */
public class DbJdbcProcessorFactory {
    public static DbProcessor getDbProcessor(GspDbType dbType) {
        DbProcessor dbProcessor = null;
        switch (dbType) {
            case Oracle:
                dbProcessor = new OraJdbcProcessor();
                break;
            case SQLServer:
                dbProcessor = new SQLServerJdbcProcessor();
                break;
            case DB2:
                dbProcessor = new DB2JdbcProcessor();
                break;
            case DM:
                dbProcessor = new DMJdbcProcessor();
                break;
            case Oscar:
                dbProcessor = new OscarJdbcProcessor();
                break;
            case Kingbase:
                dbProcessor = new KingbaseJdbcProcessor();
                break;
            default:
                dbProcessor = new DbProcessor();
                break;
        }
        return dbProcessor;
    }
}
