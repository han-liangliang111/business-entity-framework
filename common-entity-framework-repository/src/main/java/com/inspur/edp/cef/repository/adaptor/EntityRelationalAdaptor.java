/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.api.CefRtBeanUtil;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.authority.AuthFilter;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.authority.AuthorityInfoType;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.api.repository.RefColumnInfo;
import com.inspur.edp.cef.api.repository.TableNameRefContext;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.core.i18n.I18nResourceUtil;
import com.inspur.edp.cef.entity.UQConstraintMediate;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.*;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.entity.repository.AssoVariable;
import com.inspur.edp.cef.entity.repository.DataSaveParameter;
import com.inspur.edp.cef.repository.adaptoritem.BatchSqlStatement;
import com.inspur.edp.cef.repository.assembler.AbstractDataAdapterExtendInfo;
import com.inspur.edp.cef.repository.assembler.AssoCondition;
import com.inspur.edp.cef.repository.assembler.AssociationInfo;
import com.inspur.edp.cef.repository.dac.DacSaveContext;
import com.inspur.edp.cef.repository.dac.EntityDac;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.CefRunTimeException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.repository.extend.RepoExtendManager;
import com.inspur.edp.cef.repository.filter.FilterContext;
import com.inspur.edp.cef.repository.readerwriter.CefDataReader;
import com.inspur.edp.cef.repository.repo.BaseRootRepository;
import com.inspur.edp.cef.repository.utils.AssociationMappingUtil;
import com.inspur.edp.cef.repository.utils.DatabaseUtil;
import com.inspur.edp.cef.repository.utils.FilterUtil;
import com.inspur.edp.cef.repository.utils.RepositoryUtil;
import com.inspur.edp.cef.spi.entity.DeleteCheckState;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.*;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.databaseobject.api.entity.DatabaseObjectType;
import io.iec.edp.caf.databaseobject.api.entity.TempTableContext;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import lombok.var;
import org.hibernate.jpa.TypedParameterValue;
import org.hibernate.type.StandardBasicTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 据说解析型从此类继承
 */
public abstract class EntityRelationalAdaptor extends BaseEntityAdaptor {
    protected static Logger logger = LoggerFactory.getLogger(EntityRelationalAdaptor.class);
    private final Object netstRepositoryLock = new Object();
    protected EntityManager entityManager;
    /**
     * 关联部分的表名
     */
    protected String joinTableName = "";
    protected boolean batchInsert = false;
    protected String authoritySql = " JOIN (%1$s) %2$s ON %3$s = %4$s";
    protected String authorityTableAlias = "authorityTable";
    private String versionControlPropName;
    protected String GetDataWithParentJoinSql = "select %1$s from %2$s %3$s %4$s";
    private DbColumnInfoCollection containColumns;
    protected DbColumnInfoCollection extendContainColumns;
    private String queryFields;

    /**
     * 包含了当前节点表名、关联表的SQL片段
     */
    protected String queryTableName;
    //缓存dbo表名
    private String dboTableName;
    private Boolean hasMultiLangCol;
    private volatile HashMap<String, INestedRepository> nestedRepositories = new HashMap<>();
    protected HashMap<String, Integer> filteredPropDbIndexMapping = null;
    protected DbColumnInfoCollection multiLanguageColumnInfos;
    //<Key:国际化后缀别名 Name_CHS,Value:字段标签Name>
    protected Map<String, MultiLangColumnInfo> multiLanguageAlias;
    protected boolean isMultiData = false;
    protected Map<Integer, String> insertMultiIndexMap = new HashMap<>();

    protected EntityRelationalAdaptor() {
        this(true);
    }

    /**
     * @param init 解析传false,生成:true
     */
    protected EntityRelationalAdaptor(boolean init) {
        super(init);
        containColumns = new DbColumnInfoCollection();
        if (init) {
            initColumns();
        }
        this.entityManager = CefBeanUtil.getEntityManager();
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    protected abstract String getDboID();

    protected String getNodeCode() {
        return null;
    }

    //region VersionControl
    protected String getVersionControlPropName() {
        return versionControlPropName;
    }

    public HashMap<String, Date> getVersionControlValues(List<String> dataIds) {
        String propName = getVersionControlPropName();
        if (io.iec.edp.caf.commons.utils.StringUtils.isEmpty(propName)) {
            return new HashMap<>();
        }
        String sql = String.format(getGetDataByIdsSql(), getContainColumns().getPrimaryKey().getDbColumnName() + "," + propName, getTableName(), "%1$s");
        sql = formatFiscalAndMultiLang(sql);
        int batchCount = 5000;
        HashMap<String, Date> dic = new HashMap<>();
        if (dataIds != null && dataIds.size() > batchCount) {
            int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
            for (int i = 1; i <= times; i++) {
                int endIndex = i == times ? dataIds.size() : batchCount * i;
                List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);
                String sqlformat = String.format(sql, getInFilter(tempList));
                HashMap<String, Date> tempDic = convertVersionControlValues(sqlformat, tempList, null);
                dic.putAll(tempDic);
            }
        } else {
            String sqlformat = String.format(sql, getInFilter(dataIds));
            HashMap<String, Date> tempDic = convertVersionControlValues(sqlformat, dataIds, null);
            dic.putAll(tempDic);
        }

        return dic;
    }

    public Date getVersionControlValue(String dataId) {
        if (getVersionControlPropName() == null || getVersionControlPropName().equals("")) {
            return new Date(0);
        }
        String sql = String.format(getGetDataByIdSql(), getVersionControlPropName(), getTableName(), "?0 ");
        try {
            List<DbParameter> list = new ArrayList<DbParameter>();
            list.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), dataId));
            Date value = convertVersionControlValue(sql, list, dataId);
            return value;
        } catch (Exception ex) {
            throw new CefRepositoryException("cef_exception.properties", "Gsp_Cef_Retrieve_0001", new String[]{ex.getMessage()}, ex, ExceptionLevel.Info, true);
        }
    }

    private HashMap<String, Date> convertVersionControlValues(String sql, List<String> dataIds, List<DbParameter> parameters) {
        Query query = buildQueryManager(sql, parameters);
        HashMap<String, Date> dic = new HashMap<String, Date>();
        List<Object[]> resultSet = query.getResultList();
        for (String dataId : dataIds) {
            List<Object[]> objs = resultSet.stream().filter(item -> item[0].equals(dataId)).collect(Collectors.toList());
            if (objs != null && objs.size() > 0) {
                var instance = new Date(0);
                if (objs.get(0)[1] != null) {
                    instance = (Date) objs.get(0)[1];
                }
                dic.put(dataId, instance);
            } else {
                dic.put(dataId, null);
            }
        }
        return dic;
    }

    private Date convertVersionControlValue(String sql, List<DbParameter> parameters, String dataId) {
        Query query = buildQueryManager(sql, parameters);
        List resultSet = query.getResultList();
        if (resultSet == null || resultSet.isEmpty()) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5006, false, dataId);
        } else {
            Object result = resultSet.get(0);
            return result == null ? new Date(0) : (Date) result;
        }
    }


    @Override
    public List<IEntityData> query(EntityFilter filter, ArrayList<AuthorityInfo> authorities) {
        if (filter == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5029);
        }
        EntityFilter cloneFilter = filter.clone();
        buildLogicDeleteCondition(cloneFilter);
//        //分页信息需要返回，且产品部有的地方会提前记录这个变量，此处还是用原来的分页对象
        if(filter.getPagination() != null){
            cloneFilter.setPagination(filter.getPagination());
        }
        List<IEntityData> result;
        if (cloneFilter.getIsUsePagination()) {
            result = queryWithPagination(cloneFilter, authorities);
        } else {
            result = queryWithoutPagination(cloneFilter, authorities);
        }
        return result;
    }

    @Override
    public void setDataAdapterExtendInfos(ArrayList<AbstractDataAdapterExtendInfo> infos) {
        super.setDataAdapterExtendInfos(infos);
        extendContainColumns = new DbColumnInfoCollection();
        for (AbstractDataAdapterExtendInfo info : infos) {
            if (info.getDbColumnInfos() == null)
                continue;
            extendContainColumns.addRange(info.getDbColumnInfos());
        }
    }

    //region TODO 应该不需要生成
    protected abstract boolean hasPropColumnMappping();

    protected abstract HashMap<String, Integer> getPropertyColumnMapping();

    protected abstract void setPropertyColumnMapping(HashMap<String, Integer> mapping);

    //针对解析的，设置关联Mapping
    public HashMap<String, String> getAssosPropDBMapping(String propName) {
        if(!getEntityResInfo().isInitedAssMapping()){
            AssociationMappingUtil.initAssociationDbMapping(getEntityResInfo(), propName, getAssociationInfos());
        }
        DbColumnInfo dbColumnInfo = this.getContainColumns().getItem(propName);
        DataTypePropertyInfo dataTypePropertyInfo = null;
        if (dbColumnInfo == null || dbColumnInfo.getDataTypePropertyInfo() == null) {
            dataTypePropertyInfo = this.getEntityResInfo().getEntityTypeInfo().getPropertyInfo(propName);
        } else
            dataTypePropertyInfo = dbColumnInfo.getDataTypePropertyInfo();
        if (dataTypePropertyInfo.getObjectInfo() instanceof ComplexUdtPropertyInfo) {
            return ((ComplexUdtPropertyInfo) dataTypePropertyInfo.getObjectInfo()).getPropAndRefMapping();
        }
        if (dataTypePropertyInfo.getObjectInfo() instanceof AssocationPropertyInfo) {
            return ((AssocationPropertyInfo) dataTypePropertyInfo.getObjectInfo()).getAssDbMapping();
        }
        if (dataTypePropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo)
            return ((SimpleAssoUdtPropertyInfo) dataTypePropertyInfo.getObjectInfo()).getAssoInfo().getAssDbMapping();
        return new HashMap<>();
    }

    /**
     * 全部查询
     * @param filter 过滤条件
     * @param authorities 权限信息
     * @return
     */
    private List<IEntityData> queryWithoutPagination(EntityFilter filter, ArrayList<AuthorityInfo> authorities) {
        String tableName = getQueryTableNameWithAuthority(authorities, filter);
        StringBuilder sql = new StringBuilder();

        sql.append(String.format("SELECT %1$s FROM %2$s ", getQueryFields(filter), tableName));
        //endregion
        ArrayList<DbParameter> parameter = new ArrayList<DbParameter>();
        String condition = buildWhereCondition(getDB(), filter.getFilterConditions(), parameter, authorities);
        String sort = buildOrderByCondition(filter.getSortConditions());
        if (condition != null && condition.length() > 0) {
            sql.append(String.format(" WHERE %1$s", condition));
        }
        if (sort != null && sort.length() > 0) {
            sql.append(String.format(" Order By %1$s", sort));
        }
        try {
            return getDatas(RepositoryUtil.FormatMuliLangBySpecLang(sql.toString(), filter.getEnableLanguage()), parameter);
        } catch (java.lang.Exception e) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5032, e);
        }
    }

    /**
     * 分页查询
     * @param filter
     * @param authorities
     * @return
     */
    private List<IEntityData> queryWithPagination(EntityFilter filter, ArrayList<AuthorityInfo> authorities) {
        String tableName = getQueryTableNameWithAuthority(authorities, filter);
        ArrayList<DbParameter> parameter = new ArrayList<DbParameter>();
        String condition = buildWhereCondition(getDB(), filter.getFilterConditions(), parameter, authorities);
        String sort = buildOrderByCondition(filter.getSortConditions());
        Pagination pagination = filter.getPagination();
        try {
            RefObject<Pagination> tempRef_pagination = new RefObject<Pagination>(pagination);
//			return getDatas(String.format("SELECT %1$s FROM %2$s WHERE "));
            List<IEntityData> tempVar = DatabaseUtil.getPaginationData(tableName, getTableName(), RepositoryUtil.FormatMuliLangBySpecLang(getQueryFields(filter), filter.getEnableLanguage()),
                    getWrappedTableAlias().concat(".").concat(getContainColumns().getPrimaryKey().getDbColumnName()),
                    condition,
                    sort,
                    parameter,
                    tempRef_pagination,
                    this, isTableView());
            pagination = tempRef_pagination.argvalue;
            return tempVar;
        } catch (Exception e) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5008, e);
        }
    }

    /**
     * 根据过滤条件获取查询字段
     * @param filter
     * @return
     */
    protected String getQueryFields(EntityFilter filter) {
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().isUseFieldsCondition())
            return buildQueryColumns(true, filter.getFieldsFilter().getFilterFields(), filter);
        return buildQueryColumns(false, null, filter);
    }

    private String getQueryFields(AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam.getEntityfilter(getNodeCode());
        // 构造非多语列列名
        String fields = getQueryFields(filter);
        if (adaptorRetrieveParam == null || !adaptorRetrieveParam.isEnableMultiLanguage()) {
            return fields;
        }
        String multiLangFields = buildQueryColumns_MultiLanguage(filter);
        if (multiLangFields.length() > 0 && fields.length() > 0) {
            fields = fields.concat("," + multiLangFields);
        }
        queryFields = fields;
        return fields;
    }

    /**
     * 根据权限和过滤条件组织查询表名
     * @param authorities 权限信息(如果包含权限，最后会与拼接好的表名Join)
     * @param filter    过滤条件
     * @return
     */
    protected String getQueryTableNameWithAuthority(ArrayList<AuthorityInfo> authorities, EntityFilter filter) {
        String tempQueryTableName = "";
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().isUseFieldsCondition()) {
            tempQueryTableName = getQueryTableNameByFields(filter);
        } else {
            initQueryTableName();
            tempQueryTableName = queryTableName;
        }

        //String tableName = queryTableName;
        String authority = getAuthoritySql(authorities);
        return tempQueryTableName + authority;
    }

    /**
     * 根据过滤条件中的过滤字段组织表名
     * @param filter
     * @return
     */
    private String getQueryTableNameByFields(EntityFilter filter) {
        ArrayList<AssociationInfo> associationInfos = new ArrayList<>();
        //对关联进行过滤,此处要求传递的字段必须是关联本身，不能只传递带出字段
        for (AssociationInfo associationInfo : getAssociationInfos()) {
            if (filter.getFieldsFilter().getFilterFields().contains(associationInfo.getSourceColumn())) {
                associationInfos.add(associationInfo);
            }
        }
        return getTableNamesWithAssociationInfo(associationInfos, null);
    }

    private String getAuthoritySql(ArrayList<AuthorityInfo> authorities) {
        if (authorities == null || authorities.isEmpty()) {
            return "";
        }
        StringBuilder authorityBuilder = new StringBuilder();
        for (int i = 1; i < authorities.size() + 1; i++) {
            AuthorityInfo info = authorities.get(i - 1);
            if (info.getAuthType() != AuthorityInfoType.Join) {
                continue;
            }
            String aliasName = authorityTableAlias + i;
            String fieldName = trans2DbColumnWithAlias(info.getFieldName());
            String sourceFieldName = aliasName + "." + info.getSourceFieldName();

            authorityBuilder.append(String.format(authoritySql, info.getAuthoritySql(), aliasName, fieldName, sourceFieldName));
        }

        return authorityBuilder.toString();
    }

    // region buildWhereCondition
    private String buildWhereCondition(Connection db, ArrayList<FilterCondition> filter,
                                       ArrayList<DbParameter> parameter, List<AuthorityInfo> authorities) {
        //DynamicParameters
        String authCondition = getAuthFilterCondition(db, parameter, authorities);
        String beCondition = getDefaultCondition(db, parameter);

        String externalCondition = parseFilterCondition(filter, parameter, parameter.size(), false, true);

        String tenantCondition = getTenantCondition();

        String condition = String.format("%1$s%2$s%3$s%4$s", authCondition, beCondition, externalCondition, tenantCondition);
        if (condition.startsWith(" AND ")) {
            condition = condition.substring(4);
        }

        if (condition == null || condition.length() == 0) {
            return condition;
        }
        return formatMultiLang(condition);
    }

    protected String getAuthFilterCondition(Connection db, List<DbParameter> parameter,
                                            List<AuthorityInfo> authorities) {
        if (authorities == null || authorities.isEmpty()) {
            return "";
        }

        //TODO: 暂不支持多个Filter类型的authinfo, 应支持或抛出明确异常提示
        AuthorityInfo authInfo = authorities.get(0);
        if (authInfo.getAuthType() != AuthorityInfoType.Filter || authInfo.getFilter() == null) {
            return "";
        }

        String rez = parseAuthFilter(authInfo.getFilter());
        if (authInfo.getFilterParameters() != null && !authInfo.getFilterParameters().isEmpty()) {
            for (DbParameter filterPar : authInfo.getFilterParameters()) {
                GspDbDataType dataType = GspDbDataType.VarChar;
                Object value = filterPar.getValue();
                if (!StringUtils.isEmpty(filterPar.getParamName())) {
                    dataType = getDataType(filterPar.getParamName());
                    value = getTypeTransProcesser(filterPar.getParamName()).transType(value);
                }
                parameter.add(buildParam(filterPar.getParamName(), dataType, value));
            }
        }
        return rez;
    }

    private String parseAuthFilter(List<AuthFilter> filter) {
        if (filter == null || filter.isEmpty()) {
            return "";
        }
        StringBuilder conditionBuilder = new StringBuilder(" AND (");
        AuthFilter lastfilter = filter.get(filter.size() - 1);
        for (AuthFilter filterItem : filter) {
            String columnName = !StringUtils.isEmpty(filterItem.getColumnName())
                    ? trans2DbColumnWithAlias(filterItem.getColumnName(), false)
                    : "";
            columnName = KeyWordsManager.getColumnAlias(columnName);
            conditionBuilder
                    .append(columnName)
                    .append(" ")
                    .append(filterItem.getFilterBody());
            if (lastfilter != filterItem)
                conditionBuilder.append(FilterUtil.trans2DbRelation(filterItem.getRelationType()));
        }
        conditionBuilder.append(")");

        return conditionBuilder.toString();
    }

    protected void buildLogicDeleteCondition(EntityFilter entityFilter) {
        if (!getLogicDeleteInfo().isEnableLogicDelete())
            return;

        //如果原来包含过滤条件，则在后面添加逻辑删除条件，if内处理括号和最后一个条件的and关系
        //这个地方会修改原来的EntityFilter，克隆比较好 wangmj?
        if (entityFilter.getFilterConditions().size() > 0) {//修改最后一个的relation符号
            entityFilter.getFilterConditions().get(0).setLbracket(entityFilter.getFilterConditions().get(0).getLbracket() + "(");
            entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).setRbracket(entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).getRbracket() + ")");
            entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).setRelation(ExpressRelationType.And);
        }
        FilterCondition filterCondition = new FilterCondition();
        filterCondition.setValue("1");
        filterCondition.setlBracketCount(1);
        filterCondition.setFilterField(getLogicDeleteInfo().getLabelId());
        filterCondition.setCompare(ExpressCompareType.NotEqual);
        filterCondition.setRelation(ExpressRelationType.Or);

        //兼容历史数据升级，逻辑删除字段为空的场景
        FilterCondition cond = new FilterCondition();
        cond.setValue(null);
        cond.setRBracketCount(1);
        cond.setFilterField(getLogicDeleteInfo().getLabelId());
        cond.setCompare(ExpressCompareType.Is);
        entityFilter.getFilterConditions().add(filterCondition);
        entityFilter.getFilterConditions().add(cond);
    }

    /**
     * 获取指定字段对应的关联信息
     * @param propertyName
     * @return
     */
    @Override
    protected final AssociationInfo getAssociation(String propertyName) {
        for (AssociationInfo item : getAssociationInfos()) {
            if (propertyName.equals(item.getSourceColumn())) {
                return item;
            }
        }
        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5003, false, getTableAlias(), propertyName);
    }

    private String getDefaultCondition(Connection db, ArrayList<DbParameter> parameter) {
        ArrayList<FilterCondition> filterConditions = getDefaultFilterCondition();
        if (filterConditions == null || filterConditions.size() < 1) {
            return "";
        }
        StringBuilder conditionBuilder = new StringBuilder("(");
        int paramNum = parameter.size();
        for (FilterCondition filterItem : filterConditions) {
            String columnName = "";
            if (filterItem.getFilterField() != null && filterItem.getFilterField().isEmpty() == false)
                columnName = trans2DbColumnWithAlias(filterItem.getFilterField());

            GspDbDataType dataType = getDataType(filterItem.getFilterField());
            RefObject<Integer> tempRef_paramNum = new RefObject<Integer>(paramNum);
            FilterContext filterContext = new FilterContext();
            filterContext.setFilterCondition(filterItem);
            filterContext.setDbColumnName(columnName);
            filterContext.setParameters(parameter);
            filterContext.setDataType(dataType);
            filterContext.setParamNum(tempRef_paramNum);
            filterContext.setProcesser(getTypeTransProcesser(filterItem.getFilterField()));
            conditionBuilder.append(FilterUtil.parseFilterCondition(filterContext));
            paramNum = tempRef_paramNum.argvalue;
        }

        conditionBuilder.append(")");

        return conditionBuilder.toString();
    }

    private String getTenantCondition() {
        //String tenantCondition = TenantUtil.GetCondition(TableAlias);
        //if (!String.IsNullOrEmpty(tenantCondition))
        //    return " AND (" + tenantCondition + ")";
        return "";
    }

    public final ITypeTransProcesser getTypeTransProcesser(String fieldName) {
        DbColumnInfo dbColumnInfo = getAllContainColumns().getItem(fieldName);
        if (dbColumnInfo == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5011, false, getTableAlias(), fieldName);
        }
        ITypeTransProcesser processer = dbColumnInfo.getTypeTransProcesser();
        if (processer == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5012, false, getTableAlias(), fieldName);
        }
        return processer;
    }

    private String buildOrderByCondition(ArrayList<SortCondition> orderByConditions) {
        ArrayList<SortCondition> defaultSortCondition = getDefaultSortCondition();
        String defaultSort = buildDefaultOrderByCondition(defaultSortCondition);
        StringBuilder orderByBuilder = new StringBuilder(defaultSort);
        if (orderByConditions != null && orderByConditions.size() > 0) {
            ArrayList<SortCondition> sortConditions = transSortCondition(orderByConditions);
            for (SortCondition orderByItem : sortConditions) {
                if (orderByBuilder.toString() != null && !"".equals(orderByBuilder.toString())) {
                    orderByBuilder.append(", ");
                }
                String filedName = trans2DbColumnWithAlias(orderByItem.getSortField());
                orderByBuilder.append(orderByItem.trans2Sql(filedName));
            }
        }

        if (orderByBuilder.toString() == null || orderByBuilder.toString().length() == 0) {
            return getWrappedTableAlias() + "." + getContainColumns().getPrimaryKey().getDbColumnName() + " ASC";
        }
        return formatMultiLang(orderByBuilder.toString());
    }

    private ArrayList<SortCondition> transSortCondition(ArrayList<SortCondition> orderByConditions) {
        HashMap<String, SortCondition> sortConditions = new HashMap<String, SortCondition>();
        ArrayList<SortCondition> sorts = new ArrayList<SortCondition>();
        for (SortCondition sortCondition : orderByConditions) {
            if (!sortConditions.containsKey(sortCondition.getSortField().toLowerCase())) {
                sortConditions.put(sortCondition.getSortField().toLowerCase(), sortCondition);
                sorts.add(sortCondition);
            }
        }

        return sorts;
    }

    private String buildDefaultOrderByCondition(ArrayList<SortCondition> sortConditions) {
        StringBuilder orderByBuilder = new StringBuilder();
        if (sortConditions != null && sortConditions.size() > 0) {
            for (SortCondition orderByItem : sortConditions) {
                if (orderByBuilder.toString() != null && !orderByBuilder.toString().equals("")) {
                    orderByBuilder.append(", ");
                }
                String filedName = trans2DbColumnWithAlias(orderByItem.getSortField());
                orderByBuilder.append(orderByItem.trans2Sql(filedName));
            }
        }

        return String.valueOf(orderByBuilder);
    }

    public String getInIDsFilter(List<String> dataIds) {
        return String.format(" where (%1$s.%2$s IN %3$s)", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName(), FilterUtil.buildInCondition(dataIds.toArray(new String[]{}), false, getWrappedTableAlias() + "." + getContainColumns().getPrimaryKey().getDbColumnName()));
    }

    //获取分批in条件
    public List<String> getBatchInIDsFilter(List<String> dataIds) {
        int batchCount = 5000;
        List<String> batchIns = new ArrayList<>();
        if (dataIds != null && dataIds.size() > batchCount) {
            int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
            for (int i = 1; i <= times; i++) {
                int endIndex = i == times ? dataIds.size() : batchCount * i;
                List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);
                batchIns.add(String.format(" where (%1$s.%2$s IN %3$s)", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName(), FilterUtil.buildInCondition(tempList.toArray(new String[]{}), false, getWrappedTableAlias() + "." + getContainColumns().getPrimaryKey().getDbColumnName())));
            }
        } else {
            batchIns.add(String.format(" where (%1$s.%2$s IN %3$s)", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName(), FilterUtil.buildInCondition(dataIds.toArray(new String[]{}), false, getWrappedTableAlias() + "." + getContainColumns().getPrimaryKey().getDbColumnName())));
        }
        return batchIns;
    }

    public List<String> getBatchInParentIDsFilter(List<String> dataIds) {
        int batchCount = 5000;
        List<String> batchIns = new ArrayList<>();
        DbColumnInfo parentIdColumnInfo = getParentIdColumnInfo();
        if (dataIds != null && dataIds.size() > batchCount) {
            int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
            for (int i = 1; i <= times; i++) {
                int endIndex = i == times ? dataIds.size() : batchCount * i;
                List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);
                batchIns.add(String.format(" where (%1$s.%2$s IN %3$s)", getWrappedTableAlias(), parentIdColumnInfo.getDbColumnName(), FilterUtil.buildInCondition(tempList.toArray(new String[]{}), false, getWrappedTableAlias() + "." + parentIdColumnInfo.getDbColumnName())));
            }
        } else {
            batchIns.add(String.format(" where (%1$s.%2$s IN %3$s)", getWrappedTableAlias(), parentIdColumnInfo.getDbColumnName(), FilterUtil.buildInCondition(dataIds.toArray(new String[]{}), false, getWrappedTableAlias() + "." + parentIdColumnInfo.getDbColumnName())));
        }
        return batchIns;
    }

    public String getInFilter(List<String> dataIds) {
        return FilterUtil.buildInCondition(dataIds.toArray(new String[]{}), false, getWrappedTableAlias() + "." + getContainColumns().getPrimaryKey().getDbColumnName());
    }

    /**
     * @param dataIds
     * @param propName 不带别名.
     * @return
     */
    public String getInFilter(List<String> dataIds, String propName) {
        return FilterUtil.buildInCondition(dataIds.toArray(new String[]{}), false, propName);
    }

    public String getIDFilter() {
        return String.format(" where %1$s.%2$s=", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName()) + "?0";
    }

    public DbColumnInfo getParentIdColumnInfo() {
        DbColumnInfo parentIdInfo = null;
        for (DbColumnInfo columnInfo : getContainColumns()) {
            if (columnInfo.getIsParentId()) {
                parentIdInfo = columnInfo;
                break;
            }
        }
        if (parentIdInfo == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5005, getTableAlias());
        }
        return parentIdInfo;
    }

    public List<IChildEntityData> getDataWithParentJoinIds(String joinInfo, String condition
            , ArrayList<SortCondition> orderByCondition, ArrayList<String> tableAlias, List<DbParameter> dbPars) {
        return this.getDataWithParentJoinIds(joinInfo, condition, orderByCondition, tableAlias, dbPars, null);
    }

    public List<IChildEntityData> getDataWithParentJoinIds(String joinInfo, List<String> conditions
            , ArrayList<SortCondition> orderByCondition, ArrayList<String> tableAlias, List<DbParameter> dbPars, AdaptorRetrieveParam adaptorRetrieveParam) {
        initQueryTableNameWithParentAlias(tableAlias, adaptorRetrieveParam);
        List<IChildEntityData> childDatas = new ArrayList<IChildEntityData>();
        //todo conidtions过来必须有值
        if (conditions != null && conditions.size() > 0) {
            for (String condition : conditions) {
                List<DbParameter> tempdbPars = new ArrayList<>();
                if (getLogicDeleteInfo().isEnableLogicDelete()) {
                    if (dbPars != null && dbPars.size() > 0) {
                        tempdbPars.addAll(dbPars);
                    }
                    tempdbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                    condition += " and " + getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId() + " = ?" + (tempdbPars.size() - 1);
                }

                String sql = "";
                DbType dbType = CAFContext.current.getDbType();
                //对大部分数据库，关联表的顺序不会影响性能，优化器会进行处理。Oscar有参数（支持几个表）配置；Pg数据库查询优化器做的不好，当表数量大于5个时（经验）会较慢
                if (!StringUtils.isNullOrEmpty(joinInfo) && (dbType == DbType.Oscar || dbType == DbType.PgSQL)) {
                    sql = String.format(GetDataWithParentJoinSql, getQueryFields(adaptorRetrieveParam), getTableName() + " " + joinInfo, joinTableName, condition);
                } else {
                    sql = String.format(GetDataWithParentJoinSql, getQueryFields(adaptorRetrieveParam), queryTableName, joinInfo, condition);
                }

                sql += buildRetrieveOrderBy(orderByCondition);
                RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
                sql = formatFiscalAndMultiLang(sql, retrieveFilter);
                List<IEntityData> datas = getDatas(sql, tempdbPars, adaptorRetrieveParam);
                if (datas == null)
                    return null;
                for (IEntityData data : datas) {
                    childDatas.add((IChildEntityData) data);
                }
            }
        }
        return childDatas;
    }

    public List<IChildEntityData> getDataWithParentJoinIds(String joinInfo, String condition
            , ArrayList<SortCondition> orderByCondition, ArrayList<String> tableAlias, List<DbParameter> dbPars, AdaptorRetrieveParam adaptorRetrieveParam) {
        initQueryTableNameWithParentAlias(tableAlias, adaptorRetrieveParam);
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            if (dbPars == null)
                dbPars = new ArrayList<>();
            dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
            condition += " and " + getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId() + " = ?" + (dbPars.size() - 1);
        }
        String sql = "";
        DbType dbType = CAFContext.current.getDbType();
        //对大部分数据库，关联表的顺序不会影响性能，优化器会进行处理。Oscar有参数（支持几个表）配置；Pg数据库查询优化器做的不好，当表数量大于5个时（经验）会较慢
        if (!StringUtils.isNullOrEmpty(joinInfo) && (dbType == DbType.Oscar || dbType == DbType.PgSQL)) {
            sql = String.format(GetDataWithParentJoinSql, getQueryFields(adaptorRetrieveParam), getTableName() + " " + joinInfo, joinTableName, condition);
        } else {
            sql = String.format(GetDataWithParentJoinSql, getQueryFields(adaptorRetrieveParam), queryTableName, joinInfo, condition);
        }

        sql += buildRetrieveOrderBy(orderByCondition);
        RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
        sql = formatMultiLang(sql, retrieveFilter);
        List<IEntityData> datas = getDatas(sql, dbPars, adaptorRetrieveParam);
        if (datas == null)
            return null;
        List<IChildEntityData> childDatas = new ArrayList<IChildEntityData>();
        for (IEntityData data : datas) {
            childDatas.add((IChildEntityData) data);
        }
        return childDatas;
    }

    private String buildRetrieveOrderBy(ArrayList<SortCondition> orderBys) {
        if (orderBys == null || orderBys.size() < 1) {
            return "";
        }
        return " ORDER BY " + buildDefaultOrderByCondition(orderBys);
    }

    public final List<IEntityData> getDataWithParentJoinId(String joinInfo, String condition, String rootId) {
        String sql = String.format(GetDataWithParentJoinSql, queryFields, innerGetTableName(), getWrappedTableAlias(), joinInfo + " " + condition);
        ArrayList<DbParameter> parameters = new ArrayList<DbParameter>();
        parameters.add(new DbParameter("rootId", GspDbDataType.VarChar, rootId));
        return getDatas(sql, parameters);
    }

    public DbColumnInfoCollection getContainColumns() {
        return containColumns;
    }

    public DbColumnInfoCollection getAllContainColumns() {
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            DbColumnInfoCollection col = new DbColumnInfoCollection();
            col.addRange(getContainColumns());
            col.addRange(extendContainColumns);
            return col;
        }
        return getContainColumns();
    }

    protected abstract void initColumns();

    protected abstract ArrayList<FilterCondition> getDefaultFilterCondition();

    protected abstract ArrayList<SortCondition> getDefaultSortCondition();

    /**
     * 检索超过1W条数据,分拼执行合并结果
     *
     * @param sql
     * @param dataIds
     * @return
     */
    public ArrayList<IEntityData> getDatasBatch(String sql, List<String> dataIds) {
        int batchCount = 5000;
        ArrayList<IEntityData> datas = new ArrayList<>();
        if (dataIds != null && dataIds.size() > batchCount) {
            int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
            for (int i = 1; i <= times; i++) {
                int endIndex = i == times ? dataIds.size() : batchCount * i;
                List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);
                ArrayList<IEntityData> tempListData = getDatas(String.format(sql, getInFilter(tempList)), null);
                if (tempListData != null && tempListData.size() > 0) {
                    datas.addAll(tempListData);
                }
            }
            return datas;
        } else {
            return this.getDatas(String.format(sql, getInFilter(dataIds)), null, new AdaptorRetrieveParam(null));
        }
    }

    public ArrayList<IEntityData> getDatas(String sql, List<DbParameter> parameters) {
        return this.getDatas(sql, parameters, new AdaptorRetrieveParam(null));
    }

    protected final ArrayList<IEntityData> getDatas(String sql, List<DbParameter> parameters,
                                            AdaptorRetrieveParam param) {
        Query query = buildQueryManager(sql, parameters);
        List<Object[]> resultSet = null;
        try {
            resultSet = query.getResultList();
        } catch (Exception ex) {
            logger.error("tablealias:" + getTableAlias() + " 出错sql:" + sql);
            throw ex;
        }
        return innerGetDatas(resultSet, param);
    }

    public List<IEntityData> getDatasInPagination(List<Object[]> resultSet) {
        return innerGetDatas(resultSet);
    }

    public ArrayList<IEntityData> innerGetDatas(List<Object[]> resultSet) {
        return this.innerGetDatas(resultSet, null);
    }

    private ArrayList<IEntityData> innerGetDatas(List<Object[]> resultSet, AdaptorRetrieveParam param) {
        ArrayList<IEntityData> results = new ArrayList<IEntityData>();
        HashMap<String, Integer> mapping = filteredPropDbIndexMapping;
        if (mapping == null)
            mapping = getPropertyColumnMapping();

        //如果select查询字段只有一个，返回的结果不是object[]，需要兼容此场景
        boolean isArray = false;
        if(resultSet.size() > 0){
            if(resultSet.get(0) instanceof Object[]){
                isArray = true;
            }
        }
        for (Object result : resultSet) {
            Object[] mappingResult = null;
            if(isArray){
                mappingResult = (Object[]) result;
            }
            else{
                mappingResult = new Object[]{result};
            }

            IEntityData instance = createEntityInstance(new CefDataReader(mappingResult, mapping), param);
            results.add(instance);
        }
        return results;
    }

    public final IEntityData createEntityInstance(ICefReader reader) {
        return this.createEntityInstance(reader, null);
    }

    private IEntityData createEntityInstance(ICefReader reader, AdaptorRetrieveParam pram) {
        ICefData tempVar = createInstance(reader);
        IEntityData data = (IEntityData) tempVar;
        for (AbstractDataAdapterExtendInfo extendInfo : getDataAdapterExtendInfos()) {
            extendInfo.setEntityPropertiesFromDal(reader, data);
        }
        // 处理多语列，赋值至ICefDataBase.MultiLanguageInfos
        if (pram != null && pram.isEnableMultiLanguage() && tempVar instanceof IMultiLanguageData) {
            IMultiLanguageData cefDataBase = (IMultiLanguageData) tempVar;
            Map<String, MultiLanguageInfo> resultInfos = cefDataBase.getMultiLanguageInfos();
            DbColumnInfoCollection columnInfos = this.getMultiLanguageColumnInfos();
            this.getMultiLanguageAlias().forEach((alias, multiLangColumnInfo) -> {
                if (reader.hasProperty(alias)) {
                    DbColumnInfo currentPropInfo = columnInfos
                            .getItem(multiLangColumnInfo.getColumnName());
                    if (currentPropInfo != null && currentPropInfo.getIsUdtElement()) {
                        // 多语控件不支持UDT
                        return;
                    }
                    String labelId =
                            multiLangColumnInfo.getColumnName() + MultiLanguageInfo.MULTILANGUAGETOKEN;
                    if (!resultInfos.containsKey(labelId)) {
                        resultInfos.put(labelId, new MultiLanguageInfo());
                    }
                    MultiLanguageInfo info = resultInfos.get(labelId);
                    info.setPropName(labelId);
                    // dm数据库获取数据需处理
                    Object currentAliasValue =
                            currentPropInfo.getColumnType().equals(GspDbDataType.NClob) ? getClobValue(reader
                                    .readValue(alias)) : reader.readValue(alias);
                    info.getPropValueMap()
                            .put(multiLangColumnInfo.getLanguageInfo().getCode(), currentAliasValue);
                }
            });
        }
        return (IEntityData) ((tempVar instanceof IEntityData) ? tempVar : null);
    }

    private IDatabaseObjectRtService getDboService() {
        return CefRtBeanUtil.getDboRtService();
    }

    public boolean isTableView() {
        return getDboService().getDatabaseObject(getDboID()).getType() == DatabaseObjectType.View;
    }

    /**
     * 获取dbo表名
     * @return
     */
    public final String innerGetTableName() {
        if (StringUtils.isNullOrEmpty(dboTableName)) {
            try {
                dboTableName = getDboService().getTableNameWithDimensionValue(getDboID(), getVars());
            } catch (Exception e) {
                String[] messageParams = new String[]{getConfigId(), getNodeCode(), getDboID()};
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5007, e, messageParams);
            }
        }
        return dboTableName;
    }

    public final List<String> innerGetTableNames() {
        try {
            return getDboService().getTableNamesWithDimensionValues(getDboID(), null);
        } catch (Exception e) {
            String[] messageParams = new String[]{getConfigId(), getNodeCode(), getDboID()};
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5007, e, messageParams);
        }
    }

    public final String getTableNameByDimensions(HashMap<String, String> dimensions) {
        return getDboService().getTableNameWithDimensionValue(getDboID(), dimensions);
    }

    public abstract String getPrimaryKey();

    public abstract String getTableAlias();

    public abstract void setTableAlias(String value);

    // endregion

    //获取转换后的别名
    public String getWrappedTableAlias() {
        return KeyWordsManager.getTableAlias(getTableAlias());
    }

    public String getWrappedTableAlias(boolean ignoreKeyWords) {
        return KeyWordsManager.getTableAlias(getTableAlias(), ignoreKeyWords);
    }

    //protected abstract String DefaultSort { get; }
    protected abstract String getConfigId();

    ///#region initQueryTableName
    private void initQueryTableName() {
        initQueryTableNameWithParentAlias(null);
    }

    private void initQueryTableNameWithParentAlias(ArrayList<String> alias) {
        this.initQueryTableNameWithParentAlias(alias, new AdaptorRetrieveParam(null));
    }

    private void initQueryTableNameWithParentAlias(ArrayList<String> alias, AdaptorRetrieveParam adaptorRetrieveParam) {
        if (queryTableName != null && queryTableName.length() > 0) {
            return;
        }
        //跟之前一致，并且在当前实例上做缓存。
        queryTableName = getTableNamesWithAssociationInfo(getAssociationInfos(), alias);
        String fields = buildQueryColumns(false, null, null);
        if (adaptorRetrieveParam != null && adaptorRetrieveParam.isEnableMultiLanguage()) {
            String multiLangFields = buildQueryColumns_MultiLanguage(adaptorRetrieveParam.getEntityfilter(getNodeCode()));
            if (multiLangFields.length() > 0 && fields.length() > 0) {
                fields = fields.concat("," + multiLangFields);
            }
        }
        queryFields = fields;
    }

    public void buildQueryColumns() {
        buildQueryColumns(false, null, null);
    }

    public String buildQueryColumns(boolean usePropertyFilter, List<String> filterProperties, EntityFilter entityFilter) {
        //跟之前一致，并且在当前实例上做缓存。
        StringBuilder columns = new StringBuilder();
        boolean hasPropDBIndexMapping = hasPropColumnMappping();
        hasPropDBIndexMapping = false;
        HashMap<String, Integer> mapping = null;
        if (hasPropDBIndexMapping == false) {
            mapping = new HashMap<String, Integer>();
        }
        columns.append(RepoExtendManager.buildQueryFields(getTableAlias(), getContainColumns(), entityFilter, mapping));
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            String extendFields = RepoExtendManager.buildQueryFields(getTableAlias(), extendContainColumns, entityFilter, mapping);
            if (!StringUtils.isNullOrEmpty(extendFields)) {
                columns.append(",").append(extendFields);
            }
        }

        if (hasPropDBIndexMapping == false) {
            setPropertyColumnMapping(mapping);
        }

        filteredPropDbIndexMapping = mapping;
        queryFields = columns.toString();
        return queryFields;
    }

    protected DbColumnInfoCollection getMultiLanguageColumnInfos() {
        //region TODO multiLanguageColumnInfos可以缓存起来，没必要每次都new
        this.multiLanguageColumnInfos = new DbColumnInfoCollection();
        //endregion
        for (DbColumnInfo col : getContainColumns()) {
            if (col.getIsMultiLang() && !col.getIsUdtElement()) {
                this.multiLanguageColumnInfos.add(col);
                continue;
            }
        }
        return this.multiLanguageColumnInfos;
    }

    protected Map<String, MultiLangColumnInfo> getMultiLanguageAlias() {
        if (this.multiLanguageAlias != null) {
            return this.multiLanguageAlias;
        }
        this.multiLanguageAlias = new HashMap<>();
        List<EcpLanguage> currentEnableLanguages = RepositoryUtil.getAllLanguages();
        this.getMultiLanguageColumnInfos().forEach(
                columnInfo -> {
                    currentEnableLanguages.forEach(language -> {
                        MultiLangColumnInfo info = new MultiLangColumnInfo();
                        String alias = RepositoryUtil.FormatMuliLangColumnName(columnInfo.getColumnName(), language);
                        info.setColumnName(columnInfo.getColumnName());
                        info.setLanguageInfo(language);
                        this.multiLanguageAlias.put(alias, info);
                    });
                }
        );
        return this.multiLanguageAlias;
    }

    /**
     * 构建多语相关的列名 TableName.Name_CHS as Name_CHS
     *
     * @return
     */
    private String buildQueryColumns_MultiLanguage(EntityFilter filter) {
        DbColumnInfoCollection collection = this.getMultiLanguageColumnInfos();
        // 无多语字段
        if (collection.getCount() == 0) {
            return "";
        }

        StringBuilder columns = new StringBuilder();
        HashMap<String, Integer> currentIndexMap = getPropertyColumnMapping();
        if (currentIndexMap == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5013);
        }

        List<EcpLanguage> currentEnableLanguages = RepositoryUtil.getCurrentEnabledLanguages();
        for (DbColumnInfo columnInfo : collection) {
            // 使用过滤，且不包含，此处不考虑关联带出字段
            if (filter != null && !filter.getFieldsFilter().getFilterFields().contains(columnInfo.getColumnName())) {
                continue;
            }
            for (EcpLanguage language : currentEnableLanguages) {
                String columnName =
                        RepositoryUtil.FormatMuliLangColumnName(trans2DbColumnWithAlias(columnInfo.getColumnName()), language);
                String alias = RepositoryUtil.FormatMuliLangColumnName(columnInfo.getColumnName(), language);
                // 不使用过滤，则全部添加
                if (columns != null && columns.length() > 0) {
                    columns.append(",");
                }
                columns.append(columnName).append(" AS ").append(alias);
                if (!currentIndexMap.containsKey(alias)) {
                    currentIndexMap.put(alias, currentIndexMap.size());
                }
            }
        }
        return columns.toString();
    }

    protected abstract String innerGetDeleteSql();

    public String getDeleteSql() {
        String innerDelSql = innerGetDeleteSql();
        if (CAFContext.current.getDbType() == DbType.MySQL) {
            if (innerDelSql.indexOf("@TableName@") != innerDelSql.lastIndexOf("@TableName@")) {//包含两个@TableName@
                innerDelSql = innerGetDeleteSql().replaceFirst("@TableName@", getWrappedTableAlias());
            }
        }
        return innerDelSql.replace("@TableName@", innerGetTableName());
    }

    @Override
    public int delete(String id, DataSaveParameter par) {
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            return logicDelete(id, par);
        }
        DataValidator.checkForEmptyString(id, "id");
        List<DbParameter> parameters = new ArrayList<>();
        parameters.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), id));

        String sql =
                String.format("%1$s where %2$s.%3$s=?0", getDeleteSql(), getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName());
        List<FilterCondition> filter = par.getFilterCondition(getNodeCode(), id);
        if (filter != null && !filter.isEmpty()) {
            String condition = parseFilterCondition(filter, parameters, parameters.size(),false, true);
            sql = sql.concat(condition);
        }
        return executeSql(sql, parameters);
    }

    public int logicDelete(String id, DataSaveParameter par) {
        DataValidator.checkForEmptyString(id, "id");
        List<DbParameter> parameters = new ArrayList<>();
        parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "1"));
        parameters.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), id));
        String deleteSql = getModifySql() + " " + getLogicDeleteInfo().getLabelId() + " = ?0 where "
                + getContainColumns().getPrimaryKey().getDbColumnName() + "=?1 ";
        return executeSql(deleteSql, parameters);
    }

    //delete from xxx
    protected abstract String getDeleteSqlBatch();

    @Override
    public void delete(List<String> ids, DataSaveParameter par) {
        DataValidator.checkForNullReference(ids, "ids");
        if (ids.isEmpty()) {
            throw new IllegalArgumentException();
        }
        for (String item : ids) {
            FilterUtil.checkInParameterForSqlInjection(item);
        }
        executeSql(formatFiscalAndMultiLang(getDeleteSqlBatch()) + getInFilter(ids), null);
    }

    public void deleteByParent(String joinInfo, String filter, List<DbParameter> dbPars) throws SQLException {

        String idWithAlias = String.format("%1$s.%2$s", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName());
        if (CAFContext.current.getDbType() == DbType.MySQL) {
            if (getLogicDeleteInfo().isEnableLogicDelete()) {
                dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "1"));
                executeSql(String.format("update %1$s set %2$s = ?%8$s WHERE %3$s IN (SELECT t_id from(SELECT %4$s as t_id FROM %5$s %6$s WHERE %7$s) t )"
                        , getTableName(), getLogicDeleteInfo().getLabelId(), idWithAlias, idWithAlias, getTableName(), joinInfo, filter, dbPars.size() - 1), dbPars);
            } else {
                executeSql(String.format("%1$s WHERE %2$s IN (SELECT t_id from(SELECT %3$s as t_id FROM %4$s %5$s WHERE %6$s) t )"
                        , getDeleteSql(), idWithAlias, idWithAlias, getTableName(), joinInfo, filter), dbPars);
            }
        } else {
            if (getLogicDeleteInfo().isEnableLogicDelete()) {
                String primaryDbName = getContainColumns().getPrimaryKey().getDbColumnName();
                dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "1"));
                executeSql(String.format("update %1$s set %2$s = ?%8$s WHERE %3$s IN ( SELECT %4$s FROM %5$s %6$s WHERE %7$s )"
                        , innerGetTableName(), getLogicDeleteInfo().getLabelId(), primaryDbName, idWithAlias, getTableName(), joinInfo, filter, dbPars.size() - 1), dbPars);
            } else {
                executeSql(String.format("%1$s WHERE %2$s IN ( SELECT %3$s FROM %4$s %5$s WHERE %6$s )"
                        , getDeleteSql(), idWithAlias, idWithAlias, getTableName(), joinInfo, filter), dbPars);
            }
        }
    }

    public final String buildDeleteByIDFilter() {
        return String.format("%1$s.%2$s=?0", getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName());
    }

    // region Insert
    protected abstract String innerGetInsertSql();

    protected String getInsertFields() {
        //region TODO 新增字段缓存起来
        if (isMultiData) {
            return getInsertFields_MultiLanguage(getContainColumns());
        }
        //TODO 后续加入生成基础be的插入字段
        return getInsertFields(getContainColumns());
        //endregion
    }

    private String getInsertFields(DbColumnInfoCollection columns) {
        StringBuilder fields = new StringBuilder();
        for (DbColumnInfo containColumn : columns) {
            if (containColumn.getIsAssociateRefElement()) {
                continue;
            }
            //虚拟字段
            if (containColumn.isVirtual()) {
                continue;
            }
            if (!containColumn.getIsPersistent()) {
                continue;
            }
            if (fields.length() > 0)
                fields.append(",");
            String dbCol = containColumn.getDbColumnName();
            dbCol = KeyWordsManager.getColumnAlias(dbCol);

            if (containColumn.getIsMultiLang()) {
                dbCol = dbCol + "@Language@";
            }
            fields.append(dbCol);
        }
        return fields.toString();
    }

    private String getInsertFields_MultiLanguage(DbColumnInfoCollection columns) {
        StringBuilder fields = new StringBuilder();
        // 初始化
        insertMultiIndexMap = new HashMap<>();
        List<EcpLanguage> currentEnableLanguages = RepositoryUtil.getCurrentEnabledLanguages();
        for (DbColumnInfo containColumn : columns) {
            if (containColumn.getIsAssociateRefElement()) {
                continue;
            }
            if (containColumn.isVirtual()) {
                continue;
            }
            // 多语单独处理
            if (containColumn.getIsMultiLang()) {
                for (EcpLanguage language : currentEnableLanguages) {
                    String alias = RepositoryUtil
                            .FormatMuliLangColumnName(containColumn.getColumnName(), language);
                    // 不使用过滤，则全部添加
                    if (fields.length() > 0) {
                        fields.append(",");
                    }
                    fields.append(alias);
                    if (insertMultiIndexMap.containsValue(alias)) {
                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5014, alias);
                    }
                    insertMultiIndexMap.put(insertMultiIndexMap.size(), alias);
                }
                continue;
            }
            if (fields.length() > 0) {
                fields.append(",");
            }
            fields.append(containColumn.getDbColumnName());
            if (!insertMultiIndexMap.containsKey(containColumn.getColumnName())) {
                insertMultiIndexMap
                        .put(insertMultiIndexMap.size(), containColumn.getColumnName());
            }
        }
        return fields.toString();
    }

    protected String getInsertValues(RefObject<Integer> valuesCount) {
        //region TODO value缓存起来
        if (isMultiData) {
            return getInsertValues_MultiLanguage(getContainColumns(), valuesCount);
        }
        return getInsertValues(getContainColumns(), valuesCount);
        //ednregion
    }

    private String getInsertValues(DbColumnInfoCollection columns, RefObject<Integer> valuesCount) {
        StringBuilder values = new StringBuilder();
        for (DbColumnInfo containColumn : columns) {
            if (containColumn.getIsAssociateRefElement()) {
                continue;
            }
            if (containColumn.isVirtual()) {
                continue;
            }
            if (!containColumn.getIsPersistent()) {
                continue;
            }
            if (valuesCount.argvalue > 0)
                values.append(",");
            values.append("?");
            valuesCount.argvalue = valuesCount.argvalue + 1;
        }
        return values.toString();
    }

    private String getInsertValues_MultiLanguage(DbColumnInfoCollection columns,
                                                 RefObject<Integer> valuesCount) {
        if (insertMultiIndexMap.isEmpty()) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5015);
        }
        StringBuilder values = new StringBuilder();
        for (Map.Entry<Integer, String> entrySet : insertMultiIndexMap.entrySet()) {
            if (valuesCount.argvalue > 0) {
                values.append(",");
            }
            values.append("?");
            valuesCount.argvalue = valuesCount.argvalue + 1;
        }
        return values.toString();
    }

    public String getInsertSql() {
        return getInnerInsertSql().replace("@TableName@", innerGetTableName());
    }

    private String getInnerInsertSql() {
        if ((extendContainColumns == null || extendContainColumns.getCount() < 1) && !isMultiData) {
            batchInsert = false;
            String insertSql = innerGetInsertSql();
            return insertSql;
        }
        batchInsert = true;
        String insertFields = getInsertFields();
        RefObject<Integer> fieldsCount = new RefObject<>(0);
        String insertValues = getInsertValues(fieldsCount);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder = stringBuilder.append("INSERT INTO @TableName@  (").append(insertFields);
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            String extInsertFields = getInsertFields(extendContainColumns);
            //此处判断处理扩展字段全部非持久化，不拼接,
            if (extInsertFields.length() > 0) {
                stringBuilder.append(",").append(getInsertFields(extendContainColumns));
            }
        }
        stringBuilder.append(" ) Values ( ").append(insertValues);

        stringBuilder.append(getInsertValues(extendContainColumns, fieldsCount));
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    @Override
    public int insert(IEntityData data, DataSaveParameter par, DacSaveContext batcher) throws SQLException {
        DataValidator.checkForNullReference(data, "data");
        isMultiData = (data instanceof IMultiLanguageData) && !((IMultiLanguageData) data)
                .getMultiLanguageInfos().isEmpty();
        String sql = formatFiscalAndMultiLang(getInsertSql(), batcher);
        List<DbParameter> inserParameters = getInserParameters(data);
        if (batchInsert) {
            batcher.getJDBCExecutor().addBatch(this.getNodeCode(), sql, inserParameters);
            return 1;
        } else {
            return executeSql(sql, inserParameters);
        }
    }

    private List<DbParameter> getInserParameters(IEntityData data) {
        ArrayList<DbParameter> dbPars = new ArrayList<DbParameter>();
        if (isMultiData) {
            buildInsertParamters_MultiLanguage(data, dbPars);
        } else {
            buildInsertParamters(data, dbPars);
        }
        if (extendContainColumns != null && extendContainColumns.getCount() > 0)
            buildEXtendInsertParamters(data, dbPars);
        return dbPars;
    }

    protected void buildInsertParamters(IEntityData entityData, ArrayList<DbParameter> dbPars) {
        for (DbColumnInfo columnInfo : getContainColumns().getBaseDict().values()) {
            if (columnInfo.getIsAssociateRefElement()) {
                continue;
            }
            if (columnInfo.isVirtual()) {
                continue;
            }
            Object value = this.getPersistenceValue(columnInfo.getColumnName(), entityData);
            Object transValue = value == null ? null : getPropertyChangeValue(columnInfo.getColumnName(), value);
            dbPars.add(buildParam(columnInfo.getColumnName(), columnInfo.getColumnType(), transValue));
        }
    }

    private void buildInsertParamters_MultiLanguage(IEntityData entityData,
                                                    ArrayList<DbParameter> dbPars) {
        if (insertMultiIndexMap.isEmpty()) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5015);
        }
        IMultiLanguageData multiLanguageData =
                entityData instanceof IMultiLanguageData ? (IMultiLanguageData) entityData : null;
        for (Map.Entry<Integer, String> entry : insertMultiIndexMap.entrySet()) {
            String columnName = entry.getValue();
            // 多语
            if (getMultiLanguageAlias().containsKey(columnName)) {
                MultiLangColumnInfo multiLangColumnInfo = getMultiLanguageAlias().get(columnName);
                DbColumnInfo columnInfo = getContainColumns()
                        .getItem(multiLangColumnInfo.getColumnName());
                String language = multiLangColumnInfo.getLanguageInfo().getCode();
                String columnNameWithToken = multiLangColumnInfo.getColumnName()
                        + MultiLanguageInfo.MULTILANGUAGETOKEN;
                if (multiLanguageData != null && multiLanguageData.getMultiLanguageInfos()
                        .containsKey(columnNameWithToken)) {
                    Object value = multiLanguageData.getMultiLanguageInfos()
                            .get(columnNameWithToken)
                            .getPropValueMap().get(language);
                    dbPars.add(
                            buildParam(columnName, columnInfo.getColumnType(),
                                    value));
                    continue;
                }
                if (!language.equals(CAFContext.current.getLanguage())) {
                    dbPars.add(
                            buildParam(columnName, columnInfo.getColumnType(),
                                    null));
                    continue;
                }
                // 若为当前语言，需到data中对应属性中取数,走下面的普通分支
                columnName = multiLangColumnInfo.getColumnName();
            }
            // 普通
            Object value = this.getPersistenceValue(columnName, entityData);
            Object transValue =
                    value == null ? null : getPropertyChangeValue(columnName, value);
            DbColumnInfo columnInfo = getContainColumns().getItem(columnName);
            dbPars.add(
                    buildParam(columnInfo.getColumnName(), columnInfo.getColumnType(), transValue));
        }
    }

    private void buildEXtendInsertParamters(IEntityData entityData, ArrayList<DbParameter> dbPars) {
        for (DbColumnInfo columnInfo : extendContainColumns) {
            if (columnInfo.getIsAssociateRefElement()) {
                continue;
            }
            if (!columnInfo.getIsPersistent()) {
                continue;
            }
            Object transValue = null;
            if (columnInfo.getIsUdtElement()) {
                for (AbstractDataAdapterExtendInfo info : getDataAdapterExtendInfos()) {
                    transValue = info.getUdtInsertValue(columnInfo.getColumnName(), columnInfo.getBelongElementLabel(), entityData);
                    if (transValue != null)
                        break;
                }
            } else {
                Object value = entityData.getValue(columnInfo.getColumnName());
                transValue = columnInfo.getTypeTransProcesser().transType(value);
            }

            dbPars.add(buildParam(columnInfo.getColumnName(), columnInfo.getColumnType(), transValue));
        }
    }

    //endregion
    // region modify
    protected abstract String innerGetModifySql();

    private String getModifySql() {
        return innerGetModifySql().replace("@TableName@", innerGetTableName());
    }

    @Override
    public int modify(ModifyChangeDetail change, DataSaveParameter par, DacSaveContext ctx) throws SQLException {
        if ((change.getPropertyChanges() == null || change.getPropertyChanges().isEmpty()) && (
                change.getMultiLanguageInfos() == null || change.getMultiLanguageInfos().isEmpty())) {
            return 0;
        }
        List<DbParameter> parameters = new ArrayList<DbParameter>();
        RefObject<List<DbParameter>> parameters1 = new RefObject<List<DbParameter>>(parameters);

        //构造多语列
        Set<String> namesOfMultiLangColumns = new HashSet<>();
        String multiLanguageModifyValue = buildModifyValue_MultiLanguage(change.getMultiLanguageInfos(),
                namesOfMultiLangColumns,parameters1);
        String modifyValue = buildModifyValue(change.getPropertyChanges(), namesOfMultiLangColumns, parameters1, ctx);

        if (modifyValue.length() > 0) {
            if (multiLanguageModifyValue.length() > 0) {
                multiLanguageModifyValue = multiLanguageModifyValue.concat(", ");
            }
            multiLanguageModifyValue = multiLanguageModifyValue.concat(modifyValue);
        }
        parameters = parameters1.argvalue;
        if (multiLanguageModifyValue == null || multiLanguageModifyValue.length() == 0) {
            return 0;
        }
        int param = parameters.size();
        String execUpdateSql = getModifySql() + " " + multiLanguageModifyValue + " where "
                + getContainColumns().getPrimaryKey().getDbColumnName() + "=?";
        parameters.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), change.getID().length(), change.getID()));

        List<FilterCondition> filter = par.getFilterCondition(getNodeCode(), change.getID());
        if (filter != null && !filter.isEmpty()) {
            String condition = parseFilterCondition(filter, parameters, parameters.size(),true, false);
            execUpdateSql = execUpdateSql.concat(condition);
        }

        execUpdateSql = formatFiscalAndMultiLang(execUpdateSql);
        //如果开启批量执行(虽然都带了主键条件，但影响行数可能不那么准确)
        if (ctx.getBatchExecuteSql()) {
            BatchSqlStatement batchSqlStatement = ctx.getJDBCExecutor().addBatch(getNodeCode(),execUpdateSql,parameters);
            //如果修改带过滤条件，立即执行
            if (filter != null && !filter.isEmpty()) {
                int[] affectCounts = batchSqlStatement.executeBatch();
                if(affectCounts != null && affectCounts.length == 1){
                    return affectCounts[0];
                }
            }

            return 1;
        } else {
            return executeSql(execUpdateSql, parameters);
        }
    }

    /**
     *解析指定的过滤条件集合，返回条件sql片段
     * @param filter
     * @param parameters
     * @param paramNum
     * @param ignoreAlias
     * @param useNumberBinding 数绑定是否使用数字占位符 应用过滤条件的场景：query、delete、modify 这集中场景下只有modify解析占位符不适用数字
     * @return
     */
    protected final String parseFilterCondition(List<FilterCondition> filter,
                                                List<DbParameter> parameters, int paramNum, boolean ignoreAlias, boolean useNumberBinding) {
        if (filter == null || filter.size() < 1) {
            return "";
        }
        StringBuilder conditionBuilder = new StringBuilder(" AND (");
        for (FilterCondition filterItem : filter) {
            if (StringUtils.isNullOrEmpty(filterItem.getFilterField())) {
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5016);
            }
            FilterContext filterContext = new FilterContext();
            filterContext.setFilterCondition(filterItem);
            filterContext.setParameters(parameters);
            filterContext.setUseNumberBinding(useNumberBinding);
            if (StringUtils.isNullOrEmpty(filterItem.getFilterNode())) {
                paramNum = buildFieldCondition(paramNum, ignoreAlias, conditionBuilder, filterContext);
            } else {
                paramNum = buildChildNodeFieldCondition(paramNum, ignoreAlias, conditionBuilder, filterContext);
            }
        }
        conditionBuilder.append(")");

        return conditionBuilder.toString();
    }

//    private int buildFieldCondition(Connection db, List<DbParameter> parameters, int paramNum, boolean ignoreAlias, StringBuilder conditionBuilder, FilterCondition filterItem) {
    private int buildFieldCondition(int paramNum, boolean ignoreAlias, StringBuilder conditionBuilder, FilterContext filterContext) {
        String columnName = "";
        if (filterContext.getFilterCondition().getFilterField() != null && filterContext.getFilterCondition().getFilterField().isEmpty() == false)
            columnName = trans2DbColumnWithAlias(filterContext.getFilterCondition().getFilterField(), ignoreAlias);

        GspDbDataType dataType = getDataType(filterContext.getFilterCondition().getFilterField());
        RefObject<Integer> tempRef_paramNum = new RefObject<Integer>(paramNum);
        filterContext.setDbColumnName(columnName);
        filterContext.setDataType(dataType);
        filterContext.setParamNum(tempRef_paramNum);
        filterContext.setProcesser(getTypeTransProcesser(filterContext.getFilterCondition().getFilterField()));
        conditionBuilder.append(FilterUtil.parseFilterCondition(filterContext));
        paramNum = tempRef_paramNum.argvalue;
        return paramNum;
    }

    protected EntityDac getEntityDac() {
        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5021);
    }

    private int buildChildNodeFieldCondition(int paramNum, boolean ignoreAlias, StringBuilder conditionBuilder, FilterContext filterContext) {
        EntityRelationalAdaptor childAdaptor = this.getEntityDac().getChildEntityDac(filterContext.getFilterCondition().getFilterNode()).getEntityAdaptor();
        FilterCondition newCondition = new FilterCondition();
        newCondition.setlBracketCount(filterContext.getFilterCondition().getLBracketCount());
        newCondition.setRelation(filterContext.getFilterCondition().getRelation());
        newCondition.setRBracketCount(filterContext.getFilterCondition().getRBracketCount());
        newCondition.setFilterField(getPrimaryKey());
        newCondition.setCompare(ExpressCompareType.In);
        newCondition.setExpresstype(ExpressValueType.Value);

        RefObject<Integer> tempRef_paramNum = new RefObject<Integer>(paramNum);
        newCondition.setValue(childAdaptor.buildParentIdInChildQueryExpression(tempRef_paramNum, ignoreAlias, filterContext));
        FilterContext fc = new FilterContext();
        fc.setParameters(filterContext.getParameters());
        fc.setFilterCondition(newCondition);
        fc.setUseNumberBinding(filterContext.isUseNumberBinding());
        buildFieldCondition(tempRef_paramNum.argvalue, ignoreAlias, conditionBuilder, fc);
        paramNum = tempRef_paramNum.argvalue;
        return paramNum;
    }

//    private DbColumnInfo getParentIdColumn() {
//        for (DbColumnInfo dbColumnInfo : getContainColumns()) {
//            if (dbColumnInfo.getIsParentId())
//                return dbColumnInfo;
//        }
//        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5022);
//    }

    private String buildParentIdInChildQueryExpression(RefObject<Integer> tempRef_paramNum, boolean ignoreAlias, FilterContext filterContext) {
        String parentIdColumn = getParentIdColumnInfo().getColumnName();

        StringBuilder stringBuilder = new StringBuilder();
        //第一次查询后，列集合中就会有带出字段列信息
        DbColumnInfo baseColumnInfo = getContainColumns().getItem(filterContext.getFilterCondition().getFilterField());
        String tableName = getTableName();
        if (baseColumnInfo != null && baseColumnInfo.getIsAssociateRefElement()) {
            tableName = getAssoTableName(baseColumnInfo.getColumnName());
        }
        DbColumnInfo extendColumnInfo = extendContainColumns.getItem(filterContext.getFilterCondition().getFilterField());
        if (extendColumnInfo != null && extendColumnInfo.getIsAssociateRefElement()) {
            tableName = getAssoTableName(extendColumnInfo.getColumnName());
        }
        if (baseColumnInfo == null && extendColumnInfo == null) {
            initQueryTableName();
            tableName = getAssoTableName(filterContext.getFilterCondition().getFilterField());
        }
        stringBuilder.append(" SELECT ").append(getTableAlias() + "." + parentIdColumn).append(" from ").append(tableName).append(" where ");

        filterContext.getFilterCondition().setRelation(ExpressRelationType.Empty);
        filterContext.getFilterCondition().setlBracketCount(0);
        filterContext.getFilterCondition().setRBracketCount(0);

        FilterContext fc = new FilterContext();
        fc.setParameters(filterContext.getParameters());
        fc.setFilterCondition(filterContext.getFilterCondition());
        fc.setUseNumberBinding(filterContext.isUseNumberBinding());
        tempRef_paramNum.argvalue = buildFieldCondition(tempRef_paramNum.argvalue, ignoreAlias, stringBuilder, fc);
        return stringBuilder.toString();
    }

    /*
    获取关联带出字段对应查询表名
     */
    private String getAssoTableName(String assoColumnName) {
        String tableName = "";
        ArrayList<String> columns = new ArrayList<>();
        columns.add(assoColumnName);
        ArrayList<AssociationInfo> associationInfos = getAssociationInfos(columns);
        tableName = getTableNamesWithAssociationInfo(associationInfos, null);
        return tableName;
    }

    protected final String buildModifyValue(Map<String, Object> propertyChanges, Set<String> namesOfMultiLangColumns,
                                    RefObject<List<DbParameter>> parameters, DacSaveContext ctx) {
        StringBuilder modifyValue = new StringBuilder();
        int paramNum = parameters.argvalue.size();

        for (Map.Entry<String, Object> propertyChange : propertyChanges.entrySet()) {
            //非值对象的处理
            if (!(propertyChange.getValue() instanceof ValueObjModifyChangeDetail)) {
                //增加了从扩展列集合获取的逻辑
                DbColumnInfo dbColumnInfo = getContainColumns().getItem(propertyChange.getKey());
                //这个地方是不是得加上 扩展的列集合
                if (dbColumnInfo == null || dbColumnInfo.isVirtual()) {
                    continue;
                }
                //获取字段，可从dbColumnInf上直接取
                //String dbCol = trans2DbColumn(propertyChange.getKey());
                String dbCol = dbColumnInfo.getRealColumnName();

                // 多语变更集中已经包含，则普通变更集不再包含
                if (namesOfMultiLangColumns.contains(dbColumnInfo.getColumnName())) {
                    continue;
                }

                if (modifyValue.length() > 0) {
                    modifyValue.append(" , ");
                }
                modifyValue.append(KeyWordsManager.getColumnAlias(dbCol) + " = ?");
                paramNum++;
                Object value = null;
                try{
                    value = dbColumnInfo.getTypeTransProcesser().transType(propertyChange.getValue());
                }
                catch (Exception ex){
                    throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5081, ex, ExceptionLevel.Error, false, getTableAlias(), dbColumnInfo.getColumnName(), propertyChange.getValue() == null ? "null" : propertyChange.getValue().toString());
                }

                parameters.argvalue.add(buildParam(propertyChange.getKey(), dbColumnInfo.getColumnType(), getPropertyChangeValue(propertyChange.getKey(), value)));
            } else {
                //值对象的处理
                paramNum = dealUdtModifyValue(namesOfMultiLangColumns,propertyChange.getKey(), propertyChange.getValue(), paramNum, modifyValue, parameters.argvalue, null);
            }
        }

        for (AbstractDataAdapterExtendInfo info : getDataAdapterExtendInfos()) {
            info.buildModifyValues(modifyValue, propertyChanges, parameters);
        }
        return modifyValue.toString();
    }

    protected final int dealUdtModifyValue(Set<String> namesOfMultiLangColumns,String propertyName, Object propertyValue, int paramNum, StringBuilder modifyValue, List<DbParameter> parameters, String multiSuffix) {
        ArrayList<DbColumnInfo> columns = getUdtColumnInfos(propertyName);
        for (int i = 0; i < columns.size(); i++) {
            DbColumnInfo column = columns.get(i);
            if (column.getIsAssociateRefElement()) {
                continue;
            }
            String dbCol = column.getDbColumnName();
            if (column.getIsMultiLang()) {
                dbCol = dbCol + "@Language@";
            }
            if (multiSuffix != null && !"".equals(multiSuffix))
                dbCol = RepositoryUtil
                        .FormatMuliLangColumnName(dbCol, RepositoryUtil.getLanguage(multiSuffix));
            else if (column.getIsMultiLang()) {
                // 多语变更集中已经包含，则普通变更集不再包含
                if (parameters.size() != 0 && namesOfMultiLangColumns.contains(column.getColumnName())) {
                    continue;
                }
            }

            if (i > 0) {
                modifyValue.append(" , ");
            } else if (paramNum > 0 && modifyValue.length() > 0) {
                modifyValue.append(" , ");
            }
            //modifyValue.append(dbCol + " = ?" + (paramNum++));
            modifyValue.append(dbCol + " = ?");
            paramNum++;
            parameters.add(buildParam(dbCol, column.getColumnType(), getPropertyChangeValue(column.getColumnName(), propertyValue)));
        }

        return paramNum;
    }

    /**
     * 根据变更集的多语列信息，进行参数化处理
     * @param infos
     * @param namesOfMultiLangColumns 多语列的ColumnName
     * @param parameters
     * @return
     */
    protected final String buildModifyValue_MultiLanguage(Map<String, MultiLanguageInfo> infos, Set<String> namesOfMultiLangColumns,
                                                  RefObject<List<DbParameter>> parameters) {
        if (parameters.argvalue == null) {
            parameters.argvalue = new ArrayList<DbParameter>();
        }
        StringBuilder modifyValue = new StringBuilder();
        int paramNum = parameters.argvalue.size();

        for (Map.Entry<String, MultiLanguageInfo> entrySet : infos.entrySet()) {
            String propName = entrySet.getKey().split(MultiLanguageInfo.MULTILANGUAGETOKEN)[0];
            //todo columnInfo2查询了扩展列，columnInfo没有查？？后续检查逻辑
            DbColumnInfo columnInfo = getColumnInfoByColumnNameName(propName);
            if (columnInfo==null || columnInfo.isVirtual()) {
                continue;
            }
            String dbCol = columnInfo.getRealColumnName();// trans2DbColumn(propName);
            //将columnName加入国际化列，供后续环节使用
            namesOfMultiLangColumns.add(columnInfo.getColumnName());

            for (Map.Entry<String, Object> item : entrySet.getValue().getPropValueMap()
                    .entrySet()) {
                String languageCode = item.getKey();
                Object value = item.getValue();
                if (!(value instanceof ValueObjModifyChangeDetail)) {
                    String columnName = RepositoryUtil.FormatMuliLangColumnName(dbCol, RepositoryUtil.getLanguage(languageCode));
                    if (modifyValue.length() > 0) {
                        modifyValue.append(" , ");
                    }

                    //modifyValue.append(columnName + " = ?" + (paramNum++));
                    modifyValue.append(columnName + " = ?");
                    paramNum++;

                    parameters.argvalue.add(
                            buildParam(columnName, columnInfo.getColumnType(), value)
                    );
                } else {
                    paramNum = dealUdtModifyValue(namesOfMultiLangColumns, propName, value, paramNum, modifyValue, parameters.argvalue, languageCode);
                }
            }
        }
        return modifyValue.toString();
    }


    private ArrayList<DbColumnInfo> getUdtColumnInfos(String belongElementLabel) {
        ArrayList<DbColumnInfo> infos = new ArrayList<DbColumnInfo>();
        for (DbColumnInfo containColumn : getContainColumns()) {
            if (belongElementLabel.equals(containColumn.getBelongElementLabel())) {
                infos.add(containColumn);
            }
        }

        return infos;
    }

    /**
     * 从变更集中读取变更值
     * @param key 字段名称
     * @param value 待转换的值 对于修改，传入的是变更集的结构
     * @return 持久化层参数绑定值 buildParam使用
     */
    protected abstract Object getPropertyChangeValue(String key, Object value);

    protected GspDbDataType getDataType(String key) {
        DbColumnInfo columnInfo = getColumnInfoByColumnNameName(key);
        if (columnInfo == null)
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5023, false, getTableAlias(), key);
        return columnInfo.getColumnType();
    }

    private DbColumnInfo getColumnInfoByColumnNameName(String columnName) {
        DbColumnInfoCollection dbColumnInfoCollection = getContainColumns();
        DbColumnInfo columnInfo = dbColumnInfoCollection.getItem(columnName);
        if(columnInfo!=null)
            return columnInfo;

        for (AbstractDataAdapterExtendInfo extendInfo : getDataAdapterExtendInfos()) {
            columnInfo = extendInfo.tryGetColumnInfoByPropertyName(columnName);
            if (columnInfo != null)
                return columnInfo;
        }
        if (this.extendContainColumns != null && this.extendContainColumns.getCount() > 0) {
            for (DbColumnInfo item : this.extendContainColumns) {
                if (item.getColumnName().toLowerCase().equals(columnName.toLowerCase())) {
                    return item;
                }
            }
        }
        //throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5024, columnName);
        return null;
    }

    protected String trans2DbColumn(String key) {
        for (DbColumnInfo info : getContainColumns()) {
            if (info.getColumnName().equals(key)) {
                return info.getRealColumnName();
            }
        }
        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5023, false, getTableAlias(), key);
    }

    private String trans2DbColumnWithAlias(String filedName) {
        return trans2DbColumnWithAlias(filedName, false);
    }

    protected String trans2DbColumnWithAlias(String filedName, boolean ignoreAlias) {
        //这个代码会兼容 xx.xx的场景。。。
        filedName = DatabaseUtil.getColumnName(filedName);
        if (!getContainColumns().contains(filedName) && (extendContainColumns == null || !extendContainColumns.contains(filedName))) {
            StringBuilder sb = new StringBuilder();
            sb.append("当前BE(v1):" + getWrappedTableAlias() + " configId:" + getConfigId());
            sb.append("包含列信息{{");
            for (DbColumnInfo dbColumnInfo : getContainColumns()) {
                sb.append(dbColumnInfo.getColumnName() + ",");
            }
            sb.append("}}");
            logger.error("未找到字段" + filedName + "，请确认字段名称是否输入正确，或关联引用字段是否已经获取");
            logger.error(sb.toString());
            //再换种方式比较一次？
            for (DbColumnInfo columnInfo : getContainColumns()) {
                if (columnInfo.getColumnName().toLowerCase().equals(filedName.toLowerCase())) {
                    logger.error("找到字段" + filedName + "");
                }
            }
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5027, false, getTableAlias(), filedName);
        }
        DbColumnInfo columnInfo = getContainColumns().getItem(filedName);
        if (columnInfo == null)
            columnInfo = extendContainColumns.getItem(filedName);
        if (columnInfo.getIsAssociateRefElement()) {
            return getAssociateDbColumnName(columnInfo);
        }
        //return assColumns.ContainsKey(filedName) ? assColumns[filedName] : throw new Exception("Error");
        String dbName = ignoreAlias ? columnInfo.getDbColumnName() : getWrappedTableAlias() + "." + KeyWordsManager.getColumnAlias(columnInfo.getDbColumnName());
        if (columnInfo.getIsMultiLang()) {
            dbName = dbName + "@Language@";
        }
        return dbName;
    }

    protected String getAssociateDbColumnName(DbColumnInfo columnInfo) {
        if (columnInfo.getDbColumnName() == null || columnInfo.getDbColumnName().length() == 0) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5025, false, getTableAlias(), columnInfo.getColumnName(), getConfigId());
        }
        return columnInfo.getDbColumnName();
    }

    protected abstract String getGetDataByIdsSql();

    @Override
    public List<IEntityData> getDataByIDs(List<String> dataIds) {
        return getDataByIds(dataIds, null);
    }

    public List<IEntityData> getDataByIds(List<String> dataIds, EntityFilter nodeFilter) {
        return getDataByIds(dataIds, nodeFilter, null);
    }

    /**
     * 批量检索
     * @param dataIds
     * @param nodeFilter
     * @param retrieveFilter
     * @return
     */
    public List<IEntityData> getDataByIds(List<String> dataIds, EntityFilter nodeFilter, RetrieveFilter retrieveFilter) {
        initQueryTableName();
        for (String item : dataIds) {
            FilterUtil.checkInParameterForSqlInjection(item);
        }
        String tableName = getQueryTableNameWithAuthority(null, nodeFilter);
        String fields = getQueryFields(nodeFilter);

        //每批次最大个数
        int maxBatchSize = 5000;
        //总批次数
        int totalBatches = 1;
        if(dataIds.size() > maxBatchSize){
            //Math.ceil向上取整
            totalBatches =  (int) Math.ceil((double) dataIds.size() / maxBatchSize);
        }
        List<IEntityData> entityDatas = new ArrayList<>();
        for(int i=0; i< totalBatches; i++){
            List<DbParameter> parameters = new ArrayList<>();
            String querySql = String.format("select %1$s from %2$s ", fields, tableName);
            int startIndex = maxBatchSize*i;
            int endIndex = maxBatchSize*(i+1);
            if(endIndex > dataIds.size()){
                endIndex = dataIds.size();
            }
            //subList是左闭右开，子列表包含起始位置，不包含结束位置
            String externalCondition = getPKInCondition(parameters, dataIds.subList(startIndex, endIndex));
            if (externalCondition.startsWith(" AND ")) {
                externalCondition = externalCondition.substring(4);
            }
            querySql= formatFiscalAndMultiLang(querySql, retrieveFilter);
            querySql = querySql.concat(" where ").concat(externalCondition);
            List<IEntityData> datas = this.getDatas(querySql, parameters, new AdaptorRetrieveParam(null));
            entityDatas.addAll(datas);
        }

        return entityDatas;
    }

    /**
     * 获取主键的in条件片段
     * @param parameters
     * @param dataIds
     * @return
     */
    private String getPKInCondition(List<DbParameter> parameters, List<String> dataIds){
        FilterCondition filterCondition = new FilterCondition();
        DbColumnInfo idColumn = getContainColumns().getPrimaryKey();
        filterCondition.setFilterField(idColumn.getColumnName());
        filterCondition.setCompare(ExpressCompareType.In);
        filterCondition.setInValues(dataIds);
        filterCondition.setRelation(ExpressRelationType.Empty);
        List<FilterCondition> filterConditions = new ArrayList<>();
        filterConditions.add(filterCondition);
        String externalCondition = parseFilterCondition(filterConditions, parameters, parameters.size(), false, true);
        return externalCondition;
    }

    protected abstract String getGetDataByIdSql();

    @Override
    public IEntityData getDataByID(String dataId) {
        return getDataByID(dataId, null);
    }

    public IEntityData getDataByID(String dataId, EntityFilter filter) {
        RetrieveFilter rtFilter = new RetrieveFilter();
        rtFilter.getNodeFilters().put(getNodeCode(), filter);
        return this.getDataByIDWithPara(dataId, new AdaptorRetrieveParam(rtFilter));
    }

    /**
     * 检索
     * @param dataId
     * @param adaptorRetrieveParam
     * @return
     */
    public IEntityData getDataByIDWithPara(String dataId, AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam.getEntityfilter(getNodeCode());
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().getFilterFields() != null) {
            if (!StringUtils.isNullOrEmpty(getVersionControlPropName()) && !filter.getFieldsFilter().getFilterFields().contains(getVersionControlPropName()))
                filter.getFieldsFilter().getFilterFields().add(getVersionControlPropName());
            if (!StringUtils.isNullOrEmpty(getPrimaryKey()) && !filter.getFieldsFilter().getFilterFields().contains(getPrimaryKey()))
                filter.getFieldsFilter().getFilterFields().add(getPrimaryKey());
        }

        initQueryTableName();
        String tableName = getQueryTableNameWithAuthority(null, adaptorRetrieveParam != null ? adaptorRetrieveParam.getEntityfilter(getNodeCode()) : null);
        String fields = getQueryFields(adaptorRetrieveParam);
        // 赋值全局变量，当前需查询的所有字段，包含多语列
        queryFields = fields;
        String sql = String.format(getGetDataByIdSql(), fields, tableName, "?0 ");

        try {
            List<DbParameter> list = new ArrayList<DbParameter>();
            list.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), dataId));
            if (getLogicDeleteInfo().isEnableLogicDelete()) {
                sql += String.format(" and %1$s =?1 ", getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId());
                list.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
            }
            RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
            return innerGetData(formatMultiLang(sql, retrieveFilter), list, adaptorRetrieveParam);
        } catch (RuntimeException ex) {
            throw new CefRunTimeException(buildMessage(ex.getMessage()), ex);
        }
    }

    public final void generateTable() {
        //getDboService().deployTempTable(getDboID());
    }

    public TempTableContext createTempTable() {
        return getDboService().creatTempTables(getDboID());
    }

    public final void dropTable() {
//		Java版临时注释
//		ServiceManager.<IDatabaseObjectDeployService>GetService().DropTempTable(getDboID());
    }

    public void dropTempTable(TempTableContext tempTableContext) {
        getDboService().dropTempTable(tempTableContext, getDboID());
    }

    private AdapterUQChecker getAdapterUQChecker(boolean throwEx) {
        return new AdapterUQChecker(this, throwEx);
    }

    /**
     * 每一个UQConstraintMediate构造一个SQL
     *
     * @param mediates
     */
    public final void checkUniqueness(ArrayList<UQConstraintMediate> mediates) {
        checkUniqueness(mediates, true);
    }

    public final void checkUniqueness(ArrayList<UQConstraintMediate> mediates, boolean throwEx) {
        getAdapterUQChecker(throwEx).checkUniqueness(mediates);
    }

    protected IEntityData innerGetData(String sql, List<DbParameter> parameters, AdaptorRetrieveParam param) {
        ArrayList<IEntityData> datas = getDatas(sql, parameters, param);
        return datas.stream().findFirst().orElse(null);
    }

    public abstract String getParentJoin();

    public boolean isAssoParentPrimaryKey() {
        return true;
    }

    protected final void addColumn(String columnName, String dbColumnName, GspDbDataType columnType, int length, int precision, Object defaultValue
            , ITypeTransProcesser typeTransProcesser, boolean isPrimaryKey, boolean isAssociateRefElement, boolean isMultiLang, boolean isUdt
            , boolean isAssociation, boolean isEnum, String belongElementLabel, boolean isParentId) {
        DbColumnInfo tempVar = new DbColumnInfo();
        tempVar.setColumnName(columnName);
        tempVar.setDbColumnName(isAssociateRefElement ? "" : dbColumnName);
        tempVar.setColumnType(columnType);
        tempVar.setLength(length);
        tempVar.setPrecision(precision);
        tempVar.setDefaultValue(defaultValue);
        tempVar.setIsPrimaryKey(isPrimaryKey);
        tempVar.setIsAssociateRefElement(isAssociateRefElement);
        tempVar.setIsMultiLang(isMultiLang);
        tempVar.setIsParentId(isParentId);
        tempVar.setIsUdtElement(isUdt);
        tempVar.setIsAssociation(isAssociation);
        tempVar.setIsEnum(isEnum);
        tempVar.setBelongElementLabel(belongElementLabel);
        tempVar.setTypeTransProcesser(typeTransProcesser);
        getContainColumns().add(tempVar);
    }

    protected boolean getHasMultiLangCol() {
        if (hasMultiLangCol == null) {
            for (DbColumnInfo col : getContainColumns()) {
                if (col.getIsMultiLang()) {
                    hasMultiLangCol = true;
                    break;
                }
            }
            return hasMultiLangCol;
        }
        return false;
    }

    protected Query buildQueryManager(String sqlText, List<DbParameter> parameters) {
        DataValidator.checkForEmptyString(sqlText, "sqlText");
        Query query = this.getEntityManager().createNativeQuery(sqlText);
        if (parameters != null) {
            for (int i = 0; i < parameters.size(); i++) {
                DbParameter param = parameters.get(i);
                switch (param.getDataType()) {
                    case VarChar:
                    case NChar:
                    case Blob:
                    case Boolean:
                        query.setParameter(i, param.getValue());
                        break;
                    case NVarChar:
                        buildNCharParmeter(query, i, param);
                        break;
                    case Clob:
                        //TODO 后续测试DM数据库是否合适
                        query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
                        break;
                    case SmallInt:{
                        buildSmallIntParmeter(query, i, param);
                        break;
                    }
                    case Int:
                        buildIntParmeter(query, i ,param);
                        break;
                    case Long:
                        buildLongParmeter(query, i ,param);
                        break;
                    case Decimal:
                        query.setParameter(i, new TypedParameterValue(StandardBasicTypes.BIG_DECIMAL, param.getValue()));
                        break;
                    case Char:
                        if (param.getValue() == null) {
                            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.STRING, param.getValue()));

                        } else {
                            query.setParameter(i, param.getValue());
                        }
                        break;
                    case NClob:
                        if (CAFContext.current.getDbType() == DbType.Oracle || CAFContext.current.getDbType() == DbType.SQLServer) {
                            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.NTEXT, param.getValue()));
                        } else {
                            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
                        }
                        break;
                    case DateTime:
                        if (CAFContext.current.getDbType() == DbType.Oscar) {
                            if (param.getValue() == null) {
                                query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                            } else {
                                query.setParameter(i, Date.from(((Date) param.getValue()).toInstant()), TemporalType.DATE);
                            }
                            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                        } else if (CAFContext.current.getDbType() == DbType.OceanBase) {
                            if (param.getValue() == null) {
                                query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                            } else {
                                if (param.getValue() instanceof Date) {
                                    query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                                } else {
                                    query.setParameter(i, new TypedParameterValue(StandardBasicTypes.TEXT, param.getValue()));
                                }
                            }
                        } else {
                            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                        }
                        break;
                    case Date:
                        if (param.getValue() == null) {
                            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                            break;
                        }
                        if (CAFContext.current.getDbType() == DbType.OceanBase) {
                            query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
                        } else {
                            java.sql.Date date = new java.sql.Date(((Date) param.getValue()).getTime());
                            date = java.sql.Date.valueOf(date.toString());
                            query.setParameter(i, date, TemporalType.DATE);
                        }
                        break;
                    default:
                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5018);
                }
            }
        }
        return query;
    }

    protected void buildNCharParmeter(Query query, int i, DbParameter param) {
        if (CAFContext.current.getDbType() == DbType.Oracle || CAFContext.current.getDbType() == DbType.SQLServer) {//其余的驱动可能没有实现对应参数绑定方法
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.NSTRING, param.getValue()));
        } else {
            query.setParameter(i, param.getValue());
        }
    }

    /**
     * 构造SmallInt绑定参数
     * @param query
     * @param i
     * @param param
     */
    protected void buildSmallIntParmeter(Query query, int i, DbParameter param) {
        if (param.getValue() != null && param.getValue() instanceof String) {
            Object value = Short.parseShort(param.getValue().toString());
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.SHORT, value));
        } else if (param.getValue() instanceof Collection) {
            ArrayList<Short> parseResult = new ArrayList<>(((Collection<?>) param.getValue()).size());
            ((Collection<?>) param.getValue()).forEach(item -> {
                //in的参数，当ID作为条件时，会传入String数组，需要做转换
                if (item == null) {
                    parseResult.add(null);
                } else if (item instanceof String) {
                    parseResult.add(Short.parseShort(item.toString()));
                } else if (item instanceof Short) {
                    parseResult.add((Short) item);
                }  else if (item instanceof Integer) {
                    parseResult.add(((Integer)item).shortValue());
                }else {
                    throw new IllegalArgumentException(item.getClass().getName());
                }
            });
            query.setParameter(i, parseResult);
        } else {
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.SHORT, param.getValue()));
        }
    }

    /**
     * 构造Integer类型绑定参数
     * @param query
     * @param i
     * @param param
     */
    protected void buildIntParmeter(Query query, int i, DbParameter param) {
        if (param.getValue() != null && param.getValue() instanceof String) {
            Object value = Integer.parseInt(param.getValue().toString());
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.INTEGER, value));
        } else if (param.getValue() instanceof Collection) {
            ArrayList<Integer> parseResult = new ArrayList<>(((Collection<?>) param.getValue()).size());
            ((Collection<?>) param.getValue()).forEach(item -> {
                //in的参数，当ID作为条件时，会传入String数组，需要做转换
                if (item == null) {
                    parseResult.add(null);
                } else if (item instanceof String) {
                    parseResult.add(Integer.parseInt(item.toString()));
                } else if (item instanceof Integer) {
                    parseResult.add((Integer) item);
                } else {
                    throw new IllegalArgumentException(item.getClass().getName());
                }
            });
            query.setParameter(i, parseResult);
        } else {
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.INTEGER, param.getValue()));
        }
    }

    /**
     * 构造Long绑定参数
     * @param query
     * @param i
     * @param param
     */
    protected void buildLongParmeter(Query query, int i, DbParameter param) {
        if (param.getValue() == null) {
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.LONG, param.getValue()));
        } else if (param.getValue() instanceof String) {
            Object value = Long.parseLong(param.getValue().toString());
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.LONG, value));
        } else if (param.getValue() instanceof Collection) {
            ArrayList<Long> parseResult = new ArrayList<>(((Collection<?>) param.getValue()).size());
            ((Collection<?>) param.getValue()).forEach(item -> {
                //in的参数，当ID作为条件时，会传入String数组，需要做转换
                if (item == null) {
                    parseResult.add(null);
                } else if (item instanceof String) {
                    parseResult.add(Long.parseLong(item.toString()));
                } else if (item instanceof Long) {
                    parseResult.add((Long) item);
                } else {
                    throw new IllegalArgumentException(item.getClass().getName());
                }
            });
            query.setParameter(i, parseResult);
        } else {
            // be未支持long时，代码里有将dbo的long、int都当GspDbDataType.int处理，且产品有手改dbo（be中是整数dbo中是LongInt），未读写超过int最大值是程序正常
            // 支持long后，历史兼容，会有int类型的值传过来，需要做兼容处理
            query.setParameter(i, new TypedParameterValue(StandardBasicTypes.LONG, Long.parseLong(param.getValue().toString())));
        }
    }

    @Transactional
    public int executeSql(String sqlText, List<DbParameter> parameters) {

        Query query = buildQueryManager(sqlText, parameters);
        return query.executeUpdate();
    }

    protected Object executeScelar(String sqlText, List<DbParameter> parameters) {
        DataValidator.checkForEmptyString(sqlText, "sqlText");

        Query query = buildQueryManager(sqlText, parameters);
        List<Object[]> resultSet = query.getResultList();
        return resultSet.get(0)[0];
    }

    public final DbParameter buildParam(String paramName, GspDbDataType dataType, int size, Object paramValue) {
        return new DbParameter(paramName, dataType, paramValue);
    }

    public DbParameter buildParam(String paramName, GspDbDataType dataType, Object paramValue) {
        return FilterUtil.buildParam(paramName, dataType, paramValue);
    }

    protected final String formatFiscalAndMultiLang(String sql) {
        //if (EnableFiscal)
        //    sql = FormatFiscal(sql);
        //if (HasMultiLangCol)
        sql = formatMultiLang(sql);
        return sql;
    }

    protected final String formatFiscalAndMultiLang(String sql, RetrieveFilter retrieveFilter) {
        //if (EnableFiscal)
        //    sql = FormatFiscal(sql);
        //if (HasMultiLangCol)
        sql = formatMultiLang(sql, retrieveFilter);
        return sql;
    }

    protected final String formatFiscalAndMultiLang(String sql, DacSaveContext batcher) {
        sql = RepositoryUtil.FormatMuliLang(sql, batcher.getFieldSuffix());
        return sql;
    }

    private String formatMultiLang(String sql) {
        return RepositoryUtil.FormatMuliLang(sql);
    }

    protected String formatMultiLang(String sql, RetrieveFilter retrieveFilter) {
        return RepositoryUtil.FormatMuliLang(sql, retrieveFilter);
    }

    protected abstract String getJoinTableName();

    @Override
    public String getTableNameByColumns(HashMap<String, String> columns, String keyColumnName, RefObject<String> keyDbColumnName, ArrayList<String> tableAlias) {
        //跟之前一致。
        ArrayList<String> keys1 = new ArrayList<>();
        for (String key : columns.keySet()) {
            keys1.add(key);
        }
        ArrayList<AssociationInfo> associations = getAssociationInfos(keys1);
        String tableName = getTableNamesWithAssociationInfo(associations, tableAlias);
        keyDbColumnName.argvalue = trans2DbColumnWithAlias(keyColumnName);

        String[] keys = keys1.toArray(new String[]{});
        for (int i = 0; i < keys.length; i++) {
            columns.put(keys[i], trans2DbColumnWithAlias(keys[i]));
        }
        return tableName;
    }

    /**
     * 根据关联带出字段继续向下查找，拼接表名
     *
     * @param columns
     * @param keyColumnName
     * @param keyDbColumnName
     * @param tableAlias
     * @return
     */
    @Override
    public String getTableNameByRefColumns(HashMap<String, RefColumnInfo> columns, String keyColumnName, RefObject<String> keyDbColumnName, ArrayList<String> tableAlias) {
        //跟之前一致。
        ArrayList<AssociationInfo> associations = getAssociationInfos(columns);
        String tableName = getTableNamesWithAssociationInfo(associations, tableAlias);//只根据 指定的关联字段拼接表名
        keyDbColumnName.argvalue = trans2DbColumnWithAlias(keyColumnName);

        List<String> keys = new ArrayList<String>();
        for (String key : columns.keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            DbColumnInfo dbColumnInfo = getContainColumns().getItem(keys.get(i));
            RefColumnInfo tempVar = new RefColumnInfo();
            tempVar.setColumnName(trans2DbColumnWithAlias(keys.get(i)));
            tempVar.setTransProcesser(dbColumnInfo.getTypeTransProcesser());
            tempVar.setColumnType(dbColumnInfo.getColumnType());
            tempVar.setFieldReposExtendConfigClassImpl(dbColumnInfo.getFieldReposExtendConfigClassImpl());
            tempVar.setFieldReposExtendConfigId(dbColumnInfo.getFieldReposExtendConfigId());
            tempVar.setVirtual(dbColumnInfo.isVirtual());
            columns.put(keys.get(i), tempVar);
        }
        return tableName;
        //return tableName;
    }

    public String getTableNameByRefColumns(TableNameRefContext tableNameRefContext) {
        //如果关联字段指定别名，则使用指定的别名(IDP在设置关联的时候可以指定别名，避免关联条件的别名不匹配)
        if (!StringUtils.isNullOrEmpty(tableNameRefContext.getRefTableAlias())) {
            setTableAlias(tableNameRefContext.getRefTableAlias());
        }
        //获取带出字段对应的关联信息
        // 如果带出字段是关联字段或者多值UDT，将带出字段信息进行补充。
        ArrayList<AssociationInfo> associations = getAssociationInfos(tableNameRefContext.getColumns());
        String tableName = getTableNamesWithAssociationInfo(associations, tableNameRefContext.getTableAlias());//只根据 指定的关联字段拼接表名
        //获取表名后再执行一次获取关联方法，嵌套的带出字段就可以被传递到外层
        handleAssociationInfo(tableNameRefContext.getColumns());
        tableNameRefContext.getKeyDbColumnName().argvalue = trans2DbColumnWithAlias(tableNameRefContext.getKeyColumnName());

        List<String> keys = new ArrayList<String>();
        for (String key : tableNameRefContext.getColumns().keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            RefColumnInfo tempVar = new RefColumnInfo();
            tempVar.setColumnName(trans2DbColumnWithAlias(keys.get(i)));
            DbColumnInfo dbColumnInfo = getContainColumns().getItem(keys.get(i));
            tempVar.setTransProcesser(dbColumnInfo.getTypeTransProcesser());
            tempVar.setColumnType(dbColumnInfo.getColumnType());
            tempVar.setFieldReposExtendConfigClassImpl(dbColumnInfo.getFieldReposExtendConfigClassImpl());
            tempVar.setFieldReposExtendConfigId(dbColumnInfo.getFieldReposExtendConfigId());
            tempVar.setVirtual(dbColumnInfo.isVirtual());
            tableNameRefContext.getColumns().put(keys.get(i), tempVar);
        }
        return tableName;
        //return tableName;
    }


    ///#region GetJoinTable
    public final String getJoinTableName(ArrayList<String> columns, ArrayList<String> tableAlias, String currentAlias) {
        HashMap<AssociationInfo, ArrayList<String>> assos = getAssoInfos(columns);

        StringBuilder queryTables = new StringBuilder();
        for (Map.Entry<AssociationInfo, ArrayList<String>> assoItem : assos.entrySet()) {
            AssociationInfo associationInfo = assoItem.getKey();
            ArrayList<String> currentColumns = assoItem.getValue();
            HashMap<String, RefColumnInfo> associationColumns = new HashMap<String, RefColumnInfo>();
            for (String item : currentColumns) {
                associationColumns.put(associationInfo.getRefColumns().get(item), new RefColumnInfo());
            }

            if (getConfigId().equals(associationInfo.getConfigId())) {
                queryTables.append(getCurrentAssoTableName(associationInfo, tableAlias, associationColumns));
                continue;
            }
            String targetDbColumnName = "";
            associationInfo.getRefRepository().initParams(getPars());
            associationInfo.getRefRepository().initRepoVariables(getVars());
            RefObject<String> tempRef_targetDbColumnName = new RefObject<String>(targetDbColumnName);
            //根据关联带出字段继续向下查找，拼接表名
            String tableName = associationInfo.getRefRepository().getTableNameByRefColumns(associationInfo.getNodeCode(), associationColumns, associationInfo.getTargetColumn(), tempRef_targetDbColumnName, tableAlias);
            targetDbColumnName = tempRef_targetDbColumnName.argvalue;

            //根据返回信息，添加aataColumns信息
            initAssociateColumnInfo(associationInfo, associationColumns);

            String sourceColumnName = currentAlias + "." + trans2DbColumn(associationInfo.getSourceColumn());
            queryTables.append(String.format(getJoinTableName(), tableName, sourceColumnName, targetDbColumnName));
        }

        return queryTables.toString();

    }

    protected final HashMap<AssociationInfo, ArrayList<String>> getAssoInfos(ArrayList<String> columns) {
        HashMap<AssociationInfo, ArrayList<String>> assos = new HashMap<AssociationInfo, ArrayList<String>>();
        for (String column : columns) {
            AssociationInfo info = findInAssociationInfos(column);
            if (!assos.containsKey(info)) {
                assos.put(info, new ArrayList<String>());
            }
            assos.get(info).add(column);
        }

        return assos;
    }

    public CefEntityResInfoImpl getEntityResInfo() {
        return null;
    }

    /**
     * 获取带出字段对应的关联信息
     * 1.如果是普通字段，不处理
     * 2.如果是关联字段，返回关联信息（包含关联UDT），同时将带出字段添加到columnInfos中，返回到外层
     * 3.如果是关联带出字段，返回对应关联信息，不添加其他带出字段到columnInfos中
     * 4.如果是多值多列UDT，需要将列信息添加到columnInfos中，返回到外层
     * @param columnInfos
     * @return
     */
    protected ArrayList<AssociationInfo> getAssociationInfos(HashMap<String, RefColumnInfo> columnInfos) {
        ArrayList<String> columns = new ArrayList();
        for (String item : columnInfos.keySet()) {
            columns.add(item);
        }
        ArrayList<AssociationInfo> associations = new ArrayList();
        List<String> tempList = new ArrayList<>();
        for (String columnItem : columns) {
            //判断是否带出字段或者多值多列UDT字段
            //如果是多值多列字段，将多列字段包含的明细字段添加到columnInfos中，后续返回对应的数据库列等信息
            if (isRefelementfOMulitColumnUdt(columnItem)) {
                buildRefOrMultiColumnUdtAssInfo(columnItem, columnInfos, associations);
            } //如果带出字段是关联字段,这两个判断都是循环集合，后续优化可以提前取出来
            else if (isAssociationElement(columnItem)) {
                AssociationInfo assoInfo = getAssociation(columnItem);
                //将关联的带出字段添加到columnInfo中，这个地方带出字段有可能是多值多列，此处无法判断，需要在在组织完表名后重新进行过滤
                if (!associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                    //将带出字段添加到外层的columnInfos中，这个地方带出字段有可能是多值多列UDT，在组织表名前可能是无法获取的，需要组织完表名后，重新进行修复处理
                    for (String refColumn : assoInfo.getRefColumns().keySet()) {
                        if (!columnInfos.containsKey(refColumn)) {
                            columnInfos.put(refColumn, new RefColumnInfo());
                        }
                    }
                }
            }
        }
        if(tempList.size() > 0){
            for (String key : tempList){
                columnInfos.put(key, new RefColumnInfo());
            }
        }
        return associations;
    }

    /**
     * 构造带出字段和多值多列UDT的信息
     * 1.针对带出字段，返回关联信息
     * 2.针对多值多列UDT字段，将具体字段添加到columnInfos中
     * @param columnItem
     * @param columnInfos
     * @param associations
     */
    private void buildRefOrMultiColumnUdtAssInfo(String columnItem, HashMap<String, RefColumnInfo> columnInfos, ArrayList<AssociationInfo> associations){
        //如果能找到关联信息，表示是带出字段
        AssociationInfo assoInfo = findInAssociationInfos(columnItem);
        //找不到关联说明是多值多列字段，也可以用dbColumnInfo == null判断
        if (assoInfo == null && getEntityResInfo() != null) {
            //如果是多值多列udt，进来的一定是多值多列UDT，单列的在getContainColumns中有信息，不会进入此方法
            DataTypePropertyInfo dataTypePropertyInfo = getEntityResInfo().getEntityTypeInfo().getPropertyInfos().get(columnItem);
            Object udtPropertyInfo = null;
            if (dataTypePropertyInfo != null) {
                udtPropertyInfo = getEntityResInfo().getEntityTypeInfo().getPropertyInfos().get(columnItem).getObjectInfo();
            }
            //多值
            if (udtPropertyInfo != null && udtPropertyInfo instanceof ComplexUdtPropertyInfo) {
                //将带出字段替换成多值UDT的多个字段：１.移除多值UDT　２.增加多值包含的明细字段
                if (columnInfos.containsKey(columnItem)) {
                    columnInfos.remove(columnItem);
                }
                for (Map.Entry<String, DataTypePropertyInfo> entry : ((ComplexUdtPropertyInfo) udtPropertyInfo).getPropertyInfos().entrySet()) {
                    if (!columnInfos.containsKey(entry.getKey())) {
                        columnInfos.put(entry.getKey(), new RefColumnInfo());
                    }
                }
            } else {
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5072, false, getConfigId(), getTableAlias(), columnItem);
            }
        } else {
            //有可能带出了同一个关联的多个带出字段，前面的已经添加到associations中
            if (!associations.contains(assoInfo)) {
                associations.add(assoInfo);
            }
        }
    }

    /**
     * 将内层增加的带出字段，传递到外层(拼接表名后，原来的带出字段信息会增加)
     * 同时过滤掉不存在的字段，如多值UDT
     * @param columnInfos key为带出字段在关联实体上的标签， value:为字段的基本信息，返回给外层对象
     * @return
     */
    protected void handleAssociationInfo(HashMap<String, RefColumnInfo> columnInfos) {
        if(columnInfos == null || columnInfos.size() == 0)
            return ;
        ArrayList<String> columns = new ArrayList<String>();
        for (String item : columnInfos.keySet()) {
            columns.add(item);
        }
        ArrayList<AssociationInfo> associations = new ArrayList<AssociationInfo>();
        List<String> tempList = new ArrayList<>();
        for (String columnItem : columns) {
            //处理带出字段是　多值多列UDT和关联带出字段的场景
            //如果是关联带出字段，找到对应的关联信息，返回
            //如果是多值多列字段，将多列字段包含的明细字段添加到columnInfos中，后续返回对应的数据库列等信息

            if (isAssociationElement(columnItem)) {
                AssociationInfo assoInfo = getAssociation(columnItem);
                if (!associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                    for (String refColumn : assoInfo.getRefColumns().keySet()) {
                        if (!columnInfos.containsKey(refColumn)) {
                            columnInfos.put(refColumn, new RefColumnInfo());
                        }
                    }
                }
            }
        }
        if(tempList.size() > 0){
            for (String key : tempList){
                columnInfos.put(key, new RefColumnInfo());
            }
        }
        Iterator<Map.Entry<String, RefColumnInfo>> iterator = columnInfos.entrySet().iterator();
        while(iterator.hasNext()){
            String refColumnName = iterator.next().getKey();
            if(!getContainColumns().contains(refColumnName)){
                iterator.remove();
            }
        }
    }

    /**
     * 是否关联字段
     *
     * @param columnItem
     * @return
     */
    private boolean isAssociationElement(String columnItem) {
        if (getContainColumns().contains(columnItem) && getContainColumns().getItem(columnItem).getIsAssociation()) {
            return true;
        }
        if (this.extendContainColumns != null && this.extendContainColumns.contains(columnItem) && this.extendContainColumns.getItem(columnItem).getIsAssociation()) {
            return true;
        }
        return false;
    }

    private ArrayList<AssociationInfo> getAssociationInfos(ArrayList<String> columns) {
        ArrayList<AssociationInfo> associations = new ArrayList<AssociationInfo>();
        for (String columnItem : columns) {
            if (!getContainColumns().contains(columnItem) || getContainColumns().getItem(columnItem).getIsAssociateRefElement()) {
                AssociationInfo assoInfo = findInAssociationInfos(columnItem);
                if (!associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                }
            }

            if (getContainColumns().contains(columnItem) && getContainColumns().getItem(columnItem).getIsAssociation()) {
                AssociationInfo assoInfo = getAssociation(columnItem);
                if (!associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                }
            }
        }
        return associations;
    }

    /**
     * 判断字段是否关联带出字段或者多值多列Udt字段
     *  1.在组织表名前，带出字段是不存在于getContainColumns集合中的，所以得到的columnInfo是null
     *  2.针对多值多列UDT，getContainColumn中也找不到对应的列信息，所以如果找不到对应列信息，可以认为是带出字段或者多值多列UDT
     *  3.在组织别名后，带出字段在getContainColumns集合中，如果获取到了，通过getIsAssociateRefElement判断是否带出字段
     * @param columnItem
     * @return
     */
    private boolean isRefelementfOMulitColumnUdt(String columnItem) {
        //基础字段/扩展字段都不包含
        DbColumnInfo column = getContainColumns().getItem(columnItem);
        if (column != null && column.getIsAssociateRefElement()) {
            return true;
        }
        DbColumnInfo extendColumn = this.extendContainColumns.getItem(columnItem);
        if (extendColumn != null && extendColumn.getIsAssociateRefElement()) {
            return true;
        }
        if ((column == null) && (extendColumn == null)) {
            return true;
        }
        return false;
    }

    private AssociationInfo findInAssociationInfos(String columnName) {
        for (AssociationInfo associationInfo : getAssociationInfos()) {
            if (associationInfo.getRefColumns().containsKey(columnName)) {
                return associationInfo;
            }
        }
        return null;
//        throw new RuntimeException("找不到字段" + columnName);
    }

    /**
     * 根据关联信息组织查询表名
     * @param associations 关联信息集合
     * @param tableAlias    当前已组装的表别名集合
     * @return
     */
    private String getTableNamesWithAssociationInfo(ArrayList<AssociationInfo> associations, ArrayList<String> tableAlias) {
        if (tableAlias == null) {
            tableAlias = new ArrayList<String>();
            //将当前表的别名加入到集合中
            addTableAlias(tableAlias);
        } else {
            setTableAlias(getTableAlias4Association(tableAlias));
        }
        //如果不包含关联信息，返回当前表名:物理表 别名 etc: tablename tableAlias
        if (associations == null || associations.size() < 1) {
            return getTableName();
        }

        return buildTableNamesWithAssociationInfo(associations, tableAlias, getTableName());
    }

    /**
     * 获取 Join部分的名称，将基础表挪出去。
     *
     * @param associations 关联集合
     * @param tableAlias    别名集合
     * @param baseTableName 查询主表名，格式：物理表名 别名：scmsaleorder saleorder
     * @return
     */
    private String buildTableNamesWithAssociationInfo(ArrayList<AssociationInfo> associations, ArrayList<String> tableAlias, String baseTableName) {
        StringBuilder queryTables = new StringBuilder();
        StringBuilder fullTable = new StringBuilder();
        //当前表名及其别名在前
        fullTable.append(baseTableName);
        //循环添加关联表名
        for (AssociationInfo associationInfo : associations) {
            //关联带出字段集合：key为带出字段在关联对象上的编号
            HashMap<String, RefColumnInfo> associationColumns = new HashMap<String, RefColumnInfo>();
            for (Map.Entry<String, String> item : associationInfo.getRefColumns().entrySet()) {
                associationColumns.put(item.getValue(), new RefColumnInfo());
            }

            //如果是关联自身的场景
            if (getConfigId().equals(associationInfo.getConfigId()) && getNodeCode() == associationInfo.getNodeCode()) {
                queryTables.append(getCurrentAssoTableName(associationInfo, tableAlias, associationColumns));
                continue;
            }
            String targetDbColumnName = "";
            associationInfo.getRefRepository().initParams(getPars());
            RefObject<String> tempRef_targetDbColumnName = new RefObject<String>(targetDbColumnName);
            TableNameRefContext tableNameRefContext = new TableNameRefContext();
            tableNameRefContext.setColumns(associationColumns);
            tableNameRefContext.setKeyColumnName(associationInfo.getTargetColumn());
            tableNameRefContext.setKeyDbColumnName(tempRef_targetDbColumnName);
            tableNameRefContext.setTableAlias(tableAlias);
            tableNameRefContext.setRefTableAlias(associationInfo.getRefTableAlias());
            String tableName = associationInfo.getRefRepository().getTableNameByRefColumns(associationInfo.getNodeCode(), tableNameRefContext);
            targetDbColumnName = tempRef_targetDbColumnName.argvalue;
            initAssociateColumnInfo(associationInfo, associationColumns);
            String sourceColumnName = trans2DbColumnWithAlias(associationInfo.getSourceColumn());
            String[] tables = tableName.split(" LEFT OUTER JOIN ", 2);
            queryTables.append(String.format(getJoinTableName(), tables[0], sourceColumnName, targetDbColumnName));
            buildAssoCondi(queryTables, associationInfo, targetDbColumnName, tables[0]);
            //嵌套关联
            if (tables.length > 1) {
                queryTables.append(" LEFT OUTER JOIN " + tables[1]);
            }
        }
        //包含所有关联的Sql片段
        joinTableName = queryTables.toString();
        //将关联添加入全部
        fullTable.append(joinTableName);
        return fullTable.toString();
    }

    private void buildAssoCondi(StringBuilder queryTables, AssociationInfo associationInfo, String targetDbColumnName, String tableName) {
        //todo 这个地方别名处理待验证,字段如果与数据库不一致？ 前端直接用数据库的？
        if (targetDbColumnName.indexOf(".") < 0) {
            return;
        }
        String targetAlias = targetDbColumnName.substring(0, targetDbColumnName.indexOf("."));
        if (associationInfo.getAssoConditions() != null && associationInfo.getAssoConditions().size() > 0) {
            for (AssoCondition assoCondition : associationInfo.getAssoConditions()) {
                if (StringUtils.isEmpty(assoCondition.getValue())) {
                    queryTables.append(" and " + getAssoAlias(assoCondition.getLeftNodeCode(), tableName) + "." + assoCondition.getLeftField());
                    queryTables.append(StringUtils.isEmpty(assoCondition.getOperator()) ? "=" : assoCondition.getOperator());
                    queryTables.append(getAssoAlias(assoCondition.getRightNodeCode(), targetAlias) + "." + assoCondition.getRightField());
                } else {
                    queryTables.append(" and " + getAssoAlias(assoCondition.getLeftNodeCode(), targetAlias) + "." + assoCondition.getLeftField());
                    queryTables.append(StringUtils.isEmpty(assoCondition.getOperator()) ? "=" : assoCondition.getOperator());
                    //todo wangmj 先按照字符串处理，后续结合类型处理
                    queryTables.append(" '" + assoCondition.getValue() + "' ");
                }
            }
        }

        if (associationInfo.getWhere() != null && associationInfo.getWhere().length() > 0) {
            String where = associationInfo.getWhere();
            where = trimLeft(where);
            String[] array = where.split("=");
            //有可能是 code = 'test' 不带别名
            //实际应该带是带别名的
            if (array.length > 1 && array[1].indexOf(".") < 0 && array[0].indexOf(".") > 0) {//改造第一个  判断是值关联，非字段关联
                //函数暂时不考虑 AND upper(GSPWFCOMMENT.Action) = 'PASS'
                if (!(array[0].contains("(") && array[0].contains(")"))) {
                    int index = where.split("=")[0].indexOf(".");
                    //这一个处理的是 关联字段的值，非当前字段了。。。
                    where = targetAlias + array[0].substring(index) + "=" + array[1];
                }
            }
            if (where.toLowerCase().startsWith("and")) {
                queryTables.append(" " + where);
            } else {
                queryTables.append(" and " + where);
            }
        }
    }

    /**
     * 去左空格
     *
     * @param str
     * @return
     */
    public String trimLeft(String str) {
        if (str == null || str.equals("")) {
            return str;
        } else {
            return str.replaceAll("^[　 ]+", "");
        }
    }

    private String getAssoAlias(String nodeCode, String targetTableName) {
        if (getTableAlias().equals(nodeCode))
            return getTableAlias();
        return targetTableName;
    }

    /**
     * 关联自身表名拼接
     * @param associationInfo 关联信息
     * @param tableAlias    别名集合
     * @param associationColumns 关联带出字段集合，方法结束要设置对应数据库字段信息
     * @return
     */
    private String getCurrentAssoTableName(AssociationInfo associationInfo, ArrayList<String> tableAlias, HashMap<String, RefColumnInfo> associationColumns) {
        //获取别名，因为是关联自身，这个地方返回的别名肯定是，防重加数字后的别名
        String currnetAlias = getTableAlias4Association(tableAlias);
        //返回带出字段对应的关联信息
        ArrayList<AssociationInfo> associations = getAssociationInfos(associationColumns);
        //根据关联信息返回表名
        String tableName = buildTableNamesWithAssociationInfo(associations, tableAlias, innerGetTableName() + " " + currnetAlias);
        String keyDbColumnName = currnetAlias + "." + trans2DbColumn(associationInfo.getTargetColumn());

        ArrayList<String> keys = new ArrayList<String>();
        for (String key :
                associationColumns.keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            RefColumnInfo refColumnInfo = new RefColumnInfo();
            DbColumnInfo dbColumnInfo = getContainColumns().getItem(key);
            if(dbColumnInfo == null){
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5023, false, getTableAlias(), key);
            }
            //如果是带出字段，字段db名不拼接当前表,否则会出现table1.table2.fieldCode的场景
            if(dbColumnInfo.getIsAssociateRefElement()){
                refColumnInfo.setColumnName(trans2DbColumn(key));
            }
            else {
                refColumnInfo.setColumnName(currnetAlias + "." + trans2DbColumn(key));
            }

            refColumnInfo.setTransProcesser(getContainColumns().getItem(key).getTypeTransProcesser());
            refColumnInfo.setColumnType(getContainColumns().getItem(key).getColumnType());
            associationColumns.put(key, refColumnInfo);
        }
        //如果不是最外层的表这个地方有用吗？？或者应该放在前面
        initAssociateColumnInfo(associationInfo, associationColumns);

        String sourceColumnName = trans2DbColumnWithAlias(associationInfo.getSourceColumn());
        // getJoinTableName=LEFT OUTER JOIN %1$s ON %2$s = %3$s
        //如果关联自身带出字段包含关联的话，需要处理一下on条件，tableName 返回的格式为table1 left join table2 on XXXX
        //修改成table1 on XXX left join table2 on XXX
        if(associations != null && associations.size() > 0){
            StringBuilder queryTables = new StringBuilder();
            String[] tables = tableName.split(" LEFT OUTER JOIN ", 2);
            //嵌套关联
            if (tables.length > 1) {
                queryTables.append(String.format(getJoinTableName(), tables[0], sourceColumnName, keyDbColumnName));
                queryTables.append(" LEFT OUTER JOIN ").append(tables[1]).append(" ");
                return queryTables.toString();
            }
        }
        return String.format(getJoinTableName(), tableName, sourceColumnName, keyDbColumnName);

    }

    private void addTableAlias(ArrayList<String> tableAlias) {
        tableAlias.add(getWrappedTableAlias());
    }

    /**
     * 获取当前表在查询表中的别名，并添加到别名集合中
     * @param tableAlias
     * @return
     */
    private String getTableAlias4Association(ArrayList<String> tableAlias) {
        //如果别名集合中不包含当前别名，则表名没有出现重复，直接加入
        if (!tableAlias.contains(getWrappedTableAlias())) {
            tableAlias.add(getWrappedTableAlias());
            return getWrappedTableAlias();
        }

        //如果已包含，则表示已经拼接过当前表，别名要进行+1处理，并加入到别名集合中
        int index = 1;
        while (true) {
            if (tableAlias.contains(getWrappedTableAlias(true) + index)) {
                index++;
            } else {
                tableAlias.add(getWrappedTableAlias(true) + index);
                return getWrappedTableAlias(true) + index;
            }
        }
    }

    /**
     * 当前节点表名及表别名，如BPBizPaymentRequest BizPaymentRequest
     * @return
     */
    public String getTableName() {
        return innerGetTableName() + " " + getWrappedTableAlias();
    }

    /**
     *
     * @param associationInfo
     * @param associationColumns key为目标实体上引用字段的标签
     */
    private void initAssociateColumnInfo(AssociationInfo associationInfo, HashMap<String, RefColumnInfo> associationColumns) {
        //这个地方先按照原来的逻辑处理，普通关联和关联UDT的标签规则不一样:关联UDT的规则是Asso1_UDT编号_字段编号，会省略第二段逻辑
        for (Map.Entry<String, String> refColumnItem : associationInfo.getRefColumns().entrySet()) {
            //这个地方的key是按照当前实体上的字段标签来
            if (getContainColumns().contains(refColumnItem.getKey())) {
                DbColumnInfo columnInfo = getContainColumns().getItem(refColumnItem.getKey());
                RefColumnInfo refColumnInfo = associationColumns.get(refColumnItem.getValue());
                columnInfo.setDbColumnName(refColumnInfo.getColumnName());
                ITypeTransProcesser tempVar = refColumnInfo.getTransProcesser();
                columnInfo.setTypeTransProcesser((ITypeTransProcesser) ((tempVar instanceof ITypeTransProcesser) ? tempVar : null));
                columnInfo.setColumnType(refColumnInfo.getColumnType());
                columnInfo.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                columnInfo.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                columnInfo.setVirtual(refColumnInfo.isVirtual());
                columnInfo.setVariableData(associationInfo.getVariableData());
                associationColumns.remove(refColumnItem.getValue());
            } else {
                //多级关联带出的多值UDT，在这里有可能是没有的，需要过滤掉 Asso1,带出关联，关联带出多值UDT
                if (!associationColumns.containsKey(refColumnItem.getValue())) {
                    continue;
                }
                //找到带出字段在对应实体Adaptor上的DbColumnInfo
                DbColumnInfo refColumnInfo = ((EntityRelationalAdaptor) (((BaseRootRepository) associationInfo.getRefRepository()).getEntityDac(associationInfo.getNodeCode()).getEntityAdaptor())).getContainColumns().getItem(refColumnItem.getValue());
                ITypeTransProcesser tempVar2 = associationColumns.get(refColumnItem.getValue()).getTransProcesser();
                DbColumnInfo tempVar3 = new DbColumnInfo();
                //设置ColumnName为带出字段标签
                tempVar3.setColumnName(refColumnItem.getKey());
                tempVar3.setDbColumnName(associationColumns.get(refColumnItem.getValue()).getColumnName());
                if (refColumnInfo == null) {
                    tempVar3.setTypeTransProcesser((ITypeTransProcesser) ((tempVar2 instanceof ITypeTransProcesser) ? tempVar2 : null));
                    tempVar3.setColumnType(associationColumns.get(refColumnItem.getValue()).getColumnType());

                } else {
                    tempVar3.setVirtual(refColumnInfo.isVirtual());
                    tempVar3.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                    tempVar3.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                    tempVar3.setTypeTransProcesser(refColumnInfo.getTypeTransProcesser());
                    tempVar3.setColumnType(refColumnInfo.getColumnType());
                }
                tempVar3.setIsAssociateRefElement(true);
                tempVar3.setBelongElementLabel(associationInfo.getSourceColumn());
                if (associationInfo.getAssoVariables() != null && associationInfo.getAssoVariables().size() > 0) {
                    HashMap<String, String> varMap = new HashMap<>();
                    for (AssoVariable variable : associationInfo.getAssoVariables()) {
                        varMap.put(variable.getVarCode(), variable.getVarValue());
                    }
                    tempVar3.setVariableData(varMap);
                }

                DbColumnInfo columnInfo = tempVar3;
                //如果是扩展关联
                if(associationInfo.isExtend()){
                    this.extendContainColumns.add(columnInfo);
                }
                else {
                    getContainColumns().add(columnInfo);
                }
                associationColumns.remove(refColumnItem.getValue());
            }

        }
        //这个地方是处理非直接关联，内层带出的关联带出字段，规则是：当前关联字段标签_目标字段标签（udt的命名要加上udt编号）
        String refFieldCode = getRefUdtCode(associationInfo);
        Iterator<Map.Entry<String, RefColumnInfo>> iterator = associationColumns.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, RefColumnInfo> entry = iterator.next();
            //Asso_int1 : int1
            String columnName = entry.getKey();
            RefColumnInfo refRealColumnInfo = entry.getValue();
            //普通关联和关联UDT的命名规则不一样，先按照原来的逻辑处理
            String refColumnName = "";
            if(StringUtils.isNullOrEmpty(refFieldCode)){
                refColumnName = associationInfo.getSourceColumn() + "_" + columnName;
            }
            else {
                refColumnName = refFieldCode + "_" + columnName;
            }
            if(!associationInfo.getRefColumns().containsKey(refColumnName)){
                associationInfo.getRefColumns().put(refColumnName, columnName);
            }
            if (getContainColumns().contains(refColumnName)) {
                DbColumnInfo columnInfo = getContainColumns().getItem(refColumnName);
                columnInfo.setDbColumnName(refRealColumnInfo.getColumnName());
                ITypeTransProcesser tempVar = refRealColumnInfo.getTransProcesser();
                columnInfo.setTypeTransProcesser((ITypeTransProcesser) ((tempVar instanceof ITypeTransProcesser) ? tempVar : null));
                columnInfo.setColumnType(refRealColumnInfo.getColumnType());
                columnInfo.setFieldReposExtendConfigClassImpl(refRealColumnInfo.getFieldReposExtendConfigClassImpl());
                columnInfo.setFieldReposExtendConfigId(refRealColumnInfo.getFieldReposExtendConfigId());
                columnInfo.setVirtual(refRealColumnInfo.isVirtual());
                columnInfo.setVariableData(associationInfo.getVariableData());
                iterator.remove();
            }else {

                DbColumnInfo refColumnInfo = ((EntityRelationalAdaptor) (((BaseRootRepository) associationInfo.getRefRepository()).getEntityDac(associationInfo.getNodeCode()).getEntityAdaptor())).getContainColumns().getItem(columnName);
                ITypeTransProcesser tempVar2 = refRealColumnInfo.getTransProcesser();
                DbColumnInfo tempVar3 = new DbColumnInfo();
                tempVar3.setColumnName(refColumnName);
                tempVar3.setDbColumnName(refRealColumnInfo.getColumnName());
                if (refColumnInfo == null) {
                    tempVar3.setTypeTransProcesser((ITypeTransProcesser) ((tempVar2 instanceof ITypeTransProcesser) ? tempVar2 : null));
                    tempVar3.setColumnType(refRealColumnInfo.getColumnType());

                } else {
                    tempVar3.setVirtual(refColumnInfo.isVirtual());
                    tempVar3.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                    tempVar3.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                    tempVar3.setTypeTransProcesser(refColumnInfo.getTypeTransProcesser());
                    tempVar3.setColumnType(refColumnInfo.getColumnType());
                }
                tempVar3.setIsAssociateRefElement(true);
                tempVar3.setBelongElementLabel(associationInfo.getSourceColumn());
                if (associationInfo.getAssoVariables() != null && associationInfo.getAssoVariables().size() > 0) {
                    HashMap<String, String> varMap = new HashMap<>();
                    for (AssoVariable variable : associationInfo.getAssoVariables()) {
                        varMap.put(variable.getVarCode(), variable.getVarValue());
                    }
                    tempVar3.setVariableData(varMap);
                }

                DbColumnInfo columnInfo = tempVar3;
                //如果是扩展关联
                if(associationInfo.isExtend()){
                    this.extendContainColumns.add(columnInfo);
                }
                else {
                    getContainColumns().add(columnInfo);
                }
                iterator.remove();
            }

        }


        //关联带出的多值UDT
        //其余场景待补充
        //最开始的生成工程，继承EntityRelationAdaptor，没有resInfo这个东西，这里需要兼容判断
//        if (getEntityResInfo() == null) {
//            return;
//        }
//        DataTypePropertyInfo assoPropertyInfo = getEntityResInfo().getEntityTypeInfo().getPropertyInfos().get(associationInfo.getSourceColumn());

        //这个地方应该没有数据了 20240814
        // associationColumns里面都是带出字段的，需要添加到， 已经有一部分在上面的代码中被添加了
//        for (Map.Entry<String, RefColumnInfo> associationColumn : associationColumns.entrySet()) {
//            DbColumnInfo tempVar4 = new DbColumnInfo();
//            //这里不会拼上udt的编号了 当前字段_多值字段
//            //如果当前关联是关联UDT，带出字段可能需要拼上 udt的编号 if内的处理是特殊处理适配关联多值UDT字段Mapping格式
//            if (assoPropertyInfo != null && assoPropertyInfo.getObjectInfo() != null && (assoPropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo)) {
//                //通过匹配取出关联字段的名称
//                String preFix = "";
//                for (Map.Entry<String, String> refColumnItem : associationInfo.getRefColumns().entrySet()) {
//                    String key = refColumnItem.getKey();
//                    String value = refColumnItem.getValue();
//                    preFix = key.substring(0, key.indexOf("_" + value));
//                    tempVar4.setColumnName(preFix + "_" + associationColumn.getKey());
//                    break;
//                }
//            } else {
//                tempVar4.setColumnName(associationInfo.getSourceColumn() + "_" + associationColumn.getKey());
//            }
//            tempVar4.setDbColumnName(associationColumn.getValue().getColumnName());
//            tempVar4.setTypeTransProcesser((ITypeTransProcesser) ((associationColumn.getValue().getTransProcesser() instanceof ITypeTransProcesser) ? associationColumn.getValue().getTransProcesser() : null));
//            tempVar4.setColumnType(associationColumn.getValue().getColumnType());
//            tempVar4.setIsAssociateRefElement(true);
//            tempVar4.setBelongElementLabel(associationInfo.getSourceColumn());
//            tempVar4.setVariableData(associationInfo.getVariableData());
//            DbColumnInfo columnInfo = tempVar4;
//            getContainColumns().add(columnInfo);
//        }
    }

    /**
     * 获取关联UDT带出字段的前缀编号
     * @param associationInfo 关联UDT的关联信息
     * @return
     */
    private String getRefUdtCode(AssociationInfo associationInfo){
        if (getEntityResInfo() == null) {
            return "";
        }
        DataTypePropertyInfo assoPropertyInfo = getEntityResInfo().getEntityTypeInfo().getPropertyInfos().get(associationInfo.getSourceColumn());
        if (assoPropertyInfo != null && assoPropertyInfo.getObjectInfo() != null && (assoPropertyInfo.getObjectInfo() instanceof SimpleAssoUdtPropertyInfo)) {
            //通过匹配取出关联字段的名称
            String preFix = "";
            //还是通过匹配的方式获取到前缀
            for (Map.Entry<String, String> refColumnItem : associationInfo.getRefColumns().entrySet()) {
                String key = refColumnItem.getKey();
                String value = refColumnItem.getValue();
                preFix = key.substring(0, key.indexOf("_" + value));
                return preFix;
            }
        }
        return "";
    }
    // endregion

    public final boolean isDatasEffective(ArrayList<String> dataIds, RefObject<ArrayList<String>> missingDataIds) {
        missingDataIds.argvalue = new ArrayList<String>();
        if (dataIds == null || dataIds.isEmpty()) {
            return true;
        }
        if (dataIds.size() == 1) {
            boolean result = isDataEffective(dataIds.get(0));
            if (result == false) {
                missingDataIds.argvalue.add(dataIds.get(0));
            }
            return result;
        } else {
            int batchCount = 5000;
            if (dataIds != null && dataIds.size() > batchCount) {
                int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
                for (int i = 1; i <= times; i++) {
                    int endIndex = i == times ? dataIds.size() : batchCount * i;
                    List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);

                    String sql = String.format("SELECT COUNT(1) FROM %1$s WHERE (%2$s IN %3$s)", innerGetTableName(), getContainColumns().getPrimaryKey().getDbColumnName(), getInFilter(tempList, getContainColumns().getPrimaryKey().getDbColumnName()));
                    List<DbParameter> parameters = new ArrayList<DbParameter>();
                    if (getLogicDeleteInfo().isEnableLogicDelete()) {
                        sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
                        parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                    }
                    Query query = buildQueryManager(sql, parameters);
                    Integer count = Integer.valueOf(query.getResultList().get(0).toString());
                    if (count == tempList.size()) {
                        return true;
                    }
                    missingDataIds.argvalue = getMissingDataIds((ArrayList<String>) tempList);
                    if (missingDataIds.argvalue != null && missingDataIds.argvalue.size() != 0) {
                        return false;
                    }
                }
            } else {
                String sql = String.format("SELECT COUNT(1) FROM %1$s WHERE (%2$s IN %3$s)", innerGetTableName(), getContainColumns().getPrimaryKey().getDbColumnName(), getInFilter(dataIds, getContainColumns().getPrimaryKey().getDbColumnName()));
                List<DbParameter> parameters = new ArrayList<DbParameter>();
                if (getLogicDeleteInfo().isEnableLogicDelete()) {
                    sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
                    parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                }
                Query query = buildQueryManager(sql, parameters);
                Integer count = Integer.valueOf(query.getResultList().get(0).toString());
                if (count == dataIds.size()) {
                    return true;
                }
                missingDataIds.argvalue = getMissingDataIds(dataIds);
                if (missingDataIds.argvalue != null && missingDataIds.argvalue.size() != 0) {
                    return false;
                }
            }
            return true;
        }
    }

    @Deprecated
    public final boolean isRef(String dataId, String propertyName) {
        return isRef(new String[]{dataId}, propertyName);
    }

    public final boolean isRef(String[] dataIds, String propertyName) {
        AssociationInfo associationInfo = null;
        for (AssociationInfo item : getAssociationInfos()) {
            if (propertyName.equals(item.getSourceColumn())) {
                associationInfo = item;
                break;
            }
        }
        //可能存在非关联字段的场景(修改过对象类型)，如果关联禁用删除检查，则返回
        if(associationInfo != null && associationInfo.getDeleteCheckState() == DeleteCheckState.Disabled){
            return false;
        }

        List<String> tables = innerGetTableNames();
        if (tables == null || tables.isEmpty()) {
            return false;
        }
        for (String tableName : tables) {
            int totalCount = getRefDataCount(tableName, dataIds, propertyName);
            if (totalCount > 0) {
                return true;
            }
        }
        return false;
    }

    private int getRefDataCount(String tableName, String[] dataIds, String propertyName) {
        List<DbParameter> parameters = new ArrayList<DbParameter>();
        String sql;
        int index = 0;
        String dbColumnName = getContainColumns().getItem(propertyName).getDbColumnName();
        if (dataIds.length == 1) {
            sql = "Select count(1) from " + tableName + " where " + dbColumnName + " =?" + (index++);
            parameters.add(
                    new DbParameter("Id", getContainColumns().getPrimaryKey().getColumnType(), dataIds[0]));
        } else {
            sql = "Select count(1) from " + tableName + " where (" + dbColumnName + " in "
                    + FilterUtil.buildInCondition(dataIds, false, dbColumnName) + ")";
        }
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            sql += " and " + getLogicDeleteInfo().getLabelId() + " =?" + ((index++));
            parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
        }
        try {
            Query query = buildQueryManager(sql, parameters);
            return Integer.valueOf(query.getResultList().get(0).toString());
        } catch (Exception ex) {
            logger.error("获取data个数出错sql:" + sql);
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5073, ex, ExceptionLevel.Error, false, tableName, propertyName);
        }
    }

    public final boolean isDatasEffective(ArrayList<String> dataIds, RefObject<ArrayList<String>> missingDataIds, String propName) {
        missingDataIds.argvalue = new ArrayList<String>();
        if (dataIds == null || dataIds.isEmpty()) {
            return true;
        }
        if (dataIds.size() == 1) {
            boolean result = isDataEffective(dataIds.get(0), propName);
            if (result == false) {
                missingDataIds.argvalue.add(dataIds.get(0));
            }
            return result;
        } else {
            int batchCount = 5000;
            String targetDbName = getContainColumns().getItem(propName).getDbColumnName();
            if (dataIds != null && dataIds.size() > batchCount) {
                int times = dataIds.size() % batchCount == 0 ? dataIds.size() / batchCount : dataIds.size() / batchCount + 1;
                for (int i = 1; i <= times; i++) {
                    int endIndex = i == times ? dataIds.size() : batchCount * i;
                    List<String> tempList = dataIds.subList(batchCount * (i - 1), endIndex);
                    //in参数超过999时，会分解为多组，通过or连接，外部需要添加括号
                    String sql = String.format("SELECT %1$s FROM %2$s WHERE (%3$s IN %4$s)", targetDbName, innerGetTableName(), targetDbName, FilterUtil.buildInCondition(tempList, false, propName));
                    List<DbParameter> parameters = new ArrayList<DbParameter>();
                    if (getLogicDeleteInfo().isEnableLogicDelete()) {
                        sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
                        parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                    }
                    Query query = buildQueryManager(sql, parameters);
                    HashSet checkNum = new HashSet<>(query.getResultList());
                    if (checkNum.size() >= dataIds.size()) {
                        return true;
                    }
                    missingDataIds.argvalue = getMissingDataIds(tempList, propName);
                    if (missingDataIds.argvalue != null && missingDataIds.argvalue.size() != 0) {
                        return false;
                    }
                }
            } else {
                String sql = String.format("SELECT %1$s FROM %2$s WHERE (%3$s IN %4$s)", targetDbName, innerGetTableName(), targetDbName, FilterUtil.buildInCondition(dataIds, false, propName));
                List<DbParameter> parameters = new ArrayList<DbParameter>();
                if (getLogicDeleteInfo().isEnableLogicDelete()) {
                    sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
                    parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                }
                Query query = buildQueryManager(sql, parameters);
                HashSet checkNum = new HashSet<>(query.getResultList());
                if (checkNum.size() >= dataIds.size()) {
                    return true;
                }
                missingDataIds.argvalue = getMissingDataIds(dataIds, propName);
                if (missingDataIds.argvalue != null && missingDataIds.argvalue.size() != 0) {
                    return false;
                }
            }
            return true;
        }
    }

    private ArrayList<String> getMissingDataIds(ArrayList<String> dataIds) {
        String sql = String.format("Select %1$s from %2$s where (%3$s in %4$s)", getPrimaryKey(),innerGetTableName(),getPrimaryKey(),
                getInFilter(dataIds, getContainColumns().getPrimaryKey().getDbColumnName()));
        List<DbParameter> dbPars = null;
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
            dbPars = new ArrayList<>();
            dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
        }
        ArrayList<String> dbDataIds = getDataIds(sql, dbPars);
        ArrayList<String> missingDataIds = new ArrayList<String>();

        for (String item : dataIds) {
            if (dbDataIds.contains(item) == false) {
                missingDataIds.add(item);
            }
        }
        return missingDataIds;
    }

    private ArrayList<String> getMissingDataIds(List<String> dataIds, String propName) {
        String sql = String.format("Select %1$s from %2$s where (%3$s in %4$s)", propName,innerGetTableName(), propName,
                getInFilter(dataIds, propName));
        List<DbParameter> dbPars = null;
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            sql += " and " + getLogicDeleteInfo().getLabelId() + " =?0";
            dbPars = new ArrayList<>();
            dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
        }
        ArrayList<String> dbDataIds = getDataIds(sql, dbPars);
        ArrayList<String> missingDataIds = new ArrayList<String>();

        for (String item : dataIds) {
            if (dbDataIds.contains(item) == false) {
                missingDataIds.add(item);
            }
        }
        return missingDataIds;
    }

    private ArrayList<String> getDataIds(String sql, List<DbParameter> parameters) {

        Query query = buildQueryManager(sql, parameters);
        List<Object[]> result = query.getResultList();
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i) instanceof Object[]) {
                list.add(String.valueOf(result.get(i)[0]));
            } else {
                list.add(String.valueOf(result.get(i)));
            }
        }
        return list;
    }

    private boolean isDataEffective(String dataID) {
        String sql = "Select count(1) from " + innerGetTableName() + " where " + getContainColumns().getPrimaryKey().getDbColumnName() + " =?0";
        List<DbParameter> parameters = new ArrayList<DbParameter>();
        parameters.add(new DbParameter("Id", getContainColumns().getPrimaryKey().getColumnType(), dataID));
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            sql += " and " + getLogicDeleteInfo().getLabelId() + " =?1";
            parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
        }
        Query query = buildQueryManager(sql, parameters);

        return Integer.valueOf(query.getResultList().get(0).toString()) > 0;
    }

    private boolean isDataEffective(String dataID, String propName) {
        String sql = "Select count(1) from " + innerGetTableName() + " where " + getContainColumns().getItem(propName).getDbColumnName() + " =?0";
        List<DbParameter> parameters = new ArrayList<DbParameter>();
        parameters.add(new DbParameter(propName, getContainColumns().getItem(propName).getColumnType(), dataID));
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            sql += " and " + getLogicDeleteInfo().getLabelId() + " =?1";
            parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
        }
        Query query = buildQueryManager(sql, parameters);

        return Integer.valueOf(query.getResultList().get(0).toString()) > 0;
    }

    protected INestedRepository getNestedRepository(String configId) {
        if (!nestedRepositories.containsKey(configId)) {
            synchronized (netstRepositoryLock) {
                if (!nestedRepositories.containsKey(configId)) {
                    nestedRepositories.put(configId, com.inspur.edp.udt.api.UdtManagerUtils.getUdtRepositoryFactory().createRepository(configId));
                }
            }
        }
        return nestedRepositories.get(configId);
    }

    protected String buildMessage(String message) {
        String warningMessage = I18nResourceUtil.getResourceItemValue("pfcommon", "cef_exception.properties", "Gsp_Cef_Retrieve_0001");
        return String.format(warningMessage, message);
    }

    public List<String> getDistinctFJM(String fjnPropertyName, EntityFilter filter, ArrayList<AuthorityInfo> authorityInfos, Integer parentLayer) {
        String tableName = getQueryTableNameWithAuthority(authorityInfos, filter);
        StringBuilder sql = new StringBuilder();
        sql.append(String.format("SELECT  %1$s FROM %2$s ", getDistinctFJMSql(fjnPropertyName, parentLayer), tableName));
        ArrayList<DbParameter> parameter = new ArrayList<DbParameter>();
        String condition = buildWhereCondition(getDB(), filter.getFilterConditions(), parameter, authorityInfos);
        if (condition != null && condition.length() > 0) {
            sql.append(String.format(" WHERE %1$s", condition));
        }
        Query query = buildQueryManager(formatMultiLang(sql.toString()), parameter);
        List<Object> resultSet = null;
        try {
            resultSet = query.getResultList();
        } catch (Exception ex) {
            logger.error("tablealias:" + getTableAlias() + " 出错sql:" + sql);
            throw ex;
        }
        if (resultSet == null || resultSet.size() == 0)
            return new ArrayList<>();
        List<String> fjms = new ArrayList<>();
        Object value = null;
        for (Object result : resultSet) {
            value = result;
            if (value == null)
                continue;
            fjms.add((String) value);
        }
        return fjms;
    }

    private String getDistinctFJMSql(String fjnPropertyName, Integer parentLayer) {
        DbColumnInfo dbColumnInfo = getContainColumns().getItem(fjnPropertyName);
        if (dbColumnInfo == null)
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5074, false, getTableAlias(), fjnPropertyName);
        fjnPropertyName = trans2DbColumnWithAlias(dbColumnInfo.getColumnName());
        switch (CAFContext.current.getDbType()) {
            case SQLServer:
                return "distinct(substring(" + fjnPropertyName + ",0," + ((parentLayer + 1) * 4 + 1) + "))";
            case HighGo:
                return "distinct(substring(" + fjnPropertyName + " from 0 for " + ((parentLayer + 1) * 4 + 1) + "))";
            case Gbase:
            case Oscar:
                return "distinct(substring(" + fjnPropertyName + ",0," + (parentLayer + 1) * 4 + "))";
            case Kingbase:
                return "distinct(substring(" + fjnPropertyName + ",1," + (parentLayer + 1) * 4 + "))";
            case Oracle:
            case OceanBase:
                return "distinct(substr(" + fjnPropertyName + ",0," + (parentLayer + 1) * 4 + "))";
            case PgSQL:
            case OpenGauss:
            case GaussDB:
                return "distinct(substring(" + fjnPropertyName + " from 0 for " + ((parentLayer + 1) * 4 + 1) + "))";
            case DM:
                return "distinct(substring(" + fjnPropertyName + " from 0 for " + ((parentLayer + 1) * 4) + "))";
            case MySQL:
                return "distinct(substring(" + fjnPropertyName + ",1," + (parentLayer + 1) * 4 + "))";
            case DB2:
                return "distinct(left(" + fjnPropertyName + "," + (parentLayer + 1) * 4 + "))";
            default:
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5075, String.valueOf(CAFContext.current.getDbType()));
        }
    }
}
