/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.readerwriter;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CefDataReader implements ICefReader {
    private Object[] reader;
    private java.util.HashMap<String, Integer> propDBIndexMapping;
    private boolean usePropDBIndexMapping = false;

//    public CefDataReader(Object[] reader) {
//        this.reader = reader;
//    }

    public CefDataReader(Object[] reader, java.util.HashMap<String, Integer> propDBIndexMapping) {
        this.reader = reader;
        usePropDBIndexMapping = true;
        this.propDBIndexMapping = propDBIndexMapping;
    }

    public final Object readValue(String propName) {
        try {
            if (usePropDBIndexMapping && propDBIndexMapping.containsKey(propName)) {
                return reader[propDBIndexMapping.get(propName)];// (propDBIndexMapping.get(propName));
            }
        } catch (Exception e) {
            if (usePropDBIndexMapping && propDBIndexMapping.containsKey(propName)) {
                String[] message = new String[]{propName, String.valueOf(propDBIndexMapping.get(propName))};
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5070, e, message);
            } else {
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5071, e, propName);
            }
        }
        return null;
    }

    @Override
    public boolean hasProperty(String propName) {
        return propDBIndexMapping.containsKey(propName);
    }

//	public final Object getString(String propName) throws SQLException
//	{
//		if (usePropDBIndexMapping)
//		{
//			return reader.getString(propDBIndexMapping.get(propName));
//		}
//		return reader.getString(propName);
//	}
}
