/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.entity.entity.EntityDataPropertyValueUtils;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;

import java.sql.Blob;
import java.sql.SQLException;

public abstract class OracleAdapter extends EntityRelationalAdaptor {

    protected OracleAdapter() {
        super();
    }

    protected byte[] getBlobValue(Object obj) {
        if (!(obj instanceof Blob) && obj != null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5017, false, "", obj.getClass().getTypeName());
        } else {
            try {
                return obj == null ? EntityDataPropertyValueUtils.getBinaryPropertyDefaultValue() : ((Blob) obj).getBytes(1L, (int) ((Blob) obj).length());
            } catch (SQLException e) {
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5002, e, obj.toString());
            }
        }
    }
}
