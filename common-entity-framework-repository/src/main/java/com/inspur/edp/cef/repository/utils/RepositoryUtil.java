/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;


import com.inspur.edp.cef.entity.condition.RetrieveFilter;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.i18n.api.LanguageService;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.i18n.framework.api.language.ILanguageService;
import io.iec.edp.caf.runtime.config.CefBeanUtil;

import java.util.List;

public final class RepositoryUtil {
    private static ILanguageService languageService = null;

    public static String FormatMuliLang(String sql) {
        return FormatMuliLang(sql, new RetrieveFilter());
    }

    public static String FormatMuliLangBySpecLang(String sql, String language) {
        DataValidator.checkForEmptyString(sql, "sql");

        String fieldSuffix = "";
        if (!StringUtils.isNullOrEmpty(language)) {
            fieldSuffix = getLanguageService().getFieldSuffix(language);
        } else {
            fieldSuffix = getLanguageService().getFieldSuffix(CAFContext.current.getLanguage());
        }
        return sql.replaceAll("@Language@", fieldSuffix);
    }

    public static String FormatMuliLang(String sql, RetrieveFilter retrieveFilter) {
        DataValidator.checkForEmptyString(sql, "sql");

        String fieldSuffix = "";
        if (retrieveFilter != null && !StringUtils.isNullOrEmpty(retrieveFilter.getEnableLanguage())) {
            fieldSuffix = getLanguageService().getFieldSuffix(retrieveFilter.getEnableLanguage());
        } else {
            fieldSuffix = getLanguageService().getFieldSuffix(CAFContext.current.getLanguage());
        }
        return sql.replaceAll("@Language@", fieldSuffix);
    }

    public static String FormatMuliLang(String sql, String fieldSuffix) {
        DataValidator.checkForEmptyString(sql, "sql");
        return sql.replaceAll("@Language@", fieldSuffix);
    }


    public static List<EcpLanguage> getCurrentEnabledLanguages() {
        return getLanguageService().getEnabledLanguages();
    }

    public static List<EcpLanguage> getAllLanguages() {
        return getLanguageService().getAllLanguages();
    }

    public static EcpLanguage getLanguage(String languageCode) {
        return getLanguageService().getLanguage(languageCode);
    }

    /**
     * 根据语言编号获取字段后缀
     * @param languageCode
     * @return
     */
    public static String getFieldSuffix(String languageCode){
        return getLanguageService().getFieldSuffix(languageCode);
    }

    public static String FormatMuliLangColumnName(String columnName, EcpLanguage language) {
        DataValidator.checkForEmptyString(columnName, "columnName");
        String fieldSuffix = getLanguageService().getFieldSuffix(language.getId());
        // _CHS
        if (columnName.contains("@Language@")) {
            return columnName.replaceAll("@Language@", fieldSuffix);
        } else if (columnName.contains(MultiLanguageInfo.MULTILANGUAGETOKEN)) {
            return columnName.replaceAll(MultiLanguageInfo.MULTILANGUAGETOKEN, fieldSuffix);
        } else {
            return columnName.concat(fieldSuffix);
        }
    }

    private static ILanguageService getLanguageService(){
        if(languageService==null){
            languageService = CefBeanUtil.getAppCtx().getBean(ILanguageService.class);
        }
        return languageService;
    }
}
