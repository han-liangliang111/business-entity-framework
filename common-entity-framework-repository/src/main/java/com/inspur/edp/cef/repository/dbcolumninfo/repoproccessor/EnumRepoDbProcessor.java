/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;
import com.inspur.edp.cef.spi.repository.IAdaptorItem;

public class EnumRepoDbProcessor extends AbstractRepoPrcessor {

    @Override
    public Object readProperty(DbColumnInfo dbColumnInfo, DbProcessor dbProcessor,
                               IAdaptorItem adaptorItem, ICefReader reader) {
        EnumPropertyInfo enumPropertyInfo = (EnumPropertyInfo) dbColumnInfo.getDataTypePropertyInfo()
                .getObjectInfo();
        Object obj = reader.readValue(dbColumnInfo.getColumnName());
        try {
            return enumPropertyInfo.getEnumValueFromDac(obj);
        } catch (Exception e) {
            String objValue = obj == null ? "null" : obj.toString();
            String[] messageParams = new String[]{dbColumnInfo.getDataTypePropertyInfo().getPropertyName(), objValue};
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5055, e, messageParams);
        }
    }

    @Override
    public Object getPersistenceValue(DbColumnInfo dbColumnInfo, Object value) {
        return dbColumnInfo.getTypeTransProcesser().transType(value);
    }
}
