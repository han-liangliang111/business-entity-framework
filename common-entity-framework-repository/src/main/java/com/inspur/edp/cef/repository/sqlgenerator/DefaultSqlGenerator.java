package com.inspur.edp.cef.repository.sqlgenerator;

/**
 * 默认Sql生成器
 */
public class DefaultSqlGenerator extends BaseSqlGenerator{
    @Override
    public String getQuerySql() {
        return "SELECT %1$s FROM %2$s ";

    }

    @Override
    public String getInsertSql() {
        return "INSERT INTO %1$s (%2$s) VALUES(%3$s) ";
    }

    @Override
    public String getDeleteSql() {
        return "DELETE from %1$s AS %2$s ";
    } //"WHERE %3$s = %4$s";

    @Override
    public String getUpdateSql() {
        return "UPDATE %1$s %3$s SET %2$s WHERE %4$s = %5$s";
    }

    @Override
    public String getSearchSqlByRootIds() {
        return "SELECT %1$s, %2$s " + "FROM %3$s " + "%4$s " + "WHERE %2$s in %5$s ";
    }

    @Override
    public String getSearchSqlByRootId() {
        return "SELECT %1$s, %2$s " + "FROM %3$s " + "%4$s " + "WHERE %2$s = %5$s ";
    }

    @Override
    public String getDeleteByRoot() {
        return "DELETE FROM %1$s AS %2$s WHERE EXISTS (SELECT 1 FROM %3$s WHERE %4$s AND %5$s= %6$s)";
    }

    @Override
    public String getRetrieveBatchSql() {
        return "SELECT %1$s FROM %2$s WHERE %3$s IN %4$s";
    }

    @Override
    public String getRetrieveSql() {
        return "SELECT %1$s FROM %2$s WHERE %3$s = %4$s";

    }

    @Override
    public String getJoinTableName() {
        return " LEFT OUTER JOIN %1$s ON %2$s = %3$s ";
    }

    @Override
    public String getInnerJoinTableName() {
        return "JOIN @ParentTableName@ @ParentTableAlias@ ON @ParentID@ = @PrimaryID@";
    }
}
