/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

import com.inspur.edp.cef.api.repository.IRootRepository;
import com.inspur.edp.cef.entity.repository.AssoVariable;
import com.inspur.edp.cef.spi.entity.AssociationEnableState;
import com.inspur.edp.cef.spi.entity.DeleteCheckState;
import com.inspur.edp.cef.spi.repository.IAssociationInfo;

import java.util.ArrayList;
import java.util.HashMap;

public class AssociationInfo implements IAssociationInfo {
    private String privateConfigId;
    /**
     * 关联实体节点编号（默认是主节点）
     */
    private String privateNodeCode;
    /**
     * 记录关联变量:非变量 约定的传输数据
     */
    private HashMap<String, String> variableData;
    /**
     * 关联带出字段集合
     * Key是当前实体上关联带出字段的标签
     * Value是引用实体上字段的标签
     */
    private java.util.HashMap<String, String> privateRefColumns;
    /**
     * 关联实体仓库
     */
    private IRootRepository privateRefRepository;
    private java.lang.Class privateAssociationType;
    /**
     * 源字段（当前对象上的字段）
     */
    private String privateSourceColumn;
    /**
     * 目标字段（关联对象上的字段）
     */
    private String privateTargetColumn;
    private ArrayList<AssoCondition> assoConditions;
    private ArrayList<AssoVariable> assoVariables;
    private String where;
    private String refTableAlias;
    /**
     * 是否扩展关联，默认为否
     */
    private boolean isExtend = false;

    /**
     * 是否启用外键检查
     */
    private DeleteCheckState deleteCheckState = DeleteCheckState.Enabled;
    /**
     * 是否启用删除检查
     */
    private AssociationEnableState fkEnableState = AssociationEnableState.Enabled;

    public DeleteCheckState getDeleteCheckState() {
        return deleteCheckState;
    }

    public void setDeleteCheckState(DeleteCheckState deleteCheckState) {
        this.deleteCheckState = deleteCheckState;
    }

    public AssociationEnableState getFkEnableState() {
        return fkEnableState;
    }

    public void setFkEnableState(AssociationEnableState fkEnableState) {
        this.fkEnableState = fkEnableState;
    }

    public boolean isExtend() {
        return isExtend;
    }

    public void setExtend(boolean extend) {
        isExtend = extend;
    }

    public final String getConfigId() {
        return privateConfigId;
    }

    public final void setConfigId(String value) {
        privateConfigId = value;
    }

    public final String getNodeCode() {
        return privateNodeCode;
    }

    public final void setNodeCode(String value) {
        privateNodeCode = value;
    }

    public HashMap<String, String> getVariableData() {
        return variableData;
    }

    public void setVariableData(HashMap<String, String> variableData) {
        this.variableData = variableData;
    }

    public final java.util.HashMap<String, String> getRefColumns() {
        return privateRefColumns;
    }

    public final void setRefColumns(java.util.HashMap<String, String> value) {
        privateRefColumns = value;
    }

    @Override
    public final IRootRepository getRefRepository() {
        return privateRefRepository;
    }

    public final void setRefRepository(IRootRepository value) {
        privateRefRepository = value;
    }

    public final java.lang.Class getAssociationType() {
        return privateAssociationType;
    }

    public final void setAssociationType(java.lang.Class value) {
        privateAssociationType = value;
    }

    public final String getSourceColumn() {
        return privateSourceColumn;
    }

    public final void setSourceColumn(String value) {
        privateSourceColumn = value;
    }

    public final String getTargetColumn() {
        return privateTargetColumn;
    }

    public final void setTargetColumn(String value) {
        privateTargetColumn = value;
    }

    public ArrayList<AssoCondition> getAssoConditions() {
        return assoConditions;
    }

    public void setAssoConditions(ArrayList<AssoCondition> assoConditions) {
        this.assoConditions = assoConditions;
    }

    public ArrayList<AssoVariable> getAssoVariables() {
        return assoVariables;
    }

    public void setAssoVariables(ArrayList<AssoVariable> assoVariables) {
        this.assoVariables = assoVariables;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public HashMap<String, String> getAssDbMapping() {
        return getRefColumns();
    }

    public String getRefTableAlias() {
        return refTableAlias;
    }

    public void setRefTableAlias(String refTableAlias) {
        this.refTableAlias = refTableAlias;
    }
}
