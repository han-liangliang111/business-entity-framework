/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.svc.expression.api.IExpressionEvaluator;
import com.inspur.edp.svc.expression.api.entities.ExpressionEntity;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.sql.Date;
import java.time.OffsetDateTime;
import java.util.Calendar;

public class ExpressionUtil {
    private static void initSessionVariable(IExpressionEvaluator evaluator) {
        Calendar lastYear = Calendar.getInstance();
        lastYear.add(Calendar.YEAR, -1);
        evaluator.addSessionVariable("LastYear", lastYear.get(Calendar.YEAR));//上一年
        Calendar lastMonth = Calendar.getInstance();
        lastMonth.add(Calendar.MONTH, -1);
        evaluator.addSessionVariable("LastMonth", lastMonth.get(Calendar.MONTH) + 1);//上一月
        evaluator.addSessionVariable("CurrentYear", Calendar.getInstance().get(Calendar.YEAR));//当前年
        evaluator.addSessionVariable("CurrentMonth", Calendar.getInstance().get(Calendar.MONTH) + 1);//当前月
        evaluator.addSessionVariable("CurrentDateTime", CAFContext.current.getCurrentDateTime());//当前时间
        evaluator.addSessionVariable("LoginTime", CAFContext.current.getLoginTime());//登陆时间
        evaluator.addSessionVariable("UserId", CAFContext.current.getUserId());//登录用户ID
        evaluator.addSessionVariable("UserCode", CAFContext.current.getCurrentSession().getUserCode());//登陆用户编号
        evaluator.addSessionVariable("UserName", CAFContext.current.getCurrentSession().getUserName());//登陆用户名称
        evaluator.addSessionVariable("SysOrgId", CAFContext.current.getCurrentSession().getSysOrgId());//系统组织标识
        evaluator.addSessionVariable("SysOrgCode", CAFContext.current.getCurrentSession().getSysOrgCode());//系统组织编号
        evaluator.addSessionVariable("SysOrgName", CAFContext.current.getCurrentSession().getSysOrgName());//系统组织名称
        evaluator.addSessionVariable("Language", CAFContext.current.getLanguage());//当前语言
    }

    public static Object getExpressionValue(String expressionText) {

        IExpressionEvaluator evaluator = SpringBeanUtils.getBean(IExpressionEvaluator.class);
        initSessionVariable(evaluator);
        Object result = evaluator.evaluate(expressionText);
        if (result instanceof OffsetDateTime) {
            result = Date.from(((OffsetDateTime) result).toInstant());
            return FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(result);
        } else if (result instanceof Integer) {
            result = result.toString();
        }
        return result;
    }
}
