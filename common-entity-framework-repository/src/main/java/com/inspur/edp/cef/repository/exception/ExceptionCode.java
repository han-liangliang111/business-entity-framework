package com.inspur.edp.cef.repository.exception;

/**
 * @className: ExceptionCode
 * @author: sure
 * @date: 2024/2/18
 **/
public class ExceptionCode {
    //错误代码四位数：entity 1001开头 api:2001开头 spi:3001开头 core:4001开头 repository:5001
    public static final String CEF_RUNTIME_5001 = "CEF_RUNTIME_5001";
    public static final String CEF_RUNTIME_5002 = "CEF_RUNTIME_5002";
    public static final String CEF_RUNTIME_5003 = "CEF_RUNTIME_5003";
    public static final String CEF_RUNTIME_5004 = "CEF_RUNTIME_5004";
    public static final String CEF_RUNTIME_5005 = "CEF_RUNTIME_5005";
    public static final String CEF_RUNTIME_5006 = "CEF_RUNTIME_5006";
    public static final String CEF_RUNTIME_5007 = "CEF_RUNTIME_5007";
    public static final String CEF_RUNTIME_5008 = "CEF_RUNTIME_5008";
    public static final String CEF_RUNTIME_5009 = "CEF_RUNTIME_5009";
    public static final String CEF_RUNTIME_5010 = "CEF_RUNTIME_5010";
    public static final String CEF_RUNTIME_5011 = "CEF_RUNTIME_5011";
    public static final String CEF_RUNTIME_5012 = "CEF_RUNTIME_5012";
    public static final String CEF_RUNTIME_5013 = "CEF_RUNTIME_5013";
    public static final String CEF_RUNTIME_5014 = "CEF_RUNTIME_5014";
    public static final String CEF_RUNTIME_5015 = "CEF_RUNTIME_5015";
    public static final String CEF_RUNTIME_5016 = "CEF_RUNTIME_5016";
    public static final String CEF_RUNTIME_5017 = "CEF_RUNTIME_5017";
    public static final String CEF_RUNTIME_5018 = "CEF_RUNTIME_5018";
    public static final String CEF_RUNTIME_5019 = "CEF_RUNTIME_5019";
    public static final String CEF_RUNTIME_5020 = "CEF_RUNTIME_5020";
    public static final String CEF_RUNTIME_5021 = "CEF_RUNTIME_5021";
    public static final String CEF_RUNTIME_5022 = "CEF_RUNTIME_5022";
    public static final String CEF_RUNTIME_5023 = "CEF_RUNTIME_5023";
    public static final String CEF_RUNTIME_5024 = "CEF_RUNTIME_5024";
    public static final String CEF_RUNTIME_5025 = "CEF_RUNTIME_5025";
    public static final String CEF_RUNTIME_5026 = "CEF_RUNTIME_5026";
    public static final String CEF_RUNTIME_5027 = "CEF_RUNTIME_5027";
    public static final String CEF_RUNTIME_5028 = "CEF_RUNTIME_5028";
    public static final String CEF_RUNTIME_5029 = "CEF_RUNTIME_5029";
    public static final String CEF_RUNTIME_5030 = "CEF_RUNTIME_5030";
    public static final String CEF_RUNTIME_5031 = "CEF_RUNTIME_5031";
    public static final String CEF_RUNTIME_5032 = "CEF_RUNTIME_5032";
    public static final String CEF_RUNTIME_5033 = "CEF_RUNTIME_5033";
    public static final String CEF_RUNTIME_5034 = "CEF_RUNTIME_5034";
    public static final String CEF_RUNTIME_5035 = "CEF_RUNTIME_5035";
    public static final String CEF_RUNTIME_5036 = "CEF_RUNTIME_5036";
    public static final String CEF_RUNTIME_5037 = "CEF_RUNTIME_5037";
    public static final String CEF_RUNTIME_5038 = "CEF_RUNTIME_5038";
    public static final String CEF_RUNTIME_5039 = "CEF_RUNTIME_5039";
    public static final String CEF_RUNTIME_5040 = "CEF_RUNTIME_5040";
    public static final String CEF_RUNTIME_5041 = "CEF_RUNTIME_5041";
    public static final String CEF_RUNTIME_5042 = "CEF_RUNTIME_5042";
    public static final String CEF_RUNTIME_5043 = "CEF_RUNTIME_5043";
    public static final String CEF_RUNTIME_5044 = "CEF_RUNTIME_5044";
    public static final String CEF_RUNTIME_5045 = "CEF_RUNTIME_5045";
    public static final String CEF_RUNTIME_5046 = "CEF_RUNTIME_5046";
    public static final String CEF_RUNTIME_5047 = "CEF_RUNTIME_5047";
    public static final String CEF_RUNTIME_5048 = "CEF_RUNTIME_5048";
    public static final String CEF_RUNTIME_5049 = "CEF_RUNTIME_5049";
    public static final String CEF_RUNTIME_5050 = "CEF_RUNTIME_5050";
    public static final String CEF_RUNTIME_5051 = "CEF_RUNTIME_5051";
    public static final String CEF_RUNTIME_5052 = "CEF_RUNTIME_5052";
    public static final String CEF_RUNTIME_5053 = "CEF_RUNTIME_5053";
    public static final String CEF_RUNTIME_5054 = "CEF_RUNTIME_5054";
    public static final String CEF_RUNTIME_5055 = "CEF_RUNTIME_5055";
    public static final String CEF_RUNTIME_5056 = "CEF_RUNTIME_5056";
    public static final String CEF_RUNTIME_5057 = "CEF_RUNTIME_5057";
    public static final String CEF_RUNTIME_5058 = "CEF_RUNTIME_5058";
    public static final String CEF_RUNTIME_5059 = "CEF_RUNTIME_5059";
    public static final String CEF_RUNTIME_5060 = "CEF_RUNTIME_5060";
    public static final String CEF_RUNTIME_5061 = "CEF_RUNTIME_5061";
    public static final String CEF_RUNTIME_5062 = "CEF_RUNTIME_5062";
    public static final String CEF_RUNTIME_5063 = "CEF_RUNTIME_5063";
    public static final String CEF_RUNTIME_5064 = "CEF_RUNTIME_5064";
    public static final String CEF_RUNTIME_5065 = "CEF_RUNTIME_5065";
    public static final String CEF_RUNTIME_5066 = "CEF_RUNTIME_5066";
    public static final String CEF_RUNTIME_5067 = "CEF_RUNTIME_5067";
    public static final String CEF_RUNTIME_5068 = "CEF_RUNTIME_5068";
    public static final String CEF_RUNTIME_5069 = "CEF_RUNTIME_5069";
    public static final String CEF_RUNTIME_5070 = "CEF_RUNTIME_5070";
    public static final String CEF_RUNTIME_5071 = "CEF_RUNTIME_5071";
    public static final String CEF_RUNTIME_5072 = "CEF_RUNTIME_5072";
    public static final String CEF_RUNTIME_5073 = "CEF_RUNTIME_5073";
    public static final String CEF_RUNTIME_5074 = "CEF_RUNTIME_5074";
    public static final String CEF_RUNTIME_5075 = "CEF_RUNTIME_5075";
    /**
     * 根据关联字段信息获取表名报错
     */
    public static final String CEF_RUNTIME_5076 = "CEF_RUNTIME_5076";
    /**
     * 对象[{0}]上的字段[{1}]获取持久化值出错,对应BEConfigId:[{2}]
     */
    public static final String CEF_RUNTIME_5077 = "CEF_RUNTIME_5077";
    /**
     * 解析字段[{0}]对应过滤条件的值出错，对应条件值[{1}]
     */
    public static final String CEF_RUNTIME_5078 = "CEF_RUNTIME_5078";
    /**
     * 对象[{0}]上的字段[{1}]获取数据库值出错
     */
    public static final String CEF_RUNTIME_5079 = "CEF_RUNTIME_5079";
    /**
     * 对象[{0}]上的字段[{1}]转换为dbColumnInfo出错
     */
    public static final String CEF_RUNTIME_5080 = "CEF_RUNTIME_5080";
    /**
     * 对象[{0}]上的字段[{1}]转换值类型出错,传入值为[{2}]
     */
    public static final String CEF_RUNTIME_5081 = "CEF_RUNTIME_5081";

}
