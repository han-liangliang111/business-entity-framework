package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.sql.Connection;

/**
 * @author Kaixuan Shi
 * @since 2024/2/26
 */
public class Varchar2IntTransProcesser implements ITypeTransProcesser {

    public static Varchar2IntTransProcesser getInstance() {
        return Singleton.INSTANCE;
    }

    @Override
    public Object transType(FilterCondition filter, Connection db) {
        return this.transType(filter.getValue());
    }

    @Override
    public Object transType(Object value) {
        return this.transType(value, true);
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        if (value == null || "".equals(value)) {
            return null;
        }
        if (value instanceof String) {
            return Integer.parseInt(value.toString());
        }
        if (value instanceof Integer) {
            return value;
        }
        throw new IllegalArgumentException(value.getClass().toString());
    }

    private static class Singleton {
        static Varchar2IntTransProcesser INSTANCE = new Varchar2IntTransProcesser();
    }
}
