/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.repository.DbParameter;
import io.iec.caf.data.jpa.repository.CafEntityManagerWrapper;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import javax.persistence.EntityManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

/**
 * 批量SQL执行器，以提升sql（尤其是批量SQL）执行性能
 */
public class BatchSqlExecutor implements AutoCloseable {

    /**第一维度为nodecode, 第二维度为sql*/
    private LinkedHashMap<String, LinkedHashMap<String,BatchSqlStatement>> batchStmts;
    private CafEntityManagerWrapper wrapper;

    public BatchSqlExecutor(){
        this.batchStmts = new LinkedHashMap<>(4);
    }

    /**
     * 添加批次执行的sql及变量
     * @param nodeCode 对应的表编号
     * @param sql sql文本
     * @param pars 变量列表
     * @throws SQLException
     */
    public BatchSqlStatement addBatch(String nodeCode, String sql, List<DbParameter> pars) throws SQLException {
        Objects.requireNonNull(nodeCode, "nodeCode");
        Objects.requireNonNull(sql, "sql");

        //根据nodeCode、sql获取对应的PreparedStatement
        BatchSqlStatement batchSqlStatement = this.getBatchSqlStatement(nodeCode,sql);
        batchSqlStatement.addBatch(pars,1);
        return batchSqlStatement;
    }

    /**
     * 根据nodeCode、sql获取对应的BatchSqlStatement
     */
    private BatchSqlStatement getBatchSqlStatement(String nodeCode,String sql)throws SQLException{
        LinkedHashMap<String,BatchSqlStatement> map =
                this.batchStmts.computeIfAbsent(nodeCode,k-> new LinkedHashMap<>(4));
        if(!map.containsKey(sql)){
            PreparedStatement stmt = this.createPrepareStatement(sql);
            map.put(sql,new BatchSqlStatement(stmt));
        }
        return map.get(sql);
    }
    /**
     * 根据sql创建对应的PreparedStatement
     */
    private PreparedStatement createPrepareStatement(String sql) throws SQLException {
        if (wrapper == null) {
            wrapper = new CafEntityManagerWrapper(SpringBeanUtils.getBean(EntityManager.class));
        }
        return wrapper.prepareStatement(sql);
    }

    /**
     * 刷新批处理（仅清空，不清理）
     * @throws SQLException
     */
    public void flushBatch() throws SQLException {
        if (batchStmts == null || batchStmts.isEmpty()) {
            return;
        }
        //循环执行批量SQL，并close
        for (LinkedHashMap<String,BatchSqlStatement> map : batchStmts.values()) {
            if (map == null || map.isEmpty()) {
                continue;
            }
            for (BatchSqlStatement batchSqlStatement : map.values()){
                batchSqlStatement.executeBatch();
            }
        }
    }

    /**
     * 关闭资源
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        //关闭所有Statement
        this.closeAllStatements();
        //关闭连接
        if (wrapper != null) {
            wrapper.close();
        }
    }

    private void closeAllStatements() throws Exception{
        if (batchStmts == null || batchStmts.isEmpty()) {
            return;
        }
        //循环close
        for (LinkedHashMap<String,BatchSqlStatement> map : batchStmts.values()) {
            if (map == null || map.isEmpty()) {
                continue;
            }
            for (BatchSqlStatement batchSqlStatement : map.values()){
                batchSqlStatement.close();
            }
            map.clear();
        }
        //清理列表
        this.batchStmts.clear();
    }
}
