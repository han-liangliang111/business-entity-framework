package com.inspur.edp.cef.repository.adaptoritem.jdbcprocessor;

import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.i18n.framework.api.InternalI18nContext;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Date;

/**
 * @className: DbProcessor
 * @author: wangmj
 * @date: 2023/10/30
 **/
public class DbProcessor {
    private Calendar cal;

    public void setPar(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        switch (param.getDataType()) {
            case Jsonb:
                buildJsonbParmeter(stmt, i, param);
                break;
            case VarChar:
                buildVarcharParmeter(stmt, i, param);
                break;
            case NVarChar:
                buildNVarcharParmeter(stmt, i, param);
                break;
            case NChar:
                buildNCharParmeter(stmt, i, param);
                break;
            case Blob:
                buildBlobParameter(stmt, i, param);
                break;
            case Clob:
                //TODO 后续测试DM数据库是否合适
                buildClobParameter(stmt, i, param);
                break;
            case SmallInt:
                buildSmallIntParameter(stmt, i, param);
                break;
            case Int:
                buildIntParameter(stmt, i, param);
                break;
            case Long:
                buildLongParameter(stmt, i, param);
                break;
            case Decimal:
                buildDecimalParameter(stmt, i, param);
                break;
            case Char:
                buildCharParameter(stmt, i, param);
                break;
            case NClob:
                buildNClobParameter(stmt, i, param);
                break;
            case DateTime:
                buildDateTimeParameter(stmt, i, param);
                break;
            case Date:
                buildDateParameter(stmt, i, param);
                break;
            case Boolean:
                buildBooleanParameter(stmt, i, param);
                break;
            default:
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5018);
        }
    }

    protected void buildJsonbParmeter(PreparedStatement stmt, int i, DbParameter param) throws SQLException {
        stmt.setObject(i, param.getValue(), Types.OTHER);
    }

    protected void buildVarcharParmeter(PreparedStatement stmt, int i, DbParameter param) throws SQLException {
        stmt.setObject(i, param.getValue());
    }

    protected void buildNVarcharParmeter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setString(i, (String) param.getValue());
    }

    protected void buildNCharParmeter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setString(i, (String) param.getValue());
    }

    protected void buildBlobParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setBytes(i, (byte[]) param.getValue());
    }

    protected void buildClobParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setString(i, (String) param.getValue());
    }

    protected void buildSmallIntParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.SMALLINT);
        } else {
            //todo 增加类型判断
            if(param.getValue() instanceof Short){
                stmt.setShort(i, (Short) param.getValue());
            }
            else {
                stmt.setShort(i, Short.parseShort(param.getValue().toString()));
            }
        }
    }
    protected void buildIntParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.INTEGER);
        } else {
            if(param.getValue() instanceof Integer){
                stmt.setInt(i, (Integer) param.getValue());
            }
            else {
                stmt.setInt(i, Integer.parseInt(param.getValue().toString()));
            }
        }
    }

    protected void buildLongParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.BIGINT);
        } else {
            if(param.getValue() instanceof Long){
                stmt.setLong(i, (Long) param.getValue());
            }
            else{
                stmt.setLong(i, Long.parseLong(param.getValue().toString()));
            }
        }
    }

    protected void buildDecimalParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setBigDecimal(i, (BigDecimal) param.getValue());
    }

    protected void buildCharParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setString(i, (String) param.getValue());
    }

    protected void buildNClobParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setString(i, (String) param.getValue());
    }

    protected void buildDateTimeParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.TIMESTAMP);
        } else {
            if (CAFContext.current.getDbType() == DbType.OceanBase) {
                if (param.getValue() instanceof Date) {
                    stmt.setTimestamp(i, new Timestamp(((Date) param.getValue()).getTime()), getCalendar());
                } else {
                    stmt.setString(i, param.getValue().toString());
                }
            } else {
                stmt.setTimestamp(i, new Timestamp(((Date) param.getValue()).getTime()), getCalendar());
            }
        }
    }

    protected void buildBooleanParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.BOOLEAN);
        } else {
            stmt.setBoolean(i, (boolean) param.getValue());
        }
        //query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
    }

    protected void buildDateParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setNull(i, Types.DATE);
        } else {
            if (CAFContext.current.getDbType() == DbType.OceanBase) {
                if (param.getValue() instanceof Date) {
                    stmt.setTimestamp(i, new Timestamp(((Date) param.getValue()).getTime()), getCalendar());
                } else {
                    stmt.setString(i, param.getValue().toString());
                }
            } else {
                stmt.setDate(i, new java.sql.Date(((Date) param.getValue()).getTime()), getCalendar());
            }
        }
//      if (param.getValue() == null) {
//        query.setParameter(i, (Date) param.getValue(), TemporalType.DATE);
//        return;
//      }
//
//      java.sql.Date date = new java.sql.Date(((Date) param.getValue()).getTime());
//      date = java.sql.Date.valueOf(date.toString());
//      query.setParameter(i, date, TemporalType.DATE);
    }

    protected Calendar getCalendar() {
        if (cal == null) {
            cal = Calendar.getInstance(SpringBeanUtils.getBean(InternalI18nContext.class).getDBTimezone());
        }
        return cal;
    }
}
