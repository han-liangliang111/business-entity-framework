package com.inspur.edp.cef.repository.filter;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import lombok.Data;

import java.util.List;

/**
 * @className: FilterContext过滤条件上下文，用来传递过滤条件解析相关参数
 * @author: wangmj
 * @date: 2024/5/21
 **/
@Data
public class FilterContext {
    /**
     * 要解析的过滤条件对象
     */
    private FilterCondition filterCondition;

    /**
     * 过滤条件字段对应数据库字段名称
     */
    private String dbColumnName;

    /**
     * 绑定参数
     */
    List<DbParameter> parameters;

    /**
     *对应的BE字段数据类型
     */
    private GspDbDataType dataType;

    /**
     * 参数索引
     */
    private RefObject<Integer> paramNum;

    /**
     * 类型转换处理器
     */
    private ITypeTransProcesser processer;

    /**
     * 参数绑定是否使用数字占位符 应用过滤条件的场景：query、delete、modify 这几种场景下只有modify解析占位符不使用数字
     */
    private boolean useNumberBinding = true;
}
