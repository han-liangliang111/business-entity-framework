package com.inspur.edp.cef.repository.extend;

import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.repository.adaptoritem.AdaptorItem;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.cef.spi.repository.FieldContext;
import com.inspur.edp.cef.spi.repository.IFieldReposExtendProcessor;

import java.util.HashMap;
import java.util.List;

public class RepoExtendManager {
    public static String buildQueryFields(String tableAlias, DbColumnInfoCollection dbColumnInfos, EntityFilter entityFilter, HashMap<String, Integer> mapping) {
        StringBuilder columns = new StringBuilder();
        boolean hasPropDBIndexMapping = false;
        if (mapping == null) {
            mapping = new HashMap<String, Integer>();
        }
        boolean usePropertyFilter = false;
        List<String> filterProperties = null;
        if (entityFilter != null && entityFilter.getFieldsFilter() != null && entityFilter.getFieldsFilter().isUseFieldsCondition()) {
            usePropertyFilter = true;
            filterProperties = entityFilter.getFieldsFilter().getFilterFields();
        }
        int index = (mapping == null || mapping.size() == 0) ? 0 : mapping.size();
        buildQueryColumns(tableAlias, usePropertyFilter, filterProperties, dbColumnInfos, columns, mapping, hasPropDBIndexMapping, index);
        return columns.toString();
    }

    private static int buildQueryColumns(String tableAlias, boolean usePropertyFilter, List<String> filterProperties,
                                         DbColumnInfoCollection columnCollection,
                                         StringBuilder columns,
                                         HashMap<String, Integer> mapping,
                                         boolean hasPropDBIndexMapping,
                                         int index) {
        boolean containColumns = columns.length() > 0;
        if (usePropertyFilter) {
            for (DbColumnInfo containColumn : columnCollection) {
                IFieldReposExtendProcessor processor = FieldExtendManager.getInstance().getFieldReposeExtendProcessor(containColumn);
                //判断如果是虚拟字段，且使用内置的处理器，跳过
                if (containColumn.isVirtual() && (processor instanceof BuiltInFieldProcessor)) {
                    continue;
                }
                FieldContext fieldContext = new FieldContext();
                fieldContext.setDbColumnInfo(containColumn);
                fieldContext.setIndex(index);
                fieldContext.setVariableData(containColumn.getVariableData());
                //todo 这个地方进一步处理
                if (containColumn.getDbColumnName().contains(".")) {
                    fieldContext.setTableAlias(containColumn.getDbColumnName().split("\\.")[0]);
                } else {
                    fieldContext.setTableAlias(tableAlias);
                }
                if (filterProperties.contains(containColumn.getColumnName()) == false) {
                    if (containColumn.getIsAssociateRefElement() && filterProperties.contains(containColumn.getBelongElementLabel())) {
                        //这个地方要支持虚拟字段了
                    }
                    if (containColumn.getBelongElementLabel() != null && containColumn.getBelongElementLabel().isEmpty() == false && filterProperties.contains(containColumn.getBelongElementLabel())) {
                    } else
                        continue;
                }

                if (containColumns) {
                    columns.append(",");
                }
                columns.append(processor.getQueryField(fieldContext));
                containColumns = true;
                if (hasPropDBIndexMapping == false) {
                    mapping.put(containColumn.getColumnName(), index++);
                }
            }
        } else {
            for (DbColumnInfo containColumn : columnCollection) {
                IFieldReposExtendProcessor processor = FieldExtendManager.getInstance().getFieldReposeExtendProcessor(containColumn);
                //判断如果是虚拟字段，且使用内置的处理器，跳过
                if (containColumn.isVirtual() && (processor instanceof BuiltInFieldProcessor)) {
                    continue;
                }
                FieldContext fieldContext = new FieldContext();
                fieldContext.setDbColumnInfo(containColumn);
                fieldContext.setVariableData(containColumn.getVariableData());
                fieldContext.setIndex(index);
                if (containColumn.getDbColumnName().contains(".")) {
                    fieldContext.setTableAlias(containColumn.getDbColumnName().split("\\.")[0]);
                } else {
                    fieldContext.setTableAlias(tableAlias);
                }
                if (containColumns) {
                    columns.append(",");
                }
                String selectField = processor.getQueryField(fieldContext);
                //扩展的要处理一些别名，防止别名重复
                if (StringUtils.isNullOrEmpty(selectField)) {
                    String[] message = new String[]{fieldContext.getDbColumnInfo().getColumnName(), processor.getClass().getName()};
                    throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5060, message);
                }
                columns.append(selectField);
//                columns.append(trans2DbColumnWithAlias(containColumn.getColumnName())).append(" AS ").append(containColumn.getAliasName(index));
                containColumns = true;
                if (hasPropDBIndexMapping == false) {
                    mapping.put(containColumn.getColumnName(), index++);
                }
            }
        }
        return index;
    }

    /**
     * 获取查询字段
     *
     * @param entityFilter
     * @param mapping
     * @return
     */
    public String getQueryFields(EntityFilter entityFilter, HashMap<String, Integer> mapping) {
        return "";
    }
}
