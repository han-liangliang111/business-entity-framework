/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.authority.AuthorityInfoType;
import com.inspur.edp.cef.api.repository.DbParameter;
import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.GspDbType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.api.repository.RefColumnInfo;
import com.inspur.edp.cef.api.repository.TableNameRefContext;
import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.ExpressCompareType;
import com.inspur.edp.cef.entity.condition.ExpressRelationType;
import com.inspur.edp.cef.entity.condition.FieldsFilter;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.entity.condition.RetrieveFilter;
import com.inspur.edp.cef.entity.condition.SortCondition;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.entity.repository.DataSaveParameter;
import com.inspur.edp.cef.repository.adaptor.AdaptorRetrieveParam;
import com.inspur.edp.cef.repository.adaptor.EntityRelationalAdaptor;
import com.inspur.edp.cef.repository.adaptor.KeyWordsManager;
import com.inspur.edp.cef.repository.adaptor.MultiLangColumnInfo;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.adaptoritem.sqlsnippetprocessor.SqlSnippetProcessor;
import com.inspur.edp.cef.repository.assembler.AbstractDataAdapterExtendInfo;
import com.inspur.edp.cef.repository.assembler.AssoCondition;
import com.inspur.edp.cef.repository.assembler.AssociationInfo;
import com.inspur.edp.cef.repository.dac.DacSaveContext;
import com.inspur.edp.cef.repository.dac.EntityDac;
import com.inspur.edp.cef.repository.dbcolumninfo.ComplexUdtRefColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfoCollection;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.CefRunTimeException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.repository.extend.RepoExtendManager;
import com.inspur.edp.cef.repository.repo.BaseRootRepository;
import com.inspur.edp.cef.repository.utils.DatabaseUtil;
import com.inspur.edp.cef.repository.utils.FilterUtil;
import com.inspur.edp.cef.repository.utils.RepositoryUtil;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.ComplexUdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.RefDataTypePropertyInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.UdtPropertyInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.exception.ExceptionLevel;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.runtime.config.CefBeanUtil;

import javax.persistence.Query;
import java.sql.SQLException;
import java.util.*;

/**
 * 据说生成型从此类继承
 */
public abstract class EntityRelationalReposAdaptor extends EntityRelationalAdaptor {

    private CefEntityResInfoImpl entityResInfo;
    private EntityDac entityDac;
    private List<AdaptorItem> adaptorItemList;
    private DbColumnInfoCollection tempContainColumns = new DbColumnInfoCollection();
    private List<AssociationInfo> extendAssociations = new ArrayList<>();
    //是关联进入，还是直接进入 并发下构造的关联columninfo 别名可能不正确
    private boolean isInitColumn = true;

    private String tableAlias;
    private String originTableAlias = "";
    //过滤字段查询
    private FieldsFilter fieldsFilter;

    protected EntityRelationalReposAdaptor(boolean init, EntityDac entityDac) {
        super(init);
        this.entityDac = entityDac;
    }

    public EntityRelationalReposAdaptor(boolean init, EntityDac entityDac, CefEntityResInfoImpl entityResInfo, String tableAlias) {
        this(init, entityDac);
        this.entityResInfo = entityResInfo;
        this.tableAlias = tableAlias;
        this.originTableAlias = tableAlias;
        if (this.entityResInfo.getSqlSnippetCache() == null) {
            //entiyResInfo类本身，确保针对同一个resIfo，并发的时候，获取到的getContainColumns对象是唯一的
            synchronized (this.entityResInfo.getClass()) {
                if (this.entityResInfo.getSqlSnippetCache() == null) {
                    entityResInfo.setSqlSnippetCache(new SqlSnippetCache());
                }
            }
        }
        addAdaptorItem(new BaseAdaptorItem(this, 1));
    }

    public DbProcessor getDbProcessor() {
        return getSqlSnippetProcessor().getDbProcessor();
    }

    private SqlSnippetProcessor getSqlSnippetProcessor() {
        return getSqlSnippetCache().getSqlSnippetProcessor(entityDac.getGspDbtype().toString(), this);
    }


    protected String getNodeCode() {
        return entityDac.getNodeCode();
    }

    //region VersionControl
    protected String getVersionControlPropName() {
        if (entityResInfo == null)
            return "";
        return entityResInfo.getVersionControlPropertyName();
    }

    @Override
    public void setDataAdapterExtendInfos(ArrayList<AbstractDataAdapterExtendInfo> infos) {
        super.setDataAdapterExtendInfos(infos);
        extendContainColumns = new DbColumnInfoCollection();
        for (AbstractDataAdapterExtendInfo info : infos) {
            if (info.getDbColumnInfos() == null)
                continue;
            extendContainColumns.addRange(info.getDbColumnInfos());
        }
        for (AbstractDataAdapterExtendInfo info : infos) {
            if (info.getAssociationInfos() == null)
                continue;
            for (AssociationInfo associationInfo : info.getAssociationInfos()) {
                if (!extendAssociations.contains(associationInfo)) {
                    extendAssociations.add(associationInfo);
                }
            }
        }
    }

    //region TODO 应该不需要生成
    protected boolean hasPropColumnMappping() {
        return getSqlSnippetCache().getPropIndexMappingDict() != null;
    }

    protected HashMap<String, Integer> getPropertyColumnMapping() {
        return getSqlSnippetCache().getPropIndexMappingDict();
    }

    protected void setPropertyColumnMapping(HashMap<String, Integer> mapping) {
        getSqlSnippetCache().setPropIndexMappingDict(mapping);
    }

    public HashMap<String, String> getAssosPropDBMapping(String propName) {
        return ((BaseAdaptorItem) getBaseAdaptorItem()).getAssosPropDBMapping(propName);
    }

    @Override
    protected String getQueryFields(EntityFilter filter) {
        StringBuilder sb = new StringBuilder();
        HashMap<String, Integer> mapping = new HashMap<>();

        for (int i = 0; i < adaptorItemList.size(); i++) {
            if (i > 0) {
                sb.append(",").append(adaptorItemList.get(i).getQueryFields(filter, mapping));
            } else {
                sb.append(adaptorItemList.get(i).getQueryFields(filter, mapping));
            }
        }
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            //运行时定制修改Mappingying影响到了缓存
            HashMap<String, Integer> tempMapping = new HashMap<>();
            //todo 这个地方直接赋值 返回到调用处不好使。。后续研究原因
            for (Map.Entry<String, Integer> item : mapping.entrySet()) {
                tempMapping.put(item.getKey(), item.getValue());
            }
            RepoExtendManager repoExtendManager = new RepoExtendManager();
            String queryFields = repoExtendManager.buildQueryFields(getTableAlias(), extendContainColumns, filter, tempMapping);
            if (!StringUtils.isNullOrEmpty(queryFields)) {
                sb.append(",").append(queryFields);
            }
            filteredPropDbIndexMapping = tempMapping;
        } else {
            filteredPropDbIndexMapping = mapping;
            if (filteredPropDbIndexMapping == null || filteredPropDbIndexMapping.size() == 0) {//换成过滤字段的情况
                filteredPropDbIndexMapping = getBaseAdaptorItem().getSqlCache().getQueryMappingMap().get(getGspDbType().toString());
            }
        }
        return sb.toString();
    }

    private String getQueryFieldsWithOutCache(EntityFilter filter) {
        StringBuilder sb = new StringBuilder();
        HashMap<String, Integer> mapping = new HashMap<>();

        for (int i = 0; i < adaptorItemList.size(); i++) {
            if (i > 0) {
                sb.append(",").append(adaptorItemList.get(i).getQueryFieldsWithOutCache(filter, mapping));
            } else {
                sb.append(adaptorItemList.get(i).getQueryFieldsWithOutCache(filter, mapping));
            }
        }
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            String columns = RepoExtendManager.buildQueryFields(getTableAlias(), extendContainColumns, filter, mapping);
            if (columns.length() > 0) {
                sb.append(",").append(columns);
            }
        }
        filteredPropDbIndexMapping = mapping;
        return sb.toString();
    }

    private String getQueryFields(AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam.getEntityfilter(getNodeCode());
        // 构造非多语列列名
        String fields = getQueryFields(filter);
        if (adaptorRetrieveParam == null || !adaptorRetrieveParam.isEnableMultiLanguage()) {
            return fields;
        }
        String multiLangFields = buildQueryColumns_MultiLanguage(filter);
        if (multiLangFields.length() > 0 && fields.length() > 0) {
            fields = fields.concat("," + multiLangFields);
        }
        return fields;
    }

    private String getQueryFieldsWithOutCache(AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = null;
        if (adaptorRetrieveParam != null) {
            filter = adaptorRetrieveParam.getEntityfilter(getNodeCode());
        }
        // 构造非多语列列名
        String fields = getQueryFieldsWithOutCache(filter);
        if (adaptorRetrieveParam == null || !adaptorRetrieveParam.isEnableMultiLanguage()) {
            return fields;
        }
        String multiLangFields = buildQueryColumns_MultiLanguage(filter);
        if (multiLangFields.length() > 0 && fields.length() > 0) {
            fields = fields.concat("," + multiLangFields);
        }
        return fields;
    }

    @Override
    protected String getQueryTableNameWithAuthority(ArrayList<AuthorityInfo> authorities, EntityFilter filter) {
        String tempQueryTableName = "";
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().isUseFieldsCondition()) {
            this.fieldsFilter = filter.getFieldsFilter();
        }
        initQueryTableName();
        tempQueryTableName = queryTableName;
        String authority = getAuthoritySql(authorities);
        return tempQueryTableName + authority;
    }

    private String getAuthoritySql(ArrayList<AuthorityInfo> authorities) {
        if (authorities == null || authorities.size() < 1) {
            return "";
        }
        StringBuilder authorityBuilder = new StringBuilder();
        for (int i = 1; i < authorities.size() + 1; i++) {
            AuthorityInfo info = authorities.get(i - 1);
            if (info.getAuthType() != AuthorityInfoType.Join) {
                continue;
            }
            String aliasName = authorityTableAlias + i;
            String fieldName = trans2DbColumnWithAlias(info.getFieldName());
            String sourceFieldName = aliasName + "." + info.getSourceFieldName();

            authorityBuilder.append(String.format(authoritySql, info.getAuthoritySql(), aliasName, fieldName, sourceFieldName));
        }

        return authorityBuilder.toString();
    }

    @Override
    protected void buildLogicDeleteCondition(EntityFilter entityFilter) {
        if (!getLogicDeleteInfo().isEnableLogicDelete())
            return;
        if (entityFilter == null)
            entityFilter = new EntityFilter();
        if (entityFilter.getFilterConditions() == null) {
            ArrayList<FilterCondition> filterConditions = new ArrayList<>();
            entityFilter.setFilterConditions(filterConditions);
        }
        if (entityFilter.getFilterConditions().size() > 0) {//修改最后一个的relation符号
            entityFilter.getFilterConditions().get(0).setLbracket(entityFilter.getFilterConditions().get(0).getLbracket() + "(");
            entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).setRbracket(entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).getRbracket() + ")");
            entityFilter.getFilterConditions().get(entityFilter.getFilterConditions().size() - 1).setRelation(ExpressRelationType.And);
        }
        FilterCondition filterCondition = new FilterCondition();
        filterCondition.setValue("1");
        filterCondition.setFilterField(getLogicDeleteInfo().getLabelId());
        filterCondition.setCompare(ExpressCompareType.NotEqual);
        entityFilter.getFilterConditions().add(filterCondition);
    }

    @Override
    public final EntityDac getEntityDac() {
        return this.entityDac;
    }

    private String buildDefaultOrderByCondition(ArrayList<SortCondition> sortConditions) {
        StringBuilder orderByBuilder = new StringBuilder();
        if (sortConditions != null && sortConditions.size() > 0) {
            for (SortCondition orderByItem : sortConditions) {
                if (orderByBuilder.toString() != null && !orderByBuilder.toString().equals("")) {
                    orderByBuilder.append(", ");
                }
                String filedName = trans2DbColumnWithAlias(orderByItem.getSortField());
                orderByBuilder.append(orderByItem.trans2Sql(filedName));
            }
        }
        return String.valueOf(orderByBuilder);
    }

    @Override
    public final List<IChildEntityData> getDataWithParentJoinIds(String joinInfo, List<String> conditions
            , ArrayList<SortCondition> orderByCondition, ArrayList<String> tableAlias, List<DbParameter> dbPars, AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam != null ? adaptorRetrieveParam.getEntityfilter(getNodeCode()) : null;
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().isUseFieldsCondition()) {
            this.fieldsFilter = filter.getFieldsFilter();
        }
        initQueryTableNameWithParentAlias(tableAlias, adaptorRetrieveParam);
        List<IChildEntityData> childDatas = new ArrayList<IChildEntityData>();
        //todo conidtions过来必须有值
        if (conditions != null && conditions.size() > 0) {
            for (String condition : conditions) {
                List<DbParameter> tempdbPars = new ArrayList<>();
                if (getLogicDeleteInfo().isEnableLogicDelete()) {
                    if (dbPars != null && dbPars.size() > 0) {
                        tempdbPars.addAll(dbPars);
                    }
                    tempdbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
                    condition += " and " + getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId() + " = ?" + (tempdbPars.size() - 1);
                }

                String sql = "";
                DbType dbType = CAFContext.current.getDbType();
                //对大部分数据库，关联表的顺序不会影响性能，优化器会进行处理。Oscar有参数（支持几个表）配置；Pg数据库查询优化器做的不好，当表数量大于5个时（经验）会较慢
                if (!StringUtils.isNullOrEmpty(joinInfo) && (dbType == DbType.Oscar || dbType == DbType.PgSQL)) {
                    sql = String.format(GetDataWithParentJoinSql, getQueryFieldsWithOutCache(adaptorRetrieveParam), getTableName() + " " + joinInfo, joinTableName, condition);
                } else {
                    sql = String.format(GetDataWithParentJoinSql, getQueryFieldsWithOutCache(adaptorRetrieveParam), queryTableName, joinInfo, condition);
                }

                sql += buildRetrieveOrderBy(orderByCondition);
                RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
                sql = formatFiscalAndMultiLang(sql, retrieveFilter);
                List<IEntityData> datas = getDatas(sql, tempdbPars, adaptorRetrieveParam);
                if (datas == null)
                    return null;
                for (IEntityData data : datas) {
                    childDatas.add((IChildEntityData) data);
                }
            }
        }
        return childDatas;
    }

    public final List<IChildEntityData> getDataWithParentJoinIds(String joinInfo, String condition
            , ArrayList<SortCondition> orderByCondition, ArrayList<String> tableAlias, List<DbParameter> dbPars, AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam != null ? adaptorRetrieveParam.getEntityfilter(getNodeCode()) : null;
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().isUseFieldsCondition()) {
            this.fieldsFilter = filter.getFieldsFilter();
        }
        initQueryTableNameWithParentAlias(tableAlias, adaptorRetrieveParam);
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            if (dbPars == null)
                dbPars = new ArrayList<>();
            dbPars.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
            condition += " and " + getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId() + " = ?" + (dbPars.size() - 1);
        }

        String sql = "";
        DbType dbType = CAFContext.current.getDbType();
        //对大部分数据库，关联表的顺序不会影响性能，优化器会进行处理。Oscar有参数（支持几个表）配置；Pg数据库查询优化器做的不好，当表数量大于5个时（经验）会较慢
        if (!StringUtils.isNullOrEmpty(joinInfo) && (dbType == DbType.Oscar || dbType == DbType.PgSQL)) {
            sql = String.format(GetDataWithParentJoinSql, getQueryFieldsWithOutCache(adaptorRetrieveParam), getTableName() + " " + joinInfo, joinTableName, condition);
        } else {
            sql = String.format(GetDataWithParentJoinSql, getQueryFieldsWithOutCache(adaptorRetrieveParam), queryTableName, joinInfo, condition);
        }
        sql += buildRetrieveOrderBy(orderByCondition);
        RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
        sql = formatFiscalAndMultiLang(sql, retrieveFilter);
        List<IEntityData> datas = getDatas(sql, dbPars, adaptorRetrieveParam);
        if (datas == null)
            return null;
        List<IChildEntityData> childDatas = new ArrayList<IChildEntityData>();
        for (IEntityData data : datas) {
            childDatas.add((IChildEntityData) data);
        }
        return childDatas;
    }

    private String buildRetrieveOrderBy(ArrayList<SortCondition> orderBys) {
        if (orderBys == null || orderBys.size() < 1) {
            return "";
        }
        return " ORDER BY " + buildDefaultOrderByCondition(orderBys);
    }

    public GspDbType getGspDbType() {
        return entityDac.getGspDbtype();
    }

    public void addAdaptorItem(AdaptorItem adaptorItem) {
        if (adaptorItemList == null)
            adaptorItemList = new ArrayList<AdaptorItem>();
        adaptorItemList.add(adaptorItem);
    }

    public AdaptorItem getBaseAdaptorItem() {
        return adaptorItemList.get(0);
    }

    public final DbColumnInfoCollection getContainColumns(boolean isInitColumn) {
        if (isInitColumn) {
            return getContainColumns();
        } else {
            return tempContainColumns;
        }
    }

    public final DbColumnInfoCollection getContainColumns() {
        return getBaseAdaptorItem().getContainColumns();
    }

    public AdaptorItem getAdaptorItem(String columnName) {
        for (AdaptorItem item : adaptorItemList) {
            if (item.getContainColumns() != null && item.getContainColumns().getCount() > 0) {
                if (item.getContainColumns().contains(columnName))
                    return item;
            }
        }
        return adaptorItemList.get(0);
    }

    public DbColumnInfoCollection getContainColumns(String columnName) {
        DbColumnInfoCollection containedColumns = new DbColumnInfoCollection();
        for (AdaptorItem item : adaptorItemList) {
            if (item.getContainColumns() != null && item.getContainColumns().getCount() > 0) {
                if (item.getContainColumns().contains(columnName))
                    containedColumns.addRange(item.getContainColumns());
            }
        }
        return containedColumns;
    }

    /**
     * 看着像是不起作用，因为执行的时候，adaptoerItemList没有内容
     */
    protected void initColumns() {
        if (adaptorItemList != null && !adaptorItemList.isEmpty()) {
            adaptorItemList.forEach(a -> a.initColumns());
        }
    }

    @Override
    protected void initAssociations() {
        if (adaptorItemList != null && !adaptorItemList.isEmpty()) {
            adaptorItemList.forEach(a -> a.initAssociations());
        }
    }

    @Override
    protected java.util.ArrayList<AssociationInfo> getAssociationInfos() {
        ArrayList<AssociationInfo> associationInfos = new ArrayList<AssociationInfo>();
        for (AdaptorItem item : adaptorItemList) {
            if (item.getAssociationInfos() != null && item.getAssociationInfos().size() > 0) {
                associationInfos.addAll(item.getAssociationInfos());
            }
        }
        if (extendAssociations.size() > 0) {
            associationInfos.addAll(extendAssociations);
        }
        return associationInfos;
    }

    protected ArrayList<FilterCondition> getDefaultFilterCondition() {
        return null;
    }

    protected ArrayList<SortCondition> getDefaultSortCondition() {
        return null;
    }

    @Override
    public ICefData createInstance(ICefReader reader) {
        ICefData data = entityDac.createInstance();
        for (AdaptorItem item : adaptorItemList) {
            ((BaseAdaptorItem)item).initAssociationDbMappings(getEntityResInfo());
            item.setEntityValue(reader, (IEntityData) data);
        }
        return data;
    }

    public String getPrimaryKey() {
        if (getSqlSnippetCache().getPrimaryKey() == null || ""
                .equals(getSqlSnippetCache().getPrimaryKey())) {
            for (DbColumnInfo dbColumnInfo : getContainColumns()) {
                if (dbColumnInfo.getIsPrimaryKey())
                    getSqlSnippetCache().setPrimaryKey(dbColumnInfo.getDbColumnName());
            }
        }
        return getSqlSnippetCache().getPrimaryKey();
    }

    public String getTableAlias() {
        return tableAlias;
    }

    public void setTableAlias(String value) {
        tableAlias = value;
    }

    public String getOriginTableAlias() {
        return this.originTableAlias;
    }

    public final String getWrappedOriginTableAlias() {
        if (StringUtils.isNullOrEmpty(getOriginTableAlias()))
            return "";
        return KeyWordsManager.getTableAlias(getOriginTableAlias());
    }

    //protected abstract String DefaultSort { get; }
    protected String getConfigId() {
        return entityResInfo.getModelResInfo().getConfigId();
    }

    ///#region initQueryTableName
    private void initQueryTableName() {
        //if (!String.IsNullOrEmpty(queryTableName))
        //    return;
        ////跟之前一致，并且在当前实例上做缓存。
        //queryTableName = getTableNamesWithAssociationInfo(AssociationInfos, null);
        //buildQueryColumns();
        initQueryTableNameWithParentAlias(null);
    }

    private void initQueryTableNameWithParentAlias(ArrayList<String> alias) {
        this.initQueryTableNameWithParentAlias(alias, new AdaptorRetrieveParam(null));
    }

    private void initQueryTableNameWithParentAlias(ArrayList<String> alias, AdaptorRetrieveParam adaptorRetrieveParam) {
//        if (!(this.fieldsFilter != null && this.fieldsFilter.isUseFieldsCondition()) && queryTableName != null && queryTableName.length() > 0) {
//            return;
//        }
        //跟之前一致，并且在当前实例上做缓存。
        queryTableName = getTableNamesWithAssociationInfo(getAssociationInfos(), alias);
    }

    /**
     * 生成型没有加UDT字段不能为多语的判断
     * @return
     */
    protected DbColumnInfoCollection getMultiLanguageColumnInfos() {
        //region TODO multiLanguageColumnInfos可以缓存起来，没必要每次都new
        this.multiLanguageColumnInfos = new DbColumnInfoCollection();
        //endregion
        for (DbColumnInfo col : getContainColumns()) {
            if (col.getIsMultiLang()) {
                this.multiLanguageColumnInfos.add(col);
                continue;
            }
        }
        return this.multiLanguageColumnInfos;
    }

    /**
     * 构建多语相关的列名 TableName.Name_CHS as Name_CHS
     *
     * @return
     */
    private String buildQueryColumns_MultiLanguage(EntityFilter filter) {
        DbColumnInfoCollection collection = this.getMultiLanguageColumnInfos();
        // 无多语字段
        if (collection.getCount() == 0) {
            return "";
        }

        StringBuilder columns = new StringBuilder();
        HashMap<String, Integer> currentIndexMap = filteredPropDbIndexMapping;//getPropertyColumnMapping();
        if (currentIndexMap == null) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5013);
        }

        List<EcpLanguage> currentEnableLanguages = RepositoryUtil.getCurrentEnabledLanguages();
        for (DbColumnInfo columnInfo : collection) {
            if (columnInfo.isVirtual()) {
                continue;
            }
            // 使用过滤，且不包含，此处不考虑关联带出字段
            if (filter != null && !filter.getFieldsFilter().getFilterFields().contains(columnInfo.getColumnName())) {
                continue;
            }
            for (EcpLanguage language : currentEnableLanguages) {
                String columnName =
                        RepositoryUtil.FormatMuliLangColumnName(trans2DbColumnWithAlias(columnInfo.getColumnName()), language);
                String alias = RepositoryUtil.FormatMuliLangColumnName(columnInfo.getColumnName(), language);
                // 不使用过滤，则全部添加
                if (columns != null && columns.length() > 0) {
                    columns.append(",");
                }
                columns.append(columnName).append(" AS ").append(alias);
                if (!currentIndexMap.containsKey(alias)) {
                    currentIndexMap.put(alias, currentIndexMap.size());
                }
            }
        }
        return columns.toString();
    }

    // region Delete
    protected final String innerGetDeleteSql() {
        if (!getSqlSnippetCache().getInnerDeleteSqlMap().containsKey(entityDac.getGspDbtype().toString())) {
            String innerGetDeleteSql = getSqlSnippetProcessor().innerGetDeleteSql();
            getSqlSnippetCache().getInnerDeleteSqlMap().put(entityDac.getGspDbtype().toString(), innerGetDeleteSql);
            return innerGetDeleteSql;
        }
        return getSqlSnippetCache().getInnerDeleteSqlMap().get(entityDac.getGspDbtype().toString());
    }

    @Override
    public final int delete(String id, DataSaveParameter par) {
        if (getLogicDeleteInfo().isEnableLogicDelete()) {
            return logicDelete(id, par);
        }
        DataValidator.checkForEmptyString(id, "id");
        List<DbParameter> parameters = new ArrayList<>();
        parameters.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), id));
        String sql = String
                .format("%1$s where %2$s.%3$s=?0", getDeleteSql(), getWrappedTableAlias(), getContainColumns().getPrimaryKey().getDbColumnName());

        List<FilterCondition> filter = par.getFilterCondition(getNodeCode(), id);
        if (filter != null && !filter.isEmpty()) {
            String condition = parseFilterCondition(filter, parameters, parameters.size(), false, true);
            sql = sql.concat(condition);
        }

        return executeSql(sql, parameters);

    }


    public final int logicDelete(String id, DataSaveParameter par) {
        DataValidator.checkForEmptyString(id, "id");
        List<DbParameter> parameters = new ArrayList<>();
        parameters.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "1"));
        parameters.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), id));
        String deleteSql = getModifySql() + " " + getLogicDeleteInfo().getLabelId() + " = ?0 where "
                + getContainColumns().getPrimaryKey().getDbColumnName() + "=?1 ";
        List<FilterCondition> filter = par.getFilterCondition(getNodeCode(), id);
        if (filter != null && !filter.isEmpty()) {
            String condition = parseFilterCondition(filter, parameters, parameters.size(), false, true);
            deleteSql = deleteSql.concat(condition);
        }

        return executeSql(deleteSql, parameters);
    }

    //delete from xxx
    protected final String getDeleteSqlBatch() {
        if (!getSqlSnippetCache().getDeleteSqlBatchMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().getDeleteSqlBatch();
            getSqlSnippetCache().getDeleteSqlBatchMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getDeleteSqlBatchMap().get(entityDac.getGspDbtype().toString());
    }

    // endregion

    public final void logicDelete(List<String> ids, DataSaveParameter par) {
        DataValidator.checkForNullReference(ids, "ids");
        if (ids.isEmpty()) {
            throw new IllegalArgumentException();
        }
        for (String item : ids) {
            FilterUtil.checkInParameterForSqlInjection(item);
        }
        executeSql(formatFiscalAndMultiLang(getDeleteSqlBatch()) + getInFilter(ids), null);
    }

    // region Insert
    protected final String innerGetInsertSql() {
        if (!getSqlSnippetCache().getInnerInsertSqlMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().innerGetInsertSql();
            getSqlSnippetCache().getInnerInsertSqlMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getInnerInsertSqlMap().get(entityDac.getGspDbtype().toString());
    }

    protected String getInsertFields() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < adaptorItemList.size(); i++) {
            if (i > 0) {
                sb.append(",").append(adaptorItemList.get(i).getInsertFields(isMultiData));
            } else {
                sb.append(adaptorItemList.get(i).getInsertFields(isMultiData));
            }
        }
        return sb.toString();
        //region TODO 新增字段缓存起来
//        if (isMultiData) {
//            return getInsertFields_MultiLanguage(getContainColumns());
//        }
//        //TODO 后续加入生成基础be的插入字段
//        return getInsertFields(getContainColumns());
        //endregion
    }

    private String getInsertFields(DbColumnInfoCollection columns) {
        StringBuilder fields = new StringBuilder();
        boolean contain = false;
        for (DbColumnInfo containColumn : columns) {
            if (containColumn.getIsAssociateRefElement()) {
                continue;
            }
            if (!containColumn.getIsPersistent()) {
                continue;
            }
            if (contain)
                fields.append(",");
            String dbCol = containColumn.getDbColumnName();
            if (containColumn.getIsMultiLang()) {
                dbCol = dbCol + "@Language@";
            }
            fields.append(dbCol);
            contain = true;
        }
        return fields.toString();
    }

    protected String getInsertValues(RefObject<Integer> valuesCount) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < adaptorItemList.size(); i++) {
            if (i > 0) {
                sb.append(",").append(adaptorItemList.get(i).getInsertValues(isMultiData, valuesCount));
            } else {
                sb.append(adaptorItemList.get(i).getInsertValues(isMultiData, valuesCount));
            }
        }
        return sb.toString();
        //region TODO value缓存起来
//        if (isMultiData) {
//            return getInsertValues_MultiLanguage(getContainColumns(), valuesCount);
//        }
//        return getInsertValues(getContainColumns(),valuesCount);
        //ednregion
    }

    private String getInsertValues(DbColumnInfoCollection columns, RefObject<Integer> valuesCount) {
        StringBuilder values = new StringBuilder();
        for (DbColumnInfo containColumn : columns) {
            if (containColumn.getIsAssociateRefElement()) {
                continue;
            }
            if (!containColumn.getIsPersistent()) {
                continue;
            }
            if (valuesCount.argvalue > 0)
                values.append(",");
            values.append("?");
            valuesCount.argvalue = valuesCount.argvalue + 1;
        }
        return values.toString();
    }

    public final String getInsertSql() {
        return getInsertSql(new RefObject<Integer>(0));
    }

    private String getInsertSql(RefObject<Integer> fieldsCount) {
        return getInnerInsertSql(new RefObject<Integer>(0)).replace("@TableName@", innerGetTableName());
    }

    private String getInnerInsertSql(RefObject<Integer> fieldsCount) {
        Objects.requireNonNull(fieldsCount);

        String insertFields = getInsertFields();
        fieldsCount.argvalue = 0;
        String insertValues = getInsertValues(fieldsCount);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder = stringBuilder.append("INSERT INTO @TableName@  (").append(insertFields);
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            String extInsertFields = getInsertFields(extendContainColumns);
            //此处判断处理扩展字段全部非持久化，不拼接,
            if (extInsertFields.length() > 0) {
                stringBuilder.append(",").append(getInsertFields(extendContainColumns));
            }
        }
        stringBuilder.append(" ) Values ( ").append(insertValues);
        if (extendContainColumns != null && extendContainColumns.getCount() > 0) {
            stringBuilder.append(getInsertValues(extendContainColumns, fieldsCount));
        }
        stringBuilder.append(")");
        return stringBuilder.toString();
    }

    @Override
    public int insert(IEntityData data, DataSaveParameter par, DacSaveContext batcher) throws SQLException {
        DataValidator.checkForNullReference(data, "data");
        List<FilterCondition> filter = par.getFilterCondition(getNodeCode(), data.getID());
        if (filter != null && !filter.isEmpty()) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5039, false, data.getID());
        }
        //data默认都是实现IMultiLanguageData接口，主要是提交的变更中是否包含多语结构，形成多语信息
        isMultiData = (data instanceof IMultiLanguageData) && !((IMultiLanguageData) data)
                .getMultiLanguageInfos().isEmpty();

        String sql = getInsertSql(new RefObject<>(0));
        batcher.getJDBCExecutor().addBatch(this.getNodeCode(), formatFiscalAndMultiLang(sql, batcher), getInserParameters(data));
        return 1;
    }

    private List<DbParameter> getInserParameters(IEntityData data) {
        ArrayList<DbParameter> dbPars = new ArrayList<DbParameter>();
        if (isMultiData) {
            buildInsertParamters_MultiLanguage(data, dbPars);
        } else {
            buildInsertParamters(data, dbPars);
        }
        if (extendContainColumns != null && extendContainColumns.getCount() > 0)
            buildEXtendInsertParamters(data, dbPars);
        return dbPars;
    }

    @Override
    protected void buildInsertParamters(IEntityData entityData, ArrayList<DbParameter> dbPars) {
        for (DbColumnInfo columnInfo : getContainColumns().getBaseDict().values()) {
            if (columnInfo.getIsAssociateRefElement()) {
                continue;
            }
            if (columnInfo.isVirtual()) {
                continue;
            }
            Object value = this.getPersistenceValue(columnInfo.getColumnName(), entityData);
            Object transValue = value == null ? null : getPropertyChangeValue(columnInfo.getColumnName(), value);
            dbPars.add(buildParam(columnInfo, transValue));
        }
    }

    public final DbParameter buildParam(String columnName, DbColumnInfo columnInfo, Object paramValue) {
        return FilterUtil.buildParam(columnName, columnInfo.getColumnType(), columnInfo, paramValue);
    }

    public final DbParameter buildParam(DbColumnInfo columnInfo, Object paramValue) {
        return FilterUtil.buildParam(columnInfo.getColumnName(), columnInfo.getColumnType(), columnInfo, paramValue);
    }

    private void buildInsertParamters_MultiLanguage(IEntityData entityData,
                                                    ArrayList<DbParameter> dbPars) {
        insertMultiIndexMap = getBaseAdaptorItem().getInsertMultiIndexMap();
        if (insertMultiIndexMap.isEmpty()) {
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5015);
        }
        IMultiLanguageData multiLanguageData =
                entityData instanceof IMultiLanguageData ? (IMultiLanguageData) entityData : null;
        for (Map.Entry<Integer, String> entry : insertMultiIndexMap.entrySet()) {
            String columnName = entry.getValue();
            // 多语
            if (getMultiLanguageAlias().containsKey(columnName)) {
                MultiLangColumnInfo multiLangColumnInfo = getMultiLanguageAlias().get(columnName);
                DbColumnInfo columnInfo = getContainColumns()
                        .getItem(multiLangColumnInfo.getColumnName());
                String language = multiLangColumnInfo.getLanguageInfo().getCode();
                String columnNameWithToken = multiLangColumnInfo.getColumnName()
                        + MultiLanguageInfo.MULTILANGUAGETOKEN;
                if (multiLanguageData != null && multiLanguageData.getMultiLanguageInfos()
                        .containsKey(columnNameWithToken)) {
                    Object value = multiLanguageData.getMultiLanguageInfos()
                            .get(columnNameWithToken)
                            .getPropValueMap().get(language);
                    if (columnInfo.getIsUdtElement())
                        value = getPropertyChangeValue(multiLangColumnInfo.getColumnName(), value);
                    dbPars.add(
                            buildParam(columnName, columnInfo,
                                    value));
                    continue;
                }
                if (!language.equals(CAFContext.current.getLanguage())) {
                    dbPars.add(
                            buildParam(columnName, columnInfo.getColumnType(),
                                    null));
                    continue;
                }
                // 若为当前语言，需到data中对应属性中取数,走下面的普通分支
                columnName = multiLangColumnInfo.getColumnName();
            }
            // 普通
            Object value = this.getPersistenceValue(columnName, entityData);
            Object transValue =
                    value == null ? null : getPropertyChangeValue(columnName, value);
            DbColumnInfo columnInfo = getContainColumns().getItem(columnName);
            dbPars.add(
                    buildParam(columnInfo, transValue));
        }
    }

    private void buildEXtendInsertParamters(IEntityData entityData, ArrayList<DbParameter> dbPars) {
        for (DbColumnInfo columnInfo : extendContainColumns) {
            if (columnInfo.getIsAssociateRefElement()) {
                continue;
            }
            if (!columnInfo.getIsPersistent()) {
                continue;
            }
            Object transValue = null;
            if (columnInfo.getIsUdtElement()) {
                for (AbstractDataAdapterExtendInfo info : getDataAdapterExtendInfos()) {
                    transValue = info.getUdtInsertValue(columnInfo.getColumnName(), columnInfo.getBelongElementLabel(), entityData);
                    if (transValue != null)
                        break;
                }
            } else {
                Object value = entityData.getValue(columnInfo.getColumnName());
                transValue = columnInfo.getTypeTransProcesser().transType(value);
            }

            dbPars.add(buildParam(columnInfo.getColumnName(), columnInfo.getColumnType(), transValue));
        }
    }

    //endregion
    // region modify
    protected final String innerGetModifySql() {
        if (!getSqlSnippetCache().getInnerModifySqlMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().innerGetModifySql();
            getSqlSnippetCache().getInnerModifySqlMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getInnerModifySqlMap().get(entityDac.getGspDbtype().toString());
    }

    private String getModifySql() {
        return innerGetModifySql().replace("@TableName@", innerGetTableName());
    }

    protected Object getPropertyChangeValue(String key, Object value) {
        DbColumnInfo dbColumnInfo = getContainColumns().getItem(key);
        if (dbColumnInfo.isUdtRefColumn()) {
            if (value instanceof ValueObjModifyChangeDetail)
                value = ((ValueObjModifyChangeDetail) value).getData();
            if (value instanceof ICefData == false)
                return value;
//            return getNestedRepository(((UdtPropertyInfo)((ComplexUdtRefColumnInfo)dbColumnInfo).getBelongUdtPropertyInfo().getObjectInfo()).getUdtConfigId()).getPersistenceValue(((RefDataTypePropertyInfo) dbColumnInfo.getDataTypePropertyInfo()).getRefPropertyName(),
//                    (ICefData) data.getValue(((ComplexUdtRefColumnInfo)dbColumnInfo).getBelongUdtPropertyInfo().getPropertyName()));
            return getNestedRepository(((UdtPropertyInfo) ((ComplexUdtRefColumnInfo) dbColumnInfo).getBelongUdtPropertyInfo().getObjectInfo()).getUdtConfigId()).getPersistenceValue(((RefDataTypePropertyInfo) dbColumnInfo.getDataTypePropertyInfo()).getRefPropertyName(),
                    (ICefData) value);
        }
        if (dbColumnInfo.getIsUdtElement()) {
            if (value instanceof ValueObjModifyChangeDetail)
                value = ((ValueObjModifyChangeDetail) value).getData();
            if (value instanceof ICefData == false)
                return value;
            return getNestedRepository(((UdtPropertyInfo) dbColumnInfo.getDataTypePropertyInfo().getObjectInfo()).getUdtConfigId()).getPersistenceValue("",
                    (ICefData) value);
        }
        return dbColumnInfo.getPropertyChangeValue(value);
    }

    private DbColumnInfo getTempDbColumnInfo(String filedName) {
        DbColumnInfo columnInfo = null;
        columnInfo = getContainColumns().getItem(filedName);
        if (columnInfo == null) {
            if (!isInitColumn) {
                columnInfo = tempContainColumns.getItem(filedName);
            }
            if (columnInfo == null)
                columnInfo = extendContainColumns.getItem(filedName);
        } else {
            if (columnInfo.getIsAssociateRefElement() && !isInitColumn) {
                columnInfo = tempContainColumns.getItem(filedName);
            }
        }
        return columnInfo;
    }

    private String trans2DbColumnWithAlias(String filedName) {
        return trans2DbColumnWithAlias(filedName, false);
    }

    /**
     * 带别名的字段格式  表别名.数据库字段名
     *
     * @param filedName   字段名
     * @param ignoreAlias 是否忽略大小写
     * @return
     */
    protected final String trans2DbColumnWithAlias(String filedName, boolean ignoreAlias) {
        filedName = DatabaseUtil.getColumnName(filedName);
        if (!(getContainColumns().contains(filedName) || tempContainColumns.contains(filedName)) && (extendContainColumns == null || !extendContainColumns.contains(filedName))) {

            StringBuilder sb = new StringBuilder();
            sb.append("当前BE[v2]:" + getWrappedTableAlias() + " configId:" + getConfigId() + " 忽略大小写:" + ignoreAlias);
            sb.append("\r\n");
            sb.append("包含列信息{{");
            for (DbColumnInfo dbColumnInfo : getContainColumns()) {
                sb.append(dbColumnInfo.getColumnName() + ",");
            }
            sb.append("}}");
            logger.error("未找到字段" + filedName + "，请确认字段名称是否输入正确，或关联引用字段是否已经获取");
            logger.error(sb.toString());
            //再换种方式比较一次？
            for (DbColumnInfo columnInfo : getContainColumns()) {
                if (columnInfo.getColumnName().toLowerCase().equals(filedName.toLowerCase())) {
                    logger.error("找到字段" + filedName + "");
                }
            }
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5027, false, getTableAlias(), filedName);
        }
        DbColumnInfo columnInfo = null;
        columnInfo = getContainColumns().getItem(filedName);
        if (columnInfo == null) {
            if (!isInitColumn) {
                columnInfo = tempContainColumns.getItem(filedName);
            }
            if (columnInfo == null)
                columnInfo = extendContainColumns.getItem(filedName);
        } else {
            if (columnInfo.getIsAssociateRefElement() && !isInitColumn) {
                columnInfo = tempContainColumns.getItem(filedName);
            }
        }
        if (columnInfo.getIsAssociateRefElement()) {
            return getAssociateDbColumnName(columnInfo);
        }
        //return assColumns.ContainsKey(filedName) ? assColumns[filedName] : throw new Exception("Error");

        String dbName = ignoreAlias ? columnInfo.getDbColumnName()
                : getWrappedTableAlias() + "." + columnInfo.getDbColumnName();
        if (columnInfo.getIsMultiLang()) {
            dbName = dbName + "@Language@";
        }
        return dbName;
    }

    protected final String getGetDataByIdsSql() {
        if (!getSqlSnippetCache().getGetDataByIdsSqlMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().getGetDataByIdsSql();
            getSqlSnippetCache().getGetDataByIdsSqlMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getGetDataByIdsSqlMap().get(entityDac.getGspDbtype().toString());
    }

    protected final String getGetDataByIdSql() {
        if (!getSqlSnippetCache().getGetDataByIdSqlMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().getGetDataByIdSql();
            getSqlSnippetCache().getGetDataByIdSqlMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getGetDataByIdSqlMap().get(entityDac.getGspDbtype().toString());
    }

    @Override
    public IEntityData getDataByIDWithPara(String dataId, AdaptorRetrieveParam adaptorRetrieveParam) {
        EntityFilter filter = adaptorRetrieveParam.getEntityfilter(getNodeCode());
        if (filter != null && filter.getFieldsFilter() != null && filter.getFieldsFilter().getFilterFields() != null) {
            if (!StringUtils.isNullOrEmpty(getVersionControlPropName()) && !filter.getFieldsFilter().getFilterFields().contains(getVersionControlPropName()))
                filter.getFieldsFilter().getFilterFields().add(getVersionControlPropName());
            if (!StringUtils.isNullOrEmpty(getPrimaryKey()) && !filter.getFieldsFilter().getFilterFields().contains(getPrimaryKey()))
                filter.getFieldsFilter().getFilterFields().add(getPrimaryKey());
        }

        initQueryTableName();
        String tableName = getQueryTableNameWithAuthority(null, adaptorRetrieveParam != null ? adaptorRetrieveParam.getEntityfilter(getNodeCode()) : null);
        String fields = getQueryFields(adaptorRetrieveParam);
        String sql = String.format(getGetDataByIdSql(), fields, tableName, "?0 ");

        try {
            List<DbParameter> list = new ArrayList<DbParameter>();
            list.add(buildParam("ID", getContainColumns().getPrimaryKey().getColumnType(), dataId));
            if (getLogicDeleteInfo().isEnableLogicDelete()) {
                sql += String.format(" and %1$s =?1 ", getWrappedTableAlias() + "." + getLogicDeleteInfo().getLabelId());
                list.add(buildParam(getLogicDeleteInfo().getLabelId(), GspDbDataType.Char, "0"));
            }
            RetrieveFilter retrieveFilter = adaptorRetrieveParam == null ? null : adaptorRetrieveParam.getRetrieveFilter();
            return innerGetData(formatMultiLang(sql, retrieveFilter), list, adaptorRetrieveParam);
        } catch (RuntimeException ex) {
            throw new CefRunTimeException(buildMessage(ex.getMessage()), ex);
        }
    }

    protected AdaptorSqlCache getAdaptorSqlCache() {
        return getSqlSnippetCache().getAdaptorSqlCache();
    }

    protected SqlSnippetCache getSqlSnippetCache() {
        return (SqlSnippetCache) entityResInfo.getSqlSnippetCache();
    }

    public boolean getIsChild() {
        return false;
    }

    @Override
    public final String getParentJoin() {
        if (!getSqlSnippetCache().getParentJoinMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().parentJoin();
            getSqlSnippetCache().getParentJoinMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getParentJoinMap().get(entityDac.getGspDbtype().toString());
    }

    @Override
    protected Query buildQueryManager(String sqlText, List<DbParameter> parameters) {
        Query query = getDbProcessor().buildQueryManager(this.getEntityManager(), sqlText, parameters);
        return query;
    }

    @Override
    protected final String getJoinTableName() {
        if (!getSqlSnippetCache().getJoinTableNameMap().containsKey(entityDac.getGspDbtype().toString())) {
            String sql = getSqlSnippetProcessor().getJoinTableName();
            getSqlSnippetCache().getJoinTableNameMap().put(entityDac.getGspDbtype().toString(), sql);
            return sql;
        }
        return getSqlSnippetCache().getJoinTableNameMap().get(entityDac.getGspDbtype().toString());
    }

    @Override
    public String getTableNameByColumns(HashMap<String, String> columns, String keyColumnName, RefObject<String> keyDbColumnName, ArrayList<String> tableAlias) {
        //跟之前一致。
        ArrayList<String> keys1 = new ArrayList<>();
        for (String key : columns.keySet()
        ) {
            keys1.add(key);
        }
        ArrayList<AssociationInfo> associations = getAssociationInfos(keys1);
        String tableName = getTableNamesWithAssociationInfo(associations, tableAlias);
        keyDbColumnName.argvalue = trans2DbColumnWithAlias(keyColumnName);

        String[] keys = keys1.toArray(new String[]{});
        for (int i = 0; i < keys.length; i++) {
            columns.put(keys[i], trans2DbColumnWithAlias(keys[i]));
        }
        return tableName;
    }

    /**
     * 要求关联字段的拼接，在这里进行 把带出字段放在后面。。
     *
     * @param columns
     * @param keyColumnName
     * @param keyDbColumnName
     * @param tableAlias
     * @return
     */
    @Override
    public String getTableNameByRefColumns(HashMap<String, RefColumnInfo> columns, String keyColumnName, RefObject<String> keyDbColumnName, ArrayList<String> tableAlias) {
        //跟之前一致。
        isInitColumn = false;
        ArrayList<AssociationInfo> associations = getAssociationInfos(columns);
        String tableName = getTableNamesWithAssociationInfo(associations, tableAlias);
        keyDbColumnName.argvalue = trans2DbColumnWithAlias(keyColumnName);

        List<String> keys = new ArrayList<String>();
        for (String key : columns.keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            if (isColumnExists(keys.get(i))) {
                RefColumnInfo tempVar = new RefColumnInfo();
                tempVar.setColumnName(trans2DbColumnWithAlias(keys.get(i)));
                DbColumnInfo tempDbColumnInfo = getTempDbColumnInfo(keys.get(i));
                tempVar.setTransProcesser(tempDbColumnInfo.getTypeTransProcesser());
                tempVar.setColumnType(tempDbColumnInfo.getColumnType());
                tempVar.setVirtual(tempDbColumnInfo.isVirtual());
                tempVar.setFieldReposExtendConfigClassImpl(tempDbColumnInfo.getFieldReposExtendConfigClassImpl());
                tempVar.setFieldReposExtendConfigId(tempDbColumnInfo.getFieldReposExtendConfigId());
                columns.put(keys.get(i), tempVar);
            } else {
                columns.remove(keys.get(i));
            }
        }
        return tableName;
    }

    public String getTableNameByRefColumns(TableNameRefContext tableNameRefContext) {
        if (!StringUtils.isNullOrEmpty(tableNameRefContext.getRefTableAlias())) {
            setTableAlias(tableNameRefContext.getRefTableAlias());
        }
        isInitColumn = false;
        ArrayList<AssociationInfo> associations = getAssociationInfos(tableNameRefContext.getColumns());
        String tableName = getTableNamesWithAssociationInfo(associations, tableNameRefContext.getTableAlias());
        tableNameRefContext.getKeyDbColumnName().argvalue = trans2DbColumnWithAlias(tableNameRefContext.getKeyColumnName());

        List<String> keys = new ArrayList<String>();
        for (String key : tableNameRefContext.getColumns().keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            if (isColumnExists(keys.get(i))) {
                RefColumnInfo tempVar = new RefColumnInfo();
                tempVar.setColumnName(trans2DbColumnWithAlias(keys.get(i)));
                DbColumnInfo tempDbColumnInfo = getTempDbColumnInfo(keys.get(i));
                tempVar.setTransProcesser(tempDbColumnInfo.getTypeTransProcesser());
                tempVar.setColumnType(tempDbColumnInfo.getColumnType());
                tempVar.setVirtual(tempDbColumnInfo.isVirtual());

                tempVar.setFieldReposExtendConfigClassImpl(tempDbColumnInfo.getFieldReposExtendConfigClassImpl());
                tempVar.setFieldReposExtendConfigId(tempDbColumnInfo.getFieldReposExtendConfigId());
                tableNameRefContext.getColumns().put(keys.get(i), tempVar);
            } else {
                tableNameRefContext.getColumns().remove(keys.get(i));
            }
        }
        return tableName;
    }

    /**
     * 给定的列名在当前节点是否存在
     * 兼容部分场景查询报错的问题
     *
     * @param filedName
     * @return
     */
    private boolean isColumnExists(String filedName) {
        if (!(getContainColumns().contains(filedName) || tempContainColumns.contains(filedName)) && (extendContainColumns == null || !extendContainColumns.contains(filedName))) {
            return false;
        }
        return true;
    }

//    private ArrayList<AssociationInfo> getAssociationInfos(HashMap<String, RefColumnInfo> columnInfos) {
//        ArrayList<String> columns = new ArrayList<String>();
//        for (String item : columnInfos.keySet()) {
//            columns.add(item);
//        }
//        ArrayList<AssociationInfo> associations = new ArrayList<AssociationInfo>();
//        for (String columnItem : columns) {
//            //处理带出字段是　多值多列UDT和关联带出字段的场景
//            //如果是关联带出字段，找到对应的关联信息，返回
//            //如果是多值多列字段，将多列字段包含的明细字段添加到columnInfos中，后续返回对应的数据库列等信息
//            if (isRefOrUdt(columnItem)) {
//                AssociationInfo assoInfo = findInAssociationInfos(columnItem);
//                //是否多值多列udt,多值单列的不会进来。
//                if (assoInfo == null && getEntityResInfo() != null) {
//                    //如果是多值多列udt
//                    DataTypePropertyInfo dataTypePropertyInfo = getEntityResInfo().getEntityTypeInfo().getPropertyInfos().get(columnItem);
//                    Object udtPropertyInfo = null;
//                    if (dataTypePropertyInfo != null) {
//                        udtPropertyInfo = dataTypePropertyInfo.getObjectInfo();
//                    }
//                    if (udtPropertyInfo != null && udtPropertyInfo instanceof ComplexUdtPropertyInfo) {
//                        if (columnInfos.containsKey(columnItem)) {
//                            columnInfos.remove(columnItem);
//                        }
//                        for (Map.Entry<String, DataTypePropertyInfo> entry : ((ComplexUdtPropertyInfo) udtPropertyInfo).getPropertyInfos().entrySet()) {
//                            if (!columnInfos.containsKey(entry.getKey())) {
//                                columnInfos.put(entry.getKey(), new RefColumnInfo());
//                            }
//                        }
//                    } else {
//                        //详细输出一下错误信息：  是否统计一下，错误次数达到一定次数，进行回复？ 重置columns信息
//                        StringBuilder sbError = new StringBuilder();
//                        sbError.append("当前节点[getAssociationInfos]:" + getTableAlias() + " configId:" + getConfigId());
//                        sbError.append("未找到字段:" + columnItem);
//                        sbError.append("\r\n");
//                        sbError.append("包含字段信息{{");
//                        for (DbColumnInfo dbColumnInfo : getContainColumns()) {
//                            sbError.append(dbColumnInfo.getColumnName() + ",");
//                        }
//                        sbError.append("}}");
//                        logger.error(sbError.toString());
//                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5072, columnItem);
//                    }
//                } else {
//                    if (!associations.contains(assoInfo)) {
//                        associations.add(assoInfo);
//                    }
//                }
//            }
//
//            //如果是关联字段
//            if (isAssociationElement(columnItem)) {
//                AssociationInfo assoInfo = getAssociation(columnItem);
//                if (!associations.contains(assoInfo)) {
//                    associations.add(assoInfo);
//                    for (String refColumn : assoInfo.getRefColumns().keySet()) {
//                        if (!columnInfos.containsKey(refColumn)) {
//                            columnInfos.put(refColumn, new RefColumnInfo());
//                        }
//                    }
//                }
//            }
//        }
//        return associations;
//    }

//    /**
//     * 如果是关联带出字段或者Udt字段
//     *
//     * @param columnItem
//     * @return
//     */
//    private boolean isRefOrUdt(String columnItem) {
//        //基础字段/扩展字段都不包含
//        DbColumnInfo column = getContainColumns().getItem(columnItem);
//        if (column != null && column.getIsAssociateRefElement()) {
//            return true;
//        }
//        DbColumnInfo extendColumn = this.extendContainColumns.getItem(columnItem);
//        if (extendColumn != null && extendColumn.getIsAssociateRefElement()) {
//            return true;
//        }
//        if ((column == null) && (extendColumn == null)) {
//            return true;
//        }
//        return false;
//    }

    /**
     * 是否关联字段
     *
     * @param columnItem
     * @return
     */
    private boolean isAssociationElement(String columnItem) {
        if (getContainColumns().contains(columnItem) && getContainColumns().getItem(columnItem).getIsAssociation()) {
            return true;
        }
        if (this.extendContainColumns != null && this.extendContainColumns.contains(columnItem) && this.extendContainColumns.getItem(columnItem).getIsAssociation()) {
            return true;
        }
        return false;
    }

    private ArrayList<AssociationInfo> getAssociationInfos(ArrayList<String> columns) {
        ArrayList<AssociationInfo> associations = new ArrayList<AssociationInfo>();
        for (String columnItem : columns) {
            if (!getContainColumns().contains(columnItem) || getContainColumns().getItem(columnItem).getIsAssociateRefElement()) {
                AssociationInfo assoInfo = findInAssociationInfos(columnItem);
                if (associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                }
            }

            if (getContainColumns().contains(columnItem) && getContainColumns().getItem(columnItem).getIsAssociation()) {
                AssociationInfo assoInfo = getAssociation(columnItem);
                if (associations.contains(assoInfo)) {
                    associations.add(assoInfo);
                }
            }
        }
        return associations;
    }

    private AssociationInfo findInAssociationInfos(String columnName) {
        for (AssociationInfo associationInfo : getAssociationInfos()) {
            if (associationInfo.getRefColumns().containsKey(columnName)) {
                return associationInfo;
            }
        }
        return null;
//        throw new RuntimeException("找不到字段" + columnName);
    }


    private String getTableNamesWithAssociationInfo(ArrayList<AssociationInfo> associations, ArrayList<String> tableAlias) {
        //
        if (tableAlias == null || tableAlias.size() == 0) {
            tableAlias = new ArrayList<String>();
            addTableAlias(tableAlias);
        } else {
            if (StringUtils.isNullOrEmpty(this.originTableAlias)) {
                this.originTableAlias = getTableAlias();
            }
            setTableAlias(getTableAlias4Association(tableAlias));
        }
        if (associations == null || associations.size() < 1) {
            return getTableName();
        }

        return buildTableNamesWithAssociationInfo(associations, tableAlias, getTableName());
    }

    private String buildTableNamesWithAssociationInfo(ArrayList<AssociationInfo> associations, ArrayList<String> tableAlias, String baseTableName) {
        StringBuilder queryTables = new StringBuilder();
        StringBuilder fullTable = new StringBuilder().append(baseTableName);
        for (AssociationInfo associationInfo : associations) {
            if (associationInfo == null)
                continue;
            HashMap<String, RefColumnInfo> associationColumns = new HashMap<String, RefColumnInfo>();
            if (associationInfo != null) {
                for (Map.Entry<String, String> item :
                        associationInfo.getRefColumns().entrySet()) {
                    associationColumns.put(item.getValue(), new RefColumnInfo());
                }
            }

            if (getConfigId().equals(associationInfo.getConfigId()) && getNodeCode() == associationInfo.getNodeCode()) {
                queryTables.append(getCurrentAssoTableName(associationInfo, tableAlias, associationColumns));
                continue;
            }
            String targetDbColumnName = "";
            associationInfo.getRefRepository().initParams(getPars());
            RefObject<String> tempRef_targetDbColumnName = new RefObject<String>(targetDbColumnName);
            String tableName = "";
            try {
                tableName = associationInfo.getRefRepository().getTableNameByRefColumns(associationInfo.getNodeCode(), associationColumns, associationInfo.getTargetColumn(), tempRef_targetDbColumnName, tableAlias);
            } catch (Exception ex) {
                StringBuilder sbError = new StringBuilder();
                if (associationInfo != null) {
                    sbError.append("当前BE: " + getTableAlias() + " 关联BE:" + associationInfo.getRefRepository().getClass().getName());
                    sbError.append("关联节点:[" + associationInfo.getNodeCode() + "]" + " sourceColumn:" + associationInfo.getSourceColumn() + " targetColumn:" + associationInfo.getTargetColumn());
                    sbError.append("\r\n带出字段：");
                    if (associationColumns != null && associationColumns.size() > 0) {
                        for (String item : associationColumns.keySet()) {
                            sbError.append(item + ",");
                        }
                    }
                    logger.error(sbError.toString());
                }
                throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5076, ex);
            }
            targetDbColumnName = tempRef_targetDbColumnName.argvalue;

            if (isInitColumn) {
                initAssociateColumnInfo(associationInfo, associationColumns);
            } else {
                initTempAssociateColumnInfo(associationInfo, associationColumns);
            }
            if (this.fieldsFilter != null && this.fieldsFilter.isUseFieldsCondition()) {
                if (!this.fieldsFilter.getFilterFields().contains(associationInfo.getSourceColumn())) {
                    continue;
                }
            }
            String[] tables = tableName.split(" LEFT OUTER JOIN ", 2);

            String sourceColumnName = trans2DbColumnWithAlias(associationInfo.getSourceColumn());
            queryTables.append(String.format(getJoinTableName(), tables[0], sourceColumnName, targetDbColumnName));
            buildAssoCondi(queryTables, associationInfo, targetDbColumnName, tables[0]);
            if (tables.length > 1) {
                queryTables.append(" LEFT OUTER JOIN " + tables[1]);
            }
        }
        joinTableName = queryTables.toString();
        fullTable.append(joinTableName);
        return fullTable.toString();
    }

    private void buildAssoCondi(StringBuilder queryTables, AssociationInfo associationInfo, String targetDbColumnName, String tableName) {
        //todo 这个地方别名处理待验证,字段如果与数据库不一致？ 前端直接用数据库的？
        if (associationInfo.getAssoConditions() != null && associationInfo.getAssoConditions().size() > 0) {
            String targetAlias = targetDbColumnName.substring(0, targetDbColumnName.indexOf("."));
            for (AssoCondition assoCondition : associationInfo.getAssoConditions()) {
                if (StringUtils.isEmpty(assoCondition.getValue())) {
                    queryTables.append(" and " + getAssoAlias(assoCondition.getLeftNodeCode(), tableName) + "." + assoCondition.getLeftField());
                    queryTables.append(StringUtils.isEmpty(assoCondition.getOperator()) ? "=" : assoCondition.getOperator());
                    queryTables.append(getAssoAlias(assoCondition.getRightNodeCode(), targetAlias) + "." + assoCondition.getRightField());
                } else {
                    queryTables.append(" and " + getAssoAlias(assoCondition.getLeftNodeCode(), targetAlias) + "." + assoCondition.getLeftField());
                    queryTables.append(StringUtils.isEmpty(assoCondition.getOperator()) ? "=" : assoCondition.getOperator());
                    //todo wangmj 先按照字符串处理，后续结合类型处理
                    queryTables.append(" '" + assoCondition.getValue() + "' ");
                }
            }
        }
    }

    private String getAssoAlias(String nodeCode, String targetTableName) {
        if (getTableAlias().equals(nodeCode))
            return getTableAlias();
        return targetTableName;
    }

    private String getCurrentAssoTableName(AssociationInfo associationInfo, ArrayList<String> tableAlias, HashMap<String, RefColumnInfo> associationColumns) {
        String currnetAlias = getTableAlias4Association(tableAlias);
        ArrayList<AssociationInfo> associations = getAssociationInfos(associationColumns);
        String tableName = buildTableNamesWithAssociationInfo(associations, tableAlias, innerGetTableName() + " " + currnetAlias);
        String keyDbColumnName = currnetAlias + "." + trans2DbColumn(associationInfo.getTargetColumn());

        ArrayList<String> keys = new ArrayList<String>();
        for (String key :
                associationColumns.keySet()) {
            keys.add(key);
        }
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            RefColumnInfo tempVar = new RefColumnInfo();
            tempVar.setColumnName(currnetAlias + "." + trans2DbColumn(key));
            tempVar.setTransProcesser(getContainColumns().getItem(key).getTypeTransProcesser());
            tempVar.setColumnType(getContainColumns().getItem(key).getColumnType());
            associationColumns.put(key, tempVar);
        }

        if (isInitColumn) {
            initAssociateColumnInfo(associationInfo, associationColumns);
        } else {
            initTempAssociateColumnInfo(associationInfo, associationColumns);
        }

        String sourceColumnName = trans2DbColumnWithAlias(associationInfo.getSourceColumn());
        return String.format(getJoinTableName(), tableName, sourceColumnName, keyDbColumnName);

    }

    private void addTableAlias(ArrayList<String> tableAlias) {
        tableAlias.add(getWrappedTableAlias());
    }

    private String getTableAlias4Association(ArrayList<String> tableAlias) {
        if (!tableAlias.contains(getWrappedTableAlias())) {
            tableAlias.add(getWrappedTableAlias());
            return getWrappedTableAlias();
        }

        int index = 1;
        while (true) {
            if (tableAlias.contains(getWrappedTableAlias(true) + index)) {
                index++;
            } else {
                tableAlias.add(getWrappedTableAlias(true) + index);
                return getWrappedTableAlias(true) + index;
            }
        }
    }

    private void initAssociateColumnInfo(AssociationInfo associationInfo, HashMap<String, RefColumnInfo> associationColumns) {
        if (!AssoMapping.containsKey(associationInfo.getSourceColumn())) {
            AssoMapping.put(associationInfo.getSourceColumn(), new HashMap<String, String>());
        }

        for (Map.Entry<String, String> refColumnItem : associationInfo.getRefColumns().entrySet()) {
            if (!AssoMapping.get(associationInfo.getSourceColumn()).containsKey(refColumnItem.getValue())) {
                AssoMapping.get(associationInfo.getSourceColumn()).put(refColumnItem.getValue(), refColumnItem.getKey());
            }
            //这个地方也得加上 扩展字段集合？
            if (getBaseAdaptorItem().getContainColumns().contains(refColumnItem.getKey())) {
                DbColumnInfo columnInfo = getContainColumns().getItem(refColumnItem.getKey());
                RefColumnInfo refColumnInfo = associationColumns.get(refColumnItem.getValue());
                columnInfo.setDbColumnName(refColumnInfo.getColumnName());
                ITypeTransProcesser tempVar = refColumnInfo.getTransProcesser();
                columnInfo.setTypeTransProcesser((ITypeTransProcesser) ((tempVar instanceof ITypeTransProcesser) ? tempVar : null));
                columnInfo.setColumnType(refColumnInfo.getColumnType());
                columnInfo.setVirtual(refColumnInfo.isVirtual());
                columnInfo.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                columnInfo.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                associationColumns.remove(refColumnItem.getValue());
            } else {
                if (!associationColumns.containsKey(refColumnItem.getValue())) {
                    continue;
                }

                DbColumnInfo refColumnInfo = ((EntityRelationalAdaptor) (((BaseRootRepository) associationInfo.getRefRepository()).getEntityDac(associationInfo.getNodeCode()).getEntityAdaptor())).getContainColumns().getItem(refColumnItem.getValue());
                RefColumnInfo refAssocColumnInfo = associationColumns.get(refColumnItem.getValue());
                ITypeTransProcesser tempVar2 = refAssocColumnInfo.getTransProcesser();
                DbColumnInfo tempVar3 = new DbColumnInfo();
                tempVar3.setColumnName(refColumnItem.getKey());
                tempVar3.setDbColumnName(refAssocColumnInfo.getColumnName());
                tempVar3.setVirtual(refAssocColumnInfo.isVirtual());
                tempVar3.setFieldReposExtendConfigClassImpl(refAssocColumnInfo.getFieldReposExtendConfigClassImpl());
                tempVar3.setFieldReposExtendConfigId(refAssocColumnInfo.getFieldReposExtendConfigId());
                if (refColumnInfo == null) {
                    tempVar3.setTypeTransProcesser((ITypeTransProcesser) ((tempVar2 instanceof ITypeTransProcesser) ? tempVar2 : null));
                    tempVar3.setColumnType(refAssocColumnInfo.getColumnType());
                } else {
                    tempVar3.setTypeTransProcesser(refColumnInfo.getTypeTransProcesser());
                    tempVar3.setColumnType(refColumnInfo.getColumnType());
                }
                tempVar3.setIsAssociateRefElement(true);
                tempVar3.setBelongElementLabel(associationInfo.getSourceColumn());
                DbColumnInfo columnInfo = tempVar3;
                if (associationInfo.isExtend()) {
                    this.extendContainColumns.add(columnInfo);
                }
                else {
                    getBaseAdaptorItem().addColumn(columnInfo);
                }
                associationColumns.remove(refColumnItem.getValue());
            }

        }
        for (Map.Entry<String, RefColumnInfo> associationColumn : associationColumns.entrySet()) {

            DbColumnInfo tempVar4 = new DbColumnInfo();
            tempVar4.setColumnName(associationInfo.getSourceColumn() + "_" + associationColumn.getKey());
            tempVar4.setDbColumnName(associationColumn.getValue().getColumnName());
            tempVar4.setFieldReposExtendConfigClassImpl(associationColumn.getValue().getFieldReposExtendConfigClassImpl());
            tempVar4.setFieldReposExtendConfigId(associationColumn.getValue().getFieldReposExtendConfigId());
            tempVar4.setTypeTransProcesser((ITypeTransProcesser) ((associationColumn.getValue().getTransProcesser() instanceof ITypeTransProcesser) ? associationColumn.getValue().getTransProcesser() : null));
            tempVar4.setColumnType(associationColumn.getValue().getColumnType());
            tempVar4.setIsAssociateRefElement(true);
            tempVar4.setBelongElementLabel(associationInfo.getSourceColumn());

            DbColumnInfo columnInfo = tempVar4;
            if(!associationInfo.isExtend()){
                getBaseAdaptorItem().getContainColumns().add(columnInfo);
            }
            if (!AssoMapping.get(associationInfo.getSourceColumn()).containsKey(associationColumn.getKey())) {
                AssoMapping.get(associationInfo.getSourceColumn()).put(associationColumn.getKey(), associationInfo.getSourceColumn() + "_" + associationColumn.getKey());
            }
        }
    }

    private void initTempAssociateColumnInfo(AssociationInfo associationInfo, HashMap<String, RefColumnInfo> associationColumns) {
        if (!AssoMapping.containsKey(associationInfo.getSourceColumn())) {
            AssoMapping.put(associationInfo.getSourceColumn(), new HashMap<String, String>());
        }

        for (Map.Entry<String, String> refColumnItem : associationInfo.getRefColumns().entrySet()) {
            if (!AssoMapping.get(associationInfo.getSourceColumn()).containsKey(refColumnItem.getValue())) {
                AssoMapping.get(associationInfo.getSourceColumn()).put(refColumnItem.getValue(), refColumnItem.getKey());
            }
            if (tempContainColumns.contains(refColumnItem.getKey())) {
                DbColumnInfo columnInfo = tempContainColumns.getItem(refColumnItem.getKey());
                RefColumnInfo refColumnInfo = associationColumns.get(refColumnItem.getValue());
                columnInfo.setDbColumnName(refColumnInfo.getColumnName());

                ITypeTransProcesser tempVar = refColumnInfo.getTransProcesser();
                columnInfo.setTypeTransProcesser((ITypeTransProcesser) ((tempVar instanceof ITypeTransProcesser) ? tempVar : null));
                columnInfo.setColumnType(refColumnInfo.getColumnType());
                columnInfo.setVirtual(refColumnInfo.isVirtual());
                columnInfo.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                columnInfo.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                associationColumns.remove(refColumnItem.getValue());
            } else {
                //TODO：改成抛异常
                if (!associationColumns.containsKey(refColumnItem.getValue())) {
                    continue;
                }
                RefColumnInfo refColumnInfo = associationColumns.get(refColumnItem.getValue());
                ITypeTransProcesser tempVar2 = refColumnInfo.getTransProcesser();
                DbColumnInfo tempVar3 = new DbColumnInfo();
                tempVar3.setColumnName(refColumnItem.getKey());
                tempVar3.setDbColumnName(refColumnInfo.getColumnName());
                tempVar3.setTypeTransProcesser((ITypeTransProcesser) ((tempVar2 instanceof ITypeTransProcesser) ? tempVar2 : null));
                tempVar3.setColumnType(refColumnInfo.getColumnType());
                tempVar3.setIsAssociateRefElement(true);
                tempVar3.setBelongElementLabel(associationInfo.getSourceColumn());
                tempVar3.setVirtual(refColumnInfo.isVirtual());
                tempVar3.setFieldReposExtendConfigClassImpl(refColumnInfo.getFieldReposExtendConfigClassImpl());
                tempVar3.setFieldReposExtendConfigId(refColumnInfo.getFieldReposExtendConfigId());
                DbColumnInfo columnInfo = tempVar3;
                if (associationInfo.isExtend()) {
                    this.extendContainColumns.add(columnInfo);
                }
                else {
                    tempContainColumns.add(columnInfo);
                }
                associationColumns.remove(refColumnItem.getValue());
            }
        }

        for (Map.Entry<String, RefColumnInfo> associationColumn : associationColumns.entrySet()) {
            DbColumnInfo tempVar4 = new DbColumnInfo();
            tempVar4.setColumnName(associationInfo.getSourceColumn() + "_" + associationColumn.getKey());
            tempVar4.setDbColumnName(associationColumn.getValue().getColumnName());
            tempVar4.setTypeTransProcesser((ITypeTransProcesser) ((associationColumn.getValue().getTransProcesser() instanceof ITypeTransProcesser) ? associationColumn.getValue().getTransProcesser() : null));
            tempVar4.setColumnType(associationColumn.getValue().getColumnType());
            tempVar4.setIsAssociateRefElement(true);
            tempVar4.setBelongElementLabel(associationInfo.getSourceColumn());
            tempVar4.setVirtual(associationColumn.getValue().isVirtual());
            tempVar4.setFieldReposExtendConfigClassImpl(associationColumn.getValue().getFieldReposExtendConfigClassImpl());
            tempVar4.setFieldReposExtendConfigId(associationColumn.getValue().getFieldReposExtendConfigId());
            DbColumnInfo columnInfo = tempVar4;
            tempContainColumns.add(columnInfo);
            if (!AssoMapping.get(associationInfo.getSourceColumn()).containsKey(associationColumn.getKey())) {
                AssoMapping.get(associationInfo.getSourceColumn()).put(associationColumn.getKey(), associationInfo.getSourceColumn() + "_" + associationColumn.getKey());
            }
        }
    }

    @Override
    public Object readProperty(String propertyName, ICefReader reader) {
        ((BaseAdaptorItem)getBaseAdaptorItem()).initAssociationDbMappings(getEntityResInfo());
        return ((BaseAdaptorItem) getBaseAdaptorItem()).readProperty(propertyName, reader);
    }

    @Override
    public Object getPersistenceValue(String colName, ICefData data) {
        DbColumnInfo dbColumnInfo = getContainColumns().getItem(colName);
        if(dbColumnInfo == null){
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5023, false, getTableAlias(), colName);
        }
        if (dbColumnInfo.isUdtRefColumn()) {
            return getNestedRepository(((UdtPropertyInfo) ((ComplexUdtRefColumnInfo) dbColumnInfo).getBelongUdtPropertyInfo().getObjectInfo()).getUdtConfigId()).getPersistenceValue(((RefDataTypePropertyInfo) dbColumnInfo.getDataTypePropertyInfo()).getRefPropertyName(),
                    (ICefData) data.getValue(((ComplexUdtRefColumnInfo) dbColumnInfo).getBelongUdtPropertyInfo().getPropertyName()));
        }
        if (dbColumnInfo.getIsUdtElement()) {
            return getNestedRepository(((UdtPropertyInfo) dbColumnInfo.getDataTypePropertyInfo().getObjectInfo()).getUdtConfigId()).getPersistenceValue("",
                    (ICefData) data.getValue(dbColumnInfo.getDataTypePropertyInfo().getPropertyName()));
        }
        Object obj = null;
        try{
            obj = dbColumnInfo.getPersistenceValue(data.getValue(dbColumnInfo.getDataTypePropertyInfo().getPropertyName()));
        }
        catch (Exception ex){
            throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5077, ex, ExceptionLevel.Error, false, getTableAlias(), colName, getConfigId());
        }

        return obj;
    }


    public CefEntityResInfoImpl getEntityResInfo() {
        return entityResInfo;
    }
}