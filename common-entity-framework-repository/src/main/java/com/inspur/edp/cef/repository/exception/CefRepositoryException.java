package com.inspur.edp.cef.repository.exception;

import com.inspur.edp.cef.entity.exception.CefExceptionBase;
import io.iec.edp.caf.commons.exception.ExceptionLevel;

/**
 * @className: CefRepositoryException
 * @author: wangmj
 * @date: 2023/7/22
 **/
public class CefRepositoryException extends CefExceptionBase {
    public CefRepositoryException(String exceptionCode, String... messageParams) {
        super(exceptionCode, messageParams);
    }

    public CefRepositoryException(String exceptionCode, boolean isBizException, String... messageParams) {
        super(exceptionCode, isBizException, messageParams);
    }

    public CefRepositoryException(String exceptionCode, Exception innerException, String... messageParams) {
        super(exceptionCode, innerException, messageParams);
    }

    public CefRepositoryException(String exceptionCode, Exception innerException, ExceptionLevel level, boolean bizException, String... messageParams) {
        super(exceptionCode, innerException, level, bizException, messageParams);
    }

    public CefRepositoryException(String resourceFile, String exceptionCode, String[] messageParams, Exception innerException, ExceptionLevel level, boolean bizException) {
        super(resourceFile, exceptionCode, messageParams, innerException, level, bizException);
    }

    public CefRepositoryException(String exceptionCode, String message, Exception innerException, ExceptionLevel level) {
        super(exceptionCode, message, innerException, level);
    }

    public CefRepositoryException() {
        super();
    }

//    public CefRepositoryException(String message) {
//        super(message);
//    }

//    public CefRepositoryException(String message, String exceptionCode) {
//        super(message, exceptionCode);
//    }
//
//    public CefRepositoryException(String message,Exception e) {
//        super(e, message);
//    }
}
