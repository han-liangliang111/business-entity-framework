package com.inspur.edp.cef.repository.extend;

import com.inspur.edp.cef.entity.repository.BefRepositoryExtendConfiguration;
import com.inspur.edp.cef.entity.repository.ReposExtendConfig;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import com.inspur.edp.cef.spi.repository.BaseDbColumnInfo;
import com.inspur.edp.cef.spi.repository.IFieldReposExtendProcessor;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author wangmaojian
 * @create 2023/4/17
 * 管理字段插件处理器
 */
public class FieldExtendManager {
    private static final FieldExtendManager instance = new FieldExtendManager();
    private static final String builtInKey = "builtIn";
    private static BefRepositoryExtendConfiguration befRepositoryExtendConfiguration = null;
    private static boolean hasFieldExtend = false;

    static {
        befRepositoryExtendConfiguration = SpringBeanUtils.getBean(BefRepositoryExtendConfiguration.class);
        if (befRepositoryExtendConfiguration != null && befRepositoryExtendConfiguration.getFieldExtendConfigs() != null && befRepositoryExtendConfiguration.getFieldExtendConfigs().size() > 0) {
            hasFieldExtend = true;
        }
    }

    private Object lockObj = new Object();
    private volatile ConcurrentHashMap<String, IFieldReposExtendProcessor> map = new ConcurrentHashMap<>();

    private FieldExtendManager() {
        map.put(builtInKey, new BuiltInFieldProcessor());
    }

    public static FieldExtendManager getInstance() {
        return instance;
    }

    public IFieldReposExtendProcessor getFieldReposeExtendProcessor(BaseDbColumnInfo baseDbColumnInfo) {
        String filedKey = builtInKey;
        //todo 需要改成从yaml中读取配置
        if (!StringUtils.isNullOrEmpty(baseDbColumnInfo.getFieldReposExtendConfigId())) {
            filedKey = baseDbColumnInfo.getFieldReposExtendConfigId();
        }
        if (!map.containsKey(filedKey)) {
            synchronized (lockObj) {
                if (!map.containsKey((filedKey))) {
                    //构造对象。。
                    Class cl = null;
                    String classImp = getFieldExtendClassImpl(filedKey);
                    try {
                        cl = Class.forName(classImp);
                    } catch (ClassNotFoundException e) {
                        return new BuiltInFieldProcessor();
                    }
                    try {
                        map.put(filedKey, (IFieldReposExtendProcessor) cl.newInstance());
                    } catch (InstantiationException e) {
                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5057, e, classImp);
                    } catch (IllegalAccessException e) {
                        throw new CefRepositoryException(ExceptionCode.CEF_RUNTIME_5058, e, classImp);
                    }
                }
            }
        }
        return map.get(filedKey);
    }

    private String getFieldExtendClassImpl(String extendConfigId) {
        if (hasFieldExtend) {
            for (ReposExtendConfig config : befRepositoryExtendConfiguration.getFieldExtendConfigs()) {
                if (config.getId().equalsIgnoreCase(extendConfigId)) {
                    return config.getImplClassName();
                }
            }
        }
        return "";
    }
}
