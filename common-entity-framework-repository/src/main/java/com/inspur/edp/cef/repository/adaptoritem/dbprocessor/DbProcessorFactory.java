/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptoritem.dbprocessor;

import com.inspur.edp.cef.api.repository.DbTypeConverter;
import com.inspur.edp.cef.api.repository.GspDbType;
import com.inspur.edp.cef.repository.adaptoritem.DBExtendInfo;
import com.inspur.edp.cef.repository.exception.CefRepositoryException;
import com.inspur.edp.cef.repository.exception.ExceptionCode;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;

import java.util.HashMap;
import java.util.Map;

public class DbProcessorFactory {
    private static Map<GspDbType, DbProcessor> processorMap = new HashMap<>();

    public static DbProcessor getDbProcessor(GspDbType gspDbType) {
        if (processorMap.containsKey(gspDbType))
            return processorMap.get(gspDbType);
        DbProcessor dbProcessor = null;
        switch (gspDbType) {
            case Oracle:
            case OceanBase:
                dbProcessor = new OraDbProcessor();
                break;
            case Kingbase:
                dbProcessor = new KingbaseDbProcessor();
                break;
            case Oscar:
                dbProcessor = new ShenTongDbProcessor();
                break;
            case DB2:
                dbProcessor = new DB2DbProcessor();
                break;
            case SQLServer:
                dbProcessor = new SQLServerDbProcessor();
                break;
            case Unknown:
                try {
                    Class sqlGeneratorClass = null;
                    DBExtendInfo dbExtendInfo = SpringBeanUtils.getBean(DBExtendInfo.class);
                    sqlGeneratorClass = Class.forName(dbExtendInfo.getDbProcessorClass());
                    dbProcessor = (DbProcessor) sqlGeneratorClass.newInstance();
                    break;
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
                }
            default:
                dbProcessor = new DbProcessor();
                break;
        }
        processorMap.put(gspDbType, dbProcessor);
        return dbProcessor;
    }

    public static DbProcessor getDbProcessor() {
        GspDbType dbType = DbTypeConverter.transGspDbType(CAFContext.current.getDbType());
        return getDbProcessor(dbType);
    }
}
