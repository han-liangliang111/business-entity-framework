package com.inspur.edp.cef.repository.adaptoritem.jdbcprocessor;


import com.inspur.edp.cef.api.repository.DbParameter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @className: SQLServerJdbcProcessor
 * @author: wangmj
 * @date: 2024/03/12
 **/
public class SQLServerJdbcProcessor extends DbProcessor {

    @Override
    protected void buildNClobParameter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        if (param.getValue() == null) {
            stmt.setString(i, null);
        } else {
            stmt.setNString(i, (String) param.getValue());
        }
    }

    @Override
    protected void buildNVarcharParmeter(PreparedStatement stmt, int i, DbParameter param)
            throws SQLException {
        stmt.setNString(i, (String) param.getValue());
    }
}
