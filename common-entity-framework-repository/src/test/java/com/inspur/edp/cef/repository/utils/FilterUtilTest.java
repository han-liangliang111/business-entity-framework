package com.inspur.edp.cef.repository.utils;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilterUtilTest {
    @Test
    public void filterUtilTest1(){
        String filterValue = "_\\%";

        String expectValue = "\\_\\\\\\%";
        //需要转义的字符除通配符 _ 和 %外，需要把转义字符\加上
        List<String> escapeCharacters = new ArrayList<>(Arrays.asList("\\","_", "%"));

        for (String replacechar : escapeCharacters) {
            filterValue = filterValue.replace(replacechar, "\\" + replacechar);
        }
        assertEquals( filterValue, expectValue,"两个字符串应该相等");
    }

    @Test
    public void filterUtilTest2(){
        String escapeChar =  " escape '\\\\'";
        System.out.println(escapeChar);
    }
}
