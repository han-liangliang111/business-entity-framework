package com.inspur.edp.udt.api.exception;

import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;

/**
 * @className: UdtRunTimeException
 * @author: sure
 * @date: 2024/2/18
 **/
public class UdtRunTimeException extends CAFRuntimeException {
    private static final String suCode = "pfcommon";
    private static final String resFile = "udt_runtime_core.properties";

    public UdtRunTimeException() {
        super(suCode, "UdtRuntime001", "", null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error);
    }

    public UdtRunTimeException(String exceptionCode, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public UdtRunTimeException(String exceptionCode, Exception innerException, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public UdtRunTimeException(String exceptionCode, Exception innerException, ExceptionLevel level, boolean isBiz, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), isBiz);
    }

    public UdtRunTimeException(String exceptionCode, Exception innerException, ExceptionLevel level, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), true);
    }

    private static io.iec.edp.caf.commons.exception.ExceptionLevel getLevel(ExceptionLevel level) {
        io.iec.edp.caf.commons.exception.ExceptionLevel level1 = null;
        if (level == ExceptionLevel.Error) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Error;
        }
        if (level == ExceptionLevel.Warning) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Warning;
        }
        if (level == ExceptionLevel.Info) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Info;
        }
        return level1;

    }
}
