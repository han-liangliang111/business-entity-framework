//package com.inspur.edp.bef.api.be;
//
//import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
//import com.inspur.edp.cef.entity.condition.EntityFilter;
//import com.inspur.edp.cef.entity.entity.IEntityData;
//import io.iec.edp.caf.rpc.api.annotation.GspServiceBundle;
//import io.iec.edp.caf.rpc.api.annotation.RpcParam;
//import io.iec.edp.caf.rpc.api.annotation.RpcServiceMethod;
//import java.util.List;
//import java.util.Map;
//
//@GspServiceBundle(applicationName = "gsp", serviceUnitName = "pfcommon", serviceName = "")
//public interface IBESUDataRpcService {
//    /**
//     * 跨SU检索数据
//     *
//     * @param dataId 检索数据id
//     * @param retrieveParam 检索参数
//     * @param beId beId
//     * @return 检索返回值
//     */
//    @RpcServiceMethod(serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByBeId")
//    String rpcRetrieveByBeId(@RpcParam(paramName = "dataId") String dataId,
//        @RpcParam(paramName = "retrieveParam") RetrieveParam retrieveParam,
//        @RpcParam(paramName = "variables") Map<String, String> variables, @RpcParam(paramName = "beId") String beId);
//
//    /**
//     * 跨SU批量检索数据
//     *
//     * @param dataIds 检索数据id集合
//     * @param retrieveParam 检索参数
//     * @param beId beId
//     * @return 检索返回值
//     */
//    @RpcServiceMethod(serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByBeIdWithDataIds")
//    Map<String, String> rpcRetrieveByBeIdWithDataIds(@RpcParam(paramName = "dataIds") List<String> dataIds,
//        @RpcParam(paramName = "retrieveParam") RetrieveParam retrieveParam,
//        @RpcParam(paramName = "variables") Map<String, String> variables, @RpcParam(paramName = "beId") String beId);
//
//    /**
//     * 跨su查询数据
//     *
//     * @param filter 过滤条件
//     * @param beId beId
//     * @return 查询返回值
//     */
//    @RpcServiceMethod(serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcQueryByBeId")
//    List<String> rpcQueryByBeId(@RpcParam(paramName = "filter") EntityFilter filter,
//        @RpcParam(paramName = "variables") Map<String, String> variables, @RpcParam(paramName = "beId") String beId);
//
//    /**
//     * 跨SU获取数据
//     *
//     * @param nodeId be节点
//     * @param dataId 检索数据id
//     * @param retrieveParam 检索参数
//     * @param beId beId
//     * @return 检索返回值
//     **/
//    @RpcServiceMethod(serviceId = "com.inspur.edp.bef.api.be.IBESUDataRpcService.rpcRetrieveByNodeIdByBeId")
//    String rpcRetrieveByNodeIdByBeId(@RpcParam(paramName = "nodeId") String nodeId,
//        @RpcParam(paramName = "dataId") String dataId,
//        @RpcParam(paramName = "retrieveParam") RetrieveParam retrieveParam, @RpcParam(paramName = "beId") String beId);
//}
