/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.action.tcc;

import com.inspur.edp.caf.transaction.api.annoation.tcc.BusinessActionContext;
import com.inspur.edp.cef.entity.entity.IEntityData;

public interface TccHandlerContext {

    /**
     * 返回当前数据行, 如果构件挂在主表上则返回主表数据行, 挂在子表上则返回子表数据行
     * 注意, 如果一阶段从数据库删除了数据, 此处将返回null
     */
    IEntityData getData();

    /**
     * 返回tcc事务上下文
     *
     * @see BusinessActionContext
     */
    BusinessActionContext getBusinessActionContext();

    /**
     * 返回getData()方法的数据行的ID, 不同的是getData()可能返回null, 此方法不可能返回null
     */
    String getDataId();

    /**
     * 删除当前行数据, 如果一阶段数据已被删除或一次二阶段的执行过程中第二次调用此方法会报错
     */
    void deleteCurrentData();

    /**
     * 新增指定子表的数据
     */
    IEntityData createChild(String childCode);
}
