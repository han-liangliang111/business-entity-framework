/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.repository;

public class SqlContext {
    private java.util.ArrayList<String> sqls;
    private java.util.ArrayList<Object[]> parameters;

    public final java.util.ArrayList<String> getSqls() {
        if (sqls == null) {
            sqls = new java.util.ArrayList<String>();
        }
        return sqls;
    }

    public final java.util.ArrayList<Object[]> getParameters() {
        if (parameters == null) {
            parameters = new java.util.ArrayList<Object[]>();
        }
        return parameters;
    }
}
