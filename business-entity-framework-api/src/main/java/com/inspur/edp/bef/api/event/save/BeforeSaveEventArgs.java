/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.event.save;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

/**
 * BEF保存前扩展事件参数，里面包括BE的ID、数据ID列表和Lcp接口 继承自
 */
public class BeforeSaveEventArgs extends SaveEventArgs {
    public BeforeSaveEventArgs(
            BEInfo beType,
            java.util.ArrayList<String> dataIDs,
            java.util.ArrayList<IChangeDetail> changes,
            String funcSessionID) {
        super(beType, dataIDs, changes, funcSessionID);
    }

    private BeforeSaveEventArgs() {
    }

    public BeforeSaveEventArgs createNewArgs() {
        BeforeSaveEventArgs beforeSaveEventArgs = new BeforeSaveEventArgs();
        copyToNewArgs(beforeSaveEventArgs);
//    beforeSaveEventArgs.getBeInfo().setRootEntityCode(newBeInfo.getRootEntityCode());
//    beforeSaveEventArgs.getBeInfo().setBEID(newBeInfo.getBEID());
//    beforeSaveEventArgs.getBeInfo().setBEType(newBeInfo.getBEType());
        return beforeSaveEventArgs;
    }
}
