/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.be;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssemblerFactory;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.api.services.IBELogger;
import com.inspur.edp.cef.api.manager.entity.ICefEntityManagerContext;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.api.rootManager.IRootManagerContext;
import com.inspur.edp.cef.variable.api.data.IVariableData;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * BE管理类上下文接口，用于自定义动作中获取当前BE相关信息，并提供增删改查等接口。
 * <p>如需在自定义动作中执行增删改查等，可以参考如下示例：
 * <blockquote><pre>
 *     getContext().getBEManager().retrieveDefault();
 *     getContext().getBEManager().delete("xxx");
 * </pre></blockquote>
 * <p>如需在自定义动作中执行实体动作(假设实体动作编号为action1且无参数)，
 * <p>生成型可以参考如下示例：
 * <blockquote><pre>
 *     IXXXEntity entity = (IXXXEntity)getContext().getEntity("dataId");
 *     entity.edit();
 *     entity.action1();
 * </pre></blockquote>
 * <p>解析型可以参考如下示例：
 * <blockquote><pre>
 *     IBusinessEntity entity = getContext().getEntity("dataId");
 *     entity.edit();
 *     entity.executeBizAction("action1");
 * </pre></blockquote>
 * <p>关于自定义动作相关说明可以<a href="https://open.inspuronline.com/iGIX/#/document/mddoc/docs-gsp-cloud-ds%2Fdev-guide-beta%2Fquickstart%2Fbusiness-entity-development%2Fproduct-feature%2Fmanipulation_bizaction.md">点击这里</a>查看。
 */
public interface IBEManagerContext extends IRootManagerContext, ICefEntityManagerContext {
    /**
     * 获取当前业务实体管理器
     */
    IBEManager getBEManager();

    /**
     * 添加已修改的Be实体对象
     * @param businessEntity
     */
    void addModifiedEntity(IBusinessEntity businessEntity);

    /**
     * 移除已修改的BE实体对象
     * @param dataId
     */
    void removeModifiedEntity(String dataId);
    void clearModifiedEntities();

    /**
     * 获取已修改的实体对象集合
     * @return
     */
    ConcurrentHashMap<String, IBusinessEntity> getModifiedEntities();
    /**
     * 获取一组指定ID的业务实体集合
     *
     * @param ids 实体数据的唯一标识集合
     * @return 业务实体集合
     */
    @Deprecated
    List<IBusinessEntity> getEntities(List<String> ids);

    @Deprecated
    List<IBusinessEntity> getEntities(List<String> ids, boolean addDataToCache);

    Map<String, IBusinessEntity> getEntitiesMap(List<String> ids);

    /**
     * 获取通过方法获取过的业务实体集合
     *
     * @return 业务实体集合
     */
    List<IBusinessEntity> getAllEntities();

    /**
     * 获取一个指定ID的业务实体
     *
     * @param id 实体数据的唯一标识
     * @return 业务实体
     */
    IBusinessEntity getEntity(String id);

    IBusinessEntity getEntity(String id, boolean addDataToCache);

    /**
     * 获取自定义动作管理类的组装器工厂
     */
    IMgrActionAssemblerFactory getMgrActionAssemblerFactory();

    /**
     * 获取日志记录
     *
     * @return 日志记录接口
     */
    IBELogger getLogger();

    /**
     * 清空通过方法获取过的业务实体的记录。
     */
    void clearEntities();

    /**
     * 根据当前Session创建Lcp,获取Lcp实例
     *
     * @param config BE的配置标识
     * @return 本地消费代理接口
     */
    IStandardLcp getLcp(String config);

    /**
     * 功能权限检查
     *
     * @param actionCode 动作编号
     */
    void checkAuthority(String actionCode);

    IVariableData getVariables();

    java.util.HashMap<String, String> getRepoVariables();

    /**
     * 保存后是否清空缓存, 默认不清空
     */
    BufferClearingType getBufferClearingType();

    void setBufferClearingType(BufferClearingType value);

    // region i18n

    /**
     * 国际化资源项
     *
     * @return
     */
    ModelResInfo getModelResInfo();

    /**
     * 翻译后的实体名称
     *
     * @return
     */
    String getEntityI18nName(String nodeCode);

    /**
     * 翻译后的属性名称
     *
     * @param labelID
     * @return
     */
    String getPropertyI18nName(String nodeCode, String labelID);

    /**
     * 翻译后的关联带出字段名称
     *
     * @param labelID
     * @param refLabelID
     * @return
     */
    String getRefPropertyI18nName(String nodeCode, String labelID, String refLabelID);

    /**
     * 翻译后的枚举显示值
     *
     * @param labelID
     * @param enumKey
     * @return
     */
    String getEnumValueI18nDisplayName(String nodeCode, String labelID, String enumKey);

    /**
     * 翻译后的唯一性约束提示信息
     *
     * @param conCode
     * @return
     */
    String getUniqueConstraintMessage(String nodeCode, String conCode);

    IBefCallContext getCallContext();
    // endregion
}
