/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.attr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * BE管理类(BEManager)上的自定义特性标签，用于指定业务实体扩展接口、业务实体实现类、业务实体完整接口
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BEExtendAttribute {
    /**
     * 获取或设置业务实体扩展接口的类型
     */
    java.lang.Class interfaceType();

    /**
     * 获取或设置业务实体实现类的类型，该类需要继承自AbstractEntity
     */
    java.lang.Class impType();

    /**
     * 实体完整接口的类型，该接口需要继承自IBusinessEntity
     */
    java.lang.Class allInterfaceType();
}
