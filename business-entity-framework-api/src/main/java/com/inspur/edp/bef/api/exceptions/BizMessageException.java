/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.bef.entity.exception.BefFatalException;
import com.inspur.edp.bef.entity.exception.BefRunTimeBaseException;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;

public class BizMessageException extends BefException {
    private IBizMessage privateMessage;
    //
    //  protected BizMessageException(SerializationInfo info, StreamingContext context) {
    //    super(info, context);
    //  }

    public BizMessageException(String code, IBizMessage message) {
        super(
                code,
                message.getMessageParams() != null
                        ? String.format(message.getMessageFormat(), message.getMessageParams())
                        : message.getMessageFormat(),
                null,
                ExceptionLevel.Error);
        //构造异常时再抛出异常，不合适
//        if (message.getLevel().getValue() < MessageLevel.Error.getValue()) {
//            throw new BefRunTimeBaseException(ErrorCodes.BEF_RUNTIME_2001);
//        }
        setBizMessage(message);
    }

    public final IBizMessage getBizMessage() {
        return privateMessage;
    }

    public final void setBizMessage(IBizMessage msg) {
        privateMessage = msg;
    }
}
