/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.be;

import com.inspur.edp.cef.api.manager.ICefEntityManager;
//import com.inspur.edp.cef.api.resourceInfo.EntityResInfo;
//import com.inspur.edp.cef.api.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.api.rootManager.ICefRootManager;
import com.inspur.edp.cef.entity.UQConstraintConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * BE管理类接口
 */
public interface IBEManager extends ICefRootManager, ICefEntityManager, IBEService {
    /**
     * 获取业务类型，不同业务类型之间要求不能重复
     */
    String getBEType();

    BEInfo getBEInfo();

    // ResponseContext ResponseContext { get; }

    /**
     * 将提交到内存的变更集提交到数据库
     *
     * @return 是否执行成功
     */
    boolean save();

    /**
     * 清空BE管理器中所有的变更集
     */
    void clearChangeset();

    /**
     * 清空BE管理器中所有的锁
     */
    void clearLock();

    /**
     * 清空BE管理器中加载到内存的所有实体数据
     */
    void clearBuffer();

    /**
     * <p>/** 初始年度表
     *
     * @param nodeCode 节点编号
     */
    void generateTable(String nodeCode);

    /**
     * 删除表
     *
     * @param nodeCode 表ID
     */
    void dropTable(String nodeCode);

//  /**
//   * 国际化资源（已废弃，使用getModelInfos）
//   *
//   * @return 实体节点的国际化生成类
//   */
//  @Deprecated
//  ModelResInfo getModelResInfo();

    /**
     * 获取模型相关信息
     *
     * @return 模型信息类
     */
    ModelResInfo getModelInfo();

    /**
     * 国际化资源-主实体
     *
     * @return 实体节点的国际化生成类
     */
    EntityResInfo getRootEntityResInfo();

    /**
     * 写入变量值
     *
     * @param key   变量
     * @param value 值
     */
    void setRepositoryVariable(String key, String value);

    /**
     * 获取约束字段信息
     *
     * @return
     */
    java.util.HashMap<String, java.util.ArrayList<UQConstraintConfig>> getConstraintInfo();

    void setConstraintInfo(HashMap<String, ArrayList<UQConstraintConfig>> uqConstraintConfigs);

    Object createPropertyDefaultValue(String nodeCode, String propertyName);

    List<String> getDistinctFJM(String fjnPropertyName, EntityFilter filter, Integer parentLayer);
}
