/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageCollector;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.entity.changeset.ChangeDetailMerger;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

import java.util.stream.Collectors;

public class ResponseContext implements IResponseContext {

    public MessageCollector messageCollector = new MessageCollector();
    //region 内部变更
    private java.util.HashMap<String, IChangeDetail> innerChangeset;
    /**
     * 一次请求执行过程中产生的变量内部变更
     */
    private IChangeDetail privateVariableInnerChange;

    @Override
    public final java.util.HashMap<String, IChangeDetail> getInnerChangeset() {
        return (innerChangeset != null)
                ? innerChangeset
                : (innerChangeset = new java.util.HashMap<String, IChangeDetail>());
    }

    public final void mergeInnerChange(IChangeDetail... details) {
        // DataValidator.CheckForNullReference(details, "details");

        for (IChangeDetail detail : details) {
            IChangeDetail existing = null;
            existing = getInnerChangeset().get(detail.getDataID());
            if (existing == null) {
                getInnerChangeset().put(detail.getDataID(), detail.clone());
                continue;
            }
            // 新增变更集合并一个修改变更集在ChangeDetailMerger中是异常情况, 但是在这里是正常的.
            if (existing.getChangeType() == ChangeType.Added
                    && detail.getChangeType() == ChangeType.Modify) {
                continue;
            }
            ChangeDetailMerger.mergeChangeDetail(detail, existing);
        }
    }

    @Override
    public final IChangeDetail getVariableInnerChange() {
        return privateVariableInnerChange;
    }

    private void setVariableInnerChange(IChangeDetail value) {
        privateVariableInnerChange = value;
    }
    // endregion

    public final void mergeVarInnerChange(IChangeDetail change) {
        //		DataValidator.CheckForNullReference(change, "change");

        if (getVariableInnerChange() == null) {
            setVariableInnerChange(change.clone());
        } else {
            ChangeDetailMerger.mergeChangeDetail(change, getVariableInnerChange());
        }
    }

    @Override
    public java.util.List<IBizMessage> getMessages() {
        return messageCollector.getMessages();
    }

    public java.util.List<IBizMessage> getErrorMessages() {
        return messageCollector.getMessages().stream().filter(
                item -> item.getLevel() == MessageLevel.Error).collect(Collectors.toList());
    }

    public final void mergeMessage(java.util.List<IBizMessage> msgList) {
        //		DataValidator.CheckForNullReference(msgList, "msgList");

        for (IBizMessage msg : msgList) {
            messageCollector.addMessage(msg);
        }
    }

    @Override
    public void clear() {
        clearMessage();
        clearInnerChange();
        ;
    }

    public void clearMessage() {
        messageCollector.clear();
    }

    public final boolean hasErrorMessage() {
        for (IBizMessage message : getMessages()) {
            if (message.getLevel() == MessageLevel.Error) {
                return true;
            }
        }
        return false;
    }

    public void clearInnerChange() {
        getInnerChangeset().clear();
    }
    // endregion
}
