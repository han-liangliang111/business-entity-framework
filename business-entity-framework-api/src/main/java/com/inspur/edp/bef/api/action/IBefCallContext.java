/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.action;

import com.inspur.edp.cef.api.RefObject;

/**
 * 一个键值对集合形式的存储区。
 * <p>存储区中的内容在一次BE调用执行结束后自动清空。
 * 调用{@link com.inspur.edp.bef.api.lcp.IStandardLcp}的增删改查或保存或自定义动作等方法，都是一次BE调用。
 * 一次BE调用过程中如有嵌套BE调用，那么在最外层BE调用结束后自动清空。
 * <p>为避免键冲突，请勿使用长度过短的字符串作为键。键不允许为null，值允许为null。
 */
public interface IBefCallContext {

    /**
     * 根据键查找值。
     * <p>键必须存在否则会引发异常。{@link #tryGetData(String, RefObject)}方法在键不存在时不会引发异常。
     *
     * @param name 键
     * @param <T>
     * @return 值
     * @throws IllegalArgumentException {@code name}作为键在当前存储区中不存在
     * @throws NullPointerException     {@code name}为{@code null}
     * @see #containsKey(String)
     * @see #tryGetData(String, RefObject)
     */
    <T> T getData(String name);

    /**
     * 根据键设置值
     * <p>键不允许为{@code null}，值允许为{@code null}。
     * <p>如果键已经存在，则会覆盖。
     *
     * @param name
     * @param value
     * @throws NullPointerException {@code name}为{@code null}
     */
    void setData(String name, Object value);

    /**
     * 根据键获取值
     * <p>如果返回值是true代表键存在，此时可以通过{@code value.argvalue}得到值；
     * <p>如果返回值是false代表键不存在。
     *
     * @param name  键
     * @param value 值
     * @param <T>
     * @return 键是否存在
     * @throws NullPointerException {@code name}为{@code null}
     * @see #getData(String)
     */
    <T> boolean tryGetData(String name, RefObject<T> value);

    /**
     * 判断键是否存在
     *
     * @param name
     * @return 键是否存在
     * @throws NullPointerException {@code name}为{@code null}
     */
    boolean containsKey(String name);
}
