//package com.inspur.edp.bef.api.be;
//
//import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
//import com.inspur.edp.bef.api.parameter.retrieve.RetrieveResult;
//import com.inspur.edp.cef.entity.condition.EntityFilter;
//import com.inspur.edp.cef.entity.entity.IEntityData;
//import java.util.List;
//import java.util.Map;
//import org.springframework.stereotype.Service;
//
//public interface BeDataServiceWithSU {
//    RespectiveRetrieveResult retrieveByBeId(String dataId, RetrieveParam retrieveParam, String beId);
//
//    RespectiveRetrieveResult retrieveByBeIdWithParam(String dataId, RetrieveParam retrieveParam,
//        Map<String, String> variables, String beId);
//
//    RetrieveResult retrieveByBeId(List<String> dataIds, RetrieveParam retrieveParam, String beId);
//
//    RetrieveResult retrieveByBeIdWithParam(List<String> dataIds, RetrieveParam retrieveParam, Map<String, String> variables,
//        String beId);
//
//    List<IEntityData> queryByBeId(EntityFilter filter, String beId);
//
//    List<IEntityData> queryByBeIdWithParam(EntityFilter filter, Map<String, String> variables, String beId);
//}
