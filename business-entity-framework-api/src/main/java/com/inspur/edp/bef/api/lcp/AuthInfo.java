/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import lombok.Getter;
import lombok.Setter;

/**
 * 权限信息类
 */
public class AuthInfo {

    /**
     * 规则类型
     * 1.VM
     * 2.VOHelp
     */
    private String privateExtType;

    /**
     * VOId
     */
    private String privateExtend1;

    /**
     * 对于零代码权限，记录的是表单ID
     */
    private String privateExtend2;
    private String privateExtend3;
    private String privateExtend4;
    private String privateExtend5;
    private String sourceType;

    /**
     * 权限类型
     * 1.default 低代码权限（VO上没有记录，其余需要记录）
     * 2.PF_OAStyleDataPermissionController零代码权限
     * 3.VMRule 规则权限
     */
    @Getter
    @Setter
    private String authType;

    public final String getExtType() {
        return privateExtType;
    }

    public final void setExtType(String value) {
        privateExtType = value;
    }

    public final String getExtend1() {
        return privateExtend1;
    }

    public final void setExtend1(String value) {
        privateExtend1 = value;
    }

    public final String getExtend2() {
        return privateExtend2;
    }

    public final void setExtend2(String value) {
        privateExtend2 = value;
    }

    public final String getExtend3() {
        return privateExtend3;
    }

    public final void setExtend3(String value) {
        privateExtend3 = value;
    }

    public final String getExtend4() {
        return privateExtend4;
    }

    public final void setExtend4(String value) {
        privateExtend4 = value;
    }

    public final String getExtend5() {
        return privateExtend5;
    }

    public final void setExtend5(String value) {
        privateExtend5 = value;
    }

    public final String getSourceType() {
        return sourceType;
    }

    public final void setSourceType(String value) {
        sourceType = value;
    }

    public final void SourceType(String value) {
        sourceType = value;
    }

    public boolean isEmpty() {
        return privateExtType == null && privateExtend1 == null && privateExtend2 == null && privateExtend3 == null
                && privateExtend4 == null && privateExtend5 == null;
    }
}
