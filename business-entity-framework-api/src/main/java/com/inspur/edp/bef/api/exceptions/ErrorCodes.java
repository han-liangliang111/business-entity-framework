/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public final class ErrorCodes {
    /*
       cef_exception.properties, Gsp_Bef_LockVal_0001, 数据已被其他用户锁定，无法执行当前操作！
     */
    public static final String EditingLocked = "Gsp_Bef_LockVal_0001";

    /**
     * 调用者已废弃，但不敢删除
     */
    public static final String CirclDtmFound = "GSP_ADP_BEF_2008";

    /*
       bef_runtime_core.properties, BEF_RUNTIME_5078, 当前功能发生错误已不能继续使用,请关闭功能并重新打开再试。
     */
    public static final String SessionNotValid = "BEF_RUNTIME_5078";

    /*
       bef_runtime_core.properties, BEF_RUNTIME_5078, 系统正忙, 请稍候再试
     */
    public static final String SessionTimeout = "BEF_RUNTIME_5081";

    /*
       bef_runtime_core.properties, BEF_RUNTIME_1004, 您无权操作此数据
     */
    public static final String DataPermissionDenied = "BEF_RUNTIME_1004";
    /*
       bef_runtime_core.properties, BEF_RUNTIME_1005, 您无权操作此操作
     */
    public static final String FuncPermissionDenied = "BEF_RUNTIME_1005";

    //public static final String BEF_RUNTIME_2001 = "BEF_RUNTIME_2001";
}
