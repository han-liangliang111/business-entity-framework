/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.comp.validation;

import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.IValidation;
import com.inspur.edp.udt.api.Validation.IValidationContext;

import java.util.Objects;

public class TplRequiredValDecorator implements IValidation {

    private IValidation inner;

    public TplRequiredValDecorator(com.inspur.edp.cef.spi.validation.IValidation requiredVal) {
        Objects.requireNonNull(requiredVal, "requiredVal");
        inner = requiredVal;
    }

    @Override
    public boolean canExecute(IChangeDetail change) {
        return inner.canExecute(change);
    }

    @Override
    public void execute(ICefValidationContext context, IChangeDetail change) {
        if (!((IValidationContext) context).getTemplateValues().getRequired()) {
            return;
        }
        inner.execute(context, change);
    }
}
