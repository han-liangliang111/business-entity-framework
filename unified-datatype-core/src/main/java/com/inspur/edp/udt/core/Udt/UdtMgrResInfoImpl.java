/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Udt;

import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefModelResInfoImpl;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjModelResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjResInfo;

public class UdtMgrResInfoImpl extends CefValueObjModelResInfo {

    public UdtMgrResInfoImpl(String currentSu, String metadataId, String rootNodeCode,
                             String displayKey) {
        super(currentSu, metadataId, rootNodeCode, displayKey);
    }

    @Override
    protected CefValueObjResInfo getResourceInfo() {
        return null;
    }

    public CefValueObjResInfo getValueObjResInfo() {
        return getResourceInfo();
    }
}
