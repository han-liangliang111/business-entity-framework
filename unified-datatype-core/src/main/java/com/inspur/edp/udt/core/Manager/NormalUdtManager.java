/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Manager;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjModelResInfo;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeJsonDeserializer;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefChangeSerializer;
import com.inspur.edp.udt.api.Udt.IUdtContext;
import com.inspur.edp.udt.core.Udt.AbstractUdt;

public abstract class NormalUdtManager extends AbstractUdtManager {

    @Override
    protected JsonSerializer<ICefData> getDataSerializer() {
        return null;
    }

    @Override
    protected JsonDeserializer<ICefData> getDataDeserializer() {
        return null;
    }

    @Override
    public AbstractUdt createAbstractUdt(IUdtContext ctx) {
        return null;
    }

    @Override
    public ValueObjModelResInfo getModelInfo() {
        return super.getModelInfo();
    }

    @Override
    protected IValueObjData createDataCore() {
        return null;
    }

    @Override
    public INestedRepository getRepository() {
        return null;
    }

    @Override
    public IAccessorCreator getAccessorCreator() {
        return null;
    }

    @Override
    protected AbstractCefChangeSerializer getChangeSerializer() {
        return null;
    }

    @Override
    protected AbstractCefChangeJsonDeserializer getChangeDeserializer() {
        return null;
    }
}
