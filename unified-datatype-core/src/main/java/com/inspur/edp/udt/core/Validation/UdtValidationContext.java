/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Validation;

import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.spi.entity.resourceInfo.ValueObjModelResInfo;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.udt.api.Udt.ITemplateValues;
import com.inspur.edp.udt.api.Udt.IUdtContext;
import com.inspur.edp.udt.api.Udt.IUnifiedDataType;
import com.inspur.edp.udt.api.Validation.IValidationContext;
import com.inspur.edp.udt.core.Udt.UdtContext;
import com.inspur.edp.udt.entity.IUdtData;

public class UdtValidationContext implements IValidationContext {
    private UdtContext udtCtx;

    public UdtValidationContext(IUdtContext udtCtx) {
        this.udtCtx = (UdtContext) udtCtx;
    }

    public UdtContext getUdtContext() {
        return udtCtx;
    }

    public IUdtData getUdtData() {
        return (IUdtData) udtCtx.getData();
    }

    public ITemplateValues getTemplateValues() {
        return udtCtx.getTemplateValues();
    }

    //#region explicitImp
    public ICefData getData() {
        return udtCtx.getData();
    }

    //#endregion

    //#region i18n
    public ValueObjModelResInfo getModelResInfo() {
        return udtCtx.getModelResInfo();
    }

    public String getEntityI18nName() {
        return udtCtx.GetEntityI18nName();
    }

    public final String getPropertyI18nName(String labelID) {
        return udtCtx.GetPropertyI18nName(labelID);
    }

    public final String getRefPropertyI18nName(String labelID, String refLabelID) {
        return udtCtx.GetRefPropertyI18nName(labelID, refLabelID);
    }

    public final String getEnumValueI18nDisplayName(String labelID, String enumKey) {
        return udtCtx.GetEnumValueI18nDisplayName(labelID, enumKey);

    }

    @Override
    public ICefValidationContext getParentContext() {
        return ((IUnifiedDataType) udtCtx.getValueObjDataType()).getParentContext();

    }

    @Override
    public String getPropertyName() {
        return ((IUnifiedDataType) udtCtx.getValueObjDataType()).getPropertyName();
    }

    //#endregion
}
