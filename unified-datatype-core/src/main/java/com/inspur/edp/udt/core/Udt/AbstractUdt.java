/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Udt;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.datatype.CefDataTypeCacheInfo;
import com.inspur.edp.cef.core.datatype.CefValueObjCacheInfo;
import com.inspur.edp.cef.core.datatype.CefValueObject;
import com.inspur.edp.cef.core.determination.ValueObjDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjResInfo;
import com.inspur.edp.cef.spi.validation.IValueObjRTValidationAssembler;
import com.inspur.edp.udt.api.Udt.ITemplateValues;
import com.inspur.edp.udt.api.Udt.IUdtContext;
import com.inspur.edp.udt.api.Udt.IUnifiedDataType;
import com.inspur.edp.udt.core.Determination.UdtAfterCreateDtmAssembler;
import com.inspur.edp.udt.core.Determination.UdtAfterModifyDtmAssembler;
import com.inspur.edp.udt.core.Determination.UdtBeforeSaveDtmAssembler;
import com.inspur.edp.udt.core.Determination.UdtDeterminationExecutor;
import com.inspur.edp.udt.core.Validation.UdtAfterModifyValAssembler;
import com.inspur.edp.udt.core.Validation.UdtBeforeSaveValAssembler;

public abstract class AbstractUdt extends CefValueObject implements IUnifiedDataType {
    private ICefValidationContext context;
    private String propName;

    protected AbstractUdt(IUdtContext udtContext) {
        super();
        super.setContext(udtContext);
    }

    public IUdtContext getUdtContext() {
        return (IUdtContext) ((super.getContext() instanceof IUdtContext) ? super.getContext() : null);
    }

    public ITemplateValues getTemplateValues() {
        return getUdtContext().getTemplateValues();
    }

    public ICefValidationContext getParentContext() {
        return context;
    }

    public void setParentContext(ICefValidationContext context) {
        this.context = context;
    }

    public String getPropertyName() {

        return propName;
    }

    public void setPropertyName(String propName) {
        this.propName = propName;
    }

    @Override
    public void modify(ValueObjModifyChangeDetail change) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected CefDataTypeCacheInfo getDataTypeCacheInfo() {
        return getUdtEntityCacheInfo();
    }

    @Override
    protected CefValueObjCacheInfo getValueObjCacheInfo() {
        return getUdtEntityCacheInfo();
    }

    protected UdtEntityCacheInfo getUdtEntityCacheInfo() {
        return null;
    }

    @Override
    public CefValueObjResInfo getValueObjResInfo() {
        return getEntityResInfoImpl();
    }

    protected UdtResInfoImpl getEntityResInfoImpl() {
        return null;
    }

    //region dtm
    @Override
    public void afterModifyDeterminate(ICefDeterminationContext parent, ValueObjModifyChangeDetail change) {
        IValueObjRTDtmAssembler ass = getAfterModifyDtmAssembler();
        if (ass == null) {
            return;
        }

        new UdtDeterminationExecutor(getValueObjContext(), ass, parent, change).execute();
    }

    @Override
    public void retrieveDefaultDeterminate(ICefDeterminationContext parent) {
        IValueObjRTDtmAssembler ass = getRetrieveDefaultDtmAssembler();
        if (ass == null) {
            return;
        }

        new UdtDeterminationExecutor(getValueObjContext(), ass, parent, null).execute();
    }

    @Override
    public void beforeSaveDeterminate(ICefDeterminationContext parent, ValueObjModifyChangeDetail change) {
        IValueObjRTDtmAssembler ass = getBeforeSaveDtmAssembler();
        if (ass == null) {
            return;
        }

        new UdtDeterminationExecutor(getValueObjContext(), ass, parent, change).execute();
    }

    protected IValueObjRTDtmAssembler createRetrieveDefaultDtmAssembler() {
        return new UdtAfterCreateDtmAssembler();
    }

    protected IValueObjRTDtmAssembler createAfterModifyDtmAssembler() {
        return new UdtAfterModifyDtmAssembler();
    }

    protected IValueObjRTDtmAssembler createBeforeSaveDtmAssembler() {
        return new UdtBeforeSaveDtmAssembler();
    }

    //endregion

    //region val
    protected IValueObjRTValidationAssembler createAfterModifyValidationAssembler() {
        return new UdtAfterModifyValAssembler();
    }

    protected IValueObjRTValidationAssembler createBeforeSaveValidationAssembler() {
        return new UdtBeforeSaveValAssembler();
    }
    //endregion
}
