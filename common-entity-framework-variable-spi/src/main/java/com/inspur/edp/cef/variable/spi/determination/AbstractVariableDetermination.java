package com.inspur.edp.cef.variable.spi.determination;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.variable.api.determination.IVarDeterminationContext;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;

public abstract class AbstractVariableDetermination extends AbstractDeterminationAction {
    public AbstractVariableDetermination(IVarDeterminationContext context, IChangeDetail change) {
        super(context, change);
    }

    public IVarDeterminationContext getVarDtmContext() {
        return (IVarDeterminationContext) super.getContext();
    }
}
