/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.spi.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.spi.jsonser.valueobj.AbstractValueObjSerializer;
import com.inspur.edp.udt.api.Manager.serialize.IAssoInfoSerializeItem;
import com.inspur.edp.udt.api.Manager.serialize.NestedSerializeContext;

public abstract class AbstractUdtDataSerializerItem extends AbstractValueObjSerializer {

    protected void writeExtendAssoInfo(JsonGenerator writer, Object info,
                                       SerializerProvider serializerProvider) {
        IAssoInfoSerializeItem assoSerItem = getAssoInfoSerializerItem();
        if (assoSerItem == null) {
            return;
        }

        assoSerItem.writeAssociationInfo(writer, info, serializerProvider);
    }

    protected boolean readExtendAssoInfo(Object info, String propertyName, Object propertyValue,
                                         DeserializationContext serializerProvider) {
        IAssoInfoSerializeItem assoSerItem = getAssoInfoSerializerItem();
        if (assoSerItem == null) {
            return false;
        }

        return assoSerItem.readAssociationInfo(serializerProvider, info, propertyName, propertyValue);
    }

    private IAssoInfoSerializeItem getAssoInfoSerializerItem() {
        NestedSerializeContext ctx = getCefSerializeContext() instanceof NestedSerializeContext
                ? (NestedSerializeContext) getCefSerializeContext() : null;
        IAssoInfoSerializeItem result = ctx != null ? ctx.getAssociationEnrichedSerializer() : null;
        if (result != null && result instanceof AbstractAssoInfoSerializeItem) {
            ((AbstractAssoInfoSerializeItem) result).setCefSerializeContext(getCefSerializeContext());
        }
        return result;
    }
}
