/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.commonmodel.api.exception.CmRunTimeException;
import com.inspur.edp.commonmodel.api.exception.ExceptionCode;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import lombok.SneakyThrows;

import static com.fasterxml.jackson.core.JsonToken.FIELD_NAME;

public class SessionItemDeserializer extends JsonDeserializer<FuncSessionItemIncrement> {

    private final List<Tuple<String, String>> itemTypes;

    public SessionItemDeserializer(List<Tuple<String, String>> itemTypes) {
        this.itemTypes = itemTypes;
    }

    @Override
    public FuncSessionItemIncrement deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        FuncSessionItemIncrement itemIncrement = new FuncSessionItemIncrement();
        itemIncrement.setIndex(0);
        itemIncrement.setReset(false);
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == FIELD_NAME) {
            String propName = SerializerUtils.readPropertyName(jsonParser);
            readPropertyValue(itemIncrement, propName, jsonParser);
        }
        SerializerUtils.readEndObject(jsonParser);

        return itemIncrement;
    }

    private void readPropertyValue(FuncSessionItemIncrement itemIncrement, String propName, JsonParser jsonParser) throws IOException {
        switch (propName) {
            case "index":
                itemIncrement.setIndex(SerializerUtils.readPropertyValue_Integer(jsonParser));
                break;
//            case "configId":
//                itemIncrement.setConfigId(SerializerUtils.readPropertyValue_String(jsonParser));
//                break;
            case "reset":
                itemIncrement.setReset(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case "lockIds":
                itemIncrement.setLockIds(SerializerUtils.readStringArray(jsonParser));
                break;
            case "variable":
                readVariable(itemIncrement, jsonParser);
                break;
            case "changeDetails":
                readChangeDetails(itemIncrement, jsonParser);
                break;
            case "cust":
                readCustoms(itemIncrement, jsonParser);
                break;
            default:
                throw new CmRunTimeException(ExceptionCode.CM_RUNTIME_1003, propName);
        }
    }

    @SneakyThrows
    private void readCustoms(FuncSessionItemIncrement itemIncrement, JsonParser jsonParser) {
        itemIncrement.setCustoms(jsonParser.readValueAs(Map.class));
        jsonParser.nextToken();
    }

    @SneakyThrows
    private void readVariable(FuncSessionItemIncrement itemIncrement, JsonParser jsonParser) {
//        SessionIncSerializerUtil service = SpringBeanUtils.getBean(this.itemTypes.get(itemIncrement.getIndex()).getItem1(), SessionIncSerializerUtil.class);
        itemIncrement.setVarChangeString(jsonParser.readValueAsTree().toString());//service.deserializerVar(this.itemTypes.get(itemIncrement.getIndex()).getItem2(),jsonParser));
        jsonParser.nextToken();
    }

    @SneakyThrows
    private void readChangeDetails(FuncSessionItemIncrement itemIncrement, JsonParser jsonParser) {
//        SessionIncSerializerUtil service = SpringBeanUtils.getBean(this.itemTypes.get(itemIncrement.getIndex()).getItem1(), SessionIncSerializerUtil.class);
        itemIncrement.setChangeDetailString(jsonParser.readValueAsTree().toString());//service.deserializerChanges(this.itemTypes.get(itemIncrement.getIndex()).getItem2(),jsonParser));
        jsonParser.nextToken();
    }
}
