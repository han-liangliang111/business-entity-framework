/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.util;

import com.inspur.edp.commonmodel.core.session.tableentity.DBConnectInfo;
import io.iec.edp.caf.commons.dataaccess.JDBCConnectionInfo;
import io.iec.edp.caf.commons.dataaccess.SimpleDbConfigData;
import io.iec.edp.caf.data.source.DbConfigDataConvertor;
import io.iec.edp.caf.tenancy.api.entity.DbConnectionInfo;

public class DbConnectInfoUtil {
    public static JDBCConnectionInfo toJDBCConnectionInfo(String id, DBConnectInfo dbConnectInfo) {
        SimpleDbConfigData configData = new SimpleDbConfigData();
        configData.setId(id);
        configData.setDbType(dbConnectInfo.getDbType());
        configData.setConnectionString(dbConnectInfo.getConnectionString());
        JDBCConnectionInfo connectionInfo = DbConfigDataConvertor.convert(configData);
        return connectionInfo;
    }

    public static DbConnectionInfo toDbConnectionInfo(String id, DBConnectInfo dbConnectInfo) {
        DbConnectionInfo dbConnectionInfo = new DbConnectionInfo();
        dbConnectionInfo.setId(id);
        dbConnectionInfo.setDbType(dbConnectInfo.getDbType());
        dbConnectionInfo.setConnectionString(dbConnectInfo.getConnectionString());
        return dbConnectionInfo;
    }
}
