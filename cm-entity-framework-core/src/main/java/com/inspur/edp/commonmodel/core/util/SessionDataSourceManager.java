/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.util;

import com.inspur.edp.commonmodel.core.session.tableentity.DBConnectInfo;
import io.iec.edp.caf.tenancy.api.entity.DbConnectionInfo;
import io.iec.edp.caf.tenancy.core.extensions.TenantDataSourceProvider;
import io.iec.edp.caf.tenancy.core.utils.HelperTools;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class SessionDataSourceManager {
    private static SessionDataSourceManager manager = new SessionDataSourceManager();

    private Map<String, DataSource> dataSourceMap = new HashMap<>();

    private SessionDataSourceManager() {
    }

    public static SessionDataSourceManager getInstance() {
        return manager;
    }

    public DataSource getDataSource(String id, DBConnectInfo dbConnectInfo) {
        String connectionString = dbConnectInfo.getConnectionString();
        if (dataSourceMap.containsKey(connectionString)) {
            return dataSourceMap.get(connectionString);
        }

        DbConnectionInfo dbConnectionInfo = DbConnectInfoUtil.toDbConnectionInfo(id, dbConnectInfo);
        DataSource dataSource = HelperTools.toDataSource(dbConnectionInfo);
        dataSource = TenantDataSourceProvider.handleDatasource(dataSource);
        dataSourceMap.put(connectionString, dataSource);
        return dataSource;
    }
}
