/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.util;

import com.inspur.edp.commonmodel.api.exception.CmRunTimeException;
import com.inspur.edp.commonmodel.api.exception.ExceptionCode;
import io.iec.edp.caf.common.JSONSerializer;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.databaseobject.api.entity.AbstractDatabaseObject;
import io.iec.edp.caf.databaseobject.api.entity.DBInfo;
import io.iec.edp.caf.databaseobject.api.entity.DboDeployResultInfo;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectDeployService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectExportService;
import io.iec.edp.caf.databaseobject.api.service.IDatabaseObjectRtService;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
public class DboTool {
    private DBInfo dbInfo;
    private IDatabaseObjectDeployService dboDeployService;
    private IDatabaseObjectExportService dboExportService;
    private IDatabaseObjectRtService dboRtService;

    /**
     * 使用当前数据库连接
     */
    public DboTool() {
        this(null);
    }

    /**
     * 使用指定的数据库连接
     */
    public DboTool(DBInfo dbInfo) {
        this.dbInfo = dbInfo;
        dboDeployService = SpringBeanUtils.getBean(IDatabaseObjectDeployService.class);
        dboExportService = SpringBeanUtils.getBean(IDatabaseObjectExportService.class);
        dboRtService = SpringBeanUtils.getBean(IDatabaseObjectRtService.class);
    }

    public boolean isTableExist(String tableName) {
        if (dbInfo == null) {
            //没有DbInfo时，表示使用当前数据库连接
            Map<String, Boolean> tablesExists = dboRtService.isTablesExist(Collections.singletonList(tableName));
            return tablesExists.get(tableName) == Boolean.TRUE;
        } else {
            //存在DbInfo时，表示使用指定的数据库连接
            List<String> allTablesNameList = dboExportService.getAllTablesName(this.dbInfo);
            Set<String> tableNames = new HashSet<>(allTablesNameList.size());
            for (String tName : allTablesNameList) {
                tableNames.add(tName.toLowerCase());
            }
            return tableNames.contains(tableName.toLowerCase());
        }
    }

    public void deployDatabaseObjects(List<AbstractDatabaseObject> dbos) {
        if (dbInfo == null) {
            DboDeployResultInfo dboDeployResultInfo = dboDeployService.deployDatabaseObjects(dbos, null);
            if (!dboDeployResultInfo.getDeploySuccess()) {
                String[] messagesParams = new String[]{dboDeployResultInfo.getDeployResult().toString(), JSONSerializer.serialize(dbos)};
                throw new CmRunTimeException(ExceptionCode.CM_RUNTIME_1001, messagesParams);
            }
        } else {
            DboDeployResultInfo dboDeployResultInfo = dboDeployService.deployDatabaseObjects(dbos, null, dbInfo);
            if (!dboDeployResultInfo.getDeploySuccess()) {
                String[] messagesParams = new String[]{dboDeployResultInfo.getDeployResult().toString(), JSONSerializer.serialize(dbos)};
                throw new CmRunTimeException(ExceptionCode.CM_RUNTIME_1002, messagesParams);
            }
        }
    }
}
