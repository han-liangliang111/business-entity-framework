/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.variable;

import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.cef.entity.accessor.base.AccessorComparer;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.cef.variable.api.manager.IVariableManager;
import com.inspur.edp.cef.variable.core.data.BufferManager;
import com.inspur.edp.commonmodel.core.session.distributed.DistributedSessionPart;
import com.inspur.edp.commonmodel.core.session.distributed.RootEditToken;
import com.inspur.edp.commonmodel.core.session.distributed.SessionEditToken;

public class VarBufferManager extends BufferManager implements DistributedSessionPart {

    private final String alias;
    private IVariableManager privateVarManager;
    //region
    private IValueObjData orgData;

    public VarBufferManager(ICefSessionItem session, IVariableManager varMgr, String alias) {
        super(session, varMgr.getAccessorCreator());
        this.privateVarManager = varMgr;
        this.alias = alias;
    }

    private static ValueObjModifyChangeDetail compare(IValueObjData first,
                                                      IValueObjData second) {
        if (first == second) {
            return null;
        }

        ValueObjModifyChangeDetail change = new ValueObjModifyChangeDetail();
        for (String name : first.getPropertyNames()) {
            Object secondValue = second.getValue(name);
            Object firstValue = first.getValue(name);
            if (secondValue instanceof IValueObjData) {
                ValueObjModifyChangeDetail nestedChg = compare((IValueObjData) firstValue,
                        (IValueObjData) secondValue);
                if (nestedChg != null && !nestedChg.getPropertyChanges().isEmpty()) {
                    change.putItem(name, nestedChg);
                }
            } else if (!AccessorComparer.equals(firstValue, secondValue)) {
                change.setItem(name, secondValue);
            }
        }
        return change;
    }

    public final void initBuffer() {
        initCurrent();
        initReadonlyCurrent();
        if (getVarManager() != null) {
            super.getSessionItem().getVarListener().registListener(getCurrentData());
        }
    }

    private IAccessor initCurrent() {
        if (getVarManager() == null) {
            return null;
        }
        return InitBuffer_ZeroLevel((IVariableData) getVarManager().createDataType(), false);
    }

    private IAccessor initReadonlyCurrent() {
        if (getVarManager() == null) {
            return null;
        }
        return InitNextBuffer(true);
    }

    public IVariableData getCurrentData() {
        if (getVarManager() == null) {
            return null;
        }
        return (IVariableData) GetDataByLevel(0);
    }

    public IVariableData getReadonlyCurrentData() {
        return (IVariableData) GetDataByLevel(1);
    }

    public final IVariableManager getVarManager() {
        return privateVarManager;
    }

    @Override
    public void beginEdit(RootEditToken token) {
        orgData = (IValueObjData) getCurrentData().copy();
    }

    @Override
    public void endEdit(RootEditToken token) {
        ValueObjModifyChangeDetail change = compare(orgData, getCurrentData());
        if (change != null && !change.getPropertyChanges().isEmpty()) {
            token.getItemIncrement(alias).setVariable(change);
        }
    }

    @Override
    public void notifySave() {
    }
    //endregion
}
