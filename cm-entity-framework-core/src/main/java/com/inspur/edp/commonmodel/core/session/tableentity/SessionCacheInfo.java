/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.tableentity;

import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionIncrement;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;


@AllArgsConstructor
@NoArgsConstructor
public class SessionCacheInfo {

    private String Id;
    private String sessionId;
    private String sessionContent;
    private int version;
    private Date createdOn;
    private transient FuncSessionIncrement sessionChangeSet;

    public SessionCacheInfo(String sessionId, int version) {
        this.setId(sessionId + "-" + version);
        this.setSessionId(sessionId);
        this.setVersion(version);
        this.setCreatedOn(new Date());
    }

    public FuncSessionIncrement getSessionChangeSet() {
        return sessionChangeSet;
    }

    public void setSessionChangeSet(FuncSessionIncrement sessionChangeSet) {
        this.sessionChangeSet = sessionChangeSet;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String id) {
        this.sessionId = id;
    }

    public String getSessionContent() {
        return sessionContent;
    }

    public void setSessionContent(String sessionContent) {
        this.sessionContent = sessionContent;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
}
