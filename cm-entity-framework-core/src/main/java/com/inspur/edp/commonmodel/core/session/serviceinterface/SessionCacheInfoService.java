/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.serviceinterface;

import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.commonmodel.core.session.tableentity.SessionCacheInfo;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface SessionCacheInfoService {

    List<SessionCacheInfo> getSessionCacheInfos(String id, int version);

    void saveSessionCacheInfo(SessionCacheInfo info, Map<String, ICefSessionItem> items);

    void deleteSessionCacheInfos(String id, int version);

    void clearSessionCacheInfos(String id);

    boolean isLatestVersion(String id, int ver);

    List<SessionCacheInfo> traversalSessionCacheInfos(Date date);

    void batchClear(List<String> sessionIds);
}
