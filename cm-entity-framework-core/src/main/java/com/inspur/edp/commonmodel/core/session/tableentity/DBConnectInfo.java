/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.core.session.tableentity;

import io.iec.edp.caf.commons.dataaccess.DbType;

public class DBConnectInfo {

    private int tableCount = 0;

    private boolean isUseDefConnect = true;

    private String tableName = "BefSessionCacheInfo";

    private String connectionString;

    private DbType dbType;

    public int getTableCount() {
        return tableCount;
    }

    public void setTableCount(int connectCount) {
        this.tableCount = connectCount;
    }

    public boolean isUseDefConnect() {
        return isUseDefConnect;
    }

    public void setUseDefConnect(boolean useDefConnect) {
        isUseDefConnect = useDefConnect;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }
}
