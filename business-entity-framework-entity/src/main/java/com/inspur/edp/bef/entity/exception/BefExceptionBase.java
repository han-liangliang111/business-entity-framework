/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.entity.exception;

import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import io.iec.edp.caf.commons.exception.CAFRuntimeException;

//bdf异常基类
public class BefExceptionBase extends CAFRuntimeException {
    //TODO 兼容之前的com.inspur.edp.bef.entity.exception.ExceptionLevel，后续需改
//ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string exceptionMessage, System.Exception innerException = null, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)
    @Deprecated
    public BefExceptionBase(String exceptionCode, String exceptionMessage, RuntimeException innerException, com.inspur.edp.bef.entity.exception.ExceptionLevel level, boolean isBizException) {
        //super(exceptionMessage, innerException);
        super("pfcommon", exceptionCode, exceptionMessage, innerException, getLevel(level), isBizException);
    }

    //ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string resourceFile, System.Exception innerException, string[] exceptionParams, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)
    @Deprecated
    public BefExceptionBase(String exceptionCode, String resourceFile, RuntimeException innerException, String[] exceptionParams, com.inspur.edp.bef.entity.exception.ExceptionLevel level, boolean isBizException) {
        //super(innerException);
        super("pfcommon", resourceFile, exceptionCode, exceptionParams, innerException, getLevel(level), isBizException);
    }

    //ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string resourceFile, string messageCode, System.Exception innerException, string[] exceptionParams, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)
    @Deprecated
    public BefExceptionBase(String exceptionCode, String resourceFile, String messageCode, RuntimeException innerException, String[] exceptionParams, com.inspur.edp.bef.entity.exception.ExceptionLevel level, boolean isBizException) {
        //super(innerException);
        super("pfcommon", resourceFile, exceptionCode, exceptionParams, innerException, getLevel(level), isBizException);
    }

    //
    //	protected BefExceptionBase(SerializationInfo info, StreamingContext context)
    //	{
    //		super(info, context);
    //	}

    //region 继承CAFRuntimeException
    //ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string exceptionMessage, System.Exception innerException = null, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)

    public BefExceptionBase() {
        this("");
    }

    public BefExceptionBase(String message) {
        super("pfcommon", "BEFRuntime001", message, null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error);
    }

//	public BefExceptionBase(Exception e){
//		this(e, e.getMessage());
//	}

    public BefExceptionBase(Exception e, String message) {
        super("pfcommon", "BEFRuntime001", message, e, io.iec.edp.caf.commons.exception.ExceptionLevel.Error);
    }

    public BefExceptionBase(String serviceUnitCode, String exceptionCode, String exceptionMessage, RuntimeException innerException, io.iec.edp.caf.commons.exception.ExceptionLevel level, boolean isBizException) {
        super(serviceUnitCode, exceptionCode, exceptionMessage, innerException, level, isBizException);
    }

    //ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string resourceFile, System.Exception innerException, string[] exceptionParams, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)
    public BefExceptionBase(String serviceUnitCode, String exceptionCode, String resourceFile, String[] exceptionParams, RuntimeException innerException, io.iec.edp.caf.commons.exception.ExceptionLevel level, boolean isBizException) {
        super(serviceUnitCode, resourceFile, exceptionCode, exceptionParams, innerException, level, isBizException);
    }

    //ORIGINAL LINE: public BefExceptionBase(string exceptionCode, string resourceFile, System.Exception innerException, string[] exceptionParams, ExceptionLevel level = ExceptionLevel.Error, bool isBizException = false)
    public BefExceptionBase(String serviceUnitCode, String exceptionCode, String resourceFile, RuntimeException innerException, String[] exceptionParams, io.iec.edp.caf.commons.exception.ExceptionLevel level, boolean isBizException) {
        //super(innerException);
        super(serviceUnitCode, resourceFile, exceptionCode, exceptionParams, innerException, level, isBizException);
    }

    //将com.inspur.edp.bef.entity.exception.ExceptionLevel转为io.iec.edp.caf.commons.exception.ExceptionLevel
    private static io.iec.edp.caf.commons.exception.ExceptionLevel getLevel(com.inspur.edp.bef.entity.exception.ExceptionLevel level) {
        io.iec.edp.caf.commons.exception.ExceptionLevel level1 = null;
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Error) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Error;
        }
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Warning) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Warning;
        }
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Info) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Info;
        }
        return level1;

    }
}
