package com.inspur.edp.bef.entity.exception;

import io.iec.edp.caf.commons.exception.CAFRuntimeException;

/**
 * @className: BefRunTimeBaseException
 * @author: wangmj
 * @date: 2023/11/18
 **/
public class BefRunTimeBaseException extends CAFRuntimeException {
    private static final String suCode = "pfcommon";
    private static final String resFile = "bef_runtime_core.properties";

    public BefRunTimeBaseException(String exceptionCode, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, null, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public BefRunTimeBaseException(String exceptionCode, Exception innerException, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, io.iec.edp.caf.commons.exception.ExceptionLevel.Error, true);
    }

    public BefRunTimeBaseException(String exceptionCode, Exception innerException, ExceptionLevel level, boolean isBiz, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), isBiz);
    }

    public BefRunTimeBaseException(String exceptionCode, Exception innerException, ExceptionLevel level, String... messageParams) {
        super(suCode, resFile, exceptionCode, messageParams, innerException, getLevel(level), true);
    }

    private static io.iec.edp.caf.commons.exception.ExceptionLevel getLevel(com.inspur.edp.bef.entity.exception.ExceptionLevel level) {
        io.iec.edp.caf.commons.exception.ExceptionLevel level1 = null;
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Error) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Error;
        }
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Warning) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Warning;
        }
        if (level == com.inspur.edp.bef.entity.exception.ExceptionLevel.Info) {
            level1 = io.iec.edp.caf.commons.exception.ExceptionLevel.Info;
        }
        return level1;

    }
}
