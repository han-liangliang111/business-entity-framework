package com.inspur.edp.bef.entity.exception;

/**
 * @className: BefRunTimeException
 * @author: wangmj
 * @date: 2023/9/23
 **/
public class BefRunTimeException extends RuntimeException {
    public BefRunTimeException(Throwable throwable) {
        super(throwable);
    }
}
