/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.entity.exception;

//引擎发生异常时的外层异常, 产生此异常则当前功能session不允许继续使用
public class BefFatalException extends RuntimeException {
    //demo暂不考虑国际化
    private static final String ErrorCode = "Bef";
    private static final String ErrorMessage = "系统发生未知异常, 请关闭当前功能并尝试重新打开";

    public BefFatalException(RuntimeException innerException) {
//		super(ErrorCode, ErrorMessage, innerException, ExceptionLevel.Fatal, true);
    }

    public BefFatalException(String errorCode, String message) {
//		super(errorCode, com.inspur.edp.bef.core.message, null, ExceptionLevel.Fatal, true);
    }

    //	protected BefFatalException(SerializationInfo info, StreamingContext context)
    //	{
    //		super(info, context);
    //	}
}
