/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.entity.entityData.defaultValue;

public class StaticDefaultValue<T> implements IDefaultValue {
    private T privateValue;

    public StaticDefaultValue(T value) {
        setValue(value);
    }

    public final T getValue() {
        return privateValue;
    }

// Converter:
    //	public static implicit operator T(StaticDefaultValue<T> d)
    //	{
    //		return d.getValue();
    //	}
// Converter:
    //	public static implicit <T> operator StaticDefaultValue(T d)
    //	{
    //		return new StaticDefaultValue<T>(d);
    //	}

//  public void setValue(T value) {
//    privateValue = value;
//  }

    public void setValue(Object value) {
        //死循环
//		setValue((T)value);
    }
}
